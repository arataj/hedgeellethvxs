NAME=verics-$(date +%Y%m%d)
TARGET=../$NAME
ARCHIVE=$NAME.tar.gz

veric -p regression/FTC.vxs && \
veric -p regression/FTCp.vxs && \
rm -rf $TARGET && mkdir $TARGET &&
	mkdir $TARGET/example && \
cp -rp dist/ bin/ install.sh $TARGET/ && \
mkdir $TARGET/lib && cp -rp lib/verics $TARGET/lib/ && \
cp -rp \
	regression/FTC.vxs regression/FTCInterleaved.vxs \
	regression/FTCp.vxs regression/pipeline.vxs \
	regression/dcN.vxs \
	regression/prism/ipp_tcp.nm.pp regression/prism/IPP.verics \
	$TARGET/example/ && \
cd $TARGET/.. && tar czf $ARCHIVE $NAME && cd - && \
echo "created archive ../$ARCHIVE"

