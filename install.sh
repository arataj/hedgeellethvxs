#!/bin/sh

# Installation of a compiled HEDGEELLETH_VXS.
# All this does is edit the "HEDGEELLETH_VXS_DIR=..." lines in the program startup scripts
# so if you have any problems, just do this manually
# NB: This installation script should be run from within the HEDGEELLETH_VXS directory

# You are supposed to run this from the main HEDGEELLETH_VXS directory
# but in case someone is in the bin directory, change...

HEDGEELLETH_VXS_DIR=`pwd`
if [ `basename "$HEDGEELLETH_VXS_DIR"` = bin ]; then
  HEDGEELLETH_VXS_DIR=`cd ..;pwd`
fi

# Now start the 'installation'
if [ ! "$1" = "silent" ] ; then
    echo "Installing HEDGEELLETH_VXS (directory=$HEDGEELLETH_VXS_DIR)"
fi
TEMP_FILE=tmp
FILES_TO_CHANGE=`find bin -maxdepth 1 ! -type d ! -name '*.bat'`
for FILE_TO_CHANGE in $FILES_TO_CHANGE
do
  if [ -f "$HEDGEELLETH_VXS_DIR"/$FILE_TO_CHANGE ]; then
    if [ ! "$1" = "silent" ] ; then
        echo "Setting path in startup script $HEDGEELLETH_VXS_DIR/$FILE_TO_CHANGE..."
    fi
    if sed -e "s|HEDGEELLETH_VXS_DIR=.*|HEDGEELLETH_VXS_DIR=$HEDGEELLETH_VXS_DIR|g" "$HEDGEELLETH_VXS_DIR"/$FILE_TO_CHANGE > "$HEDGEELLETH_VXS_DIR"/$TEMP_FILE ; then
      /bin/mv "$HEDGEELLETH_VXS_DIR"/$TEMP_FILE "$HEDGEELLETH_VXS_DIR"/$FILE_TO_CHANGE
      chmod 755 "$HEDGEELLETH_VXS_DIR"/$FILE_TO_CHANGE
    else
      echo "Error: Failed to modify startup scripts."
      exit 0
    fi
  else
    echo "Error: Could not locate startup script $HEDGEELLETH_VXS_DIR/$FILE_TO_CHANGE"
    exit
  fi
done
if [ ! "$1" = "silent" ] ; then
    echo "Installation complete."
fi
