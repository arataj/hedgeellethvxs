package pl.iitis.verics.assembler;

import pl.iitis.verics.agent.AgentSystem;

/*
 * VxsCompilation.java
 *
 * Created on Oct 3, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

/**
 * A compilation data of a single <code>Vxs</code> file.
 * 
 * @author Artur Rataj
 */
public class VxsCompilation {
    /**
     * Name of the input <code>Vxs</code> file.
     */
    public final String FILE_NAME_VXS;
    /**
     * The preprocessors; helds error mapping data, and the contents of the VERICS
     * and VXSP files.
     */
    public VxsPreprocessor PREPROCESSOR;
    /**
     * An agent system.
     */
    public AgentSystem AS;
    /**
     * A preprocessed VXS file, with splitting of property operators applied,
     * ready for further parsing.
     */
    public String VXSP2;
    /**
     * If not null, the parsed <code>VXSP2</code>, i.e. the generated output file,
     * is then read into this buffer. Used only for regression tests, the default is null.
     */
    public StringBuilder OUT = null;
    
    /**
     * Creates a new compilation context for a single file.
     * 
     * @param fileNameVxs name of the input <code>Vxs</code> file.
     */
    public VxsCompilation(String fileNameVxs) {
        FILE_NAME_VXS = fileNameVxs;
    }
    
}
