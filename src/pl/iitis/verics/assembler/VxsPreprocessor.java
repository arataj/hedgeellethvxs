/*
 * VxsPreprocessor.java
 *
 * Created on Jun 22, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import java.io.*;
import java.util.Map.Entry;

import pl.gliwice.iitis.hedgeelleth.compiler.SemanticCheck;
import pl.gliwice.iitis.hedgeelleth.compiler.code.*;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.clazz.AbstractJavaClass;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.LineIndex;
import pl.gliwice.iitis.hedgeelleth.interpreter.InterpreterUtils;
import pl.gliwice.iitis.hedgeelleth.interpreter.container.RuntimeMethod;
import pl.gliwice.iitis.hedgeelleth.interpreter.exception.InterpreterException;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.AbstractOutPosConverter;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.ArithmeticInterpreter;
import pl.gliwice.iitis.hedgeelleth.interpreter.op.BaseInterpreter;
import pl.gliwice.iitis.hedgeelleth.modules.*;
import pl.gliwice.iitis.hedgeelleth.pta.PTARange;
import pl.gliwice.iitis.verics.VericsFrontend;

/**
 * A preprocessor of Vxs files.
 * 
 * @author Artur Rataj
 */
public class VxsPreprocessor implements AbstractOutPosConverter {
    /**
     * An extension of Vxs source files.
     */
    public static final String HEDGEELLETH_EXTENSION_VXS ="vxs";
    /**
     * Put as/into a comment in a VXSP file, prefixes the line number int the
     * respective VXS file.
     */
    public static final String LINE_COMMENT_STR = "//#LINE";
    /**
     * Number of prefixing columns in the input fil.
     */
    final int PREPARATION_COL_OFFSET = VericsPrint.INDENT.length() - 1;
    /**
     * Number of lines in the prefix added to prepare the file, equal to the
     * number of lines in <code>VericsPrint.PREFIX</code>.
     */
    final int PREPARATION_LINE_OFFSET = 2;
    /**
     * Maximum possible number of operations in interpreted code.
     */
    protected static final int MAX_NUM_INTERPRETED_OPS = 1000000;
    
    /**
     * Verics frontend.
     */
    VericsFrontend FRONTEND;
    /**
     * The Hc compiler's options.
     */
    HedgeellethCompiler.Options HC_OPTIONS;
    /**
     * Optimizations to use.
     */
    List<Optimizer.Optimization> OPTIMIZATIONS;
    /**
     * Options of the generator.
     */
    GeneratorOptions GENERATOR_OPTIONS;
    /**
     * Indexed with Verics file position, returns line in the original Vxs file.
     */
    Integer[] lineIndex;
    /**
     * Indexed with Verics file position, returns column in the original Vxs file.
     */
    Integer[] colIndex;
    /**
     * Name of the VXS file.
     */
    String VXS_FILE_NAME;
    /**
     * Verics file contents.
     */
    public String VERICS_RAW;
    /**
     * Line index of <code>VERICS_RAW</code>; null for none.
     */
    LineIndex VERICS_RAW_LINE_INDEX;
    /**
     * Some unit testt require the compilation even in the case on a parse
     * error. SHould be used only in tests.
     */
    public static VxsCompilation VXSC = null;
    /**
     * This field contains the VXSP code. Regression tests may require the preprocessor
     * output even in the case on a parse error.
     */
    public String VXSP;
    /**
     * Name of the stream with the class which holds defines.
     */
    public static final String DEFINES_STREAM_NAME = "verics.D";
    /**
     * Code of the class with defines.
     */
    public String DEFINES = null;

    /**
     * Initializes the preprocessor.
     */
    public VxsPreprocessor() {
        FRONTEND = new VericsFrontend();
        HC_OPTIONS = new HedgeellethCompiler.Options();
        HC_OPTIONS.enableGeneratorTags = false;
        OPTIMIZATIONS = new LinkedList<>();
        OPTIMIZATIONS.add(Optimizer.Optimization.BASIC);
        GENERATOR_OPTIONS = new GeneratorOptions();
        GENERATOR_OPTIONS.enabledGeneratorTags = HC_OPTIONS.enableGeneratorTags;
        GENERATOR_OPTIONS.primaryRangeChecking =
                HC_OPTIONS.primaryRangeChecking;
    }
    /**
     * Prepares a Verics file out of Vsx file.
     * 
     * @param fileName Vxs file
     * @param inStream if not null, used instead of <code>fileName</code>,
     * not closed by this method
     * @param fullLibrary create a version for the full library
     * @return reader of the resulting Verics file
     */
    private StringReader prepare(String fileName, InputStream inStream,
            boolean fullLibrary) throws ParseException {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            InputStream in = null;
            try {
                if(inStream != null)
                    in = inStream;
                else
                    in = new FileInputStream(fileName);
                VericsPrint parser = new VericsPrint(in, outStream,
                    fileName != null ? VxsCLI.getFileType(fileName) : VxsCLI.FileType.UNKNOWN,
                    fullLibrary);
                outStream.flush();
                VXS_FILE_NAME = fileName;
                VERICS_RAW = new String(outStream.toByteArray(),
                        "UTF-8");
                lineIndex = parser.LINE_MAP.toArray(new Integer[parser.LINE_MAP.size()]);
                colIndex = parser.COL_MAP.toArray(new Integer[parser.COL_MAP.size()]);
            } catch(CompilerException e) {
                if(in != null && in != inStream)
                    in.close();
                if(fileName != null)
                    e.completeStreamName(fileName);
                throw e;
            }
            if(in != inStream)
                in.close();
            StringReader reader = new StringReader(VERICS_RAW);
            return reader;
        } catch(IOException e) {
            throw new ParseException(new StreamPos(
                    fileName != null ? fileName : null, 1, 1),
                    ParseException.Code.IO, e.getMessage());
        }
    }
    protected static String getDefineClassCode(CliConstants cc) {
        StringBuilder out = new StringBuilder();
        out.append("pk verics;\n\n");
        out.append("ap class D {\n");
        for(Entry<String, List<Literal>> e : cc.defines.entrySet()) {
            out.append("    ap$ ");
            String name = e.getKey();
            boolean array = cc.array.get(name);
            CliConstants.CType type = cc.type.get(name);
            switch(type) {
                case INTEGER:
                    out.append("Z");
                    break;
                    
                case DOUBLE:
                    out.append("R");
                    break;
                    
                case BOOLEAN:
                    out.append("B");
                    break;
                    
                case STRING:
                    out.append("String");
                    break;
                    
                default:
                    throw new RuntimeException("Unknown type");
            }
            if(array)
                out.append("[]");
            else if(e.getValue().size() != 1)
                throw new RuntimeException("a scalar type but not a single value");
            out.append(" " + name + " := ");
           if(array)
               out.append("{");
           boolean first = true;
           for(Literal l : e.getValue()) {
               if(!first)
                   out.append(", ");
               switch(type) {
                   case INTEGER:
                       out.append(l.getMaxPrecisionInteger());
                       break;
                       
                   case DOUBLE:
                       out.append(l.getMaxPrecisionInteger());
                       break;
                       
                   case BOOLEAN:
                       out.append(l.getBoolean() ? "T" : "F");
                       break;
                       
                   case STRING:
                       if(l.type.type == null)
                           out.append("null");
                       else
                           out.append("\"" + l.getString().
                                   replace("\\", "\\\\").
                                   replace("\"", "\\\"")
                                   + "\"");
                       break;
                       
                   default:
                       throw new RuntimeException("Unknown type");
               }
               first = false;
           }
           if(array)
               out.append("}");
            out.append(";\n");
        }
        out.append("}\n");
        return out.toString();
    }
    /**
     * Compiler the preprocessor's code.
     * 
     * @param library definition of library files
     * @param fileName name of the original file, if
     * an input stream is used instead can hold its name or be null 
     * @param defines constant defines, null list for none
     * @param reader reader of the prepared text
     */
    protected CodeCompilation compile(VxsLibrary library, String fileName,
            InputStream inStream, CliConstants defines, Reader reader)
            throws CompilerException {
        //
        // the verics language compiler
        //
        Compilation compilation = new Compilation(
                new CompilationOptions(HC_OPTIONS));
        CompilerException parseErrors = new CompilerException();
        // compile library
        for(String libraryFileName : library.FILES)
            FRONTEND.parse(compilation, library.TOP_DIRECTORY, library.LIBRARY_PATH,
                    libraryFileName, parseErrors);
        AbstractJavaClass definesExternal = null;
        for(AbstractJavaClass c : compilation.getClasses())
            if(c.getQualifiedName().toString().equals("verics.D")) {
                definesExternal = c;
                break;
            }
        if(definesExternal != null) {
            if(!defines.defines.isEmpty())
                throw new ParseException(definesExternal.getStreamPos(),
                        ParseException.Code.DUPLICATE,
                        "both cli defines and definition class");
        } else {
            // compile defines
            DEFINES = getDefineClassCode(defines);
            FRONTEND.parseVxs(compilation, library.LIBRARY_PATH,
                    DEFINES_STREAM_NAME, new StringReader(DEFINES), parseErrors);
        }
        // compile model file
        FRONTEND.parseVxs(compilation, library.LIBRARY_PATH,
                fileName, reader, parseErrors);
        //
        if(parseErrors.reportsExist())
            throw parseErrors;
        java.lang.reflect.Constructor[] constructors = FRONTEND.getSemanticCheckClass().
                getConstructors();
        if(constructors.length != 1)
            throw new RuntimeException("semantic checker should have exactly one " +
                    "public constructor");
        // semantic check
        try {
            SemanticCheck c = (SemanticCheck)constructors[0].newInstance(
                    compilation, null);
        } catch(Exception e) {
            String cause;
            if(e.getCause() != null) {
                if(e.getCause() instanceof CompilerException)
                        throw (CompilerException)e.getCause();
                cause = ", cause is: " + e.getCause().toString() + " at " +
                    e.getCause().getStackTrace()[0].toString();
            } else
                cause = "";
            String s = "could not create semantic checker: " +
                    e.toString() + cause;
            throw new RuntimeException(s);
        }
        // static analysis
        StaticAnalysis sa = new StaticAnalysis(compilation);
        // generate code
        Generator generator = new Generator(compilation,
                GENERATOR_OPTIONS);
        CodeCompilation codeCompilation = generator.compile();
        // optimize
        Optimizer optimizer = new Optimizer(OPTIMIZATIONS);
        optimizer.optimize(codeCompilation,
                HC_OPTIONS.optimizerTuning);
        return codeCompilation;
    }
    /**
     * Interpreter the main method.
     * 
     * @param codeCompilation compilation
     * @return what the interpreter printed
     */
    protected String interpret(CodeCompilation codeCompilation)
            throws CompilerException, InterpreterException {
        //
        // the verics language interpreter
        //
        CodeClass mainMethodClass = null;
        for(CodeClass c : codeCompilation.classes.values())
            if(c.mainMethod != null) {
                if(mainMethodClass == null)
                    mainMethodClass = c;
                else {
                    throw new RuntimeException("more than one main method");
                }
            }
        if(mainMethodClass == null)
            throw new RuntimeException("no main method");
        BaseInterpreter interpreter = new ArithmeticInterpreter(codeCompilation);
        interpreter.setOpCounterLimit(MAX_NUM_INTERPRETED_OPS);
        interpreter.typeRangeMap = PTARange.getTADDDefaults(false);
        List<String> runtimeArgs = new LinkedList<>();
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        PrintWriter outWriter = new PrintWriter(outStream);
        interpreter.setOutStream(outWriter);
        interpreter.setOutPosConverter(this);
        RuntimeMethod rm = InterpreterUtils.interpret(interpreter, mainMethodClass,
                runtimeArgs);
        if(!rm.pendingThreads.isEmpty())
            throw new RuntimeException("pending threads");
        outWriter.close();
        try {
            outStream.flush();
            String out = new String(outStream.toByteArray(), "UTF-8");
            outStream.close();
            return out;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        } catch (IOException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Maps a Verics file position into a Vxs file position.
     * 
     * @param pos position to modify; must belong to the VXS file
     * @param direction 0 do not search nearest non--internal region,
     * -1 search backwards, 1 search forwards
     * @param message if not null, modifies the report's message if internal
     * area of the Verics file, or passes the report's message in the case of a
     * runtime exception
     */
    private void vericsToVxs(StreamPos pos, int direction,
            CompilerException.Report message) {
//if(message != null)
//    pos = pos;
        int index = VERICS_RAW_LINE_INDEX.getPos(pos.line - 1, Math.max(pos.column - 1, 0));
        //r.pos.column -= PREPARATION_COL_OFFSET;
        //r.pos.line -= PREPARATION_LINE_OFFSET;
        // VERICS_RAW.substring(index) VERICS_RAW.length()
        int vsLine = lineIndex[index];
        int vsCol = colIndex[index];
        boolean atOperator = false;
if(message == null &&
        index < VERICS_RAW.length() && VERICS_RAW.charAt(index) == '+')
    vsLine = vsLine;
        if((vsLine == -1 || vsCol == -1) &&
                index < VERICS_RAW.length() && VERICS_RAW.charAt(index) == '+' &&
                // parse errors make it unlikely, that an operator is actually
                // recognized and complaining
                (message != null && message.code != CompilerException.Code.PARSE)) {
            atOperator = true;
            // it might be an operator, which does not like one of its operand
            if(index < VERICS_RAW.length() - 2 &&
                    VERICS_RAW.charAt(index + 2) == '(')
                direction = 1;
            else if(index >= 2 &&
                    VERICS_RAW.charAt(index - 2) == ')')
                direction = -1;
        }
        boolean internal = false;
        while(vsLine == -1 || vsCol == -1) {
            // invalid brace strings or a missing/superfluous brace
            // caused an error in the internal part
            internal = true;
            if(direction == 0)
                break;
            index += direction;
            if(index == -1 || index == VERICS_RAW.length()) {
                vsLine = -1;
                vsCol = -1;
                break;
            } else {
                vsLine = lineIndex[index];
                vsCol = colIndex[index];
            }
        }
        if(vsLine == -1 || vsCol == -1) {
            pos.name = "internal";
            throw new RuntimeException("error at internal verics code: " +
                    message.getDescription());
        }
        if(internal && !atOperator && message != null)
            if(direction == 1)
                message.message = "invalid evaluation";
            else
                message.message = "invalid separation";
        pos.line = vsLine;
        if(pos.line != -1)
            ++pos.line;
        pos.column = vsCol + 1;
    }
    /**
     * Maps a Verics file position into a Vxs file position.
     * 
     * @param vxsFileName name of the vxs file; needed to distinguish it
     * from the library files; null if a stream is preprocessed and not a file
     * @param pos position to modify
     * @param direction 0 do not search nearest non--internal region,
     * -1 search backwards, 1 search forwards
     * @param message if not null, modifies the report's message if internal
     * area of the Verics file, or passes the report's message in the case of a
     * runtime exception
     */
    public void vericsToVxs(String vxsFileName, StreamPos pos, int direction,
            CompilerException.Report message) {
        if(VERICS_RAW_LINE_INDEX == null)
            VERICS_RAW_LINE_INDEX = new LineIndex(VERICS_RAW);
        if(pos.line <= 1 || pos.column <= 1 ||
                (pos.name != null && (vxsFileName == null || !pos.name.equals(vxsFileName)))) {
            // not in the original vs file
            if(pos.name == null || pos.name.equals(vxsFileName)) {
                throw new RuntimeException("error in the internal Verics template: " +
                        pos.toString() + ": " + message.getDescription());
            }
        } else
            VxsPreprocessor.this.vericsToVxs(pos, direction, message);
    }
    /**
     * Modifies the positions within stack trace, which point to
     * the Vxs file.
     * 
     * @param vxsFileName name of the Vxs file
     * @param stackTrace textual form of the stack trace
     * @return the stack trace with positions remapped
     */
    public String vericsToVxs(String vxsFileName, String stackTrace) {
        StringBuilder out = new StringBuilder();
        Scanner sc = new Scanner(stackTrace);
        boolean first = true;
        while(sc.hasNext()) {
            String s = sc.next();
            if(s.endsWith(","))
                s = s.substring(0, s.length() - 1);
            StreamPos pos = StreamPos.parse(s);
            if(pos.name.equals(vxsFileName))
                VxsPreprocessor.this.vericsToVxs(pos, -1, null);
            if(!first)
                out.append(", ");
            if(pos.line == -1)
                out.append("<internal>");
            else
                out.append(pos.toString());
            first = false;
        }
        return out.toString();
    }
    /**
     * Preprocesses a file.
     * 
     * @param library library definition
     * @param fileName name of the original file, if
     * an input stream is used instead can hold its name or be null 
     * @param inStream if not null, used instead of <code>fileName</code>,
     * not closed by this method
     * @param defines constant defines, empty list for none
     */
    public String preprocess(VxsLibrary library, String fileName, InputStream inStream,
            CliConstants defines) throws CompilerException {
        if(fileName != null && inStream != null)
            throw new RuntimeException("both file and stream specified");
        StringReader prepared = null;
        String out = null;
        try {
            prepared = prepare(fileName, inStream, library.FULL);
            CodeCompilation compilation = compile(library,
                    fileName, inStream, defines, prepared);
            prepared.close();
            try {
                out = interpret(compilation);
            } catch(InterpreterException e) {
                throw new CompilerException(e.getStreamPos(),
                        CompilerException.Code.ILLEGAL,
                        "" + e.getRawDescription());
            }
            VXSP = out;
            return out;
        } catch(CompilerException e) {
            if(VERICS_RAW != null) {
                // the parse stage went ok, any errors are from the later
                // stages, thus from processing the Vericss file or later, thus a remapping
                // is required if not library
                for(CompilerException.Report r : new TreeSet<>(e.getReports())) {
                    if(r.pos.name == null || r.pos.name.equals(fileName)) {
                        // not a library or defines file
                        if(r.pos != null) { // VERICS_RAW.substring(811)
                            vericsToVxs(fileName, r.pos, -1, r);
                        }
                        int p = r.message.lastIndexOf(MessageStyle.STACK_TRACE_PREFIX);
                        if(p != -1) {
                            p += MessageStyle.STACK_TRACE_PREFIX.length() + 1;
                            r.message = r.message.substring(0, p) +
                                    vericsToVxs(fileName, r.message.substring(p).trim());
                        }
                    } else if(r.pos.name.equals(DEFINES_STREAM_NAME)) {
                        String d = getDefineClassCode(defines);
                        LineIndex li = new LineIndex(d);
                        r.message += ": " + li.selectLine(d, r.pos.line - 1).trim();
                        r.pos.line = -1;
                        r.pos.column = 0;
                    }
                }
            }
            pl.gliwice.iitis.hedgeelleth.cli.Compiler.applyMessageStyle(e,
                    MessageStyle.Type.VERICS, library.LIBRARY_PATH);
            VXSP = out;
            throw e;
        }
    }
    /**
     * Converts positions of println() statements in the Verics printer
     * into respective lines in the Vxs file.
     * 
     * @param pos <code>println</code>'s position
     * @return a description of the mapped position
     */
    @Override
    public String convertPos(StreamPos pos) {
        if(pos.name != null && !pos.name.equals(VXS_FILE_NAME))
            return "";
        else {
            VxsPreprocessor.this.vericsToVxs(VXS_FILE_NAME, pos = new StreamPos(pos), 1, null);
            return " " + LINE_COMMENT_STR + pos.line;
        }
    }
}
