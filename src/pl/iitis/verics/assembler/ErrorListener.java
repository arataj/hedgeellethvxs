/*
 * Main.java
 *
 * Created on Jun 22, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 *
 * @author Artur Rataj
 */
public class ErrorListener extends BaseErrorListener {
    /**
     * Name of the parsed stream.
     */
    public final String STREAM_NAME;
    /**
     * Error reports, empty list for none.
     */
    public final ParseException errors = new ParseException();
    
    public ErrorListener(String streamName) {
        STREAM_NAME = streamName;
    }
    public boolean reportsExist() {
        return errors.reportsExist();
    }
    @Override
    public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol,
            int line, int charPositionInLine, String msg, RecognitionException e) {
        final String NO_VIABLE_STR = "no viable alternative at input ";
// System.out.println("message = " + msg);
        if(msg.startsWith(NO_VIABLE_STR))
            msg = "unexpected " + msg.substring(NO_VIABLE_STR.length());
        errors.addReport(new ParseException.Report(
                new StreamPos(STREAM_NAME, line, charPositionInLine + 1),
                ParseException.Code.PARSE,
                msg));
    }
}
