/*
 * VericsPrint.java
 *
 * Created on Jun 26, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;

/**
 * A parser of a Verics assembler file into a Verics file for printing
 * the assemler file preprocessed.
 * 
 * @author Artur Rataj
 */
public class VericsPrint extends AbstractParseMap {
    /**
     * A prefix of the Verics file to generate.
     */
    protected String PREFIX =
            "//\n" +
            "// THIS FILE IS MACHINE GENERATED.\n" +
            "//\n" +
            "im$ verics.lang.Math.*;\n" +
            "im$ verics.D.*;\n" +
            "$IMPORT\n" +
            "ap class VericsPrinter {\n" +
            "\n";
    protected String PREFIX_FULL_LIBRARY_MISC_IMPORT =
            "im$ verics.text.Text.*;\n" +
            "";
    /**
     * A suffix of the Verics file to generate.
     */
    protected String SUFFIX =
            "\n" +
            "    ap$ V main() {\n" +
            "    }\n" +
            "}\n";
    /**
     * An indent of the dynamic code.
     */
    public static String INDENT = "        ";

    static enum Mode {
        ASSEMBLY_PROLOGUE,
        ASSEMBLY,
        VERICS,
        ERROR,
    }
    /**
     * Parses a Vxs file.
     * 
     * @param in input, format Verics assembler; not closed by this method
     * @param out output, format Verics; not closed by this method
     * @param fileType type of the preprocessed file
     * @param fullLibrary create a version for the full library
     */
    public VericsPrint(InputStream in, OutputStream out,
            VxsCLI.FileType fileType, boolean fullLibrary) throws IOException, ParseException {
        super(null, 0, 0);
        ParseException errors = new ParseException();
        Scanner sc = CompilerUtils.newScanner(in);
        Writer writer = new OutputStreamWriter(out);
        String miscImport;
        if(fullLibrary)
            miscImport = PREFIX_FULL_LIBRARY_MISC_IMPORT;
        else
            miscImport = "";
        String prefixFinal = PREFIX.replace("$IMPORT", miscImport);
        writer.append(prefixFinal);
        fillNoMatch(prefixFinal.length());
        boolean modelDefinitionFound = false;
        int lineNum = 0;
        Mode mode;
        boolean endOfPrologue = false;
        boolean inBrackets = false;
        boolean noExpression = false;
        String command = "";
        boolean inComment = false;
        StreamPos commandPos = new StreamPos(null, -1, -1);
//System.out.println("****");
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
//if(line.indexOf("$rpt") != -1)
//    line = line;
//if(lineNum == 14)
//    line = line;
//System.out.println(line);
            // relates to the begining of this line
            int commentBeg = line.indexOf("/*");
            if(commentBeg == 0)
                // no way that it is a preprocessor 
                inComment = true;
            // now check the line's end
            commentBeg = line.lastIndexOf("/*");
            int commentEnd = line.lastIndexOf("*/");
            int commentPos = line.indexOf("//");
            String code;
            if(commentPos != -1)
                code = line.substring(0, commentPos);
            else
                code = line;
            int hashPos = code.indexOf("#");
            if(hashPos > 0) {
                errors.addReport(new ParseException.Report(
                        new StreamPos(null, lineNum + 1, 1 + hashPos),
                        ParseException.Code.ILLEGAL, "`#' not at the beginning of line"));
                mode = Mode.ERROR;
            } else if(hashPos == 0 && !inComment) {
                mode = Mode.VERICS;
                endOfPrologue = true;
                // an in--preprocessor comment, if any
                commentBeg = -1;
                commentEnd = -1;
            } else {
                if(endOfPrologue)
                    mode = Mode.ASSEMBLY;
                else
                    mode = Mode.ASSEMBLY_PROLOGUE;
            }
            switch(mode) {
                case ASSEMBLY_PROLOGUE:
                {
                    if(command.isEmpty()) {
                        command = "";
                        commandPos.line = lineNum + 1;
                        commandPos.column = 1;
                    }
                    int semicolonPos = code.indexOf(";");
                    if(fileType == VxsCLI.FileType.PRISM_P) {
                       String s = code.trim();
                       switch(s) {
                            case "ctmc":
                            case "dtmc":
                            case "mdp":
                            case "pta":
                                // add a fake semicolon after Prism's
                                // declaration of the model class
                                semicolonPos = code.length();
                                modelDefinitionFound = true;
                                break;
                                
                            default:
                                /* empty */
                                break;
                        }
                    }
                    if(semicolonPos == -1) {
                        command += code + "\n";
                    } else {
                        command += code.substring(0, semicolonPos);
                        try {
                            ParseCommand pc = new ParseCommand(command,
                                                commandPos.line - 1, commandPos.column - 1,
                                                fileType);
                            pc.process(inComment);
                            if(pc.OUT != null) {
                                writer.append(INDENT + pc.OUT);
                                fillNoMatch(INDENT.length());
                                copyMatch(pc);
                            }
                        } catch(ParseException e) {
                            for(ParseException.Report r : e.getReports())
                                r.pos.add(commandPos);
                            errors.addAllReports(e);
                        }
                        // take into accound the fake semicolon
                        semicolonPos = Math.min(code.length() - 1, semicolonPos);
                        command = code.substring(
                                semicolonPos + 1);
                        commandPos.line = lineNum + 1;
                        commandPos.column = semicolonPos + 1 + 1;
                    }
                    // fall through
                }
                case ASSEMBLY:
                    ParsePrint pp = new ParsePrint(line, lineNum, inBrackets,
                            inComment, noExpression);
                    inBrackets = pp.IN_BRACKETS;
//if(inBrackets)
//    inBrackets = inBrackets;
                    noExpression = pp.NO_EXPRESSION;
                    writer.append(INDENT + pp.OUT);
                    fillNoMatch(INDENT.length());
                    copyMatch(pp);
                    // check for out--of--preprocessor comments,
                    // to set <code>inComment</code> for the
                    // next line
                    if(!inComment && commentBeg > 0)
                        inComment = true;
                    if(inComment && commentEnd > 0) {
                        if(commentBeg < commentEnd)
                            inComment = false;
                    }
                    break;

                case VERICS:
//                    if(!command.trim().isEmpty() &&
//                            StringAnalyze.compactWhitespace(command.trim(), false, null).
//                                startsWith(ParseCommand.getConstDeclarationStr(fileType)))
//                        errors.addReport(new ParseException.Report(commandPos,
//                            ParseException.Code.ILLEGAL, "pending prologue command"));
                    command = "";
                    commandPos.line = lineNum + 1;
                    commandPos.column = 1;
                    if(inBrackets)
                        errors.addReport(new ParseException.Report(commandPos,
                            ParseException.Code.ILLEGAL, "pending bracketed contents"));
                    String vericsLine = StringAnalyze.removeTrailingSpace(line).substring(1) + "\n";
                    writer.append(INDENT + vericsLine);
                    fillNoMatch(INDENT.length());
                    for(int i = 0; i < vericsLine.length(); ++i) {
                        LINE_MAP.add(lineNum);
                        COL_MAP.add(i + 1);
                    }
                    break;

                case ERROR:
                    command = "";
                    /* empty */
                    break;
                    
                default:
                    throw new RuntimeException("unknown mode");
            }
            ++lineNum;
        }
        if(command.isEmpty()) {
            commandPos.line = lineNum + 1;
            commandPos.column = 1;
        }
        if(fileType == VxsCLI.FileType.PRISM_P &&
                !modelDefinitionFound)
            errors.addReport(new ParseException.Report(commandPos,
                ParseException.Code.ILLEGAL, "no model class found in Prism file"));
        if(inBrackets)
            errors.addReport(new ParseException.Report(commandPos,
                ParseException.Code.ILLEGAL, "unterminated bracket string"));
        writer.append(SUFFIX);
        fillNoMatch(SUFFIX.length());
        writer.close();
        if(errors.reportsExist())
            throw errors;
    }
}
