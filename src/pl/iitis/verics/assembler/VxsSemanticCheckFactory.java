/*
 * VxsSemanticCheckFactory.java
 *
 * Created on Apr 11, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.AbstractPrimaryTransformer;
import pl.gliwice.iitis.hedgeelleth.compiler.SemanticCheck;
import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary.SemanticCheckFactory;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * A factory of <code>VxsSemanticCheck</code>
 * 
 * @author Artur Rataj
 */
public class VxsSemanticCheckFactory extends SemanticCheckFactory {
    @Override
    public SemanticCheck newInstance(AbstractPrimaryTransformer transformer)
            throws CompilerException {
        return new VxsSemanticCheck(transformer);
    }
}
