/*
 * VarUsage.java
 *
 * Created on Mar 25, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.expr;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

import pl.iitis.verics.agent.*;
import pl.iitis.verics.assembler.ParserUtils;
import pl.iitis.verics.assembler.VxsOptions;
import pl.iitis.verics.assembler.VxsSemanticCheckFactory;

/**
 * Finds variables used  within each module.
 * 
 * @author Artur Rataj
 */
public class VarUsage  implements Visitor<SortedSet<AsVariable>> {
    /**
     * Agent system.
     */
    final AgentSystem AS;
    /**
     * Currently parsed module.
     */
    AsModule module;
    /**
     * Access errors.
     */
    CompilerException errors;

    /**
     * Finds certain expressions which use integer arithmetics, replaces
     * them with boolean equivalents. Checks for access rights.
     * 
     * @param as agent system to mody
     * @param conversion conversion options
     */
    public VarUsage(AgentSystem as) throws CompilerException {
        AS = as;
        errors = new CompilerException();
        for(AsModule m : as.moduleList) {
            module = m;
            module.usedVars = new TreeSet<>();
            for(AbstractExpression e : module.PROTOCOL.guards) {
                module.usedVars.addAll(find(e));
            }
            for(AsStatement s : module.STATEMENTS) {
                module.usedVars.addAll(find(s.GUARD));
                if(s.UPDATE_RIGHT != null)
                    module.usedVars.addAll(find(s.UPDATE_RIGHT));
            }
        }
        if(errors.reportsExist())
            throw errors;
    }
    /**
     * Checke the access rights of a given set of variables.
     * 
     * @param streamPos a source stream position, required to construct a possible error
     * @param vars variables to check
     */
    private void check(StreamPos streamPos, SortedSet<AsVariable> vars) {
        for(AsVariable v : vars) {
            if(!v.COMMON.KNOWN.contains(module))
                errors.addReport(new CompilerException.Report(streamPos,
                    CompilerException.Code.ILLEGAL,
                    "variable " + v.getQualifiedName() + " is private"));
        }
    }
    /**
     * Collects variable usage in a single expression, checks access rights.
     * 
     * @param module module to which belongs the expression
     * @param e expression to search
     * @param errors if an access error is found, it is added to this exception
     * @return variables found
     */
    private SortedSet<AsVariable> find(AbstractExpression e) {
        SortedSet<AsVariable> vars;
        try {
             vars = (SortedSet<AsVariable>)e.accept(this);
        } catch(CompilerException ce) {
            throw new RuntimeException("unexpected: " + ce.toString());
        }
        return vars;
    }
    public SortedSet<AsVariable> visit(Node n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(SynchronizedStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(Block n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(Compilation c) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(EmptyExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(AllocationExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(IndexExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(BlockExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(ReturnStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(ThrowStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(LabeledStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(JumpStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public SortedSet<AsVariable> visit(BranchStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    @Override
    public SortedSet<AsVariable> visit(AbstractExpression e) throws CompilerException {
        SortedSet<AsVariable> d = new TreeSet<>();
        if(e instanceof AsActionExpression || e instanceof AsOtherExpression) {
            /* empty */
        } else
            throw new UnsupportedOperationException("Not supported");
        return d;
    }
    @Override
    public SortedSet<AsVariable> visit(ConstantExpression e) throws CompilerException {
        SortedSet<AsVariable> d = new TreeSet<>();
        return d;
    }
    @Override
    public SortedSet<AsVariable> visit(PrimaryExpression e) throws CompilerException {
        SortedSet<AsVariable> vars = new TreeSet<>();
        String identifier = e.toString();
        int pos = identifier.indexOf('.');
        // a variable or a constant
        if(pos != -1) {
            AsTyped t = AS.lookupValue(identifier);
            if(t instanceof AsVariable)
                vars.add((AsVariable)t);
        }
        check(e.getStreamPos(), vars);
        return vars;
    }
    @Override
    public SortedSet<AsVariable> visit(UnaryExpression e) throws CompilerException {
        SortedSet<AsVariable> vars = new TreeSet<>();
        SortedSet<AsVariable> sub = (SortedSet<AsVariable>)e.sub.accept(this);
        vars.addAll(sub);
        return vars;
    }
    @Override
    public SortedSet<AsVariable> visit(BinaryExpression e) throws CompilerException {
        SortedSet<AsVariable> vars = new TreeSet<>();
if(e.toString().indexOf("head2") != -1)
    e = e;
        SortedSet<AsVariable> left = (SortedSet<AsVariable>)e.left.accept(this);
        SortedSet<AsVariable> right = (SortedSet<AsVariable>)e.right.accept(this);
        vars.addAll(left);
        vars.addAll(right);
        return vars;
    }
    @Override
    public SortedSet<AsVariable> visit(AssignmentExpression e) throws CompilerException {
        throw new RuntimeException("unexpected embedded assignment");
    }
    public SortedSet<AsVariable> visit(CallExpression e) throws CompilerException {
        SortedSet<AsVariable> vars = new TreeSet<>();
        for(AbstractExpression a : e.arg) {
            SortedSet<AsVariable> arg = (SortedSet<AsVariable>)a.accept(this);
            vars.addAll(arg);
        }
        return vars;
    }
}
