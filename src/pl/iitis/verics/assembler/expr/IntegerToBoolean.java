/*
 * IntegerToBoolean.java
 *
 * Created on Mar 25, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.expr;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;

import pl.iitis.verics.agent.*;
import pl.iitis.verics.assembler.ParserUtils;
import pl.iitis.verics.assembler.VxsOptions;
import pl.iitis.verics.assembler.VxsSemanticCheckFactory;

/**
 * Converts synchronisation labels into guards. Works only for
 * a subset of models.
 * 
 * @author Artur Rataj
 */
public class IntegerToBoolean  implements Visitor<IntegerToBoolean.Description> {
    /**
     * A set of two constants values 0 and 1.
     */
    protected final static SortedSet<AsConstant> BOOLEAN_SET = new TreeSet<>();
    static {
        BOOLEAN_SET.add(new AsConstant(null, "#false", new Literal(false)));
        BOOLEAN_SET.add(new AsConstant(null, "#true", new Literal(true)));
    };
    /**
     * Description of an expression.
     */
    static protected class Description {
        /**
         * All actions used within this expression.
         */
        SortedSet<String> actions = new TreeSet<>();
        /**
         * If the expression contains an integer arithmetic, which may still
         * require an avaluation of the need of conversion.
         */
        boolean ia = false;
        /**
         * If the expression contains any mathematical function, which may still
         * require an avaluation of the need of conversion.
         */
        boolean functions = false;
        /**
         * All variables used by this expression.
         */
        SortedSet<AsVariable> vars = new TreeSet<>();
        
        /**
         * Adds all traits of <code>other</code> to this description.
         * 
         * @param other another description
         */
        void add(Description other) {
            actions.addAll(other.actions);
            if(other.ia)
                ia = true;
            if(other.functions)
                functions = true;
            vars.addAll(other.vars);
        }
        /**
         * If actions should be converted into a boolean equation.
         * 
         * @return If there are both actions and the integer arithmetics
         */
        public boolean actionsToConvert() {
            return ia && !actions.isEmpty();
        }
    }
    /**
     * Agent system.
     */
    final AgentSystem AS;
    /**
     * Conversion options.
     */
    final VxsOptions.Conversion CONVERSION;
    /**
     * If the expression is not nested within an integer arithmetic.
     */
    boolean nestedInteger;
    
    /**
     * Finds certain expressions which use integer arithmetics, replaces
     * them with boolean equivalents.
     * 
     * @param as agent system to mody
     * @param conversion conversion options
     */
    public IntegerToBoolean(AgentSystem as, VxsOptions.Conversion conversion)
            throws CompilerException {
        AS = as;
        CONVERSION = conversion;
        nestedInteger = false;
        for(AsModule module : as.moduleList) {
            List<AbstractExpression> newGuards = new LinkedList<>();
            for(AbstractExpression e : module.PROTOCOL.guards) {
                Description d;
                d = transform(e);
                // for the case where a whole expression needs to be converted
                newGuards.add(toBoolean(e, d));
            }
            module.PROTOCOL.guards.clear();
            module.PROTOCOL.guards.addAll(newGuards);
            for(AsStatement s : module.STATEMENTS) {
                Description d;
                d = transform(s.GUARD);
                // for the case where a whole expression needs to be converted
                s.GUARD = toBoolean(s.GUARD, d);
                if(s.UPDATE_RIGHT != null) {
                    d = transform(s.UPDATE_RIGHT);
                    // for the case where a whole expression needs to be converted
                    s.UPDATE_RIGHT = toBoolean(s.UPDATE_RIGHT, d);
                }
            }
        }
        for(AsFormula f : as.formulaList) {
            Description d = transform(f.EXPR);
            // for the case where a whole expression needs to be converted
            f.EXPR = toBoolean(f.EXPR, d);
        }
        Description d = transform(as.initFilter);
        as.initFilter = toBoolean(as.initFilter, d);
    }
    /**
     * Transforms a single expression.
     * 
     * @param e expression to transform
     * @return description of the transformed expression
     */
    private Description transform(AbstractExpression e) {
        try {
            Description node = (Description)e.accept(this);
            return node;
        } catch(CompilerException ce) {
            throw new RuntimeException("unexpected: " + ce.toString());
        }
    }
    public Description visit(Node n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(SynchronizedStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(Block n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(Compilation c) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(EmptyExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(AllocationExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(IndexExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(BlockExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(ReturnStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(ThrowStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(LabeledStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(JumpStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Description visit(BranchStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    @Override
    public Description visit(AbstractExpression e) throws CompilerException {
        Description d = new Description();
        if(e instanceof AsActionExpression) {
            AsActionExpression a = (AsActionExpression)e;
            d.actions.add(a.getQualifiedName());
        } else if(e instanceof AsOtherExpression)
            ;
        else
            throw new UnsupportedOperationException("Not supported");
        return d;
    }
    @Override
    public Description visit(ConstantExpression e) throws CompilerException {
        Literal c = e.getSimpleConstant();
        if(!c.type.isBoolean() && c.getBoolean())
            throw new RuntimeException("unexpected");
        Description d = new Description();
        return d;
    }
    @Override
    public Description visit(PrimaryExpression e) throws CompilerException {
        Description d = new Description();
        String identifier = e.toString();
        int pos = identifier.indexOf('.');
        // a variable or a constant
        if(pos != -1) {
            AsTyped t = AS.lookupValue(identifier);
            if(t instanceof AsVariable)
                d.vars.add((AsVariable)t);
        } else {
            // an integer constant needs to use a non--boolean logic operator if
            // coupled with an action expression, and thus the integer arithmetic
            // will eventually be found by a caller without seeting the respective
            // flag here
        }
        return d;
    }
    @Override
    public Description visit(UnaryExpression e) throws CompilerException {
        boolean outerNestedInteger = nestedInteger;
        Description d = new Description();
        if(!e.operatorType.isBoolean()) {
            nestedInteger = true;
            d.ia = true;
        }
        Description sub = (Description)e.sub.accept(this);
        if(!nestedInteger) {
            e.sub = toBoolean(e.sub, sub);
        }
        d.add(sub);
        nestedInteger = outerNestedInteger;
        return d;
    }
    @Override
    public Description visit(BinaryExpression e) throws CompilerException {
        boolean outerNestedInteger = nestedInteger;
        Description d = new Description();
        boolean actionComparison = e.left instanceof AsActionExpression ||
                e.right instanceof AsActionExpression;
        boolean splitting = e.operatorType.isBoolean();
        if(CONVERSION.allowVariableComparison &&
                (e.left instanceof ConstantExpression || e.left instanceof PrimaryExpression) &&
                (e.right instanceof ConstantExpression || e.right instanceof PrimaryExpression))
            splitting = true;
        if(!splitting) {
            nestedInteger = true;
            d.ia = true;
        }
        Description left = (Description)e.left.accept(this);
        Description right = (Description)e.right.accept(this);
        if(!nestedInteger) {
            e.left = toBoolean(e.left, left);
            e.right = toBoolean(e.right, right);
        }
        d.add(left);
        d.add(right);
        if(!nestedInteger && splitting) {
            // <code>splitting</code> makes <code>!nestedInteger</code>
            // true also in <code>e.*.accept(this)</code>, and does
            // not create a need for conversion; hence, if there
            // was a reason for conversion, then it was already within
            // these calls; hence, the integer arithmetic of the type
            // requiring a conversion, if any, is gone
            d.ia = false;
            d.functions = false;
        }
        nestedInteger = outerNestedInteger;
        return d;
    }
    @Override
    public Description visit(AssignmentExpression e) throws CompilerException {
        throw new RuntimeException("unexpected embedded assignment");
    }
    public Description visit(CallExpression e) throws CompilerException {
        boolean outerNestedInteger = nestedInteger;
        Description d = new Description();
        switch(e.method.name) {
            case "mod":
            case "abs":
                nestedInteger = true;
                d.ia = true;
                break;
                
            default:
                throw new RuntimeException("unknown function: " + e.method.name);
        }
        d.functions = true;
        List<AbstractExpression> newArgs = new LinkedList<>();
        for(AbstractExpression a : e.arg) {
            Description arg = (Description)a.accept(this);
            if(!nestedInteger) {
                newArgs.add(toBoolean(a, arg));
            } else
                newArgs.add(a);
            d.add(arg);
        }
        e.arg = newArgs;
        nestedInteger = outerNestedInteger;
        return d;
    }
    /**
     * A variable in the form apt for <code>product()</code>.
     */
    static protected class VariableWithListOfValues {
        Variable variable;
        List<AsConstant> values;
        
        public VariableWithListOfValues(Variable variable, List<AsConstant> values) {
            this.variable = variable;
            this.values = values;
        }
        public VariableWithListOfValues(String name, List<AsConstant> values) {
            this.variable = new Variable(null, null,
                    new Type(Type.PrimitiveOrVoid.BOOLEAN), name);
            this.values = values;
        }
    }
    /**
     * Returns knowns, as pointed by the counter.
     * 
     * @param vars variables
     * @param counter the combination which points to a unique set of knowns
     * @return list of knowns
     */
    protected Map<Variable, AsConstant> getKnowns(
            List<VariableWithListOfValues> vars, int[] counter) {
        int num = vars.size();
        Map<Variable, AsConstant> knowns = new HashMap<>();
        for(int i = 0; i < num; ++i) {
            VariableWithListOfValues vl = vars.get(i);
            knowns.put(vl.variable, vl.values.get(counter[i]));
        }
        return knowns;
    }
    /**
     * Increases the counter to point to a new combination of knowns.
     * 
     * @param vars variables
     * @param counter the combination which points to a unique set of knowns
     * @return if an overflow occured
     */
    protected boolean increase(List<VariableWithListOfValues> vars, int[] counter){
        int num = counter.length;
        // set the next combination
        boolean overflow = false;
        for(int i = num - 1; i >= 0; --i) {
            if(counter[i] == vars.get(i).values.size() - 1) {
                counter[i] = 0;
                if(i == 0)
                    overflow = true;
            } else {
                ++counter[i];
                break;
            }
        }
        return overflow;
    }
    /**
     * <p>Looks into <code>d</code> to check, if <code>expr</code> needs to
     * be converted to a boolean equivalent. If no, returns the original
     * <code>expr</code>. Otherwise, return the boolean equivalent</p>
     * 
     * <p>The conversion is performed on expressions, which contain both action
     * expressions and the integer arithmetics.</p>
     * 
     * @param expr expression to possibly compute its equivalent; the caller should
     * assure, that it is not nested in an integer arithmetic expression; not modified
     * by this method
     * @param d description of <code>expr</code>; modified by this method
     * according to the output expression
     * @return either the original expression, or the boolean equivalent
     */
    protected AbstractExpression toBoolean(AbstractExpression expr, Description d)
            throws CompilerException {
        if((CONVERSION.integerActionToBoolean && d.actionsToConvert()) ||
                (CONVERSION.integerFunctionToBoolean && d.functions) ||
                (CONVERSION.integerToBoolean && d.ia)) {
            if(CONVERSION.integerActionToBoolean)
                d.ia = false;
            if(CONVERSION.integerFunctionToBoolean)
                d.functions = false;
if(!d.actionsToConvert())
    d = d;
            StreamPos position = expr.getStreamPos();
            AbstractExpression sum = null;
            SortedMap<String, SortedSet<AsConstant>> vars = new TreeMap<>();
            List<VariableWithListOfValues> variables = new LinkedList<>();
            for(AsVariable v : d.vars) {
                vars.put(v.getQualifiedName(), v.RANGE.set);
                variables.add(new VariableWithListOfValues(v.toVariable(),
                        new LinkedList<>(vars.get(v.getQualifiedName()))));
            }
            for(String a : d.actions) {
                vars.put(a, BOOLEAN_SET);
                variables.add(new VariableWithListOfValues(a,
                        new LinkedList<>(vars.get(a))));
            }
            int numCombinations = 1;
            for(SortedSet values : vars.values())
                numCombinations *= values.size();
//            for(Entry<String, SortedSet<AsConstant>> entry : vars.entrySet())
//                variables.add(new VariableWithListOfValues(entry.getKey(),
//                        new LinkedList<>(entry.getValue())));
            int[] counter = new int[variables.size()];
            CompilerException error = new CompilerException();
            final int MAX_NUM_REPORTS = 5;
            for(int i = 0; i < numCombinations; ++i)
                try {
                    Map<Variable, AsConstant> knowns = getKnowns(variables, counter);
                    Literal c = ParserUtils.toConst(AS, expr, knowns,
                            new VxsSemanticCheckFactory());
                    if(increase(variables, counter) && i != numCombinations - 1)
                        throw new RuntimeException("unexpected overflow");
                    boolean add;
//if(c == null)
//    ParserUtils.toConst(AS, expr, knowns,
//          new VxsSemanticCheckFactory());
                    if(c.type.isBoolean())
                        add = c.getBoolean();
                    else {
                        error.addReport(new CompilerException.Report(expr.getStreamPos(),
                            CompilerException.Code.ILLEGAL, "should evaluate to boolean, " +
                                    "is " + c.toString()));
                        if(error.getReports().size() >= MAX_NUM_REPORTS)
                            break;
                        add = false;
                    }
                    if(add) {
                        AbstractExpression product = null;
                        for(VariableWithListOfValues vl : variables) {
                            AbstractExpression p;
                            String name = vl.variable.name;
                            AsConstant value = knowns.get(vl.variable);
                            if(d.actions.contains(name)) {
                                int pos = name.indexOf(".");
                                p = new AsActionExpression(AS, position, AS.SCOPE,
                                        AS.lookupModule(name.substring(0, pos)),
                                        name.substring(pos + 1));
                                if(!value.getBoolean())
                                    p = new UnaryExpression(position, AS.SCOPE,
                                            UnaryExpression.Op.CONDITIONAL_NEGATION, p);
                            } else
                                p = new BinaryExpression(position, AS.SCOPE, BinaryExpression.Op.EQUAL,
                                        Hedgeelleth.newVariableExpression(position, AS.SCOPE, vl.variable),
                                        new ConstantExpression(position, AS.SCOPE, knowns.get(vl.variable)));
                            if(product == null)
                                product = p;
                            else
                                product = new BinaryExpression(position, AS.SCOPE,
                                        BinaryExpression.Op.CONDITIONAL_AND,
                                        product, p);
                        }
                        if(product == null)
                            product = new ConstantExpression(position, AS.SCOPE,
                                    new Literal(true));
                        if(sum == null)
                            sum = product;
                        else
                            sum = new BinaryExpression(position, AS.SCOPE,
                                    BinaryExpression.Op.CONDITIONAL_OR,
                                    sum, product);
                    }
                } catch(CompilerException e) {
                    error.addAllReports(e);
                    if(error.getReports().size() >= MAX_NUM_REPORTS)
                        break;
                }
            if(sum == null)
                sum = new ConstantExpression(position, AS.SCOPE,
                        new Literal(false));
            if(error.reportsExist())
                throw error;
            return sum;
        } else
            return expr;
    }
}
