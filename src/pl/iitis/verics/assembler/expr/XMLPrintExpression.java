/*
 * TextPrintExpression.java
 *
 * Created on Mar 29, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.expr;

import org.jdom2.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Visitor;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

import pl.iitis.verics.agent.*;

/**
 * Prints expressions in a text form.
 * 
 * @author Artur Rataj
 */
public class XMLPrintExpression extends AbstractPrintExpression implements Visitor<Element> {
    /**
     * Module reference
     */
    public final static String E_MODULE_REF = "module_ref";
    /**
     * Formula reference.
     */
    public final static String E_FORMULA_REF = "formula_ref";
    /**
     * Operator.
     */
    public final static String A_OP = "operator";
    /**
     * Coalition.
     */
    public final static String E_COALITION = "coalition";
    /**
     * Action expression.
     */
    public final static String E_ACTION_EXPR = "expr_action";
    /**
     * A protocol's "other" guard.
     */
    public final static String E_OTHER_EXPR = "expr_other";
    /**
     * Constant expression.
     */
    public final static String E_CONSTANT_EXPR = "const";
    /**
     * Unary expression.
     */
    public final static String E_UNARY_EXPR = "expr_unary";
    /**
     * Binary expression.
     */
    public final static String E_BINARY_EXPR = "expr_binary";
    /**
     * Assignment expression.
     */
    public final static String E_ASSIGNMENT_EXPR = "expr_assign";
    /**
     * Function expression.
     */
    public final static String E_FUNCTION_EXPR = "expr_function";
    /**
     * Variable.
     */
    public final static String E_VAR = "var";
    /**
     * Name.
     */
    public final static String A_NAME = "name";
    /**
     * Value (e.g. of a constant).
     */
    public final static String A_VALUE = "value";
    /**
     * Type.
     */
    public final static String A_TYPE = "type";
    
    /**
     * Creates a new print visitor.
     * 
     * @param as agent system; its field <code>toStringRestoreFormulaEnabled</code>
     * is taken into account
     * @param module a module, its locals won't be printed as qualified; null if a global
     * section
     */
    public XMLPrintExpression(AgentSystem as, AsModule module) {
        super(as, module);
    }
    public Element visit(Node n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(SynchronizedStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(Block n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(Compilation c) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(EmptyExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(AllocationExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(IndexExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(BlockExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(ReturnStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(ThrowStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(LabeledStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(JumpStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public Element visit(BranchStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    /**
     * If to replace a textual representation with formula name.
     * 
     * @param e expression
     * @return formula name or null
     */
    protected Element checkFormula(AbstractExpression e) {
        AsFormula f = AS.isReplaced(e);
        if(AS.toStringRestoreFormulaEnabled && f != null) {
            Element node = new Element(E_FORMULA_REF);
            node.setText(f.NAME);
            return node;
        } else
            return null;
    }
    @Override
    public Element visit(AbstractExpression e) throws CompilerException {
        if(e instanceof AsActionExpression) {
            AsActionExpression a = (AsActionExpression)e;
            Element node = new Element(E_ACTION_EXPR);
            node.setText(a.MODULE + "." + a.ACTION);
            return node;
        } else if(e instanceof AsOtherExpression) {
            return new Element(E_OTHER_EXPR);
        } else
            throw new UnsupportedOperationException("Not supported");
    }
    static String booleanToInteger(String identifier) {
        if(identifier.equals("true"))
            identifier = "1";
        else if(identifier.equals("false"))
            identifier = "0";
        return identifier;
    }
    @Override
    public Element visit(ConstantExpression e) throws CompilerException {
        Element formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            Element node = new Element(E_CONSTANT_EXPR);
            String text;
            if(e.literal instanceof AsConstant) {
                AsConstant c = (AsConstant)e.literal;
//                int pos = c.NAME.indexOf('.');
//                if(MODULE != null && pos != -1 && c.NAME.substring(0, pos).equals(MODULE.NAME))
//                    return c.NAME.substring(pos + 1);
//                else
                    text = c.NAME;
            } else
                text = e.literal.toString();
            node.setText(booleanToInteger(text));
            return node;
        }
    }
    @Override
    public Element visit(PrimaryExpression e) throws CompilerException {
        Element formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            // can be only a name constant, an arithmetic constant or a
            // fully qualified variable
            String identifier = e.toString();
            int pos = identifier.indexOf('.');
            String type;
            String name;
            if(pos != -1) {
                type = E_VAR;
                name = e.toString();
            } else {
                type = E_CONSTANT_EXPR;
                name = ((AsConstant)e.prefix.contents).NAME;
                if(name == null) {
                    try {
                        identifier = booleanToInteger(identifier);
                        Double.parseDouble(identifier);
                    } catch(NumberFormatException f) {
                            throw new RuntimeException("expected an arithmetic or boolean constant");
                    }
                    name = identifier;
                }
            }
            Element node = new Element(type);
            node.setText(name);
            return node;
        }
    }
    @Override
    public Element visit(UnaryExpression e) throws CompilerException {
        Element formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            Element node = new Element(E_UNARY_EXPR);
            node.setAttribute(new Attribute(A_OP, translateOpName(
                    e.opToString(), true)));
            if(e.coalition != null) {
                Element coalition = new Element(E_COALITION);
                AsCoalition c = (AsCoalition)e.coalition;
                for(AsModule m : c.PLAYERS) {
                    Element player = new Element(E_MODULE_REF);
                    player.addContent(m.NAME);
                    coalition.addContent(player);
                }
                node.addContent(coalition);
            }
            Element sub = (Element)e.sub.accept(this);
            node.addContent(sub);
            return node;
        }
    }
    @Override
    public Element visit(BinaryExpression e) throws CompilerException {
        Element formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            Element node = new Element(E_BINARY_EXPR);
            node.setAttribute(new Attribute(A_OP,
                    translateOpName(BinaryExpression.getOperatorName(e.operatorType),
                            true)));
            Element left = (Element)e.left.accept(this);
            node.addContent(left);
            Element right = (Element)e.right.accept(this);
            node.addContent(right);
            return node;
        }
    }
    @Override
    public Element visit(AssignmentExpression e) throws CompilerException {
        Element formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            Element node = new Element(E_ASSIGNMENT_EXPR);
            Element lvalue = (Element)e.lvalue.accept(this);
            node.addContent(lvalue);
            Element rvalue = (Element)e.rvalue.accept(this);
            node.addContent(rvalue);
            return node;
        }
    }
    public Element visit(CallExpression e) throws CompilerException {
        Element formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            Element node = new Element(E_FUNCTION_EXPR);
            node.setAttribute(new Attribute(A_NAME,
                    e.method.name));
            Element args = new Element("arguments");
            for(AbstractExpression a : e.arg) {
                Element rvalue = (Element)a.accept(this);
                args.addContent(rvalue);
            }
            node.addContent(args);
            return node;
        }
    }
    @Override
    public Element print(AbstractExpression e) {
        try {
            Element node = (Element)e.accept(this);
            return node;
        } catch(CompilerException ce) {
            throw new RuntimeException("unexpected: " + ce.toString());
        }
    }
}
