/*
 * TextPrintExpression.java
 *
 * Created on Mar 29, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.expr;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Compilation;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Visitor;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;

import pl.iitis.verics.agent.*;
import pl.iitis.verics.assembler.VxsCLI.FileType;

/**
 * Prints expressions in a text form.
 * 
 * @author Artur Rataj
 */
public class TextPrintExpression extends AbstractPrintExpression implements  Visitor<String> {
    /**
     * A output format flavour to use.
     */
    final FileType FT;
    /**
     * Creates a new print visitor.
     * 
     * @param as agent system; its field <code>toStringRestoreFormulaEnabled</code>
     * is taken into account
     * @param module a module, its locals won't be printed as qualified; null if a global
     * section
     * @param fileType a output format flavour to use
     */
    public TextPrintExpression(AgentSystem as, AsModule module,
            FileType ft) {
        super(as, module);
        FT = ft;
    }
    public String visit(Node n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(SynchronizedStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(Block n) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(Compilation c) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(EmptyExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(AllocationExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(IndexExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(BlockExpression e) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(ReturnStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(ThrowStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(LabeledStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(JumpStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    public String visit(BranchStatement s) throws CompilerException {
        throw new UnsupportedOperationException("Not supported");
    }
    /**
     * If to replace a textual representation with formula name.
     * 
     * @param e expression
     * @return formula name or null
     */
    protected String checkFormula(AbstractExpression e) {
        AsFormula f = AS.isReplaced(e);
        if(AS.toStringRestoreFormulaEnabled && f != null)
            return f.NAME;
        else
            return null;
    }
    @Override
    public String visit(AbstractExpression e) throws CompilerException {
        if(e instanceof AsActionExpression) {
            AsActionExpression a = (AsActionExpression)e;
            return a.toString(MODULE);
        } else if(e instanceof AsOtherExpression)
            return e.toString();
        else
            throw new UnsupportedOperationException("Not supported");
    }
    @Override
    public String visit(ConstantExpression e) throws CompilerException {
        String formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            if(e.literal instanceof AsConstant) {
                AsConstant c = (AsConstant)e.literal;
//                int pos = c.NAME.indexOf('.');
//                if(MODULE != null && pos != -1 && c.NAME.substring(0, pos).equals(MODULE.NAME))
//                    return c.NAME.substring(pos + 1);
//                else
                    return c.NAME;
            } else {
                Literal c = e.literal;
                if(c.type.isBoolean() && c.getBoolean()) {
                    if(MODULE.variableList.isEmpty())
                        throw new CompilerException(e.getStreamPos(), CompilerException.Code.ILLEGAL,
                                "can not replace a true expression with true comparison: " +
                                        "module has no variables");
                    AsVariable v = MODULE.variableList.get(0);
                    return v.NAME + "=" + v.NAME;
                } else
                    return c.toString();
            }
        }
    }
    @Override
    public String visit(PrimaryExpression e) throws CompilerException {
        String formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            String name = e.toString();
            int pos = name.indexOf('.');
            if(MODULE != null && pos != -1 && name.substring(0, pos).equals(MODULE.NAME))
                return name.substring(pos + 1);
            else {
                pos = name.indexOf('=');
                if(pos != -1)
                    return name.substring(0, pos).trim();
                else
                    return name;
            }
        }
    }
    @Override
    public String visit(UnaryExpression e) throws CompilerException {
        String formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            String sub = (String)e.sub.accept(this);
            String opString = translateOpName(e.opToString(), false);
            int pos = opString.indexOf('#');
            return opString.substring(0, pos) +
                    sub +
                    opString.substring(pos + 1);
        }
    }
    @Override
    public String visit(BinaryExpression e) throws CompilerException {
        String formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            String left = (String)e.left.accept(this);
            String operator = translateOpName(
                    BinaryExpression.getOperatorName(e.operatorType), false);
            String right = (String)e.right.accept(this);
            if(e.left instanceof BinaryExpression) {
                BinaryExpression leftB = (BinaryExpression)e.left;
                if(leftB.operatorType == e.operatorType &&
                        StringAnalyze.inParentheses(left, false))
                    left = left.trim().substring(1, left.length() - 1).trim();
            }
            return "( " + left + " " + operator + " " + right + " )";
        }
    }
    @Override
    public String visit(AssignmentExpression e) throws CompilerException {
        String formula = checkFormula(e);
        if(formula != null)
            return formula;
        else {
            String lvalue = (String)e.lvalue.accept(this);
            String rvalue = (String)e.rvalue.accept(this);
            return "( " + lvalue + " = " + rvalue + " )";
        }
    }
    public String visit(CallExpression e) throws CompilerException {
        switch(FT) {
            case MCMAS:
                throw new CompilerException(e.getStreamPos(), CompilerException.Code.ILLEGAL,
                        "MCMAS does not support math functions, use a conversion to boolean");
                
            default:
                throw new RuntimeException("unexpected flavour");
        }
    }
    @Override
    public String print(AbstractExpression e) throws CompilerException {
        String s = ((String)e.accept(this)).trim();
        if(StringAnalyze.inParentheses(s, false))
            s = s.substring(1, s.length() - 1);
        return s;
    }
}
