/*
 * AbstractPrintExpression.java
 *
 * Created on Mar 26, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.expr;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

import pl.iitis.verics.agent.*;

/**
 * Configurable print of an expression.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractPrintExpression {
    /**
     * Agent system.
     */
    final AgentSystem AS;
    /**
     * The expression's owner module.
     */
    final AsModule MODULE;
    
    /**
     * Creates a new print visitor.
     * 
     * @param as agent system; its field <code>toStringRestoreFormulaEnabled</code>
     * is taken into account
     * @param module a module, its locals won't be printed as qualified; null if a global
     * section
     */
    public AbstractPrintExpression(AgentSystem as, AsModule module) {
        AS = as;
        MODULE = module;
    }
    /**
     * Translates operator name to a format suitable for MCMAS
     * and XML backends.
     * 
     * @param operatorName Verics' operator name
     * @param xml if to apply additional transition suitable only for
     * the XML output; the caller should take into account, that it will
     * lose the coalition information
     * @return translated operator name
     */
    public static String translateOpName(String operatorName,
            boolean xml) {
        switch(operatorName) {
            case "&&":
                operatorName = "and";
                break;

            case "||":
                operatorName = "or";
                break;

            case "==":
                operatorName = "=";
                break;

            case "!=":
                operatorName = "<>";
                break;

            default:
                /* empty */
        }
        if(xml) {
//            if(operatorName.startsWith("!#")) // equalsTo ?
//                operatorName = "!";
            if(operatorName.startsWith("!"))
                operatorName = "not" + operatorName.substring(1, operatorName.length() - 1);
            int pos = operatorName.indexOf("(");
            if(pos != -1)
                operatorName = operatorName.substring(0, pos);
        }
        return operatorName;
    }
    /**
     * Prints an expression.
     * 
     * @param e expression
     * @return textual representation
     */
    abstract public Object print(AbstractExpression e) throws CompilerException;
}
