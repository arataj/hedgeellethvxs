/*
 * VxsLibrary.java
 *
 * Created on Mar 4, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

/**
 * Dfiens the set of library files, from the system and from the local libraries.
 * 
 * @author Artur Rataj
 */
public class VxsLibrary {
    /**
     * The directory where is compiler is run.
     */
    public final PWD TOP_DIRECTORY;
    /**
     * Path to the system library.
     */
    public final String LIBRARY_PATH;
    /**
     * List of all files in this library.
     */
    public List<String> FILES;
    /**
     * If this includes a full version of the standard library.
     */
    public boolean FULL;
    
    /**
     * Creates a new definition of what to include within the library.
     * 
     * @param topDirectory the directory where is compiler is run
     * @param libraryPath path to the system library
     * @param full include whole system library; typically true for the command line,
     * where the overhead difference is not that important
     * @param localLibrary additional local files to put into the library
     */
    public VxsLibrary(PWD topDirectory, String libraryPath,
            boolean full, List<String> localLibrary) throws CompilerException {
        TOP_DIRECTORY = topDirectory;
        LIBRARY_PATH = libraryPath;
        FILES = new LinkedList<>();
        FULL = full;
        pl.gliwice.iitis.hedgeelleth.cli.Compiler.addLibraryFiles(
                FILES, TOP_DIRECTORY, LIBRARY_PATH, false, true, full, true);
        FILES.addAll(localLibrary);
    }
}
