/*
 * VxsStaticAnalysisFactory.java
 *
 * Created on Apr 11, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary.StaticAnalysisFactory;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.iitis.verics.agent.AgentSystem;

/**
 * Produces a custom <code>StaticAnalysis</code>, which handles
 * <code>AsActionExpression</code>.
 * 
 * @author Artur Rataj
 */
public class VxsStaticAnalysisFactory extends StaticAnalysisFactory {
    final AgentSystem AS;
    public VxsStaticAnalysisFactory(AgentSystem as) {
        AS = as;
    }
    @Override
    public StaticAnalysis newInstance(Method method,
            Map<Variable, Literal> knowns) throws CompilerException {
        return new VxsStaticAnalysis(AS, method, knowns);
    }
}
