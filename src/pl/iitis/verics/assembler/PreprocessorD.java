/*
 * PreprocessorD.java
 *
 * Created on Feb 1, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;
import pl.gliwice.iitis.verics.parser.VericsParser;

/**
 * Preprocessor constants, visible as fields in a class <code>D</code>.
 * 
 * This is a subclass of <code>CliConstants</code> with a more convenient
 * method for adding strings.
 * 
 * @author Artur Rataj
 */
public class PreprocessorD extends CliConstants {
    /**
     * Adds a string scalar.
     * 
     * This is a convenience method, which does not require to specify the
     * name of the string class.
     * 
     * @param name name of the constant
     * @param value value of the constant
     */
    public void add(String name, String value) {
        add(name, value, VericsParser.STRING_CLASS);
    }
    /**
     * Adds a string array.
     * 
     * This is a convenience method, which does not require to specify the
     * name of the string class.
     * 
     * @param name name of the constant
     * @param value values of the constant
     */
    public void add(String name, String[] value) {
        add(name, value, VericsParser.STRING_CLASS);
    }
}
