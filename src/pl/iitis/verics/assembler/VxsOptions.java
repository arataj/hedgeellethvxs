/*
 * VxsOptions.java
 *
 * Created on Sep 3, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

import pl.iitis.verics.assembler.VxsCLI.FileType;

/**
 * Options for translating Vxs files.
 * 
 * @author Artur Rataj
 */
public class VxsOptions {
    /**
     * Translation options which control an integer conversion.
     */
    public static class Conversion {
        /**
         * Perform a translation from protocol action formulas evaluated as integers to the
         * the boolean equivalent. The default is <code>false</code>.
         */
        public boolean integerActionToBoolean = false;
        /**
         * Perform a translation from integer math functions to the boolean equivalent.
         * The default is <code>false</code>.
         */
        public boolean integerFunctionToBoolean = false;
        /**
         * Perform a translation from all integer arithmetic to the boolean equivalent.
         * A superset of <code>integerActionToBoolean</code> and
         * <code>integerFunctionToBoolean</code>. The default is <code>false</code>.
         */
        public boolean integerToBoolean = false;
        /**
         * Leave intact any comparison of any combination variable/constant.
         * The default is <code>true</code>.
         */
        public boolean allowVariableComparison = true;
    }
    /**
     * Translation options, including the conversion.
     */
    public static class Translation {
        /**
         * Perform a translation from synchronisation labels to guards even if the output format
         * does not require it.  The default is <code>false</code>, unless for the MCMAS output.
         */
        public boolean syncToGuards = false;
        /**
         * Conversion options.
         */
        public Conversion conversion = new Conversion();
        /**
         * Output format. Can be overridden by a forced output file name.
         */
        public FileType outputFormat = FileType.MCMAS;
    }
    /**
     * If only a help message shoul be displayed. The default if <code>false</code>.
     */
    boolean help = false;
    /**
     * Files to translate. Empty list for none.
     */
    public List<String> files;
//    /**
//     * Shows syntax of all available logics and exit, uses ASCII only. The default is <code>false</code>.
//     */
//    boolean showLogicAscii = false;
    /**
     * Shows syntax of all available logics and exit, uses Unicode.  The default is <code>false</code>.
     */
    boolean showLogicUnicode = false;
    /**
     * Save preprocessor output to a .vxsp file and exit. The default is <code>false</code>.
     */
    boolean preprocessOnly = false;
    /**
     * Translation options.
     */
    Translation translation;
    /**
     * Creates a new instance of options of the Vxs compiler.
     */
    public VxsOptions() {
        files = new LinkedList<>();
        translation = new Translation();
    }
}
