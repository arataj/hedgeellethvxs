/*
 * VxsStaticAnalysis.java
 *
 * Created on Apr 11, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.gliwice.iitis.hedgeelleth.compiler.sa.StaticAnalysis;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.iitis.verics.agent.AgentSystem;
import pl.iitis.verics.agent.AsActionExpression;
import pl.iitis.verics.agent.AsConstant;
import pl.iitis.verics.assembler.expr.IntegerToBoolean;

/**
 * A static analysis, which handles <code>AsActionExpression</code>
 * as a boolean variable reference and changes boolean values to 0 or 1
 * if an arithmetic operation is performed on them.
 * 
 * @author Artur Rataj
 */
public class VxsStaticAnalysis extends StaticAnalysis {
    /**
     * Agent system.
     */
    protected final AgentSystem AS;
    
    public VxsStaticAnalysis(AgentSystem as, Method method,
            Map<Variable, Literal> knowns) throws CompilerException {
        super(knowns);
        AS = as;
        checkSolitary(method);
    }
    @Override
    public ConstantExpression visit(AbstractExpression e) {
        if(e instanceof AsActionExpression) {
            AsActionExpression a = (AsActionExpression)e;
            for(Variable v : knowns.keySet())
                if(v.name.equals(a.getQualifiedName())) {
                    return new ConstantExpression(a.getStreamPos(),
                            (BlockScope)a.outerScope, knowns.get(v));
                }
        }
        return null;
    }
    @Override
    public ConstantExpression visit(PrimaryExpression e) throws CompilerException {
        // AsConstant c = ParserUtils.toConst(AS, e, (Map<Variable, AsConstant>)knowns);
        return super.visit(e);
//        if(c == null)
//            return null;
//        else
//            return new ConstantExpression(null, AS.SCOPE, c);
    }
    @Override
    public ConstantExpression visit(UnaryExpression e) throws CompilerException {
        AbstractExpression tmpSub = e.sub;
        ConstantExpression c;
        try {
            if(!e.operatorType.isBoolean() && e.sub instanceof AsActionExpression) {
                ConstantExpression sub = ConstantExpression.convertToInteger(
                    (ConstantExpression)e.sub.accept(this));
                if(sub == null)
                    return null;
                else
                    e.sub = sub;
            }
            c = super.visit(e);
        } finally {
            e.sub = tmpSub;
        }
        return c;
    }
    @Override
    public ConstantExpression visit(BinaryExpression e) throws CompilerException {
        AbstractExpression tmpLeft = e.left;
        AbstractExpression tmpRight = e.right;
        ConstantExpression c;
        try {
            if(!e.operatorType.isBoolean()) {
                if(e.left instanceof AsActionExpression) {
                    ConstantExpression left = ConstantExpression.convertToInteger(
                            (ConstantExpression)e.left.accept(this));
                    if(left == null)
                        return null;
                    else
                        e.left = left;
                }
                if(e.right instanceof AsActionExpression) {
                    ConstantExpression right = ConstantExpression.convertToInteger(
                        (ConstantExpression)e.right.accept(this));
                    if(right == null)
                        return null;
                    else
                        e.right = right;
                }
            }
            c = super.visit(e);
        } finally {
            e.left = tmpLeft;
            e.right = tmpRight;
        }
        return c;
    }
    /**
     * Evaluates a built--in Vxs function.
     * 
     * @param method a method which represents the function
     * @param arg arguments of the function
     * @return a constant value if all arguments are constants, otherwise null
     */
    private ConstantExpression evaluateFunction(Method method, List<AbstractExpression> arg)
            throws CompilerException {
        if(method == ParserUtils.METHOD_ABS) {
            if(arg.size() != 1)
                throw new CompilerException(arg.get(0).getStreamPos(),
                        CompilerException.Code.INVALID,
                        "a single argument expected");
            Literal sub = arg.get(0).getSimpleConstant();
            if(sub != null) {
                if(sub.type.isOfIntegerTypes())
                    return new ConstantExpression(null, AS.SCOPE,
                            new Literal(Math.abs(sub.getMaxPrecisionInteger())));
                else
                    throw new CompilerException(arg.get(0).getStreamPos(),
                            CompilerException.Code.INVALID,
                            "an integer value expected");
            }
        } else if(method == ParserUtils.METHOD_MOD) {
            if(arg.size() != 2)
                throw new CompilerException(arg.get(0).getStreamPos(),
                        CompilerException.Code.INVALID,
                        "two arguments expected");
            Literal left = arg.get(0).getSimpleConstant();
            Literal right = arg.get(1).getSimpleConstant();
            if(left != null && right != null) {
                if(!left.type.isOfIntegerTypes())
                    throw new CompilerException(arg.get(0).getStreamPos(),
                            CompilerException.Code.INVALID,
                            "an integer value expected");
                if(!right.type.isOfIntegerTypes())
                    throw new CompilerException(arg.get(1).getStreamPos(),
                            CompilerException.Code.INVALID,
                            "an integer value expected");
                return new ConstantExpression(null, AS.SCOPE,
                        new Literal(left.getMaxPrecisionInteger() % right.getMaxPrecisionInteger()));
            }
        } else
            throw new RuntimeException("unknown built--in function: " +
                    method.toString());
        return null;
    }
    @Override
    public ConstantExpression visit(CallExpression e) throws CompilerException {
        boolean integerArguments = false;
        switch(e.method.name) {
            case "mod":
            case "abs":
                integerArguments = true;
                break;
                
            default:
                throw new RuntimeException("unknown function: " + e.method.name);
        }
        ConstantExpression c;
        if(integerArguments) {
            List<AbstractExpression> tmpArg = e.arg;
            try {
                List<AbstractExpression> newArgs = new LinkedList<>();
                for(AbstractExpression a : e.arg) {
                    ConstantExpression arg = ConstantExpression.convertToInteger(
                            (ConstantExpression)a.accept(this));
                    if(arg == null)
                        return null;
                    else
                        newArgs.add(arg);
                }
                e.arg = newArgs;
                super.visit(e);
                c = evaluateFunction(e.method, e.arg);
            } finally {
                e.arg = tmpArg;
            }
        } else
            c = super.visit(e);
        return c;
    }
}
