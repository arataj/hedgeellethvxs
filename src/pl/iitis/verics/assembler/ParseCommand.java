/*
 * ParseCommand.java
 *
 * Created on Jul 2, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;

/**
 * Parses a prologue command.
 * 
 * @author Artur Rataj
 */
public class ParseCommand extends AbstractParseMap {
    /**
     * Type of the preprocessed file.
     */
    private final VxsCLI.FileType FILE_TYPE;
    /**
     * Creates a new instance of <code>ParsePrint</code>.
     * 
     * @param raw input string, raw assembly text + preprocessor expressions
     * @param lineNum line number, beginning at 0
     * @param colNum column number, beginning at 0
     * @param fileType type of the preprocessed file
     */
    public ParseCommand(String raw, int lineNum, int colNum,
            VxsCLI.FileType fileType) throws ParseException {
        super(raw, lineNum, colNum);
        FILE_TYPE = fileType;
    }
    /**
     * Prefix all identifiers that start with a letter and are followed by letters, digits
     * or underscores, with ".", begining at a given position.
     * 
     * @param text text to modify
     * @param pos start position
     * @return modified text
     */
    public String prefixFields(String text, int pos) {
        StringBuilder out = new StringBuilder();
        out.append(text.substring(0, pos));
        boolean in = false;
        while(pos < text.length()) {
            char c = text.charAt(pos);
            if(!in && Character.isLetter(c)) {
                out.append('.');
                in = true;
            } else if(in && !Character.isLetterOrDigit(c) && c != '_')
                in = false;
            out.append(c);
            ++pos;
        }
        return out.toString();
    }
    /**
     * Processes a command. Ignores preprocessor directives within
     * strarred comments, must be told if the beginning of the line
     * is in a comment already.
     * 
     * @param inComment if the line begins in a comment
     */
    protected final void process(boolean inComment) throws ParseException {
//if(RAW.indexOf("/*") != -1)
//    inComment = inComment;
        OUT = null;
        String CONST_DECLARATION_STR;
        final String INT_STR;
        final String DOUBLE_STR;
        switch(FILE_TYPE) {
            case VSP:
            case VS:
            case VXSP:
            case VXS:
                CONST_DECLARATION_STR = "common ";
                INT_STR = "Z ";
                DOUBLE_STR = "R ";
                break;
                
            case PRISM_P:
                CONST_DECLARATION_STR = "const ";
                INT_STR = "int ";
                DOUBLE_STR = "double ";
                break;
                
            case EXTERNAL_P:
                CONST_DECLARATION_STR = null;
                INT_STR = null;
                DOUBLE_STR = null;
                break;
                
            default:
                throw new RuntimeException("unknown file type");
        }
        int[] map = new int[RAW.length()*2];
        String s = StringAnalyze.compactWhitespace(RAW, false, map);
        int compactedLength = s.length();
        int indexDiff = 0;
        boolean changed;
        do {
            changed = false;
            while(Character.isWhitespace(s.charAt(0))) {
                s = s.substring(1);
                indexDiff += 1;
                changed = true;
            }
            if(inComment || s.startsWith("/*")) {
                int pos;
                if((pos = s.indexOf("*/")) != -1) {
                    s = s.substring(pos + 2);
                    indexDiff += pos;
                    inComment = false;
                    changed = true;
                } else
                    inComment = true;
            }
        } while(changed);
        if(CONST_DECLARATION_STR != null &&
                !inComment && s.startsWith(CONST_DECLARATION_STR)) {
            String t = s.substring(CONST_DECLARATION_STR.length());
            indexDiff += CONST_DECLARATION_STR.length();
            boolean floating = false;
            if(t.startsWith(INT_STR) || (floating = t.startsWith(DOUBLE_STR))) {
                String u;
                OUT = "ap$ ";
                indexDiff += -4;
                if(!floating) {
                    String c = t.substring(INT_STR.length()).trim();
                    String d = prefixFields(c, c.indexOf("="));
                    OUT += "Z " + d;
                    indexDiff += -2 + t.length() - d.length();
                } else {
                    String c = t.substring(DOUBLE_STR.length()).trim();
                    String d = prefixFields(c, c.indexOf("="));
                    OUT += "R " + d;
                    indexDiff += -2 + t.length() - d.length();
                }
                switch(FILE_TYPE) {
                    case VSP:
                    case VS:
                    case VXSP:
                    case VXS:
                        /* empty */
                        break;

                    case PRISM_P:
                        OUT = OUT.replaceFirst("=", ":=");
                        break;
                        
                    case EXTERNAL_P:
                        throw new RuntimeException("unexpected");
                        
                    default:
                        throw new RuntimeException("unknown file type");
                }
                OUT += ";\n";
                for(int i = 0; i < OUT.length(); ++i) {
                    int indexBase = Math.min((compactedLength - 1)*2, (i + indexDiff)*2);
                    int lineDiff = map[indexBase + 0];
                    int colDiff = map[indexBase + 1];
                    LINE_MAP.add(LINE_NUM + lineDiff);
                    COL_MAP.add(COL_NUM + colDiff);
                }
            }
        }
    }
}
