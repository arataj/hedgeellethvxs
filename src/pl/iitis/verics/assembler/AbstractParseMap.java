/*
 * AbstractParseLine.java
 *
 * Created on Jul 2, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

/**
 * A parsed string, with references to positions in the input raw string,
 * with a parsed line of Vxs. The line may contain a constant declaration.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractParseMap {
    /**
     * Input string, raw assembly text + preprocessor expressions
     * of the form ${}. In the Vxs language. Can be null if not needed.
     */
    public String RAW;
    /**
     * Initial line number, for error messages; beginning at 0. -1 for no match.
     */
    public int LINE_NUM;
    /**
     * Initial column number, for error messages; beginning at 0. if -1,
     * then the interpretation depends on the line number: also -1 no match,
     * otherwise only the line is known.
     */
    public int COL_NUM;
    /**
     * Indexed by output string, returns raw line since 0 or -1 for unimportant.
     */
    public List<Integer> LINE_MAP;
    /**
     * Indexed by output string, returns raw column since 0 or -1 for unimportant.
     */
    public List<Integer> COL_MAP;
    /**
     * Output string, indexes <code>COL_MAP</code>. In the Verics language.
     */
    public String OUT;
    
    /**
     * An abstract parse map. <code>OUT</code> is assigned null.
     * 
     * @param raw input string, raw assembly text + preprocessor expressions
     * @param lineNum line number, beginning at 0
     * @param colNum column number, beginning at 0
     */
    public AbstractParseMap(String raw, int lineNum, int colNum) {
        RAW = raw;
        LINE_NUM = lineNum;
        COL_NUM = colNum;
        LINE_MAP = new LinkedList<>();
        COL_MAP = new LinkedList<>();
    }
    /**
     * Appends a given number of subsequent entries in the position map with
     * -1, meaning, "no match in tghe original file"
     * 
     * @param length number of entries to apend
     */
    protected void fillNoMatch(int length) {
        for(int i = 0; i < length; ++i) {
            LINE_MAP.add(-1);
            COL_MAP.add(-1);
        }
    }
    /**
     * Appends another position map to this map.
     * 
     * @param map the map to append
     */
    protected void copyMatch(AbstractParseMap map) {
        if(map.OUT.length() != map.LINE_MAP.size())
            throw new RuntimeException("line map mismatch");
        if(map.OUT.length() != map.COL_MAP.size())
            throw new RuntimeException("column map mismatch");
        LINE_MAP.addAll(map.LINE_MAP);
        COL_MAP.addAll(map.COL_MAP);
    }
}
