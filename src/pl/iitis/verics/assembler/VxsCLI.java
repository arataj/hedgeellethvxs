/*
 * Main.java
 *
 * Created on Jun 22, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import java.io.*;

import com.martiansoftware.jsap.*;
import org.antlr.v4.runtime.*;

import pl.gliwice.iitis.hedgeelleth.About;
import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.LineIndex;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;

import pl.gliwice.iitis.verics.VericsFrontend;
import pl.iitis.verics.agent.AgentSystem;
import pl.iitis.verics.agent.property.Generator;
import pl.iitis.verics.assembler.expr.IntegerToBoolean;
import pl.iitis.verics.assembler.expr.VarUsage;
import pl.iitis.verics.assembler.io.*;
import pl.iitis.verics.assembler.parser.VxspLexer;
import pl.iitis.verics.assembler.parser.VxspParser;

/**
 * Main class of the Verics assembly compiler.
 * 
 * @author Artur Rataj
 */
public class VxsCLI {
    /**
     * Default file name extension of a file in the obsolete Vs format.
     */
    public static final String VS_FILE_NAME_EXTENSION = "vs";
    /**
     * Default file name extension of a Vs file after preprocessing,
     * and thus with no preprocessor directives already.
     */
    public static final String VS_PREPROCESSED_FILE_NAME_EXTENSION = "vsp";
    /**
     * Default file name extension of a Vxs file.
     */
    public static final String VXS_FILE_NAME_EXTENSION = "vxs";
    /**
     * Default file name extension of a Vs file after preprocessing,
     * and thus with no preprocessor directives already.
     */
    public static final String VXS_PREPROCESSED_FILE_NAME_EXTENSION = "vxsp";
    /**
     * Default file name extension of a MCMAS file.
     */
    public static final String MCMAS_FILE_NAME_EXTENSION = "ispl";
    /**
     * Default file name extension of an XML file.
     */
    public static final String XML_FILE_NAME_EXTENSION = "xml";
    /**
     * Default file name extension of a Prism preprocessor file.
     */
    public static final String PRISM_PREPROCESSOR_FILE_NAME_EXTENSION = "nm.pp";
    /**
     * Default file name extension of a Tex preprocessor file.
     */
    public static final String TEX_PREPROCESSOR_FILE_NAME_EXTENSION = "tex.pp";
    /**
     * Default file name extension of a Prism file.
     */
    public static final String PRISM_FILE_NAME_EXTENSION = "nm";
    /**
     * Default file name extension of a Tex file.
     */
    public static final String TEX_FILE_NAME_EXTENSION = "tex";
    /**
     * A prefix added befor each CLI message.
     */
    protected static final String MESSAGE_PREFIX = "veric";
    /**
     * Regression tests may require the compilation even in the case on a parse
     * error. This field should be used only in tests.
     */
    public static VxsCompilation VXSC = null;
    /**
     * Command--line defines constants, keyed with names. The
     * lists of values should have arithmetic values and be sorted.<br>
     * 
     * The default is an empty map.
     */
    public static CliConstants defines;
    /**
     * If not null, forces the name of the output file. Requires only a single
     * model file.
     * 
     * The default is null.
     */
    public static String forceOutFilename;
    /**
     * File types.
     */
    public static enum FileType {
        /**
         * The obsolete agent system format, which used a kind of synchronisation
         * then translated to guards.
         */
        VS,
        /*
         * VS preprocessed, and thus without any preprocessor
         * directives already.
         */
        VSP,
        /**
         * The current MDP format.
         */
        VXS,
        /*
         * VXS preprocessed, and thus without any preprocessor
         * directives already.
         */
        VXSP,
        VERICS,
        MCMAS,
        /**
         * A generic XML.
         */
        XML,
        /*
         * Prism with Vs preprocessor directives.
         */
        PRISM_P,
        /*
         * Some external format with Vs preprocessor directives.
         */
        EXTERNAL_P,
        /**
         * Prism.
         */
        PRISM,
        /**
         * Some external format.
         */
        EXTERNAL,
        /**
         * Some unknown file type.
         */
        UNKNOWN,
    };
    
    /**
     * Deduces file type from file name extension.
     * 
     * @param fileName file name
     * @return file type, null if not recognized
     */
    public static final FileType getFileType(String fileName) {
        FileType type;
        if(fileName.endsWith("." + VS_FILE_NAME_EXTENSION))
            type = FileType.VS;
        else if(fileName.endsWith("." + VS_PREPROCESSED_FILE_NAME_EXTENSION))
            type = FileType.VSP;
        else if(fileName.endsWith("." + VXS_FILE_NAME_EXTENSION))
            type = FileType.VXS;
        else if(fileName.endsWith("." + VXS_PREPROCESSED_FILE_NAME_EXTENSION))
            type = FileType.VXSP;
        else if(fileName.endsWith("." + VericsFrontend.FILE_NAME_EXTENSION))
            type = FileType.VERICS;
        else if(fileName.endsWith("." + VxsCLI.MCMAS_FILE_NAME_EXTENSION))
            type = FileType.MCMAS;
        else if(fileName.endsWith("." + VxsCLI.XML_FILE_NAME_EXTENSION))
            type = FileType.XML;
        else if(fileName.endsWith("." + VxsCLI.PRISM_PREPROCESSOR_FILE_NAME_EXTENSION))
            type = FileType.PRISM_P;
        else if(fileName.endsWith("." + VxsCLI.PRISM_FILE_NAME_EXTENSION))
            type = FileType.PRISM;
        else if(fileName.endsWith("." + VxsCLI.TEX_PREPROCESSOR_FILE_NAME_EXTENSION))
            type = FileType.EXTERNAL_P;
        else if(fileName.endsWith("." + VxsCLI.TEX_FILE_NAME_EXTENSION))
            type = FileType.EXTERNAL;
        else
            type = null;
        return type;
    }
    final static char FORMAT_XML = 'x';
    final static char FORMAT_ISPL = 'i';
    final static char FORMAT_PRISM = 'p';
    /**
     * Parses CLI options. If a help option is given, full syntax of arguments
     * is displayed to the stdout.
     * 
     * @param args CLI options
     * @return Vs options
     */
    protected static VxsOptions parseOptions(String[] args) throws CompilerException {
            final String HELP = "help";
            // final String SHOW_SYNTAX_LOGIC_ASCII = "show syntax of logics, use ASCII only";
            final String SHOW_SYNTAX_LOGIC_UNICODE = "show syntax of all available logics";
            final String PREPROCESS_ONLY = "preprocess only";
            final String SYNC_LABEL_TO_GUARD = "sync labels to protocol action guards";
            final String INTEGER_SYNC_TO_BOOLEAN = "integer action evaluation to boolean";
            final String INTEGER_FUNCTION_TO_BOOLEAN = "integer math functions to boolean";
            final String INTEGER_TO_BOOLEAN = "-S, -F, arithmetic operators to boolean";
            final String GET_CONST = "constant";
            final String OUTPUT_FILE = "name of the output file";
            final String OUTPUT_FORMAT = "output format";
            final String INPUT_FILES = "source_file_";
            try {
                JSAP jsap = new JSAP();
                //
                // cli options
                //
                // -h -- help
                Switch helpParam = new Switch(HELP).
                    setShortFlag('h').
                    setLongFlag("help");
                helpParam.setHelp("show help and exit");
                jsap.registerParameter(helpParam);
                /*
                // --show-logic-syntax
                Switch slsaParam = new Switch(SHOW_SYNTAX_LOGIC_ASCII).
                    setLongFlag("show-logic-syntax");
                slsaParam.setHelp("shows syntax of all available logics and exit, uses ASCII only");
                jsap.registerParameter(slsaParam);
                 */
                // --show-logic-syntax
                Switch slsuParam = new Switch(SHOW_SYNTAX_LOGIC_UNICODE).
                    setLongFlag("show-logic-syntax");
                slsuParam.setHelp(SHOW_SYNTAX_LOGIC_UNICODE + " and exit");
                jsap.registerParameter(slsuParam);
                // -D, --define
                FlaggedOption constParam = new FlaggedOption(GET_CONST)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setRequired(false) 
                                        .setShortFlag('D') 
                                        .setLongFlag("define")
                                        .setList(true)
                                        .setListSeparator(',');
                constParam.setHelp("constant definitions: <name>=<value(s)>,... " +
                        "where <value(s)> is an alphanumeric constant or a set of integer values <min>~<max>[:<step>]");
                constParam.setCategory("MODEL");
                jsap.registerParameter(constParam);
                // -L, --label-to-guard
                Switch syncToGuardsParam = new Switch(SYNC_LABEL_TO_GUARD).
                    setShortFlag('L').
                    setLongFlag("label-to-guard");
                syncToGuardsParam.setHelp(SYNC_LABEL_TO_GUARD);
                syncToGuardsParam.setCategory("CONVERSION");
                jsap.registerParameter(syncToGuardsParam);
                // -S, --integer-sync-to-boolean
                Switch intSyncToBooleanParam = new Switch(INTEGER_SYNC_TO_BOOLEAN).
                    setShortFlag('S').
                    setLongFlag("integer-sync-to-boolean");
                intSyncToBooleanParam.setHelp(INTEGER_SYNC_TO_BOOLEAN);
                intSyncToBooleanParam.setCategory("CONVERSION");
                jsap.registerParameter(intSyncToBooleanParam);
                // -F, --integer-function-to-boolean
                Switch intFunctionToBooleanParam = new Switch(INTEGER_FUNCTION_TO_BOOLEAN).
                    setShortFlag('F').
                    setLongFlag("integer-function-to-boolean");
                intFunctionToBooleanParam.setHelp(INTEGER_FUNCTION_TO_BOOLEAN);
                intFunctionToBooleanParam.setCategory("CONVERSION");
                jsap.registerParameter(intFunctionToBooleanParam);
                // -Z, --integer-to-boolean
                Switch intToBooleanParam = new Switch(INTEGER_TO_BOOLEAN).
                    setShortFlag('Z').
                    setLongFlag("integer-to-boolean");
                intToBooleanParam.setHelp(INTEGER_TO_BOOLEAN);
                intToBooleanParam.setCategory("CONVERSION");
                jsap.registerParameter(intToBooleanParam);
                // -p, --preprocess-only
                Switch preprocessOnlyParam = new Switch(PREPROCESS_ONLY).
                    setShortFlag('p').
                    setLongFlag("preprocess-only");
                preprocessOnlyParam.setHelp(PREPROCESS_ONLY);
                preprocessOnlyParam.setCategory("OUTPUT");
                jsap.registerParameter(preprocessOnlyParam);
                // -o --output-format
                FlaggedOption outputFormatParam = new FlaggedOption(OUTPUT_FORMAT)
                                        .setStringParser(JSAP.CHARACTER_PARSER)
                                        .setDefault("" + FORMAT_XML) 
                                        .setRequired(false) 
                                        .setShortFlag('o') 
                                        .setLongFlag("output-format");
                outputFormatParam.setHelp("set output format: " +
                        FORMAT_XML + " generic XML, " +
                        FORMAT_ISPL + " ISPL"/*, " +
                        FORMAT_PRISM + " Prism; overridable by -O"*/);
                outputFormatParam.setCategory("OUTPUT");
                jsap.registerParameter(outputFormatParam);
                // -O
                FlaggedOption outFileParam = new FlaggedOption(OUTPUT_FILE)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setRequired(false) 
                                        .setShortFlag('O') ;
                outFileParam.setHelp("forces the name of the output file"/*; " +
                        "requires a single " + VXS_FILE_NAME_EXTENSION + " file"*/);
                outFileParam.setCategory("OUTPUT");
                jsap.registerParameter(outFileParam);
                // input files
                UnflaggedOption inputFilesParam = new UnflaggedOption(INPUT_FILES)
                                        .setStringParser(JSAP.STRING_PARSER)
                                        .setRequired(false)
                                        .setGreedy(true);
                inputFilesParam.setCategory("");
                jsap.registerParameter(inputFilesParam);
                inputFilesParam.setHelp("source files: " +
                        "*." + VXS_FILE_NAME_EXTENSION + " " +
                        "*." + VericsFrontend.FILE_NAME_EXTENSION);
                //
                JSAPResult config = jsap.parse(args);
                if(!config.success()) {
                    Iterator<String> it = (Iterator<String>)config.getErrorMessageIterator();
                    StringBuilder errors = new StringBuilder();
                    while(it.hasNext()) {
                        if(errors.length() != 0)
                            errors.append("; ");
                        errors.append(it.next());
                    }
                    throw new JSAPException(errors.toString() + "; use -h for help");
                }
                VxsOptions vxsOptions = new VxsOptions();
                vxsOptions.help = config.getBoolean(HELP);
                /*
                vxsOptions.showLogicAscii = config.getBoolean(SHOW_SYNTAX_LOGIC_ASCII);
                 */
                vxsOptions.showLogicUnicode = config.getBoolean(SHOW_SYNTAX_LOGIC_UNICODE);
                vxsOptions.preprocessOnly = config.getBoolean(PREPROCESS_ONLY);
                vxsOptions.translation.syncToGuards = config.getBoolean(SYNC_LABEL_TO_GUARD);
                vxsOptions.translation.conversion.integerActionToBoolean = config.getBoolean(INTEGER_SYNC_TO_BOOLEAN);
                vxsOptions.translation.conversion.integerFunctionToBoolean = config.getBoolean(INTEGER_FUNCTION_TO_BOOLEAN);
                vxsOptions.translation.conversion.integerToBoolean = config.getBoolean(INTEGER_TO_BOOLEAN);
                defines = pl.gliwice.iitis.hedgeelleth.cli.Main.parseCLIConstants(
                        config.getStringArray(GET_CONST), true);
                switch(config.getChar(OUTPUT_FORMAT)) {
                    case FORMAT_XML:
                        vxsOptions.translation.outputFormat = FileType.XML;
                        break;

                    case FORMAT_ISPL:
                        vxsOptions.translation.outputFormat = FileType.MCMAS;
                        break;
                        
                    case FORMAT_PRISM:
                        vxsOptions.translation.outputFormat = FileType.PRISM;
                        break;

                    default:
                        throw new JSAPException("unknown output format " +
                                config.getChar(OUTPUT_FORMAT));
                }
                forceOutFilename = config.getString(OUTPUT_FILE);
                vxsOptions.files = CompilerUtils.expandFiles(
                        Arrays.asList(config.getStringArray(INPUT_FILES)));
                if(vxsOptions.help) {
                    System.out.println("Vxs compiler " +
                            /*"Verics language version " + */ About.VERSION + "\n\n" +
                            "command line syntax:\n\n" + jsap.getHelp());
                }
                if(vxsOptions.showLogicUnicode) {
                    /*
                    if(vxsOptions.showLogicAscii && vxsOptions.showLogicUnicode)
                        throw new JSAPException("can not use both --" + slsaParam.getLongFlag() +
                                " and --" + slsuParam.getLongFlag());
                     */
                    Generator.USE_SYMBOLS = vxsOptions.showLogicUnicode;
                    Generator.printWholeSyntax();
                }
                return vxsOptions;
            } catch(JSAPException e) {
                throw new CompilerException(null, CompilerException.Code.INVALID,
                        e.getMessage());
            }
    }
    /**
     * Any line which begins with the character `#' is removed; instead
     * printed to the standard output without the `#' and the line number
     * suffix.
     * 
     * @param in input string
     * @param removeLineNumberSuffix if to remove line number suffixes
     * @return filtered string
     */
    protected static String filterStdout(String in, boolean removeLineNumberSuffix) {
        StringBuilder out = new StringBuilder();
        Scanner sc = new Scanner(in);
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            if(removeLineNumberSuffix) {
                int pos = line.lastIndexOf(VxsPreprocessor.LINE_COMMENT_STR);
                if(pos != -1)
                    line = line.substring(0, pos);
            }
            if(line.startsWith("##"))
                System.out.println(line.substring(2));
            else
                out.append(line + "\n");
        }
        return out.toString();
    }
    /**
     * Maps a VXSP file to VXS, ont the basis of <code>##LINE</code> comments.
     * @param vxsp contents of the VXSP file
     * @return a line map, keyes with lines numbers in VXSP, begining at 1
     */
    private static Map<Integer, Integer> getLineMap(String vxsp) {
        Map<Integer, Integer> map = new HashMap<>();
        int lastVsCount = 1;
        int vspCount = 1;
        Scanner sc = new Scanner(vxsp).useLocale(Locale.ROOT);
        while(sc.hasNextLine()) {
            String line = sc.nextLine();
            if(line.isEmpty()) {
                map.put(vspCount, lastVsCount);
            } else {
                int pos = line.lastIndexOf(VxsPreprocessor.LINE_COMMENT_STR);
                if(pos == -1)
                    throw new RuntimeException("no line map infomation");
                String numStr = line.substring(pos + VxsPreprocessor.LINE_COMMENT_STR.length()).trim();
                try {
                    int vsCount = Integer.parseInt(numStr);
                    map.put(vspCount, vsCount);
                    lastVsCount = vsCount;
                } catch(NumberFormatException e) {
                    throw new RuntimeException("line map infomation is invalid");
                }
            }
            ++vspCount;
        }
        if(vxsp.endsWith("\n"))
            map.put(vspCount, lastVsCount);
        return map;
    }
    /**
     * Converts Vxsp errors to Vxs errors: maps lines and adds the contents
     * of a respective Vxsp line, along with a column indicator.
     * 
     * @param vc compilation
     * @param errors errors to convert
     */
    private static void convertToVxs(VxsCompilation vc, CompilerException errors)
            throws CompilerException {
        Map<Integer, Integer> lineMap = getLineMap(vc.VXSP2);
        LineIndex li = new LineIndex(vc.VXSP2);
        for(CompilerException.Report r : errors.getReports())
            if(r.pos.line != -1) {
//if(r.pos == null)
//    r = r;
//if(lineMap.get(r.pos.line) == null)
//    lineMap = lineMap;
                int vsLineNum = lineMap.get(r.pos.line);
                String vsLine;
                try {
                    String vs = CompilerUtils.readFile(new File(vc.PREPROCESSOR.VXS_FILE_NAME));
                    LineIndex liVs = new LineIndex(vs);
                    vsLine = liVs.selectLine(vs, vsLineNum - 1);
                } catch (IOException e) {
                    throw new CompilerException(null, CompilerException.Code.IO,
                            "i/o error re--reading parsed file " +
                                vc.PREPROCESSOR.VXS_FILE_NAME + ": " + e.toString());
                }
                String vspLine = li.selectLine(vc.VXSP2, r.pos.line - 1);
                int pos = vspLine.lastIndexOf(VxsPreprocessor.LINE_COMMENT_STR);
                if(pos == -1)
                    // empty line
                    pos = 0;
                if(StringAnalyze.removeTrailingSpace(vsLine).equals(
                        StringAnalyze.removeTrailingSpace(vspLine.substring(0, pos))))
                    r.pos = new StreamPos(vc.PREPROCESSOR.VXS_FILE_NAME, vsLineNum, r.pos.column);
                else {
                    StringBuilder line = new StringBuilder();
                    line.append(vspLine.substring(0, r.pos.column - 1) +
                            CompilerException.LINE_POSITION_MARK +
                            vspLine.substring(r.pos.column - 1, pos).trim());
        //            for(int i = 0; i < r.pos.column - 1; ++i)
        //                line.append(" ");
        //            line.append("^");
                    r.pos = new StreamPos(vc.PREPROCESSOR.VXS_FILE_NAME, vsLineNum, 0);
                    r.message = r.message + ": «" + line + "»";
                }
        }
    }
    /**
     * Parses a single Vsp file.
     * 
     * @param vc compilation
     */
    public static void parseVxsp(VxsCompilation vc) throws CompilerException {
        CompilerException errors = new CompilerException();
        try {
            ErrorListener el = new ErrorListener(vc.FILE_NAME_VXS);
            try {
                VxspParser p = new VxspParser(new CommonTokenStream(
                        new VxspLexer(new ANTLRInputStream(vc.VXSP2))));
                p.removeErrorListeners();
                p.addErrorListener(el);
                vc.AS = p.compilationUnit(vc.FILE_NAME_VXS).result;
                el.errors.addAllReports(p.errors);
                if(!el.errors.reportsExist()) {
                    try {
                        vc.AS.completeParse();
                    } catch(CompilerException e) {
                        el.errors.addAllReports(e);
                    }
                }
            } catch(RecognitionException e) {
                throw new RuntimeException("unexpected: " + e.toString());
            }
            if(el.reportsExist())
                throw el.errors;
            else {
                ParserUtils.semanticCheck(vc.AS);
            }
        } catch(CompilerException e) {
            errors.addAllReports(e);
        }
        if(errors.reportsExist()) {
            convertToVxs(vc, errors);
            throw errors;
        }
    }
    /**
     * Serializes an agent system.
     * 
     * @param as agent system
     * @param fileName name of the file to write to
     * @param outputFormat output format, MCMAS or XML
     * @param outBuffer if not null, the contents of the saved file is copied to this
     * buffer; typically true only for compiler tests
     */
    private static void generate(AgentSystem as, String fileName, 
            FileType outputFormat, StringBuilder outBuffer)
            throws CompilerException {
        try {
            AbstractBackend backend;
            switch(outputFormat) {
                case MCMAS:
                    backend = new MCMASBackend(as, fileName);
                    break;
                    
                case XML:
                    backend = new XMLBackend(as, fileName);
                    break;
            }
            if(outBuffer != null)
                outBuffer.append(CompilerUtils.readFile(new File(fileName)));
        } catch(IOException e) {
            throw new CompilerException(new StreamPos(fileName, -1, 0),
                    CompilerException.Code.IO,
                    "could not write: " + e.getMessage());
        }
    }
    /**
     * <p>Compiles a <code>.vxs</code> file.</p>
     * 
     * <p>Calls <code>parseVxs()</code>, then <code>parseVxsp()</code>.</p>
     * 
     * @param localLibrary library files, added beside the global library
     * @param fileName name of the file to read
     * @param defines constant defines
     * @param forceSyncToGuards perform a translation from synchronisation labels
     * to guards, even if the output format does not require it
     * @param saveOut if to save the contents of the output file into
     * <code>VxsCompilation</code>; true only for regression tests
     * @param forceOutFilename  if not null, forces the output file name;
     * has a priority over <code>options.outputFormat</code>
     */
    public static VxsCompilation compileVxs(List<String> localLibrary,
            String fileName, CliConstants defines, VxsOptions.Translation options,
            boolean saveOut, String forceOutFilename) throws CompilerException {
        VxsLibrary library = new VxsLibrary(new PWD(),
                pl.gliwice.iitis.hedgeelleth.cli.Compiler.getCompilerLibraryDirectory(
                        VxsCLI.class, System.getProperty("user.dir")),
                true, localLibrary);
        VxsCompilation vc = PreprocessorAPI.parseVxs(library, fileName, null,
                defines, true, false);
        parseVxsp(vc);
        String outFileName;
        if(forceOutFilename != null)
            outFileName = forceOutFilename;
        else {
            String extension;
            switch(options.outputFormat) {
                case XML:
                    extension = XML_FILE_NAME_EXTENSION;
                    break;

                case MCMAS:
                    extension = MCMAS_FILE_NAME_EXTENSION;
                    break;

                case PRISM:
                    extension = PRISM_FILE_NAME_EXTENSION;
                    break;

                default:
                    throw new RuntimeException("unexpected format: " + options.outputFormat.toString());
            }
            outFileName = CompilerUtils.replaceExtension(fileName,
                extension);
        }
        FileType ft = getFileType(outFileName);
if(ft == null)
    ft = ft;
        switch(ft) {
            case MCMAS:
                break;

            case XML:
                break;

            default:
                throw new RuntimeException("can not compile to " + ft.toString());
        }
        try {
            VarUsage usage = new VarUsage(vc.AS);
            if(options.syncToGuards) {
    //            if(!vc.AS.synchronous)
    //                throw new CompilerException(new StreamPos(fileName, -1, 0),
    //                        CompilerException.Code.INVALID,
    //                        "can not convert sync to guards if system is not synchronous");
                SyncToGuards sc = new SyncToGuards(vc.AS);
                sc.complete();
                // check new protocol statements
                ParserUtils.semanticCheck(vc.AS);
            }
            if(options.conversion.integerActionToBoolean ||
                    options.conversion.integerFunctionToBoolean ||
                    options.conversion.integerToBoolean) {
                IntegerToBoolean i2b = new IntegerToBoolean(vc.AS, options.conversion);
            }
            ParserUtils.nameShadowCheck(vc.AS);
            if(saveOut)
                vc.OUT = new StringBuilder();
            generate(vc.AS, outFileName, options.outputFormat, vc.OUT);
        } catch(CompilerException e) {
            convertToVxs(vc, e);
            throw e;
        }
        return vc;
    }
    /**
     * CLI.
     * 
     * @param args CLI arguments
     */
    public static void main(String[] args) {
        String[] args_ = {
            //"-oi", "-Z", "/home/art/projects/svn/HedgeellethVxs/regression/dcN.vxs",
            //"-p", "/home/art/modelling/mc/muddy.vs",
            //"-p", "/home/art/modelling/mc/dc.vs",
            //"-p", "/home/art/modelling/nn/spiking.nm.pp",
            //"-p", "/home/art/modelling/prism/red/red.nm.pp",
            //"-p", "/home/art/modelling/ngreen/ngn/simple2.nm.pp",
            // "-p", "/home/art/modelling/prism/red/mm1_service.nm.pp"
            //"regression/FTC.vxs"
            //"-D", "HURST=60,LAMBDA=50",
            //    "-p", "/home/art/modelling/prism/red/IPP3.verics", "/home/art/modelling/prism/red/ipp_tcp.nm.pp"
            //"/home/art/modelling/prism/red/mm1_service.nm.pp"
            //"-p", "/home/art/modelling/mc/dcN.vxs"
            //"-p", "/home/art/projects/svn/HedgeellethVxs/regression/FTCp.vxs"
            "regression/a.vxs",
        };
        // args = args_;
        try {
            VxsOptions options = parseOptions(args);
            if(!options.help && !options.showLogicUnicode) {
                CompilerException errors = new CompilerException();
                List<String> library = new LinkedList<>();
                List<String> model = new LinkedList<>();
                for(String fileName : options.files) {
                    if(fileName.endsWith(VericsFrontend.FILE_NAME_EXTENSION))
                        library.add(fileName);
                    else
                        model.add(fileName);
                }
                if(forceOutFilename != null && model.size() > 1) {
                    errors.addReport(new CompilerException.Report(
                           null, CompilerException.Code.IO,
                           "can not force output file name if more than a single model file is present"));
                    throw errors;
                }
                if(model.isEmpty())
                    errors.addReport(new CompilerException.Report(
                           null, CompilerException.Code.IO,
                           "no input files"));
                else {
                    for(String fileName : model) {
                        PWD d = new PWD();
                        fileName = d.getRelativePath(fileName);
                        FileType ft = getFileType(fileName);
                        if(ft == null)
                            errors.addReport(new CompilerException.Report(
                                   new StreamPos(fileName), CompilerException.Code.UNKNOWN,
                                    "unknown file type"));
                        else {
                            switch(ft) {
                                case VS:
                                    errors.addReport(new CompilerException.Report(
                                           new StreamPos(fileName), CompilerException.Code.UNKNOWN,
                                            "obsolete file type"));
                                    break;
                                    
                                case VXS:
                                case PRISM_P:
                                case EXTERNAL_P:
                                    try {
                                        if(options.preprocessOnly)
                                            PreprocessorAPI.preprocess(true, library, fileName, null,
                                                    defines, forceOutFilename, null);
                                        else {
                                            switch(ft) {
                                                case VXS:
                                                    compileVxs(library, fileName, defines, options.translation,
                                                            false, forceOutFilename);
                                                    break;

                                                default:
                                                    errors.addReport(new CompilerException.Report(
                                                           new StreamPos(fileName), CompilerException.Code.NOT_IMPLEMENTED,
                                                           "file type " + ft.toString() + " can not be compiled"));
                                            }
                                        }
                                    } catch(CompilerException e) {
                                        errors.addAllReports(e);
                                    }
                                    break;

                                default:
                                    errors.addReport(new CompilerException.Report(
                                           new StreamPos(fileName), CompilerException.Code.NOT_IMPLEMENTED,
                                           "file type " + ft.toString() + " not supported as input"));
                            }
                        }
                    }
                }
                if(errors.reportsExist())
                    throw errors;
            }
        } catch(CompilerException e) {
            System.out.println(e.getMessage(MESSAGE_PREFIX));
            System.exit(1);
        }
    }
}
