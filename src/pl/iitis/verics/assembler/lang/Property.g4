/* THIS FILE IS MACHINE GENERATED */

grammar Property;

/**
 * A single property.
 */
property[AgentSystem as]
:
'property' (
		 propertyLTLStatement[$as] { $as.addProperty($propertyLTLStatement.result); }
	|	 propertyLTLKStatement[$as] { $as.addProperty($propertyLTLKStatement.result); }
	|	 propertyLTLKDStatement[$as] { $as.addProperty($propertyLTLKDStatement.result); }
	|	 propertyACTLStatement[$as] { $as.addProperty($propertyACTLStatement.result); }
	|	 propertyACTLKStatement[$as] { $as.addProperty($propertyACTLKStatement.result); }
	|	 propertyACTLKDStatement[$as] { $as.addProperty($propertyACTLKDStatement.result); }
	|	 propertyACTLAStatement[$as] { $as.addProperty($propertyACTLAStatement.result); }
	|	 propertyACTLAKStatement[$as] { $as.addProperty($propertyACTLAKStatement.result); }
	|	 propertyACTLAKDStatement[$as] { $as.addProperty($propertyACTLAKDStatement.result); }
	|	 propertyELTLStatement[$as] { $as.addProperty($propertyELTLStatement.result); }
	|	 propertyELTLKStatement[$as] { $as.addProperty($propertyELTLKStatement.result); }
	|	 propertyELTLKDStatement[$as] { $as.addProperty($propertyELTLKDStatement.result); }
	|	 propertyECTLStatement[$as] { $as.addProperty($propertyECTLStatement.result); }
	|	 propertyECTLKStatement[$as] { $as.addProperty($propertyECTLKStatement.result); }
	|	 propertyECTLKDStatement[$as] { $as.addProperty($propertyECTLKDStatement.result); }
	|	 propertyECTLAStatement[$as] { $as.addProperty($propertyECTLAStatement.result); }
	|	 propertyECTLAKStatement[$as] { $as.addProperty($propertyECTLAKStatement.result); }
	|	 propertyECTLAKDStatement[$as] { $as.addProperty($propertyECTLAKDStatement.result); }
	|	 propertyCTLAStatement[$as] { $as.addProperty($propertyCTLAStatement.result); }
	|	 propertyCTLAKStatement[$as] { $as.addProperty($propertyCTLAKStatement.result); }
	|	 propertyCTLAKDStatement[$as] { $as.addProperty($propertyCTLAKDStatement.result); }
) ';'
;

/*
 * Grammar of LTL.
 */
propertyLTLStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'LTL' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.LTL, $tName.text);
} expr = propertyLTL[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyLTL[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	sub = propertyLTL_Xi[$as] {
		$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
;
propertyLTL_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyLTL_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyLTL_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyLTL_Xi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyLTL_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyLTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyLTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyLTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyLTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of LTLK.
 */
propertyLTLKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'LTLK' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.LTLK, $tName.text);
} expr = propertyLTLK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyLTLK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
;
propertyLTLK_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyLTLK_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyLTLK_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyLTLK_Xi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyLTLK_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyLTLK_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyLTLK_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyLTLK_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyLTLK_Xi_sub_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyLTLK_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyLTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of LTLKD.
 */
propertyLTLKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'LTLKD' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.LTLKD, $tName.text);
} expr = propertyLTLKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyLTLKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
;
propertyLTLKD_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyLTLKD_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyLTLKD_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyLTLKD_Xi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyLTLKD_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Oc' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Kh' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyLTLKD_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyLTLKD_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyLTLKD_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyLTLKD_Xi_sub_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyLTLKD_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyLTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Oc' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Kh' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyLTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ACTL.
 */
propertyACTLStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ACTL' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ACTL, $tName.text);
} expr = propertyACTL[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyACTL[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	left = propertyACTL_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTL_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyACTL_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	'(' subParenthesized = propertyACTL[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyACTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	( 'A' { $pos = getStreamPos(); } 'X' { $pos2 = getStreamPos(); } )  sub = propertyACTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.X, $sub.result));
	}
|	( 'A' { $pos = getStreamPos(); } 'F' { $pos2 = getStreamPos(); } )  sub = propertyACTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.F, $sub.result));
	}
|	( 'A' { $pos = getStreamPos(); } 'G' { $pos2 = getStreamPos(); } )  sub = propertyACTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.G, $sub.result));
	}
|	'A' { $pos = getStreamPos(); } '(' left = propertyACTL[$as] 'U' { $pos2 = getStreamPos(); } right = propertyACTL[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.U, $left.result, $right.result));
	}
|	'A' { $pos = getStreamPos(); } '(' left = propertyACTL[$as] 'R' { $pos2 = getStreamPos(); } right = propertyACTL[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.R, $left.result, $right.result));
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ACTLK.
 */
propertyACTLKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ACTLK' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ACTLK, $tName.text);
} expr = propertyACTLK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyACTLK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	left = propertyACTLK_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLK_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyACTLK_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	'(' subParenthesized = propertyACTLK[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyACTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	( 'A' { $pos = getStreamPos(); } 'X' { $pos2 = getStreamPos(); } )  sub = propertyACTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.X, $sub.result));
	}
|	( 'A' { $pos = getStreamPos(); } 'F' { $pos2 = getStreamPos(); } )  sub = propertyACTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.F, $sub.result));
	}
|	( 'A' { $pos = getStreamPos(); } 'G' { $pos2 = getStreamPos(); } )  sub = propertyACTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.G, $sub.result));
	}
|	'A' { $pos = getStreamPos(); } '(' left = propertyACTLK[$as] 'U' { $pos2 = getStreamPos(); } right = propertyACTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.U, $left.result, $right.result));
	}
|	'A' { $pos = getStreamPos(); } '(' left = propertyACTLK[$as] 'R' { $pos2 = getStreamPos(); } right = propertyACTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.R, $left.result, $right.result));
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ACTLKD.
 */
propertyACTLKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ACTLKD' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ACTLKD, $tName.text);
} expr = propertyACTLKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyACTLKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	left = propertyACTLKD_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLKD_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyACTLKD_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	'(' subParenthesized = propertyACTLKD[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyACTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	( 'A' { $pos = getStreamPos(); } 'X' { $pos2 = getStreamPos(); } )  sub = propertyACTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.X, $sub.result));
	}
|	( 'A' { $pos = getStreamPos(); } 'F' { $pos2 = getStreamPos(); } )  sub = propertyACTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.F, $sub.result));
	}
|	( 'A' { $pos = getStreamPos(); } 'G' { $pos2 = getStreamPos(); } )  sub = propertyACTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.G, $sub.result));
	}
|	'A' { $pos = getStreamPos(); } '(' left = propertyACTLKD[$as] 'U' { $pos2 = getStreamPos(); } right = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.U, $left.result, $right.result));
	}
|	'A' { $pos = getStreamPos(); } '(' left = propertyACTLKD[$as] 'R' { $pos2 = getStreamPos(); } right = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.R, $left.result, $right.result));
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Oc' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Kh' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ACTLA.
 */
propertyACTLAStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ACTL' { $pos = getStreamPos(); } '*' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ACTLA, $tName.text);
} expr = propertyACTLA[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyACTLA[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyACTLA_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLA_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyACTLA_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyACTLA[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyACTLA[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'A' { $pos = getStreamPos(); } sub = propertyACTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyACTLA_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyACTLA_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyACTLA_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyACTLA_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyACTLA_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLA_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyACTLA_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyACTLA_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyACTLA_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyACTLA_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyACTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyACTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyACTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of ACTLAK.
 */
propertyACTLAKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ACTL' { $pos = getStreamPos(); } '*' 'K' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ACTLAK, $tName.text);
} expr = propertyACTLAK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyACTLAK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyACTLAK_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLAK_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyACTLAK_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyACTLAK[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyACTLAK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'A' { $pos = getStreamPos(); } sub = propertyACTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyACTLAK_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyACTLAK_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyACTLAK_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyACTLAK_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyACTLAK[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyACTLAK_Psi[$as] {
		$result = $subPsi.result;
	}
;
propertyACTLAK_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyACTLAK_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLAK_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyACTLAK_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyACTLAK_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyACTLAK_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyACTLAK_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyACTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyACTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyACTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of ACTLAKD.
 */
propertyACTLAKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ACTL' { $pos = getStreamPos(); } '*' 'KD' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ACTLAKD, $tName.text);
} expr = propertyACTLAKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyACTLAKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyACTLAKD_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLAKD_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyACTLAKD_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyACTLAKD[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyACTLAKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'A' { $pos = getStreamPos(); } sub = propertyACTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Oc' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Kh' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyACTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyACTLAKD_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyACTLAKD_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyACTLAKD_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyACTLAKD_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyACTLAKD[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyACTLAKD_Psi[$as] {
		$result = $subPsi.result;
	}
;
propertyACTLAKD_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyACTLAKD_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyACTLAKD_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyACTLAKD_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyACTLAKD_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyACTLAKD_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyACTLAKD_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyACTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyACTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyACTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of ELTL.
 */
propertyELTLStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ELTL' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ELTL, $tName.text);
} expr = propertyELTL[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyELTL[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	sub = propertyELTL_Xi[$as] {
		$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
;
propertyELTL_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyELTL_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyELTL_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyELTL_Xi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyELTL_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyELTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyELTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyELTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyELTL_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ELTLK.
 */
propertyELTLKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ELTLK' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ELTLK, $tName.text);
} expr = propertyELTLK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyELTLK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
;
propertyELTLK_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyELTLK_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyELTLK_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyELTLK_Xi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyELTLK_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyELTLK_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyELTLK_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyELTLK_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyELTLK_Xi_sub_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyELTLK_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyELTLK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ELTLKD.
 */
propertyELTLKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ELTLKD' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ELTLKD, $tName.text);
} expr = propertyELTLKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyELTLKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($sub.result.getStreamPos(), $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
;
propertyELTLKD_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyELTLKD_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyELTLKD_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyELTLKD_Xi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyELTLKD_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyELTLKD_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyELTLKD_Xi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  | 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } right = propertyELTLKD_Xi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyELTLKD_Xi_sub_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyELTLKD_Xi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'X' { $pos = getStreamPos(); } sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyELTLKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyELTLKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ECTL.
 */
propertyECTLStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ECTL' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ECTL, $tName.text);
} expr = propertyECTL[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyECTL[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	left = propertyECTL_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTL_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyECTL_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	'(' subParenthesized = propertyECTL[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyECTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	( 'E' { $pos = getStreamPos(); } 'X' { $pos2 = getStreamPos(); } )  sub = propertyECTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.X, $sub.result));
	}
|	( 'E' { $pos = getStreamPos(); } 'F' { $pos2 = getStreamPos(); } )  sub = propertyECTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.F, $sub.result));
	}
|	( 'E' { $pos = getStreamPos(); } 'G' { $pos2 = getStreamPos(); } )  sub = propertyECTL[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.G, $sub.result));
	}
|	'E' { $pos = getStreamPos(); } '(' left = propertyECTL[$as] 'U' { $pos2 = getStreamPos(); } right = propertyECTL[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.U, $left.result, $right.result));
	}
|	'E' { $pos = getStreamPos(); } '(' left = propertyECTL[$as] 'R' { $pos2 = getStreamPos(); } right = propertyECTL[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.R, $left.result, $right.result));
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ECTLK.
 */
propertyECTLKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ECTLK' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ECTLK, $tName.text);
} expr = propertyECTLK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyECTLK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	left = propertyECTLK_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLK_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyECTLK_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	'(' subParenthesized = propertyECTLK[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyECTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	( 'E' { $pos = getStreamPos(); } 'X' { $pos2 = getStreamPos(); } )  sub = propertyECTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.X, $sub.result));
	}
|	( 'E' { $pos = getStreamPos(); } 'F' { $pos2 = getStreamPos(); } )  sub = propertyECTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.F, $sub.result));
	}
|	( 'E' { $pos = getStreamPos(); } 'G' { $pos2 = getStreamPos(); } )  sub = propertyECTLK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.G, $sub.result));
	}
|	'E' { $pos = getStreamPos(); } '(' left = propertyECTLK[$as] 'U' { $pos2 = getStreamPos(); } right = propertyECTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.U, $left.result, $right.result));
	}
|	'E' { $pos = getStreamPos(); } '(' left = propertyECTLK[$as] 'R' { $pos2 = getStreamPos(); } right = propertyECTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.R, $left.result, $right.result));
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLK[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ECTLKD.
 */
propertyECTLKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ECTLKD' { $pos = getStreamPos(); } tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ECTLKD, $tName.text);
} expr = propertyECTLKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyECTLKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	left = propertyECTLKD_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLKD_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyECTLKD_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op,
	StreamPos pos2

]
:
	'(' subParenthesized = propertyECTLKD[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyECTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	( 'E' { $pos = getStreamPos(); } 'X' { $pos2 = getStreamPos(); } )  sub = propertyECTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.X, $sub.result));
	}
|	( 'E' { $pos = getStreamPos(); } 'F' { $pos2 = getStreamPos(); } )  sub = propertyECTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.F, $sub.result));
	}
|	( 'E' { $pos = getStreamPos(); } 'G' { $pos2 = getStreamPos(); } )  sub = propertyECTLKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new UnaryExpression($pos2, $as.SCOPE, UnaryExpression.Op.G, $sub.result));
	}
|	'E' { $pos = getStreamPos(); } '(' left = propertyECTLKD[$as] 'U' { $pos2 = getStreamPos(); } right = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.U, $left.result, $right.result));
	}
|	'E' { $pos = getStreamPos(); } '(' left = propertyECTLKD[$as] 'R' { $pos2 = getStreamPos(); } right = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, new BinaryExpression($pos2, $as.SCOPE, BinaryExpression.Op.R, $left.result, $right.result));
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLKD[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
/*
 * Grammar of ECTLA.
 */
propertyECTLAStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ECTL' { $pos = getStreamPos(); } '*' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ECTLA, $tName.text);
} expr = propertyECTLA[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyECTLA[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyECTLA_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLA_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyECTLA_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyECTLA[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyECTLA[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'E' { $pos = getStreamPos(); } sub = propertyECTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyECTLA_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyECTLA_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyECTLA_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyECTLA_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyECTLA_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLA_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyECTLA_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyECTLA_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyECTLA_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyECTLA_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyECTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyECTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyECTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of ECTLAK.
 */
propertyECTLAKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ECTL' { $pos = getStreamPos(); } '*' 'K' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ECTLAK, $tName.text);
} expr = propertyECTLAK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyECTLAK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyECTLAK_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLAK_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyECTLAK_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyECTLAK[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyECTLAK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'E' { $pos = getStreamPos(); } sub = propertyECTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyECTLAK_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyECTLAK_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyECTLAK_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyECTLAK_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyECTLAK[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyECTLAK_Psi[$as] {
		$result = $subPsi.result;
	}
;
propertyECTLAK_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyECTLAK_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLAK_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyECTLAK_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyECTLAK_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyECTLAK_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyECTLAK_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyECTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyECTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyECTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of ECTLAKD.
 */
propertyECTLAKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'ECTL' { $pos = getStreamPos(); } '*' 'KD' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.ECTLAKD, $tName.text);
} expr = propertyECTLAKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyECTLAKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyECTLAKD_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLAKD_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyECTLAKD_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyECTLAKD[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyECTLAKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'E' { $pos = getStreamPos(); } sub = propertyECTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyECTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyECTLAKD_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyECTLAKD_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyECTLAKD_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyECTLAKD_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyECTLAKD[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyECTLAKD_Psi[$as] {
		$result = $subPsi.result;
	}
;
propertyECTLAKD_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyECTLAKD_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyECTLAKD_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyECTLAKD_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyECTLAKD_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyECTLAKD_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyECTLAKD_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyECTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyECTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyECTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of CTLA.
 */
propertyCTLAStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'CTL' { $pos = getStreamPos(); } '*' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.CTLA, $tName.text);
} expr = propertyCTLA[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyCTLA[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyCTLA_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyCTLA_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyCTLA_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyCTLA[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyCTLA[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'E' { $pos = getStreamPos(); } sub = propertyCTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
|	'A' { $pos = getStreamPos(); } sub = propertyCTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyCTLA_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyCTLA_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyCTLA_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyCTLA_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyCTLA_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyCTLA_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyCTLA_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyCTLA_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyCTLA_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyCTLA_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyCTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyCTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyCTLA_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of CTLAK.
 */
propertyCTLAKStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'CTL' { $pos = getStreamPos(); } '*' 'K' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.CTLAK, $tName.text);
} expr = propertyCTLAK[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyCTLAK[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyCTLAK_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyCTLAK_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyCTLAK_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyCTLAK[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyCTLAK[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'E' { $pos = getStreamPos(); } sub = propertyCTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
|	'A' { $pos = getStreamPos(); } sub = propertyCTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAK_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyCTLAK_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyCTLAK_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyCTLAK_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyCTLAK_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyCTLAK[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyCTLAK_Psi[$as] {
		$result = $subPsi.result;
	}
;
propertyCTLAK_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyCTLAK_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyCTLAK_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyCTLAK_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyCTLAK_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyCTLAK_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyCTLAK_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyCTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyCTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyCTLAK_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
/*
 * Grammar of CTLAKD.
 */
propertyCTLAKDStatement[AgentSystem as] returns [ AsProperty result ]
locals [
	StreamPos pos,
	AsProperty out
]
:
'CTL' { $pos = getStreamPos(); } '*' 'KD' tName = Identifier ':='  {
	$out = new AsProperty($pos, AsProperty.Type.CTLAKD, $tName.text);
} expr = propertyCTLAKD[$as] {
	$out.EXPR = $expr.result;
	$result = $out;
}
;
propertyCTLAKD[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyCTLAKD_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyCTLAKD_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
;
propertyCTLAKD_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyCTLAKD[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'!' { $pos = getStreamPos(); } subPhi = propertyCTLAKD[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, $subPhi.result);
	}
|	'E' { $pos = getStreamPos(); } sub = propertyCTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.E, $sub.result);
	}
|	'A' { $pos = getStreamPos(); } sub = propertyCTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.A, $sub.result);
	}
|	'K' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Eg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Dg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Cg' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'K' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Kc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Eg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Eg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Dg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Dg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Cg' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Cg, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Oc' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'Kh' { $pos = getStreamPos(); } '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Oc' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Oc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	'!' { $pos = getStreamPos(); } 'Kh' '(' coalition = coalitionGroup[$as] ',' subSuper = propertyCTLAKD_Xi_super[$as] ')' {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.NEG_Khc, $subSuper.result);
				((UnaryExpression)$result).coalition = $coalition.result;
	}
|	expr = relationalExpression[$as, false] {
		$result = $expr.result;
	}
;
propertyCTLAKD_Xi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyCTLAKD_sub[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyCTLAKD_Psi_sub[$as] {
		$result = $subPsi.result;
	}
;
propertyCTLAKD_Xi_super[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	subPhi = propertyCTLAKD[$as] {
		$result = $subPhi.result;
	}
|	subPsi = propertyCTLAKD_Psi[$as] {
		$result = $subPsi.result;
	}
;
propertyCTLAKD_Psi[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	left = propertyCTLAKD_Psi_sub[$as] ( ( 
				'&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; }  | 
				'||' { $op = BinaryExpression.Op.CONDITIONAL_OR; }  | 
				'->' { $op = BinaryExpression.Op.IMPLICATION; }  | 
				'<->' { $op = BinaryExpression.Op.EQUIVALENCE; }  ) { $pos = getStreamPos(); } right = propertyCTLAKD_Psi_sub[$as] {
			    $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result); } )* {
		$result = $left.result;
	}
|	leftXi = propertyCTLAKD_Xi[$as] ( ( 
				'U' { $op = BinaryExpression.Op.U; }  | 
				'R' { $op = BinaryExpression.Op.R; }  ) { $pos = getStreamPos(); } rightXi = propertyCTLAKD_Xi[$as] {
			    $leftXi.result = new BinaryExpression($pos, $as.SCOPE, $op, $leftXi.result, $rightXi.result); } )* {
		$result = $leftXi.result;
	}
;
propertyCTLAKD_Psi_sub[AgentSystem as] returns [ AbstractExpression result ]
locals [
	StreamPos pos,
	BinaryExpression.Op op
]
:
	'(' subParenthesized = propertyCTLAKD_Psi[$as] ')' {
		$result = $subParenthesized.result;
	}
|	'X' { $pos = getStreamPos(); } sub = propertyCTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.X, $sub.result);
	}
|	'F' { $pos = getStreamPos(); } sub = propertyCTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.F, $sub.result);
	}
|	'G' { $pos = getStreamPos(); } sub = propertyCTLAKD_Xi[$as] {
		$result = new UnaryExpression($pos, $as.SCOPE, UnaryExpression.Op.G, $sub.result);
	}
;
