grammar Header;

@lexer::header {
    package pl.iitis.verics.assembler.parser;
}

@parser::header {
    package pl.iitis.verics.assembler.parser;

    import java.util.*;
    import java.io.*;

    import org.antlr.v4.runtime.Token;
    
    import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
    import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
    import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
    import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
    import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
    import pl.iitis.verics.agent.*;
    import pl.iitis.verics.agent.property.*;
    import pl.iitis.verics.assembler.ParserUtils;
}

@parser::members {
    /**
     * Name of current parsed stream.
     */
    String streamName;
    /**
     * Errors. After the parsing, the parser's own parse exceptions are
     * appended here.
     */
    public ParseException errors = new ParseException();
    /**
     * If the latest (top--most is more recent) constExpression declared a
     * new global constant, then here is its value. Otherwise null.
     */
    AsConstant newlyDeclaredConstant = null;

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }
    public String getStreamName() {
        return streamName;
    }
    public StreamPos getStreamPos(Token token) {
        return new StreamPos(streamName, token);
    }
    public StreamPos getStreamPos() {
        return new StreamPos(streamName,
                        getCurrentToken().getLine(), getCurrentToken().getCharPositionInLine() + 1);
    }
    public void add(ParseException e) {
        errors.addAllReports(e);
    }
}
