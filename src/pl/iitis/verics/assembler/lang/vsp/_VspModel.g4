/**
 * Parser of preprocessed vs files.
 *
 * Copyright (c) 2015 Artur Rataj.
 */
grammar VspModel;

/**
  * A model file.
  *
  * @param streamName name of the stream to parse
  * @return a compilation of TADDs
  */
compilationUnit[String streamName] returns [ AgentSystem result ]
locals [
    AgentSystem as = new AgentSystem(streamName);
]
:
{
    setStreamName(streamName);
}
clazz[$as]
globalConstants[$as]
modulesOrFormulas[$as]
properties[$as] {
    $result = $as;
}
;

/**
 * Model name and type.
 *
 * @param as the compiled agent system
 */
clazz[AgentSystem as]
locals [
    ParseException error = new ParseException()
]
:
    'class' tType = Identifier {
        try {
            $as.type = AgentSystem.Type.parseType($tType.text);
        } catch(ParseException e) {
            e.completePos(getStreamPos());
            add(e);
        }
    }
    ';'
;

/**
 * Global constants.
 *
 * @param an empty list, to be completed with global constants
 */
globalConstants[AgentSystem as]
:
( globalConstant[$as] )*
;

/**
 * A single global constant.
 *
 * @param an agent system
 */
globalConstant[AgentSystem as]
locals [
    StreamPos pos = null,
    boolean floating,
    String name,
    AsConstant constant = null
]
:
    'const' { $pos = getStreamPos(); }
    ( 'Z' { $floating = false; } | 'R' { $floating = true; } )
    tName=Identifier { $name = $tName.text; }
    ':=' c = constExpression[$as, false] {
        if(!$floating && $c.result != null && $c.result.isFloating())
            add(new ParseException($c.result.getStreamPos(),
                ParseException.Code.INVALID,
                "loss of precision: R to Z"));
        AsConstant d;
        if($floating && $c.result != null && !$c.result.isFloating())
           d = new AsConstant($pos, $name, new Literal($c.result.getDouble()));
        else
           d = new AsConstant($pos, $name, $c.result);
        $as.addConstant(d);
    }
';'
;
catch[RecognitionException e] {
    add(new ParseException(getStreamPos(), ParseException.Code.INVALID,
        "invalid constant declaration: " + e.getMessage()));
    throw e;
}

/**
 * A mix of modules and definitions.
 *
 * @param as the compiled agent system
 */
modulesOrFormulas[AgentSystem as] :
(
   module[$as]
   |
   formula[$as]
)*
;

/**
 * A single module.
 *
 * @param as the compiled agent system
 */
module[AgentSystem as]
locals [
    boolean forcePrivateVariables = false,
    String name,
    AsModule m = null,
    ParseException errors = new ParseException()
]
:
( 'agent' tName=Identifier { $forcePrivateVariables = true; $name = $tName.text; } |
          tName=Identifier {
              $name = $tName.text;
              if($tName.text.equals("environment")) {
                  $forcePrivateVariables = false; 
              } else {
                  $forcePrivateVariables = true;
                  add(new ParseException(new StreamPos(null, $tName),
                      ParseException.Code.INVALID,
                      "expected either `environment' or `agent'"));
              }
          } ) {
      $m = new AsModule(getStreamPos(), $forcePrivateVariables, $name);
      $as.addModule($m);
} '{'
    ( variable = stateVarDeclaration[$as] {
        if($variable.result != null)
            $m.addVariable($variable.result);
    } )*
    ( stat = statement[$as] {
        if($stat.result != null)
            $m.STATEMENTS.add($stat.result);
    } )*
'}' ';'
;

/**
 * A declaration of a state variable.
 */
stateVarDeclaration[AgentSystem as] returns [AsVariable result]
locals [
   StreamPos pos = null,
   String name,
   List<AsConstant> constants = null,
]
:
'Z' ( '{' range = constRangeList[$as] '}' )?
         tName=Identifier { $pos = getStreamPos(); $name = $tName.text; } ':='
                init = initValueSpaces[$as]
         ';' {
    if($init.result != null) {
        AsVariable v = new AsVariable($pos, $as.getParsedModule(), $name, $init.result,
            new AsRange($range.result));
        $result = v;
    }
}
;

/**
 * List of constants, comma--separated.
 */
constRangeList[AgentSystem as] returns [List<AsConstant> result]
locals [
    List<AsConstant> out = new ArrayList<AsConstant>()
] 
:
cr = constRange[$as] {
     if($cr.result != null)
        $out.addAll($cr.result);
} (
     ',' cr = constRange[$as] {
        if($cr.result != null)
               $out.addAll($cr.result);
     }
)*
( ',' )? {
     $result = $out;
}
;

constRange[AgentSystem as] returns [ List<AsConstant> result ]
:
left = constBoundary[$as] ( '...' right = constBoundary[$as] )? {
    if($left.result != null) {
        try {
            if($right.ctx == null || $right.result == null)
                $result = ParserUtils.iterate($left.result, $left.result);
            else
                $result = ParserUtils.iterate($left.result, $right.result);
        } catch(ParseException e) {
            e.completePos(getStreamPos());
            add(e);
            $result = null;
        }
    }
}
;

constBoundary[AgentSystem as] returns [ AsConstant result ]
:
left = constExpression[$as, true] ( ':=' right = constExpression[$as, false] )? { // !!!
    $result = $left.result;
}
;

initValueSpaces[AgentSystem as] returns [AsInitValue result]
:
out = initValueSpace[$as] ( ',' next = initValueSpace[$as] {
        if($out.result != null)
            $out.result.add($next.result);
    } )* ( ',' )? {
    $result = $out.result;
}
;

initValueSpace[AgentSystem as] returns [AsInitValue result]
locals [
    StreamPos pos,
    AsConstant space
]
:
{ $space = new AsConstant(getStreamPos(), null, new Literal(0)); }
(
    ce = constExpression[$as, false] ':' '(' { $pos = getStreamPos(); } set = initValueSet[$as] ')'
|
    '(' { $pos = getStreamPos(); } set = initValueSet[$as] ')'
|
    set = initValueSet[$as] {
        $pos = $set.result.get(0).getStreamPos();
    }
 ) {
    if($space.type.getPrimitive() != Type.PrimitiveOrVoid.INT)
        add(new ParseException($space.getStreamPos(),
            ParseException.Code.INVALID,
            "space number must be integer"));
    else {
        boolean floating = false;
        boolean booleaN = true;
        for(AsConstant c : $set.result) {
            if(c.isFloating())
                floating = true;
            if(!c.isBoolean())
                booleaN = false;
        }
        boolean mixed = false;
        for(AsConstant c : $set.result) {
            if(c.isBoolean() && !booleaN) {
                add(new ParseException(c.getStreamPos(),
                    ParseException.Code.INVALID,
                    "mixed integer and boolean constants"));
                mixed = true;
                break;
            }
        }
        if(!mixed) {
            if(floating)
                add(new ParseException($pos,
                    ParseException.Code.INVALID,
                    "state variables must be constant"));
            else {
                AsInitValue init =  new AsInitValue(floating, booleaN);
                Set<Integer> values = new HashSet<Integer>();
                init.add($space.getInteger(), $set.result);
                $result = init;
            }
        }
    }
}
;

initValueSet[AgentSystem as] returns [List<AsConstant> result]
locals [
    StreamPos pos,
    List<AsConstant> all = new ArrayList<AsConstant>()
]
:
s = initValueRange[$as] {
    if($s.result != null)
        $all.addAll($s.result);
} ( ',' s = initValueRange[$as] {
    if($s.result != null)
        $all.addAll($s.result);
} )*
( ',' )? {
    $result = $all;
}
;

initValueRange[AgentSystem as] returns [List<AsConstant> result]
:
low = constExpression[$as, false] ( '...' high = constExpression[$as, false] )? {
    if($low.result != null) {
         try {
            if($high.ctx == null || $high.result == null)
                $result = ParserUtils.iterate($low.result, $low.result);
            else
                $result = ParserUtils.iterate($low.result, $high.result);
        } catch(ParseException e) {
            e.completePos(getStreamPos());
            add(e);
            $result = null;
        }
    }
}
;

/**
  * A single statement.
  */
statement[AgentSystem as] returns [ AsStatement result ]
locals [
    StreamPos pos,
    AsLabel label
]
:
'[' { $pos = getStreamPos(); } ( tLabel = Identifier )?
    ( ':' syncGuard = expression[$as, false] )? ']' {
    if($tLabel == null)
        $label = null;
    else {
        String n = $tLabel.text;
        if(n.equals(AsLabel.SYNC_KEY_NONE)) {
            add(new ParseException(getStreamPos(),
                ParseException.Code.ILLEGAL,
                AsLabel.SYNC_KEY_NONE + " can not be used as a label name"));
            $label = null;
        } else
            $label = new AsLabel($pos, n);
    }
} guard = expression[$as, false] '->' 
(
    updateLeft = variableRef[$as] ':=' updateRight = expression[$as, false]
|
    'true'
)
';' {
    $result = new AsStatement($pos, $as.getParsedModule(),
        $label, $syncGuard.ctx != null ? $syncGuard.result : null, $guard.result,
        $updateLeft.ctx == null ? null : $updateLeft.result,
        $updateRight.ctx == null ? null : $updateRight.result);
}
;

/**
  * A reference to an already declared state variable.
  */
variableRef[AgentSystem as] returns [ AsVariable result ] :
name = qualifiedIdentifier {
    AsTyped v = as.lookupValue($name.result);
    if(v instanceof AsVariable)
        $result = (AsVariable)v;
    else if(v instanceof AsConstant)
        add(new ParseException(getStreamPos(),
            ParseException.Code.ILLEGAL,
            "can not update a constant"));
    else
        add(new ParseException(getStreamPos(),
            ParseException.Code.INVALID,
            "variable not found"));
}
;

/**
 * An expression which must be reducible to a constant at the parsing time.
 */
constExpression[AgentSystem as, boolean allowDeclaration] returns [ AsConstant result ]
locals [
    AsConstant c
]
:
expr = expression[$as, $allowDeclaration] {
    try {
        if(($c = ParserUtils.toConst($as, $expr.result, null)) == null)
            add(new ParseException($expr.result.getStreamPos(),
                ParseException.Code.INVALID,
                "constant expression expected"));
        $result = $c;
    } catch(ParseException e) {
        e.completePos($expr.result.getStreamPos());
        add(e);
    } 
}
;

/**
 * An expression.
 */
expression[AgentSystem as, boolean allowDeclaration] returns [AbstractExpression result] :
pass = booleanExpression[$as, $allowDeclaration] {
    $result = $pass.result;
}
;

/**
 * A boolean expression.
 *
 * @param scope scope of this expression
 */
booleanExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = relationalExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; } |
      '||' { $op = BinaryExpression.Op.CONDITIONAL_OR; } )
    right = relationalExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new AsBinaryExpression($as, $pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * A relational expression.
 *
 * @param scope scope of this expression
 */
relationalExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = addExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '==' { $op = BinaryExpression.Op.EQUAL; } | '<>' { $op = BinaryExpression.Op.INEQUAL; } |
      '<' { $op = BinaryExpression.Op.LESS; } | '>' { $op = BinaryExpression.Op.GREATER; } |
      '<=' { $op = BinaryExpression.Op.LESS_OR_EQUAL; } | '>=' { $op = BinaryExpression.Op.GREATER_OR_EQUAL; } )
    right = addExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new AsBinaryExpression($as, $pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * An addition/subtraction/negation expression.
 *
 * @param scope scope of this expression
 */
addExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = multExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '+' { $op = BinaryExpression.Op.PLUS; } | '-' { $op = BinaryExpression.Op.MINUS; } )
    right = multExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new AsBinaryExpression($as, $pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * A multiplication/division expression.
 *
 * @param scope scope of this expression
 */
multExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = unaryExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '*' { $op = BinaryExpression.Op.MULTIPLY; } | '/' { $op = BinaryExpression.Op.DIVIDE; } |
      '%' { $op = BinaryExpression.Op.MODULUS; } | '%+' { $op = BinaryExpression.Op.MODULUS_POS; } )
    right = unaryExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new AsBinaryExpression($as, $pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * An unary expression: arithmetic negation or boolean not.
 *
 * @param scope scope of this expression
 */
unaryExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    UnaryExpression.Op op,
    StreamPos pos
]
:
(
    (
         { $pos = getStreamPos(); }
         ( '-' { $op = UnaryExpression.Op.NEGATION; } | '!' { $op = UnaryExpression.Op.CONDITIONAL_NEGATION; } )
         sub = primaryExpression[$as, $allowDeclaration] {
             if($sub.result != null)
                 $sub.result = new AsUnaryExpression($as, $pos, $as.SCOPE, $op, $sub.result);
         }
    )
    |
         sub = primaryExpression[$as, $allowDeclaration]
) {
    $result = $sub.result;
}
;

/**
 * A primary expression.
 */
primaryExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
:
vf = valueOrFormula[$as, $allowDeclaration] {
    if($vf.result != null) {
        ValueOrFormula v = $vf.result;
        if(v.VALUE != null)
            $result = ParserUtils.newPrimary(as, v.VALUE);
        else {
            $result = v.FORMULA.EXPR;
            $as.register($result, v.FORMULA);
        }
    }
} |
'(' expr = expression[$as, $allowDeclaration] ')' {
    $result = $expr.result;
}
;

/**
 * A single formula.
 */
formula[AgentSystem as]
locals [
    StreamPos pos
]
:
'def' { $pos = getStreamPos(); } tName = Identifier ':=' expr = expression[$as, false] ';' {
    if($expr.result != null) {
        AsFormula f = new AsFormula($pos, $tName.text, $expr.result);
        $as.addFormula(f);
    }
}
;

/**
 * Properties.
 */
properties[AgentSystem as] :
( property[$as] {
  }
)*
;

/**
 * A variable or a constant.
 *
 * @param scope scope
 */
valueOrFormula[AgentSystem as, boolean allowDeclaration]
returns [ ValueOrFormula result ]
locals [
    StreamPos pos
] :
{ $pos = getStreamPos(); }
(
    t = number {
        $result = new ValueOrFormula((AsConstant)$t.result);
    }
    |
    name = qualifiedIdentifier {
        // local variables and (global) constants have priority before formulas
        AsTyped typed = $as.lookupValue($name.result);
        if(typed == null) {
            // formulas have priority before new constants
            AsFormula f = as.lookupFormula($name.result);
            if(f == null) {
                boolean qualified = $name.result.indexOf('.') != -1;
                if(allowDeclaration && !qualified) {
                    double max = 0;
                    for(AsConstant c : $as.constantList)
                        if(!c.isBoolean()) {
                            double v = c.getMaxPrecisionFloatingPoint();
                            if(max < v)
                                max = v;
                        }
                    AsConstant newConstant = new AsConstant(getStreamPos(), $name.result,
                        new Literal((int)(max + 1)));
                    $as.addConstant(newConstant);
                    $result = new ValueOrFormula(newConstant);
                } else {
                    if(!qualified) {
                        // assume a global constant; to be assigned later
                        // during the semantic check
                        $result = new ValueOrFormula(new AsConstant(getStreamPos(), $name.result));
                    } else {
                        // assume a qualified variable; to be assigned later
                        // during the semantic check
                        $result = new ValueOrFormula(new AsVariable(getStreamPos(), null,
                            $name.result, null, null));
                    }
                }
             } else
                $result = new ValueOrFormula(f);
        } else
            $result = new ValueOrFormula(typed);
    }
)
;

coalitionGroup[AgentSystem as] returns [ AsCoalition result ] :
tName = Identifier {
    AsModule m = as.lookupModule($tName.text);
    if(m == null)
        add(new ParseException(getStreamPos(),
                ParseException.Code.UNKNOWN,
                "unknown module: " + $tName.text));
    else
        $result = new AsCoalition(m);
}
;

/* keywords */
AGENT: 'agent';
CLASS: 'class';
CONST: 'const';
DEF: 'def';
/* ENVIRONMENT: 'environment'; */
FALSE: 'false';
MODULE: 'module';
PROPERTY: 'property';
R: 'R';
TRUE: 'true';
Z: 'Z';

/* OPERATORS */
PLUS: '+';
MINUS: '-';
MULT: '*';
DIV: '/';
PERCENT: '%';
EQUAL: '==';
LESS: '<';
GREATER: '>';
LESS_EQ: '<=';
GREATER_EQ: '>=';
INEQUAL: '!=';
RARROW: '->';
DARROW: '<->';
ASSIGN: ':=';
LAND: '&&';
LOR: '||';
LNOT: '!';

/* SEPARATORS */
COMMA: ',';
COLON: ':';
SEMICOLON: ';';
DOT: '.';
LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
LSQUARE: '[';
RSQUARE: ']';
ELLIPSIS: '...';

/* GRAMMAR IDENTIFIERS */
LTL: 'LTL';
LTLK: 'LTLK';
LTLKD: 'LTLKD';
ELTL: 'ELTL';
ELTLK: 'ELTLK';
ELTLKD: 'ELTLKD';
ECTL: 'ECTL';
ECTLK: 'ECTLK';
ECTLKD: 'ECTLKD';
ACTL: 'ACTL';
ACTLK: 'ACTLK';
ACTLKD: 'ACTLKD';
CTL: 'CTL';
KD: 'KD';

/* PROPERTY OPERATORS */
A: 'A';
E: 'E';
F: 'F';
G: 'G';
U: 'U';
X: 'X';
AF: 'AF';
EF: 'EF';
AG: 'AG';
EG: 'EG';
AX: 'AX';
EX: 'EX';
Eg: 'Eg';
Dg: 'Dg';
Cg: 'Cg';
Kh: 'Kh';
K: 'K';
Oc: 'Oc';
