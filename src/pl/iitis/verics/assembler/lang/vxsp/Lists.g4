grammar Lists;

//
// Various lists. Requires Standard.g4.
//

/**
 * List of identifiers, comma--separated.
 */
identifierList returns [List<String> result]
locals [
    List<String> out = new ArrayList<String>()
] 
:
((cr = Identifier { $out.add($cr.text); } | 'environment' { $out.add("environment"); })
(
     ',' (cr = Identifier { $out.add($cr.text); } | 'environment' { $out.add("environment"); })
)*
( ',' )?)? {
     $result = $out;
}
;

/**
 * A qualified identifier, that takes into account the keyword "environment".
 */
qualifiedIdentifierEnv returns [ String result ]
locals [
    StringBuilder out = new StringBuilder()
] :
(
  tName = Identifier { $out.append($tName.text); }
|
  'environment' { $out.append("environment"); }
)
( '.' tName = Identifier { $out.append('.' + $tName.text); } )? {
     $result = $out.toString();
}
;

/**
 * List of expressions, comma--separated.
 */
expressionList[AgentSystem as, boolean allowDeclaration]
returns [List<AbstractExpression> result]
locals [
    List<AbstractExpression> out = new ArrayList<>()
] 
:
(cr = expression[$as, $allowDeclaration] { $out.add($cr.result); }
(
     ',' cr = expression[$as, $allowDeclaration] { $out.add($cr.result); } 
)*
( ',' )?)? {
     $result = $out;
}
;
