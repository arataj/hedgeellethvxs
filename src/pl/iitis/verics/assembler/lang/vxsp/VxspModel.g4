/**
 * Parser of preprocessed vs files.
 *
 * Copyright (c) 2015 Artur Rataj.
 */
grammar VxspModel;

/**
  * A model file.
  *
  * @param streamName name of the stream to parse
  * @return a compilation of TADDs
  */
compilationUnit[String streamName] returns [ AgentSystem result ]
locals [
    AgentSystem as
]
:
{
    setStreamName(streamName);
    $as = new AgentSystem(streamName);
}
clazz[$as]
globalConstants[$as]
modulesOrFormulas[$as]
initFilter[$as]
properties[$as] {
    $result = $as;
}
;

/**
 * Model name and type.
 *
 * @param as the compiled agent system
 */
clazz[AgentSystem as]
locals [
    ParseException error = new ParseException()
]
:
    'class' tType = Identifier {
        try {
            AgentSystem.Type.parseType($tType.text, $as);
        } catch(ParseException e) {
            e.completePos(getStreamPos());
            add(e);
        }
    } ( 'sync' { $as.synchronous = true; } )?
    ';'
;

/**
 * Global constants.
 *
 * @param an empty list, to be completed with global constants
 */
globalConstants[AgentSystem as]
:
( globalConstant[$as] )*
;

/**
 * A single global constant.
 *
 * @param an agent system
 */
globalConstant[AgentSystem as]
locals [
    StreamPos pos = null,
    boolean floating,
    String name,
    AsConstant constant = null
]
:
    'common' { $pos = getStreamPos(); }
    ( 'Z' { $floating = false; } | 'R' { $floating = true; } )
    tName=Identifier { $name = $tName.text;
        /*
        try {
            CompilerUtils.checkSymbolName($pos, $name,
                true);
        } catch(ParseException e) {
            add(e);
        }
         */
    }
    ':=' c = constExpression[$as, false] {
        if(!$floating && $c.result != null && $c.result.isFloating())
            add(new ParseException($c.result.getStreamPos(),
                ParseException.Code.INVALID,
                "loss of precision: R to Z"));
        AsConstant d;
        if($floating && $c.result != null && !$c.result.isFloating())
           d = new AsConstant($pos, $name, new Literal($c.result.getDouble()));
        else
           d = new AsConstant($pos, $name, $c.result);
        $as.addConstant(d);
    }
';'
;
catch[RecognitionException e] {
    add(new ParseException(getStreamPos(), ParseException.Code.INVALID,
        "invalid constant declaration: " + e.getMessage()));
    throw e;
}

/**
 * A mix of modules and definitions.
 *
 * @param as the compiled agent system
 */
modulesOrFormulas[AgentSystem as] :
(
   module[$as] { $as.inModule = false; }
   |
   formula[$as]
)*
;

/**
 * A single module.
 *
 * @param as the compiled agent system
 */
module[AgentSystem as]
locals [
    boolean forcePrivateVariables = false,
    String name,
    AsModule m = null,
    ParseException errors = new ParseException()
]
:
 (
     'agent' { $forcePrivateVariables = true; } tName=Identifier
    |
     'module' { $forcePrivateVariables = false; } tName=Identifier
    |
     'environment' { $forcePrivateVariables = false;
        switch($as.type) {
            case MDP:
                if($as.fullyObservable)
                    add(new ParseException(getStreamPos(),
                        ParseException.Code.INVALID,
                        "non-PO MDP does not allow an environment"));
                break;

            default:
                throw new RuntimeException("unknown model class");
        }
     }
) {
      if($tName == null)
          $name = "environment";
      else
          $name = $tName.text;
      $m = new AsModule(getStreamPos(), $forcePrivateVariables, $name);
      $as.addModule($m);
      $as.inModule = true;
} '{'
    ( variable = stateVarDeclaration[$as] {
        if($variable.result != null) {
            /*
            if($forcePrivateVariables && $variable.result.COMMON.knownProto == null)
                $variable.result.COMMON.knownProto = new LinkedList<>();
             */
            $m.addVariable($variable.result);
        }
    } )*
    ( protocol = protocolDeclaration[$as] {
        if($protocol.result != null) {
            $m.PROTOCOL.setStreamPos($protocol.result.getStreamPos());
            $m.PROTOCOL.add($protocol.result);
        }
    } )?
    ( stat = statement[$as] {
        if($stat.result != null)
            $m.STATEMENTS.add($stat.result);
    } )*
'}' ';'
;

/**
 * A declaration of a state variable.
 */
stateVarDeclaration[AgentSystem as] returns [AsVariable result]
locals [
   StreamPos pos = null,
   String name,
   List<AsConstant> constants = null,
]
:
common = commonDef 'Z' {
} '{' range = constRangeList[$as] '}'
         tName=Identifier { $pos = getStreamPos(); $name = $tName.text; }
         ( ':=' init = initValueSpaces[$as] )?
         ';' {
    if($common.result != null) {
        switch($as.type) {
            case MDP:
                if($as.fullyObservable) {
                    if($common.result.knownProto != null)
                        add(new ParseException(new StreamPos(null, $common.start),
                            ParseException.Code.INVALID,
                            "non-PO MDP allows only a default common definition"));
                }
                break;

            default:
                throw new RuntimeException("unknown model class");
        }
        AsRange r = new AsRange($range.result);
        AsInitValue i;
        if($init.ctx == null) {
            i = new AsInitValue(false, false);
            i.add(0, r.set);
        } else
            i = $init.result;
        AsVariable v = new AsVariable($pos, $as.getParsedModule(), $name,
                $common.result, r, i);
        $result = v;
    }
    /*
    List<String> names = $common.result.knownProto;
    String owner = $as.getParsedModule().NAME;
    if(names != null && !names.contains(owner))
        names.add(owner);
     */
}
;

/**
 * Definition of access rights. If the list of names not empty, called should add the name of
 * the current module if not already present.
 */
commonDef returns [AsCommon result]
locals [
    StreamPos pos,
    List<String> moduleNames = null
]
:
{
    $result = null;
}
(
    'common' { $pos = getStreamPos(); }
            ( '{' list = identifierList { $moduleNames = $list.result; } '}' )? {
        $result = new AsCommon($pos, $moduleNames);
    }
)?
{
    if($result == null)
        $result = new AsCommon(getStreamPos());
};

/**
 * List of constants, comma--separated.
 */
constRangeList[AgentSystem as] returns [List<AsConstant> result]
locals [
    List<AsConstant> out = new ArrayList<AsConstant>()
] 
:
cr = constRange[$as] {
     if($cr.result != null)
        $out.addAll($cr.result);
} (
     ',' cr = constRange[$as] {
        if($cr.result != null)
               $out.addAll($cr.result);
     }
)*
( ',' )? {
     $result = $out;
}
;

constRange[AgentSystem as] returns [ List<AsConstant> result ]
:
left = constBoundary[$as] ( '...' right = constBoundary[$as] )? {
    if($left.result != null) {
        try {
            if($right.ctx == null || $right.result == null)
                $result = ParserUtils.iterate($left.result, $left.result);
            else
                $result = ParserUtils.iterate($left.result, $right.result);
        } catch(ParseException e) {
            e.completePos(getStreamPos());
            add(e);
            $result = null;
        }
    }
}
;

constBoundary[AgentSystem as] returns [ AsConstant result ]
:
left = constExpression[$as, true] ( ':=' right = constExpression[$as, false] )? {
    $result = $left.result;
    if($right.ctx != null) {
        if(newlyDeclaredConstant != null) {
            AsConstant initializedConstant = new AsConstant(
                        newlyDeclaredConstant.getStreamPos(), newlyDeclaredConstant.NAME,
                        $right.result);
            $as.replaceConstant(newlyDeclaredConstant, initializedConstant);
        } else
            add(new ParseException(new StreamPos(null, $right.start),
                ParseException.Code.INVALID,
                "can initialize only a newly declared constant"));
    }
}
;

initValueSpaces[AgentSystem as] returns [AsInitValue result]
:
out = initValueSpace[$as] ( ',' next = initValueSpace[$as] {
        if($out.result != null)
            $out.result.add($next.result);
    } )* ( ',' )? {
    $result = $out.result;
}
;

initValueSpace[AgentSystem as] returns [AsInitValue result]
locals [
    StreamPos pos
]
:
    set = initValueSet[$as] {
        $pos = $set.result.get(0).getStreamPos();
    }
{
    boolean floating = false;
    boolean booleaN = true;
    for(AsConstant c : $set.result) {
        if(c.isFloating())
            floating = true;
        if(!c.isBoolean())
            booleaN = false;
    }
    boolean mixed = false;
    for(AsConstant c : $set.result) {
        if(c.isBoolean() && !booleaN) {
            add(new ParseException(c.getStreamPos(),
                ParseException.Code.INVALID,
                "mixed integer and boolean constants"));
            mixed = true;
            break;
        }
    }
    if(!mixed) {
        if(floating)
            add(new ParseException($pos,
                ParseException.Code.INVALID,
                "state variables must be integer"));
        else {
            AsInitValue init =  new AsInitValue(floating, booleaN);
            Set<Integer> values = new HashSet<Integer>();
            init.add(0, $set.result);
            $result = init;
        }
    }
}
;

initValueSet[AgentSystem as] returns [List<AsConstant> result]
locals [
    StreamPos pos,
    List<AsConstant> all = new ArrayList<AsConstant>()
]
:
s = initValueRange[$as] {
    if($s.result != null)
        $all.addAll($s.result);
} ( ',' s = initValueRange[$as] {
    if($s.result != null)
        $all.addAll($s.result);
} )*
( ',' )? {
    $result = $all;
}
;

initValueRange[AgentSystem as] returns [List<AsConstant> result]
:
low = constExpression[$as, false] ( '...' high = constExpression[$as, false] )? {
    if($low.result != null) {
         try {
            if($high.ctx == null || $high.result == null)
                $result = ParserUtils.iterate($low.result, $low.result);
            else
                $result = ParserUtils.iterate($low.result, $high.result);
        } catch(ParseException e) {
            e.completePos(getStreamPos());
            add(e);
            $result = null;
        }
    }
}
;

/**
 * A protocol.
 */
protocolDeclaration[AgentSystem as] returns [AsProtocol result]
locals [
   StreamPos pos = null,
   AbstractExpression other = null;
]
:
'protocol' { $pos = getStreamPos(); } '{' {
    $result = new AsProtocol($pos, $as.getParsedModule());
} (
    actions = identifierList ':='
(
    guard = expression[$as, false]
|
    { $pos = getStreamPos(); } 'other' { $other = new AsOtherExpression($pos, $as.SCOPE); }
)
{
        if($actions.ctx != null && ($guard.ctx != null || $other != null))
            $result.add($other != null ? $other : $guard.result, $actions.result, false);
    } ';'
)*
'}' ';'
;

/**
  * A single statement.
  */
statement[AgentSystem as] returns [ AsStatement result ]
locals [
    StreamPos pos,
    AsLabel label,
    AbstractExpression other
]
:
'[' { $pos = getStreamPos(); } ( tLabel = Identifier )? ']' {
    if($tLabel == null)
        $label = null;
    else {
        String n = $tLabel.text;
        if(n.equals(AsLabel.SYNC_KEY_NONE)) {
            add(new ParseException(getStreamPos(),
                ParseException.Code.ILLEGAL,
                AsLabel.SYNC_KEY_NONE + " can not be used as a label name"));
            $label = null;
        } else
            $label = new AsLabel($pos, n);
    }
}
(
    guard = expression[$as, false] { $other = null; }
|
    { $pos = getStreamPos(); } 'other' { $other = new AsOtherExpression($pos, $as.SCOPE); }
)
'->' 
(
    updateLeft = variableRef[$as] ':=' updateRight = expression[$as, false]
|
    'true'
)
';' {
    $result = new AsStatement($pos, $as.getParsedModule(),
        $label, $other != null ? $other : $guard.result,
        $updateLeft.ctx == null ? null : $updateLeft.result,
        $updateRight.ctx == null ? null : $updateRight.result);
}
;

/**
  * A reference to an already declared state variable. Needs a semantic check
  * later.
  */
variableRef[AgentSystem as] returns [ AsVariable result ] :
    name = qualifiedIdentifierEnv {
        String name = $name.text;
        if(name.indexOf(".") == -1)
            if($as.inModule)
                name = $as.getParsedModule().NAME + "." + name;
            else
                add(new ParseException(getStreamPos(),
                    ParseException.Code.INVALID,
                    "unqualified variable outside of a module"));
        $result = new AsVariable(new StreamPos(null, $name.start), null,
            name, null, null, null);
}
;

/**
 * A filter on initial values.
 *
 * @param as the agent system eing compiled
 */
initFilter[AgentSystem as]
locals [
]
:
(
    'init' filter=expression[$as, false] {
        if($filter.result != null)
            $as.initFilter = $filter.result;
    } ';'
)?
;

/**
 * An expression which must be reducible to a constant at the parsing time.
 */
constExpression[AgentSystem as, boolean allowDeclaration] returns [ AsConstant result ]
locals [
    AsConstant c
]
:
expr = expression[$as, $allowDeclaration] {
    try {
        if($expr.result == null)
            add(new ParseException(getStreamPos(),
                ParseException.Code.INVALID,
                "constant expression expected"));
        else if(($c = ParserUtils.toConst($as, $expr.result, null, null)) == null)
            add(new ParseException($expr.result.getStreamPos(),
                ParseException.Code.INVALID,
                "constant expression expected"));
        $result = $c;
    } catch(ParseException e) {
        e.completePos($expr.result.getStreamPos());
        add(e);
    } 
}
;

/**
 * An expression.
 */
expression[AgentSystem as, boolean allowDeclaration] returns [AbstractExpression result] :
{
   newlyDeclaredConstant = null;
}
pass = booleanExpression[$as, $allowDeclaration] {
    $result = $pass.result;
}
;

/**
 * A boolean expression.
 *
 * @param scope scope of this expression
 */
booleanExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = relationalExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '&&' { $op = BinaryExpression.Op.CONDITIONAL_AND; } |
      '||' { $op = BinaryExpression.Op.CONDITIONAL_OR; } )
    right = relationalExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * A relational expression.
 *
 * @param scope scope of this expression
 */
relationalExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = addExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '==' { $op = BinaryExpression.Op.EQUAL; } | '<>' { $op = BinaryExpression.Op.INEQUAL; } |
      '<' { $op = BinaryExpression.Op.LESS; } | '>' { $op = BinaryExpression.Op.GREATER; } |
      '<=' { $op = BinaryExpression.Op.LESS_OR_EQUAL; } | '>=' { $op = BinaryExpression.Op.GREATER_OR_EQUAL; } )
    right = addExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * An addition/subtraction/negation expression.
 *
 * @param scope scope of this expression
 */
addExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = multExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '+' { $op = BinaryExpression.Op.PLUS; } | '-' { $op = BinaryExpression.Op.MINUS; } )
    right = multExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * A multiplication/division expression.
 *
 * @param scope scope of this expression
 */
multExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    BinaryExpression.Op op,
    StreamPos pos
]
:
left = unaryExpression[$as, $allowDeclaration] (
    { $pos = getStreamPos(); }
    ( '*' { $op = BinaryExpression.Op.MULTIPLY; } | '/' { $op = BinaryExpression.Op.DIVIDE; } |
      '%' { $op = BinaryExpression.Op.MODULUS; } | '%+' { $op = BinaryExpression.Op.MODULUS_POS; } )
    right = unaryExpression[$as, $allowDeclaration] {
        if($left.result != null && $right.result != null)
            $left.result = new BinaryExpression($pos, $as.SCOPE, $op, $left.result, $right.result);
    }
)* {
   $result = $left.result;
}
;

/**
 * An unary expression: arithmetic negation or boolean not.
 *
 * @param scope scope of this expression
 */
unaryExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    UnaryExpression.Op op,
    StreamPos pos
]
:
(
    (
         { $pos = getStreamPos(); }
         ( '-' { $op = UnaryExpression.Op.NEGATION; } | '!' { $op = UnaryExpression.Op.CONDITIONAL_NEGATION; } )
         sub = primaryExpression[$as, $allowDeclaration] {
             if($sub.result != null)
                 $sub.result = new UnaryExpression($pos, $as.SCOPE, $op, $sub.result);
         }
    )
    |
         sub = primaryExpression[$as, $allowDeclaration]
) {
    $result = $sub.result;
}
;

/**
 * A primary expression.
 */
primaryExpression[AgentSystem as, boolean allowDeclaration]
returns [ AbstractExpression result ]
locals [
    StreamPos pos,
    Method function = null
]
:
{ $pos = getStreamPos(); } vf = valueOrFormula[$as, $allowDeclaration] {
    if($vf.result != null) {
        ValueOrFormula v = $vf.result;
        if(v.VALUE != null) {
            if(v.VALUE instanceof AsLabel) {
                AsLabel label = (AsLabel)v.VALUE;
                $result = new AsActionExpression($as, label.getStreamPos(), $as.SCOPE,
                        null, label.NAME);
            } else
                $result = ParserUtils.newPrimary(as, $pos, v.VALUE);
        } else {
            $result = v.FORMULA.EXPR;
            $as.register($result, v.FORMULA);
        }
    }
} |
'(' expr = expression[$as, $allowDeclaration] ')' {
    newlyDeclaredConstant = null;
    $result = $expr.result;
} |
tName = Identifier { $pos = getStreamPos(); }
               '(' exprList = expressionList[$as, $allowDeclaration] ')' {
     int numArgs = -1;
     switch($tName.text) {
         case "abs":
             $function = ParserUtils.METHOD_ABS;
             numArgs = 1;
             break;

         case "mod":
             $function = ParserUtils.METHOD_MOD;
             numArgs = 2;
             break;

         default:
             add(new ParseException(getStreamPos(),
                     ParseException.Code.UNKNOWN,
                     "unknown function: " + $tName.text));
     }
     newlyDeclaredConstant = null;
     if($function != null) {
         if($exprList.result.size() != numArgs)
             add(new ParseException(getStreamPos(),
                     ParseException.Code.INVALID,
                     "expected " + numArgs + " arguments"));
         $result = new CallExpression($pos, $as.SCOPE, $function, $exprList.result);
     } else
         $result = null;
}
;

/**
 * A single formula.
 */
formula[AgentSystem as]
locals [
    StreamPos pos
]
:
'def' { $pos = getStreamPos(); } tName = Identifier ':=' expr = expression[$as, false] ';' {
    if($expr.result != null) {
        AsFormula f = new AsFormula($pos, $tName.text, $expr.result);
        $as.addFormula(f);
    }
}
;

/**
 * Properties.
 */
properties[AgentSystem as] :
( property[$as] {
  }
)*
;

/**
 * A variable or a constant.
 *
 * @param scope scope
 */
valueOrFormula[AgentSystem as, boolean allowDeclaration]
returns [ ValueOrFormula result ]
locals [
    StreamPos pos
] :
{
    $pos = getStreamPos();
    newlyDeclaredConstant = null;
}
(
    t = number {
        $result = new ValueOrFormula((AsConstant)$t.result);
    }
    |
    name = qualifiedIdentifierEnv {
        // local variables and (global) constants have priority before formulas
        String varName = $name.result;
        if(varName.indexOf(".") == -1 && $as.inModule) {
            varName = $as.getParsedModule().NAME + "." + varName;
        }
        AsTyped typed = $as.lookupValue(varName);
        if(typed == null)
            typed = $as.lookupValue($name.result);
        if(typed == null) {
            // formulas have priority before new constants
            AsFormula f = as.lookupFormula($name.result);
            if(f == null) {
                boolean qualified = $name.result.indexOf('.') != -1;
                if(allowDeclaration && !qualified) {
                    double max = 0;
                    for(AsConstant c : $as.constantList)
                        if(!c.isBoolean()) {
                            double v = c.getMaxPrecisionFloatingPoint();
                            if(max < v)
                                max = v;
                        }
                    AsConstant newConstant = new AsConstant($pos, $name.result,
                        new Literal((int)(max + 1)));
                    $as.addConstant(newConstant);
                    newlyDeclaredConstant = newConstant;
                    $result = new ValueOrFormula(newConstant);
                } else {
                    if(!qualified) {
                        /* //; to be assigned later
                        // during the semantic check
                        $result = new ValueOrFormula(new AsConstant($pos, $name.result));
                          */
                        String name = $name.result;
                        // a global constant has a priority
                        AsConstant constant = $as.lookupConstant(name);
                        if(constant != null)
                            $result = new ValueOrFormula(constant);
                        else {
                            //if(name.indexOf('.') != -1)
                            //    throw new RuntimeException("expected an unqualified identifier");
                            if($as.inModule)
                                name = $as.getParsedModule().NAME + "." + name;
                            else
                                add(new ParseException($pos,
                                    ParseException.Code.INVALID,
                                    "unknown constant or unqualified symbol outside of a module: " + name));
                            $result = new ValueOrFormula(new AsLabel($pos, name));
                        }
                    } else {
                        // can mean a variable or an action; to be assigned/changed to a protocol
                        // action later during the semantic check
                        $result = new ValueOrFormula(new AsVariable(getStreamPos(), null,
                            $name.result, null, null, null));
                    }
                }
             } else
                $result = new ValueOrFormula(f);
        } else
            $result = new ValueOrFormula(typed);
    }
)
;

coalitionGroup[AgentSystem as] returns [ AsCoalition result ] :
tName = Identifier {
    AsModule m = as.lookupModule($tName.text);
    if(m == null)
        add(new ParseException(getStreamPos(),
                ParseException.Code.UNKNOWN,
                "unknown module: " + $tName.text));
    else
        $result = new AsCoalition(m);
}
;

/* keywords */
AGENT: 'agent';
CLASS: 'class';
CONST: 'const';
COMMON: 'common';
DEF: 'def';
ENVIRONMENT: 'environment';
FALSE: 'false';
INIT: 'init';
MODULE: 'module';
PROPERTY: 'property';
PROTOCOL: 'protocol';
R: 'R';
TRUE: 'true';
Z: 'Z';

/* OPERATORS */
PLUS: '+';
MINUS: '-';
MULT: '*';
DIV: '/';
PERCENT: '%';
EQUAL: '==';
LESS: '<';
GREATER: '>';
LESS_EQ: '<=';
GREATER_EQ: '>=';
INEQUAL: '!=';
RARROW: '->';
DARROW: '<->';
ASSIGN: ':=';
LAND: '&&';
LOR: '||';
LNOT: '!';

/* SEPARATORS */
COMMA: ',';
COLON: ':';
SEMICOLON: ';';
DOT: '.';
LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
LSQUARE: '[';
RSQUARE: ']';
ELLIPSIS: '...';

/* GRAMMAR IDENTIFIERS */
LTL: 'LTL';
LTLK: 'LTLK';
LTLKD: 'LTLKD';
ELTL: 'ELTL';
ELTLK: 'ELTLK';
ELTLKD: 'ELTLKD';
ECTL: 'ECTL';
ECTLK: 'ECTLK';
ECTLKD: 'ECTLKD';
ACTL: 'ACTL';
ACTLK: 'ACTLK';
ACTLKD: 'ACTLKD';
CTL: 'CTL';
KD: 'KD';

/* PROPERTY OPERATORS */
A: 'A';
E: 'E';
F: 'F';
G: 'G';
U: 'U';
X: 'X';
AF: 'AF';
EF: 'EF';
AG: 'AG';
EG: 'EG';
AX: 'AX';
EX: 'EX';
Eg: 'Eg';
Dg: 'Dg';
Cg: 'Cg';
Kh: 'Kh';
K: 'K';
Oc: 'Oc';
