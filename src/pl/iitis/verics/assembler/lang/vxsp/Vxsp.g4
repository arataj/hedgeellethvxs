/**
 * Parser of the Vxsp files.
 */
grammar Vxsp;

import Header, VxspModel, Property, Standard, Lists;
