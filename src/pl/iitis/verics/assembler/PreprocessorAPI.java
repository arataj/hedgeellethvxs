/*
 * PreprocessorAPI.java
 *
 * Created on Jan 31, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.PWD;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;
import static pl.iitis.verics.assembler.VxsCLI.filterStdout;

/**
 * An API for using the preprocessor programmatically.
 * 
 * @author Artur Rataj
 */
public class PreprocessorAPI {
    /**
     * If to use full system library; typuically true for a CLI
     * interface, as the relative overhead is small; if used as as API,
     * in the case when there is a large number of individual compilations,
     * can be set to false in order to decrease the overhead.
     */
    boolean LIBRARY_FULL;
    /**
     * Local library of Verics files, empty list for none.
     */
    protected List<String> LOCAL_LIBRARY;
    /**
     * Defines.
     */
    protected CliConstants DEFINES;
    /**
     * Creates a new instance of the API.
     * 
     * @param full copied to <code>LIBRARY_FULL</code>
     * @param library local library of Verics files, empty list or null for
     * none; used beside the standard Verics library
     * @param defines defines, can be null for none
     */
    public PreprocessorAPI(boolean full, List<String> library, CliConstants defines) {
        LIBRARY_FULL = full;
        LOCAL_LIBRARY = library;
        if(LOCAL_LIBRARY == null)
            LOCAL_LIBRARY = new LinkedList<>();
        DEFINES = defines;
        if(DEFINES == null)
            DEFINES = new CliConstants();
    }
    /**
     * Creates a new instance of the API. This is a convenience constructor.
     * No full system library is loaded.
     * 
     * @param defines defines, can be null for none
     */
    public PreprocessorAPI(CliConstants defines) {
        this(false, null, defines);
    }
    /**
     * Preprocesses one stream into another.
     * 
     * @param in input stream; not closed by this method
     * @param out output stream; closed by this method
     */
    public void preprocess(InputStream in, OutputStream out) throws CompilerException {
        preprocess(LIBRARY_FULL, LOCAL_LIBRARY,
            null, in,
            DEFINES,
            null, out);
    }
    /**
     * Preprocesses a file into stream.
     * 
     * @param fileName input file name
     * @param out output stream; closed by this method
     */
    public void preprocess(String fileName, OutputStream out) throws CompilerException {
        try(InputStream s = new FileInputStream(fileName)) {
            preprocess(LIBRARY_FULL, LOCAL_LIBRARY,
                null, s,
                DEFINES,
                null, out);
        } catch(IOException e) {
            throw new CompilerException(new StreamPos(fileName),
                    CompilerException.Code.IO,
                    "can not read input file");
        }
    }
    /**
     * Preprocesses a file into a string.
     * 
     * @param fileName input file name
     * @param out output stream; closed by this method
     */
    public String preprocess(String fileName) throws CompilerException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        preprocess(fileName, out);
        try {
            return out.toString("UTF-8");
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException("unexpected: " + e.getMessage());
        }
    }
    /**
     * Parses a single file, in particular a Vxs or Prism file.
     * 
     * @param library library definition
     * @param fileName name of the model file to parse
     * @param inStream if not null, used instead of <code>fileName</code>,
     * not closed by this method
     * @param defines constant defines
     * @param splitPropertyOperators if to apply
     * @param removeLineNumberSuffix if to remove line number suffixes
     * <code>PropertyPreprocessor.process()</code>
     */
    public static VxsCompilation parseVxs(VxsLibrary library,
            String fileName, InputStream inStream,
            CliConstants defines, boolean splitPropertyOperators,
            boolean removeLineNumberSuffix) throws CompilerException {
        VxsCompilation c = new VxsCompilation(fileName != null ?
                fileName : null);
        CompilerException errors = new CompilerException();
        c.PREPROCESSOR = new VxsPreprocessor();
        try {
            c.PREPROCESSOR.preprocess(library, fileName, inStream, defines);
            if(splitPropertyOperators) {
                PropertyPreprocessor pp = new PropertyPreprocessor(c.PREPROCESSOR.VXSP);
                c.VXSP2 = pp.process();
            } else
                c.VXSP2 = c.PREPROCESSOR.VXSP;
            c.VXSP2 = filterStdout(c.VXSP2, removeLineNumberSuffix);
        } catch(CompilerException e) {
            errors.addAllReports(e);
        }
        // only for tests
        VxsCLI.VXSC = c;
        if(errors.reportsExist())
            throw errors;
        return c;
    }
    /**
     * <p>A general static method which preprocesses:
     * 
     * (a) either <code>.vxs</code> file, saves the output to a
     * respective <code>.vxsp</code> file; splits property operators,
     * postfixes each line with the original line number
     * 
     * (b) or <code>.nm.pp</code> file, saves the output to a
     * respective <code>.nm</code> file; property operator
     * splitting is omitted in that case and line number suffixes
     * are removed;
     * 
     * (c) any file if forced output file name or output stream.</p>
     * 
     * @param full if to use the full system library; see <code>FULL_LIBRARY</code>
     * for details
     * @param localLibrary library files, added beside the global library
     * @param fileName name of the model file to read
     * @param stream if not null, used instead of <code>fileName</code>;
     * never closed by this method
     * @param defines constant defines
     * @param forceOutFilename  if not null, forces the output file name
     * @param forceOutStream if not null, forces the output stream; can not
     * be used together with <code></code>; closed by this method after
     * the write
     */
    public static void preprocess(boolean full, List<String> localLibrary,
            String fileName, InputStream stream, CliConstants defines, String forceOutFilename,
            OutputStream forceOutStream) throws CompilerException {
        String outFileName;
        VxsCLI.FileType ft;
        if(forceOutFilename != null || forceOutStream != null) {
            if(forceOutFilename != null && forceOutStream != null)
                throw new RuntimeException("file and stream specified simultaneously");
            if(forceOutFilename != null)
                outFileName = forceOutFilename;
            else
                outFileName = "<out stream>";
            ft = VxsCLI.FileType.UNKNOWN;
        } else {
            ft = VxsCLI.getFileType(fileName);
            if(ft == null)
                throw new RuntimeException("unknown file type but output not forced");
            String name;
            switch(ft) {
                case PRISM_P:
                case EXTERNAL_P:
                    name = null;
                    break;
                    
                case VS:
                    name = VxsCLI.VS_PREPROCESSED_FILE_NAME_EXTENSION;
                    break;
                    
                case VXS:
                    name = VxsCLI.VXS_PREPROCESSED_FILE_NAME_EXTENSION;
                    break;
                    
                default:
                    throw new RuntimeException("unexpected file format");
            }
            outFileName = CompilerUtils.replaceExtension(fileName, name);
        }
        VxsLibrary library = new VxsLibrary(new PWD(),
                    pl.gliwice.iitis.hedgeelleth.cli.Compiler.getCompilerLibraryDirectory(
                        VxsCLI.class, System.getProperty("user.dir")),
                    full, localLibrary);
        VxsCompilation vc = parseVxs(library, fileName, stream, defines,
                ft == VxsCLI.FileType.VS || ft == VxsCLI.FileType.VXS,
                ft != VxsCLI.FileType.VS && ft != VxsCLI.FileType.VXS);
        try {
            StringBuilder s = new StringBuilder();
            Scanner sc = new Scanner(vc.VXSP2);
            while(sc.hasNextLine()) {
                String line = sc.nextLine();
                int pos = line.lastIndexOf(VxsPreprocessor.LINE_COMMENT_STR);
                if(pos != -1)
                    line = line.substring(0, pos);
                s.append(line + "\n");
            }
            if(forceOutStream != null)
                CompilerUtils.save(forceOutStream, s.toString());
            else
                CompilerUtils.save(outFileName, s.toString());
        } catch(IOException e) {
            throw new CompilerException(new StreamPos(outFileName),
                    CompilerException.Code.IO, "could not save file: " +
                    e.getMessage());
        }
    }
}
