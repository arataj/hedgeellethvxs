/*
 * SyncToGuards.java
 *
 * Created on Mar 25, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import pl.iitis.verics.agent.AsActionExpression;
import java.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;

import pl.iitis.verics.agent.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary.SemanticCheckFactory;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

/**
 * Converts synchronisation labels into guards. Works only for
 * a subset of models.
 * 
 * @author Artur Rataj
 */
public class SyncToGuards {
    
    /**
     * The processed agent system.
     */
    final AgentSystem AS;
    /**
     * Additional actions for each module.
     */
    final Map<AsModule, SortedSet<String>> ACTIONS;
    /**
     * Additional protocol for each module.
     */
    final Map<AsModule, AsProtocol> PROTOCOL;
    /**
     * Additional evolution for each module.
     */
    final Map<AsModule, List<AsStatement>> EVOLUTION;
    
    /**
     * Finds actions, protocoles and evolution functions which
     * can replace the synchronisations.
     * 
     * @param as agent system, not modified by this constructor
     */
    public SyncToGuards(AgentSystem as) throws CompilerException {
        AS = as;
        ACTIONS = new HashMap<>();
        PROTOCOL = new HashMap<>();
        EVOLUTION = new HashMap<>();
        for(AsModule module : as.moduleList) {
            SortedSet<String> labels = new TreeSet<>();
            for(AsStatement s : module.STATEMENTS)
                if(s.LABEL != null)
                    labels.add(AsProtocol.stripIndex(module, s.LABEL.NAME));
            ACTIONS.put(module, labels);
            processModule(module, true);
            processModule(module, false);
        }
    }
    /**
     * Processes a single module.
     * 
     * @param module module
     * @param protocol true if to generate the protocol, false if to generate
     * the evolution function
     */
    private void processModule(AsModule module, boolean protocol)
            throws CompilerException {
        List<List<AsConstant>> states = new LinkedList<>();
        for(AsVariable v : module.variableList) {
            if(states.isEmpty()) {
                // first sequence
                for(AsConstant c : v.RANGE.set) {
                    List<AsConstant> initial = new LinkedList<>();
                    initial.add(c);
                    states.add(initial);
                }
            } else {
                // Cartesian product
                List<List<AsConstant>> next = new LinkedList<>();
                for(List<AsConstant> list : states)
                    for(AsConstant c : v.RANGE.set) {
                        List<AsConstant> initial = new LinkedList<>(list);
                        initial.add(c);
                        next.add(initial);
                    }
                states = next;
            }
        }
        SemanticCheckFactory semanticFactory = new VxsSemanticCheckFactory();
        AsProtocol moduleProtocol;
        List<AsStatement> moduleStatements;
        if(protocol) {
            moduleProtocol = new AsProtocol(null, module);
            moduleStatements = null;
        } else {
            moduleProtocol = null;
            moduleStatements = new LinkedList<>();
        }
        Set<AsStatement> used = new HashSet<>();
        for(List<AsConstant> list : states) {
            AbstractExpression guard = null;
            StringBuilder stateStr = new StringBuilder();
            Map<Variable, AsConstant> vars = new HashMap<>();
            Iterator<AsConstant> listI = list.iterator();
            boolean first = true;
            for(AsVariable v : module.variableList) {
                AsConstant c = listI.next();
                StreamPos pos = module.getStreamPos();
                AbstractExpression a = new BinaryExpression(pos,
                        AS.SCOPE, BinaryExpression.Op.EQUAL,
                        ParserUtils.newPrimary(AS, pos, v), ParserUtils.newPrimary(AS, pos, c));
                if(guard == null)
                    guard = a;
                else
                    guard = new BinaryExpression(module.getStreamPos(),
                            AS.SCOPE, BinaryExpression.Op.CONDITIONAL_AND, guard, a);
                first = false;
                vars.put(v.toVariable(), c);
            }
            SortedSet<String> labels = new TreeSet<>();
            List<AsStatement> statements = new LinkedList<>();
            for(AsStatement s : module.STATEMENTS) {
                AsConstant simpleGuard = ParserUtils.toConst(AS, s.GUARD, null, semanticFactory);
                boolean alwaysTrue = simpleGuard != null && simpleGuard.getBoolean();
                if(s.LABEL != null) {
                    try {
                        AsConstant guardValue = ParserUtils.toConst(AS, s.GUARD, vars, semanticFactory);
                        if(guardValue == null)
                            throw new CompilerException(s.getStreamPos(), CompilerException.Code.ILLEGAL,
                                    ": a guard uses non-local variable");
                        if((alwaysTrue && (protocol || !used.contains(s))) ||
                                (!alwaysTrue && guardValue.getBoolean())) {
                            used.add(s);
                            if(protocol)
                                labels.add(s.LABEL.NAME);
                            else {
                                AbstractExpression updateRight;
                                if(s.UPDATE_LEFT != null) {
                                    AsConstant update = ParserUtils.toConst(AS, s.UPDATE_RIGHT, vars,
                                            semanticFactory);
                                    if(update == null)
                                        throw new CompilerException(s.UPDATE_RIGHT.getStreamPos(), CompilerException.Code.ILLEGAL,
                                            "could not evaluate");
                                    if(update.NAME == null)
                                        throw new CompilerException(s.UPDATE_RIGHT.getStreamPos(), CompilerException.Code.ILLEGAL,
                                            "a statement with converted label expects a named constant here");
                                    updateRight = ParserUtils.newPrimary(AS,
                                            s.UPDATE_RIGHT.getStreamPos(), update);
                                } else
                                    updateRight = null;
                                AbstractExpression condition;
                                AsActionExpression action = new AsActionExpression(AS, module.getStreamPos(),
                                        AS.SCOPE, module, AsProtocol.stripIndex(module, s.LABEL.NAME));
                                if(alwaysTrue)
                                    condition = action;
                                else
                                    condition = new BinaryExpression(module.getStreamPos(), AS.SCOPE,
                                            BinaryExpression.Op.CONDITIONAL_AND, guard, action);
                                List<AsStatement> synced = AS.getSynced(s.LABEL, module);
                                for(AsStatement sSynced : synced)
                                    condition = new BinaryExpression(module.getStreamPos(), AS.SCOPE,
                                            BinaryExpression.Op.CONDITIONAL_AND, condition,
                                            new AsActionExpression(AS, module.getStreamPos(), AS.SCOPE,
                                                sSynced.OWNER, AsProtocol.stripIndex(sSynced.OWNER,
                                                        sSynced.LABEL.NAME)));
                                if(!AS.synchronous) {
                                    SCAN_NEXT_MODULE:
                                    for(AsModule mOther : AS.moduleList)
                                        if(mOther != module) {
                                            for(AsStatement sSynced : synced)
                                                if(mOther == sSynced.OWNER)
                                                    continue SCAN_NEXT_MODULE;
                                            for(AsStatement o : mOther.STATEMENTS)
                                                if(o.LABEL == null)
                                                    condition = new BinaryExpression(module.getStreamPos(), AS.SCOPE,
                                                            BinaryExpression.Op.CONDITIONAL_AND, condition,
                                                            new AsActionExpression(AS, module.getStreamPos(), AS.SCOPE,
                                                                o.OWNER, o.LABEL == null ? AsLabel.SYNC_KEY_NONE :
                                                                        AsProtocol.stripIndex(o.OWNER, o.LABEL.NAME)));
                                        }
                                }
                                statements.add(new AsStatement(module.getStreamPos(), module,
                                        null, condition, s.UPDATE_LEFT, updateRight));
                            }
                        }
                    } catch(ParseException e) {
                            throw e;
                    }
                } else {
//                    if(!protocol &&
//                            (alwaysTrue || s.GUARD instanceof AsOtherExpression)) {
//                        s.GUARD = new AsActionExpression(AS, s.GUARD.getStreamPos(), AS.SCOPE,
//                                module, AsLabel.SYNC_KEY_NONE);
//                    }
                }
            }
            if(protocol) {
                if(!labels.isEmpty())
                    moduleProtocol.add(guard, labels, true);
            } else
                moduleStatements.addAll(statements);
        }
        if(protocol) {
            PROTOCOL.put(module, moduleProtocol);
        } else {
            EVOLUTION.put(module, moduleStatements);
        }
    }
    /**
     * Completes the agent system by replacing its synced statements by extended
     * protocol and evolution definitions.
     */
    public void complete() throws CompilerException {
        for(AsModule module : AS.moduleList) {
            for(String action : ACTIONS.get(module)) {
                if(module.ACTIONS.contains(action))
                    throw new CompilerException(module.getStreamPos(),
                            CompilerException.Code.DUPLICATE, "action clash: " + action);
                module.ACTIONS.add(action);
            }
            module.PROTOCOL.add(PROTOCOL.get(module));
            boolean noneAppended = false;
            for(AsStatement s : new LinkedList<>(module.STATEMENTS)) {
                if(s.LABEL != null)
                    module.STATEMENTS.remove(s);
                else if(s.UPDATE_LEFT == null) {
//                    boolean gTrue = s.GUARD instanceof AsActionExpression &&
//                            ((AsActionExpression)s.GUARD).ACTION.equals(
//                                AsLabel.SYNC_KEY_NONE);
                    boolean gTrue = s.GUARD.getSimpleConstant() != null &&
                            s.GUARD.getSimpleConstant().type.isBoolean() &&
                            s.GUARD.getSimpleConstant().getBoolean();
                    boolean gOther = s.GUARD instanceof AsOtherExpression;
                    if(gTrue || gOther) {
                        s.GUARD = new AsActionExpression(AS, s.GUARD.getStreamPos(), AS.SCOPE,
                                module, AsLabel.SYNC_KEY_NONE);
                        module.ACTIONS.add(AsLabel.SYNC_KEY_NONE);
                        if(!module.PROTOCOL.containsOtherNone()) {
                            List<String> a = new LinkedList<>();
                            a.add(AsLabel.SYNC_KEY_NONE);
                            module.PROTOCOL.add(new AsOtherExpression(s.getStreamPos(),
                                    AS.SCOPE), a, false);
                        }
                        if(gTrue) {
                            Iterator<AbstractExpression> gI = module.PROTOCOL.guards.iterator();
                            for(List<String> a : module.PROTOCOL.actions) {
                                AbstractExpression g = gI.next();
                                if(!a.contains(AsLabel.SYNC_KEY_NONE)) {
                                    List<AbstractExpression> guards = module.PROTOCOL.map.get(
                                            AsLabel.SYNC_KEY_NONE);
                                    if(guards == null) {
                                        guards = new LinkedList<>();
                                        module.PROTOCOL.map.put(AsLabel.SYNC_KEY_NONE, guards);
                                    }
                                    guards.add(g);
                                    a.add(AsLabel.SYNC_KEY_NONE);
                                }
                            }
                            noneAppended = true;
                        }
                    }
                }
            }
            module.STATEMENTS.addAll(EVOLUTION.get(module));
        }
    }
}
