/*
 * ParsePrint.java
 *
 * Created on Jul 1, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.compiler.util.text.StringAnalyze;

/**
 * Converts assembly lines into Verics print statements, memorizes source
 * positions.
 * 
 * @author Artur Rataj
 */
public class ParsePrint extends AbstractParseMap {
    /**
     * If the beginning of this line is within brackets, which began somewhere within the
     * previous lines
     */
    public boolean IN_BRACKETS;
    /**
     * If the beginning of this line is within a comment, which began somewhere within the
     * previous lines
     */
    public boolean IN_COMMENT;
    /**
     * If an expression prefix "(" has been printed, but no non--empty expression
     * has been printed so far, if at all.
     */
    public boolean NO_EXPRESSION;
    /**
     * Creates a new instance of <code>ParsePrint</code>.
     * 
     * @param raw input string, raw assembly text + preprocessor expressions;
     * can be null if not needed or constructed later
     * @param lineNum line number, beginning at 0
     * @param inBrackets if the beginning of this line is within brackets, which
     * began somewhere within the previous lines
     */
    public ParsePrint(String raw, int lineNum, boolean inBrackets,
            boolean inComment, boolean noExpression) throws ParseException {
        super(raw, lineNum, 0);
        IN_BRACKETS = inBrackets;
        IN_COMMENT = inComment;
        NO_EXPRESSION = noExpression;
        OUT = assemblyToVericsPrint(RAW, LINE_NUM);
    }
    /**
     * Returns, which characters in a string are within a comment.
     * 
     * @param inComment if the beginning of the line is already within a comment
     * @param line the string to test
     * @return array whose indices reference characters in <code>line</code>
     */
    protected boolean[] getCommentMap(boolean inComment, String line) {
        boolean[] map = new boolean[line.length()];
        for(int i = 0; i < line.length(); ++i) {
            char c0 = line.charAt(i);
            char c1;
            if(i < line.length() - 1)
                c1 = line.charAt(i + 1);
            else
                c1 = 0;
            if(c0 == '/' && c1 == '*') {
                inComment = true;
                map[i] = inComment;
                ++i;
            }
            map[i] = inComment;
            if(c0 == '*' && c1 == '/') {
                ++i;
                map[i] = inComment;
                inComment = false;
            }
        }
        return map;
    }
    /**
     * Like String.getIndex(String, int), but ignores quoted delimiters.
     * The quotation character is a single or a double quote, non--escaped.
     * 
     * @param line string to analyze
     * @param lineNum line number, for error messages; beginning at 0
     * @param delimiter not--quoted delimiter to search; can not be a single
     * or a double quote
     * @param analyzeQuotePos quote toggle analysis starts at this index
     * @param pos begin searching for the delimiter at this position; quote
     * analysis is nevertheless performed from <code>analyzeQuotePos</code>
     * @param checkTerminatedQuote if true and a in-brackets quote not
     * terminated before EOL is found, an exception is thrown
     * @return index of the first occurence of the unquoted delimiter, -1 if not found
     */
    protected final int indexOfNotQuoted(String line, int lineNum, String delimiter,
            int analyzeQuotePos, int pos, boolean checkTerminatedQuote) throws ParseException {
//if(line.indexOf("i < 3") != -1)
//    line = line;
        boolean[] quoted = new boolean[line.length()];
        boolean qS = false;
        boolean qD = false;
        for(int i = analyzeQuotePos; i < line.length(); ++i) {
            char c = line.charAt(i);
            boolean escaped = false;
            for(int j = i - 1; j >= analyzeQuotePos; --j) {
                char d = line.charAt(j);
                if(d != '\\')
                    break;
                escaped = !escaped;
            }
            if(escaped && (c == '"' || c == '\'') && !(qS || qD))
                throw new ParseException(new StreamPos(null, lineNum + 1, i + 1),
                        ParseException.Code.ILLEGAL, "escaped quote outside quotation");
            if(c == '"' && !escaped && !qS) {
                qD = !qD;
                quoted[i] = true;
            } else if(c == '\'' && !escaped && !qD) {
                qS = !qS;
                quoted[i] = true;
            } else
                quoted[i] = qS || qD;
        }
        while(pos < line.length()) {
            int i = line.indexOf(delimiter, pos);
            if(i == -1 || !quoted[i]) {
                if(checkTerminatedQuote && i == -1 && quoted[line.length() - 1])
                    throw new ParseException(null, ParseException.Code.ILLEGAL,
                            "unterminated bracketed quote");
                return i;
            } else
                pos = i + 1;
        }
        return -1;
    }
    /**
     * Converts an assembly line with possible Verics variable references to an
     * equivalent Verics print statement.
     * 
     * @param line input line
     * @param lineNum line number, for error messages; beginning at 0
     * @return string with a single Verics print statement
     */
    protected final String assemblyToVericsPrint(String line, int lineNum) throws ParseException {
        final String PREFIX = "println(";
        final String SUFFIX = ");\n";
        final String OPEN_BRACE_STR = "${";
        final String CLOSE_BRACE_STR = "}";
        final String PLUS_OP_STR = " + ";
        final int OPEN_LEN = OPEN_BRACE_STR.length();
        StringBuilder out = new StringBuilder();
        if(!IN_BRACKETS) {
            out.append(PREFIX);
            int outIndex = out.length();
            fillNoMatch(outIndex);
        }
        line = StringAnalyze.removeTrailingSpace(line);
if(line.indexOf("E/DESTI") != -1)
    line = line;
        boolean[] comment = getCommentMap(IN_COMMENT, line);
        boolean first = true;
        int pos = 0;
        try {
            while(pos < line.length()) {
                int openBrace = line.indexOf(OPEN_BRACE_STR, pos);
                boolean locallyInBraces = openBrace != -1;
                int closeBrace = indexOfNotQuoted(line, lineNum, CLOSE_BRACE_STR,
                        Math.max(0, openBrace), pos, locallyInBraces);
                if(openBrace != -1 && comment[openBrace])
                    openBrace = -1;
                if(closeBrace != -1 && comment[closeBrace])
                    closeBrace = -1;
                if(closeBrace == -1)
                    closeBrace = line.length();
                if(IN_BRACKETS) {
                    //if(openBrace == -1)
                    //    openBrace = -OPEN_LEN;
                    if(openBrace != -1 && openBrace < closeBrace)
                        throw new ParseException(new StreamPos(null, lineNum + 1, openBrace + 1),
                                ParseException.Code.ILLEGAL, "nested opening brace");
                    openBrace = -OPEN_LEN;
                } else {
                    if(openBrace == -1)
                        openBrace = line.length();
                    if(closeBrace < openBrace) {
                        // ignore that brace, it does not relate to the preprocessor
                        closeBrace = indexOfNotQuoted(line, lineNum, CLOSE_BRACE_STR,
                                openBrace, openBrace + 1, locallyInBraces);
                        if(closeBrace != -1 && comment[closeBrace])
                            closeBrace = -1;
                        if(closeBrace == -1)
                            closeBrace = line.length();
                    }
                }
    //else
    //  openBrace = openBrace;
                List<Integer> qMap = new ArrayList<>();
                if(!first || !IN_BRACKETS) {
                    if(!first) {
                        out.append(PLUS_OP_STR);
                        fillNoMatch(PLUS_OP_STR.length());
                    }
                    out.append('"');
                    fillNoMatch(1);
                    String q = CompilerUtils.escapeQuotesSlashes(line.substring(
                            pos, Math.max(0, openBrace)),
                            false, qMap);
                    out.append(q);
                    for(int i = 0; i < q.length(); ++i) {
                        LINE_MAP.add(lineNum);
                        COL_MAP.add(pos + qMap.get(i));
                    }
                    out.append('"');
                    //fillNoMatch(1);
                    // let the first position after the print contents be the mark
                    // of an error after the contents, even if possibly caused by
                    // it
                    LINE_MAP.add(lineNum);
                    COL_MAP.add(pos +
                            (q.length() == 0 ? 0 : qMap.get(q.length() - 1) + 1));
                }
                boolean prevInBrackets = IN_BRACKETS;
                IN_BRACKETS = openBrace < closeBrace && closeBrace == line.length();
                first = false;
                int rawIndex = pos;
                if(!qMap.isEmpty())
                    rawIndex += qMap.get(qMap.size() - 1) + 1;
                if(openBrace < line.length()) {
                    if(closeBrace == -1)
                        throw new ParseException(new StreamPos(null, lineNum + 1, openBrace + 1),
                                ParseException.Code.MISSING, "matching closing brace not found");
                    if(!prevInBrackets) {
                        out.append(PLUS_OP_STR + "(");
                        fillNoMatch(PLUS_OP_STR.length() + 1);
                        NO_EXPRESSION = true;
                    }
                    String expr = line.substring(openBrace + OPEN_LEN, closeBrace);
                    if(!expr.isEmpty()) {
                        out.append(expr);
                        NO_EXPRESSION = false;
                    }
                    if(openBrace >= 0)
                        // does it actually occur in the line, so that it should be skipped?
                        rawIndex += OPEN_LEN;
                    for(int i = openBrace + OPEN_LEN; i < closeBrace; ++i) {
                        LINE_MAP.add(lineNum);
                        COL_MAP.add(rawIndex++);
                    }
                    if(!IN_BRACKETS) {
                        if(NO_EXPRESSION) {
                            out.append("\"\"");
                            fillNoMatch(2);
                            NO_EXPRESSION = false;
                        }
                        out.append(")");
                        fillNoMatch(1);
                    }
                    pos = closeBrace + CLOSE_BRACE_STR.length();
                } else
                    pos = openBrace;
            }
        } catch(ParseException e) {
            e.completePos(new StreamPos(null, lineNum + 1, 0));
            throw e;
        }
        if(!IN_BRACKETS) {
            out.append(SUFFIX);
            fillNoMatch(SUFFIX.length());
        }
        return out.toString();
    }
}
