/*
 * ParserUtils.java
 *
 * Created on Oct 2, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import java.util.Map.Entry;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Method;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.NameList;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary.SemanticCheckFactory;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;

import pl.iitis.verics.agent.*;
import pl.iitis.verics.agent.property.AsProperty;
import pl.iitis.verics.agent.AsActionExpression;

/**
 * Helper utilities for <code>VxsParser</code>.
 * 
 * @author Artur Rataj
 */
public class ParserUtils {
    /**
     * A specifier of the function abs(x).
     */
    public static Method METHOD_ABS = new Method("abs");
    /**
     * A specifier of the function mod(x).
     */
    public static Method METHOD_MOD = new Method("mod");
    
    /**
      * Creates a primary expression out of a constant.
      * 
      * @param as agent system
      * @param pos position in the source stream of the expression to create
      * @param v a constant
      */
    public static PrimaryExpression newPrimary(AgentSystem as, StreamPos pos,
            AsConstant c) {
        return new PrimaryExpression(
                pos, as.SCOPE, PrimaryExpression.ScopeMode.ALL,
                new PrimaryPrefix(c.getStreamPos(), c),
                new LinkedList<>());
    }
    /**
      * Creates a primary expression out of a variable or a name holder.
      * 
      * @param as agent system
      * @param pos position in the source stream of the expression to create
      * @param v variable or only a name holder, to look the variable later
      */
    public static PrimaryExpression newPrimary(AgentSystem as, StreamPos pos,
            AsVariable v) {
        return Hedgeelleth.newVariableExpression(pos, as.SCOPE,
                v.toVariable());
    }
    /**
      * Creates a primary expression out of a typed value. This method calls
      * either <code>newPrimary(AsConstant)</code> or
      * <code>newPrimary(AsVariable)</code>.
      * 
      * @param as agent system
      * @param pos position in the source stream of the expression to create
      * @param t a constant or a variable
      */
    public static PrimaryExpression newPrimary(AgentSystem as, StreamPos pos,
            AsTyped t) {
        if(t instanceof AsConstant)
            return newPrimary(as, pos, (AsConstant)t);
        else if(t instanceof AsVariable)
            return newPrimary(as, pos, (AsVariable)t);
        else
            throw new RuntimeException("enexpected type");
    }
    /**
     * Evaluates a constant expression.
     * 
     * @param as agent system, provides global constants
     * @param expr expression to evaluate
     * @param stateKnows null or state variables with known values
     * @param semanticFactory factory of <code>SemanticCheck</code>,
     * null for the default
     * @return value of <code>expr</code> or null if can not be evaluated
     */
    public static AsConstant toConst(AgentSystem as, AbstractExpression expr,
             Map<Variable, AsConstant> stateKnowns,
             SemanticCheckFactory semanticFactory) throws ParseException {
        try {
            ParsedExpression pe = new ParsedExpression(as, expr, false, semanticFactory);
            Map<Variable, Literal> knownsV = new HashMap<>();
            Map<Variable, AsConstant> knowns = new HashMap<>(pe.KNOWNS);
            if(stateKnowns != null)
                knowns.putAll(stateKnowns);
            for(Entry<Variable, AsConstant> vc : knowns.entrySet())
                knownsV.put(vc.getKey(), (Literal)vc.getValue());
            Literal c = pe.EXPR.evaluate(knownsV);
            if(c == null)
                return null;
            else {
                String name;
                Literal d;
                if((d = expr.getSimpleConstant()) instanceof AsConstant)
                    name = ((AsConstant)d).NAME;
                else
                    name = null;
                return new AsConstant(expr.getStreamPos(), name, c);
            }
        } catch (CompilerException e) {
            ParseException f = new ParseException();
            f.addAllReports(e);
            throw f;
        }
    }
    /**
     * Returns a variable referenced by a variable name holder.
     * 
     * @param as agent system
     * @param holder name holder or a variable
     */
    public static AsVariable checkVariable(AgentSystem as, AsVariable holder)
            throws ParseException {
//if(holder.OWNER == null)
//    as = as;
        String name;
        if(holder.OWNER != null)
            name = holder.getQualifiedName();
        else
            name = holder.NAME;
        AsTyped v = as.lookupValue(name);
        if(v instanceof AsVariable)
            return (AsVariable)v;
        else if(v instanceof AsConstant)
            throw new ParseException(holder.getStreamPos(),
                ParseException.Code.ILLEGAL,
                "variable expected");
        else
            throw new ParseException(holder.getStreamPos(),
                ParseException.Code.INVALID,
                "variable not found");
    }
    /**
     * Checks, if the fractional part is equal to zero.
     * 
     * @param pos source position, to be put into a possible parse exception
     * @param d value to test
     */
    public static void checkIsInteger(StreamPos pos, double d) throws ParseException {
        if(d != (int)d)
            throw new ParseException(pos,
                ParseException.Code.ILLEGAL,
                "must evaluate to integer");
    }
    /**
     * Checks, if a vaue fits into the type <code>int</code>.
     * 
     * @param pos source position, to be put into a possible parse exception
     * @param d value to test
     */
    public static void checkFitsInt(StreamPos pos, double d) throws ParseException {
        if(d > Integer.MAX_VALUE)
            throw new ParseException(pos,
                ParseException.Code.ILLEGAL,
                "value too great to fit into integer");
        if(d < Integer.MIN_VALUE)
            throw new ParseException(pos,
                ParseException.Code.ILLEGAL,
                "value too small to fit into integer");
    }
    /**
     * Returns a list of constants, whose values are <code>low</code>,
     * <code>low + 1</code>, ... <code>high</code>. The first and last constant
     * are respectively <code>low</code> and <code>high</code>.
     * 
     * @param low the lowest constant, can not have a non--zero fractional part
     * @param high the highest constant, can not have a non--zero fractional part
     * @return a list of constants sorted by increasing value
     */
    public static List<AsConstant> iterate(AsConstant low, AsConstant high)
            throws ParseException {
        List<AsConstant> out = new LinkedList<>();
        double start = low.getMaxPrecisionFloatingPoint();
        double stop = high.getMaxPrecisionFloatingPoint();
        if(stop < start)
            throw new ParseException(high.getStreamPos(),
                ParseException.Code.ILLEGAL,
                "can not have a lower value");
        checkIsInteger(low.getStreamPos(), start);
        checkIsInteger(high.getStreamPos(), stop);
        checkFitsInt(low.getStreamPos(), start);
        checkFitsInt(high.getStreamPos(), stop);
        int startI = (int)start;
        int stopI = (int)stop;
        for(int i = startI; i <= stopI; ++i) {
            AsConstant c;
            if(i == startI)
                c = low;
            else if(i == stopI)
                c = high;
            else
                c = new AsConstant(null, null, new Literal(i));
            out.add(c);
        }
        return out;
    }
    /**
     * Lookups symbols in a primary expression.
     * 
     * @param as agent system
     * @param m module within the agent system
     * @param p primary expression within the module
     * @return <code>p</code> or if nor parsed, the
     * parsed derivative
     */
    protected static AbstractExpression lookupSymbolsPrimary(AgentSystem as, AsModule m,
            PrimaryExpression p) throws CompilerException {
        if(p.prefix.contents instanceof AsConstant) {
            AsConstant c = (AsConstant)p.prefix.contents;
            String name = c.NAME;
            // check if an unnamed literal like <code>false</code> or <code>0</code>
            if(name != null) {
                AsTyped t = as.lookupValue(name);
//if(t == null)
//    as.lookupValue(name);
                if(t != null) {
                    if(!(t instanceof AsConstant))
                        throw new RuntimeException("constant expected");
                    p = newPrimary(as, p.getStreamPos(), t);
// p = p;
                } else
                    throw new CompilerException(p.getStreamPos(),
                            CompilerException.Code.UNKNOWN,
                            "unknown constant: " + name);
            }
        } else if(p.prefix.contents instanceof NameList) {
            String name = ((NameList)p.prefix.contents).toString();
            AsVariable v = null;
            // variables have a priority over protocol actions, but it should
            // otherwise be prevented that a module has a variable and an
            // action of the same name
            try {
                v = checkVariable(as, new AsVariable(null, null, name, null, null, null));
            } catch(ParseException e) {
                AsActionExpression ae = as.getActionExpression(p.getStreamPos(),
                        name);
                if(ae == null)
                    throw new CompilerException(p.getStreamPos(),
                        CompilerException.Code.UNKNOWN,
                        "unknown variable or protocol action: " + name);
                return ae;
            }
            
        }
        return p;
    }
    /**
     * Lookups symbols in an abstract expression.
     * 
     * @param as agent system
     * @param m module within the agent system
     * @param expr expression within the module
     */
    protected static AbstractExpression lookupSymbols(AgentSystem as, AsModule m,
            AbstractExpression expr) throws CompilerException {
        if(expr == null)
            return null;
        else if(expr instanceof UnaryExpression) {
            UnaryExpression ue = (UnaryExpression)expr;
            ue.sub = lookupSymbols(as, m, ue.sub);
        } else if(expr instanceof BinaryExpression) {
            BinaryExpression be = (BinaryExpression)expr;
            be.left = lookupSymbols(as, m, be.left);
            be.right = lookupSymbols(as, m, be.right);
        } else if(expr instanceof PrimaryExpression) {
            expr = ParserUtils.lookupSymbolsPrimary(as, m, (PrimaryExpression) expr);
        } else if(expr instanceof AssignmentExpression) {
            AssignmentExpression a = (AssignmentExpression)expr;
            a.lvalue = (PrimaryExpression)lookupSymbolsPrimary(as, m, a.lvalue);
            a.rvalue = lookupSymbols(as, m, a.rvalue);
        } else if(expr instanceof CallExpression) {
            CallExpression c = (CallExpression)expr;
            List<AbstractExpression> arg = new LinkedList<>();
            for(AbstractExpression a : c.arg)
                arg.add(lookupSymbols(as, m, a));
            c.arg = arg;
        } else if(expr instanceof AsActionExpression) {
            AsActionExpression a = (AsActionExpression)expr;
            if(a.MODULE == null) {
                int pos = a.ACTION.indexOf('.');
                if(pos == -1)
                    throw new RuntimeException("a qualified label expected");
                String owner = a.ACTION.substring(0, pos);
                AsModule module = as.lookupModule(owner);
                if(module == null)
                    throw new CompilerException(a.getStreamPos(),
                            CompilerException.Code.UNKNOWN,
                            "unknown module: " + owner);
                a.MODULE = module;
                a.ACTION = a.ACTION.substring(pos + 1);
            }
            if(!a.MODULE.PROTOCOL.map.containsKey(a.ACTION))
                // in the case of an unknown symbol, this expression could
                // have been wrongly classified as an action expression,
                // make the error message neutral
                throw new CompilerException(a.getStreamPos(),
                        CompilerException.Code.UNKNOWN,
                        "unknown symbol: " + a.ACTION);
        } else if(expr instanceof AsOtherExpression) {
            /* empty */
        } else if(expr instanceof ConstantExpression) {
            Literal l = expr.getSimpleConstant();
            if(l.type.isBoolean() && l.getBoolean())
                ;
            else
                throw new RuntimeException("constant expression can be only true");
        } else
            throw new RuntimeException("unknown expression");
        return expr;
    }
    /**
     * Finishes a semantic check of an agent system, i.e. tests the parts not
     * tested already by the parser. This includes variables refererred by qualified
     * identifiers and forwardly declared constants.
     * 
     * @param as an agent system
     */
    public static void semanticCheck(AgentSystem as) throws CompilerException {
        // find constants
        for(AsModule m : as.moduleList) {
            List<AbstractExpression> guards = new LinkedList<>();
            for(AbstractExpression e  : m.PROTOCOL.guards)
                guards.add(lookupSymbols(as, m, e));
            m.PROTOCOL.guards.clear();
            m.PROTOCOL.guards.addAll(guards);
            for(AsStatement s : m.STATEMENTS) {
                s.GUARD = lookupSymbols(as, m, s.GUARD);
                if(s.UPDATE_LEFT != null)
                    s.UPDATE_LEFT = ParserUtils.checkVariable(as, s.UPDATE_LEFT);
                s.UPDATE_RIGHT = lookupSymbols(as, m, s.UPDATE_RIGHT);
            }
        }
        for(AsProperty p : as.propertyList)
            new ParsedExpression(as, p.EXPR, false, null);
    }
    /**
     * Checks if there is no name shadowing within a module.
     * 
     * @param as an agent system
     */
    public static void nameShadowCheck(AgentSystem as) throws CompilerException {
        CompilerException errors = new CompilerException();
        for(AsModule m : as.moduleList) {
            Set<String> variables = new TreeSet<>();
            Set<String> actions = new TreeSet<>();
            Set<String> labels = new TreeSet<>();
            for(AsVariable v : m.variableList)
                variables.add(v.NAME);
            for(String a : m.PROTOCOL.map.keySet())
                actions.add(a);
            for(AsStatement s : m.STATEMENTS) {
                if(s.LABEL != null)
                    labels.add(s.LABEL.NAME);
            }
            for(String v : variables)
                if(actions.contains(v))
                    errors.addReport(new CompilerException.Report(m.getStreamPos(),
                            CompilerException.Code.DUPLICATE,
                            "name conflict between a variable and a protocol action: " + v));
            for(String v : variables)
                if(labels.contains(v))
                    errors.addReport(new CompilerException.Report(m.getStreamPos(),
                            CompilerException.Code.DUPLICATE,
                            "name conflict between a variable and an action label: " + v));
            for(String a : actions)
                if(labels.contains(a))
                    errors.addReport(new CompilerException.Report(m.getStreamPos(),
                            CompilerException.Code.DUPLICATE,
                            "name conflict between a protocol action and an action label: " + a));
        }
        if(errors.reportsExist())
            throw errors;
    }
}
