/*
 * ParsedExpression.java
 *
 * Created on Oct 15, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Variable;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary.SemanticCheckFactory;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.solitary.SolitaryExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

import pl.iitis.verics.agent.*;

/**
 * An expression after the semantic check, and suitable for evaluation.
 * 
 * @author Artur Rataj
 */
public class ParsedExpression {
    /**
     * List of knowns in the scope of the expression.
     */
    public final Map<Variable, AsConstant> KNOWNS;
    /**
     * The expression wrapped into a solitary expression.
     */
    public final SolitaryExpression EXPR;
    
    /**
     * Parses an expression, doing a semantic check on it on the same time.
     * 
     * @param as an agent system, to which the expression belons
     * @param expr the expression to parse
     * @param isConst if the expression is expected to not use variables; otherwise,
     * also variables are added to the scope
     * @param semanticFactory factory of <code>SemanticCheck</code>,
     * null for the default
     */
    public ParsedExpression(AgentSystem as, AbstractExpression expr, boolean isConst,
            SemanticCheckFactory semanticFactory) throws CompilerException {
        KNOWNS = as.getKnowns();
        Set<Variable> all = new HashSet<>(KNOWNS.keySet());
        if(!isConst)
            all.addAll(as.getUnknowns());
        EXPR = new SolitaryExpression(expr, all,
                semanticFactory == null ? new SemanticCheckFactory() : semanticFactory,
                new VxsStaticAnalysisFactory(as));
    }
}
