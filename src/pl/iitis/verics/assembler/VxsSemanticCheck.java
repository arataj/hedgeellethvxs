/*
 * VxsSemanticCheck.java
 *
 * Created on Apr 11, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.AbstractPrimaryTransformer;
import pl.gliwice.iitis.hedgeelleth.compiler.SemanticCheck;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Type;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.iitis.verics.agent.AsActionExpression;
import pl.iitis.verics.agent.AsOtherExpression;

/**
 * A semantic check which aims at doing nothing but setting types with
 * the assumption, that the expressions are correct.
 * 
 * @author Artur Rataj
 */
public class VxsSemanticCheck extends SemanticCheck {
    public VxsSemanticCheck(AbstractPrimaryTransformer transformer) throws CompilerException {
        super(null, transformer);
    }
    @Override
    public Object visit(AbstractExpression e) {
        if(e instanceof AsActionExpression || e instanceof AsOtherExpression) {
            e.setResultType(new Type(Type.PrimitiveOrVoid.BOOLEAN));
        } else        
            throw new RuntimeException("unexpected");
        return null;
    }
    @Override
    public Object visit(PrimaryExpression e)  throws CompilerException {
        super.visit(e);
        // e.setResultType(new Type(Type.PrimitiveOrVoid.INT));
        return null;
    }
    @Override
    public Object visit(UnaryExpression e) throws CompilerException {
        e.sub.accept(this);
        Type type;
        if(e.operatorType.isBoolean())
            type = new Type(Type.PrimitiveOrVoid.BOOLEAN);
        else
            type = new Type(Type.PrimitiveOrVoid.INT);
        e.setResultType(type);
        return null;
    }
    @Override
    public Object visit(BinaryExpression e)  throws CompilerException {
        e.left.accept(this);
        e.right.accept(this);
        Type type;
        if(e.operatorType.isBoolean() || e.operatorType.isRelational())
            type = new Type(Type.PrimitiveOrVoid.BOOLEAN);
        else
            type = new Type(Type.PrimitiveOrVoid.INT);
        e.setResultType(type);
        return null;
    }
    @Override
    public Object visit(CallExpression e) throws CompilerException {
        for(AbstractExpression a : e.arg)
            a.accept(this);
        boolean integer = false;
        switch(e.method.name) {
            case "mod":
            case "abs":
                integer = true;
                break;
                
            default:
                throw new RuntimeException("unknown function: " + e.method.name);
        }
        Type type;
        if(integer)
            type = new Type(Type.PrimitiveOrVoid.INT);
        else
            type = new Type(Type.PrimitiveOrVoid.BOOLEAN);
        e.setResultType(type);
        return null;
    }
}
