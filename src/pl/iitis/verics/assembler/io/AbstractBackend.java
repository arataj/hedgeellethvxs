/*
 * AbstractBackend.java
 *
 * Created on Oct 15, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.io;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.iitis.verics.agent.*;
import pl.iitis.verics.assembler.expr.AbstractPrintExpression;

/**
 * <p>An abstract backend of the VXS compiler.</p>
 * 
 * <p>A single agent system is not reentrant by more than a
 * single backend at once.</p>
 * 
 * @author Artur Rataj
 */
public abstract class AbstractBackend {
    /**
     * The file to write to.
     */
    protected final File OUT_FILE;
    /**
     * The agent system to store in the file.
     */
    protected final AgentSystem AS;
    /**
     * Print visitor for the currently printed section, module--sensitive.
     */
    protected AbstractPrintExpression printVisitor = null;
    
    /**
     * Creates a new abstract backend, and calls <code>generate()</code>.
     * 
     * @param as the agent system to serialize
     * @param fileName name of the file to create
     */
    public AbstractBackend(AgentSystem as, String fileName) throws CompilerException {
        OUT_FILE = new File(fileName);
        this.AS = as;
        try(OutputStreamWriter out = new FileWriter(OUT_FILE)) {
            generate(out);
        } catch(CompilerException e) {
            throw e;
        } catch(IOException e) {
            throw new CompilerException(new StreamPos(fileName, -1, 0),
                    CompilerException.Code.IO,
                    "could not write: " + e.getMessage());
        }
    }
    /**
     * Returns a short name of this backend.
     * 
     * @return a name
     */
    abstract public String getName();
    /**
     * Saves an agent system to a stream.
     * 
     * @param out output stream; this method neither opens nor closes it
     */
    abstract protected void generate(OutputStreamWriter out) throws CompilerException;
}
