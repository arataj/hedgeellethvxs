/*
 * MCMASBackend.java
 *
 * Created on Oct 15, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.io;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.ConstantExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

import pl.iitis.verics.agent.*;
import pl.iitis.verics.agent.property.AsProperty;
import pl.iitis.verics.assembler.VxsCLI.FileType;
import pl.iitis.verics.assembler.expr.TextPrintExpression;

/**
 * A backend for generating MCMAS files.
 * 
 * @author Artur Rataj
 */
public class MCMASBackend extends AbstractBackend {
    /**
     * Indent size.
     */
    final String INDENT = "    ";
    /**
     * Creates a new MCMAS backend.
     * 
     * @param as the agent system to serialize
     * @param fileName name of the MCMAS file to create
     */
    public MCMASBackend(AgentSystem as, String fileName) throws CompilerException {
        super(as, fileName);
    }
    @Override
    public String getName() {
        return "MCMAS";
    }
    /**
     * Prints a declaration of a variable.
     * 
     * @param out writer
     * @param v variable
     */
    protected void printVarDeclaration(OutputStreamWriter out, AsVariable v)
            throws IOException, CompilerException {
        out.append(INDENT + v.NAME + ": {");
        Set<AsConstant> set = v.RANGE.set;
        if(set == null)
            throw new CompilerException(v.getStreamPos(), CompilerException.Code.NOT_IMPLEMENTED,
                    getName() + " only supports variables defined with discrete values");
        boolean first = true;
        for(AsConstant c : set) {
            if(c.NAME == null)
                throw new CompilerException(v.getStreamPos(), CompilerException.Code.NOT_IMPLEMENTED,
                        getName() + " only supports named constants");
            if(!first)
                out.append(", ");
            out.append(c.NAME);
            first = false;
        }
        out.append("};\n");
    }
    /**
     * Appends the contents of the section <code>Obsvars</code>.
     * 
     * @param m module
     * @param out output stream to append to
     */
    protected void printObservableVars(AsModule m, OutputStreamWriter out)
            throws CompilerException, IOException {
        List<AsVariable> observable = new LinkedList<>();
        for(AsVariable v : m.usedVars)
            if(v.OWNER != m)
                observable.add(v);
        if(!observable.isEmpty()) {
            out.append("Obsvars:\n");
            for(AsVariable v : observable) {
                printVarDeclaration(out, v);
            }
            out.append("end Obsvars\n");
        }
    }
    /**
     * Appends the contents of the section <code>Vars</code>.
     * 
     * @param m module
     * @param out output stream to append to
     */
    protected void printVars(AsModule m, OutputStreamWriter out)
            throws CompilerException, IOException {
        out.append("Vars:\n");
        for(AsVariable v : m.variableList) {
            printVarDeclaration(out, v);
        }
        out.append("end Vars\n");
    }
    /**
     * Appends the contents of the section <code>Actions</code>.
     * 
     * @param m module
     * @param out output stream to append to
     */
    protected void printActions(AsModule m, OutputStreamWriter out) throws IOException {
        out.append("Actions = {");
        boolean first = true;
        for(String l : m.ACTIONS) {
            if(!first)
                out.append(", ");
            out.append(l);
            first = false;
        }
        out.append("};\n");
    }
    /**
     * Translates an agent name to MCMAS--specific agent name.
     * It only means, that "environment" will be translated to "Environment",
     * other names returns as they are.
     * 
     * @param name name to translate
     * @return MCMAS--specific name
     */
    protected String getAgentName(String name) {
        if(name.equals("environment"))
            return "Environment";
        else
            return name;
    }
    /**
     * Returns a representation of an expression current print visitor--style.
     * 
     * @param expr expression
     * @param expandFormula if to expand formulas
     * @return textual form
     */
    protected String toString(AbstractExpression expr, boolean expandFormula)
            throws CompilerException {
        StringBuilder out = new StringBuilder();
        boolean prevRestoreFormula = AS.toStringRestoreFormulaEnabled;
        AS.toStringRestoreFormulaEnabled = expandFormula;
        String s = (String)printVisitor.print(expr);
        Scanner sc = new Scanner(s).useLocale(Locale.ROOT);
        boolean skipNext = false;
        String modifyNext = null;
        boolean first = true;
        while(sc.hasNext()) {
            String token = sc.next();
//System.out.println("token = " + token);
            if(skipNext) {
                skipNext = false;
                continue;
            }
            if(modifyNext != null) {
                if(modifyNext.equals("!A")) {
                    if(token.equals("G(")) {
                        token = "(!AG(";
                    } else
                        throw new RuntimeException("unknown modification");
                } else
                    throw new RuntimeException("unknown modification");
                modifyNext = null;
            }
            final String ENVIRONMENT = "environment.";
            if(token.startsWith(ENVIRONMENT)) {
                token = "Environment." +
                        token.substring(ENVIRONMENT.length());
            } else
            /*if(token.equals("=="))
                token = "=";
            else if(token.equals("=")) {
                skipNext = true;
                continue;
            } else if(token.equals("&&"))
                token = "and";
            else if(token.equals("||"))
                token = "or";
            else*/ if(token.equals("!A(")) {
                modifyNext = "!A";
                continue;
            }
            if(!first)
                out.append(" ");
            out.append(token);
            first = false;
        }
        AS.toStringRestoreFormulaEnabled = prevRestoreFormula;
        return out.toString();
    }
//    /**
//     * Returns a part of a synchronisation expression with
//     * a statement in another module.
//     * 
//     * @param s a statement in another module
//     * @return a textual part of a synchronisation expression
//     */
//    protected String getSync(AsStatement s) throws IOException {
//        return " and " + getAgentName(s.OWNER.NAME) +
//            ".Action=" +
//            stripIndex(s.OWNER, AsLabel.getKey(s.LABEL));
//    }
    /**
     * Appends the contents of the section <code>Protocol</code>.
     * 
     * @param as agent system
     * @param module module
     * @param out output stream to append to
     */
    protected void printProtocol(AgentSystem as, AsModule module,
            OutputStreamWriter out) throws CompilerException, IOException {
        out.append("Protocol:\n");
        Iterator<AbstractExpression> gI = module.PROTOCOL.guards.iterator();
        Iterator<List<String>> aI = module.PROTOCOL.actions.iterator();
        while(gI.hasNext()) {
            AbstractExpression guard = gI.next();
            List<String> actions = aI.next();
            String g;
            if(guard instanceof ConstantExpression &&
                    guard.getSimpleConstant().type.isBoolean() &&
                    guard.getSimpleConstant().getBoolean()) {
                if(module.variableList.isEmpty() || module.PROTOCOL.guards.size() == 1)
                    g = AsOtherExpression.NAME;
                else {
                    AsVariable v = module.variableList.get(0);
                    g = v.NAME + "=" + v.NAME;
                }
            } else
                g = toString(guard, true);
            out.append(INDENT + g + ": {");
            boolean first = true;
            for(String a : actions) {
                if(!first)
                    out.append(", ");
                out.append(a);
                first = false;
            }
            out.append("};\n");
        }
        out.append("end Protocol\n");
    }
    /**
     * Appends the contents of the section <code>Evolution</code>.
     * 
     * @param as agent system
     * @param module module
     * @param out output stream to append to
     */
    protected void printStatements(AgentSystem as, AsModule module,
            OutputStreamWriter out) throws CompilerException, IOException {
        out.append("Evolution:\n");
        for(AsStatement s : module.STATEMENTS) {
            if(s.LABEL != null)
                throw new CompilerException(module.getStreamPos(), CompilerException.Code.MISSING,
                    getName() + " does not directly support sync labels, " +
                            "they should be converted to guards");
            out.append(INDENT);
            if(s.UPDATE_LEFT != null) {
                out.append(s.UPDATE_LEFT.OWNER == module ? s.UPDATE_LEFT.NAME :
                        s.UPDATE_LEFT.getQualifiedName());
                out.append(" = ");
                out.append(toString(s.UPDATE_RIGHT, true));
            } else {
                if(module.variableList.isEmpty())
                    throw new CompilerException(module.getStreamPos(), CompilerException.Code.MISSING,
                        "evolution but no local variables");
                String name = module.variableList.get(0).NAME;
                out.append(name + " = " + name);
            }
            out.append(" if ");
//            Literal simple = s.GUARD.getSimpleConstant();
//            if(simple != null && simple.getBoolean())
//                out.append("Action = " + AsLabel.SYNC_KEY_NONE);
//            else
                out.append(toString(s.GUARD, true));
            out.append(";\n");
        }
        out.append("end Evolution\n");
    }
//    /**
//     * Appends the contents of the section <code>Protocol</code>.
//     * 
//     * @param protocol true for a short protocol form, false for an evolution form
//     * @param as agent system
//     * @param module module
//     * @param out output stream to append to
//     */
//    protected void printStatements_(boolean protocol,
//            AgentSystem as, AsModule module,
//            OutputStreamWriter out) throws CompilerException, IOException {
//        String sectionName;
//        if(protocol)
//            sectionName = "Protocol";
//        else
//            sectionName = "Evolution";
//        out.append(sectionName + ":\n");
//        List<List<AsConstant>> states = new LinkedList<>();
//        for(AsVariable v : module.variableList) {
//            if(states.isEmpty()) {
//                // first sequence
//                for(AsConstant c : v.RANGE.set) {
//                    List<AsConstant> initial = new LinkedList<>();
//                    initial.add(c);
//                    states.add(initial);
//                }
//            } else {
//                // Cartesian product
//                List<List<AsConstant>> next = new LinkedList<>();
//                for(List<AsConstant> list : states)
//                    for(AsConstant c : v.RANGE.set) {
//                        List<AsConstant> initial = new LinkedList<>(list);
//                        initial.add(c);
//                        next.add(initial);
//                    }
//                states = next;
//            }
//        }
//        Set<AsStatement> used = new HashSet<>();
//        for(List<AsConstant> list : states) {
//            StringBuilder stateStr = new StringBuilder();
//            /*
//            Map<Variable, AsConstant> vars = as.getKnowns();
//            // we check only against the internal state
//            for(AsModule m : as.moduleList)
//                if(m != module)
//                    for(AsVariable v : m.variableList)
//                        vars.put(v.toVariable(), new AsConstant(null, null, new Literal(true)));
//             */
//            Map<Variable, AsConstant> vars = new HashMap<>();
//            Iterator<AsConstant> listI = list.iterator();
//            boolean first = true;
//            for(AsVariable v : module.variableList) {
//                AsConstant c = listI.next();
//                if(!first)
//                    stateStr.append(" and ");
//                stateStr.append(v.NAME + " = " + c.NAME);
//                first = false;
//                vars.put(v.toVariable(), c);
//            }
//            if(protocol)
//                out.append(INDENT + stateStr + ": {");
//            SortedSet<String> labels = new TreeSet<>();
//            for(AsStatement s : module.STATEMENTS) {
//                try {
//                    AsConstant simpleGuard = ParserUtils.toConst(as, s.GUARD, null);
//                    boolean alwaysTrue = simpleGuard != null && simpleGuard.getBoolean();
////if(alwaysTrue)
////    s = s;
////if(ParserUtils.toConst(as, s.GUARD, vars) == null)
////    s = s;
//                    AsConstant guardValue = ParserUtils.toConst(as, s.GUARD, vars);
//                    if(guardValue == null)
//                        throw new CompilerException(s.getStreamPos(), CompilerException.Code.ILLEGAL,
//                                ": a guard uses non-local variable");
//                    if((alwaysTrue && (protocol || !used.contains(s))) ||
//                            (!alwaysTrue && guardValue.getBoolean())) {
//                        used.add(s);
//                        if(protocol)
//                            labels.add(AsLabel.getKey(s.LABEL));
//                        else {
//                            out.append(INDENT);
//                            if(s.UPDATE_LEFT == null) {
//                                String n = module.variableList.get(0).NAME;
//                                out.append(n + " = " + n);
//                            } else {
//                                if(s.UPDATE_LEFT.OWNER == null)
//                                    s.UPDATE_LEFT = ParserUtils.checkVariable(as, s.UPDATE_LEFT);
//                                out.append(s.UPDATE_LEFT.NAME + " = ");
//                                AsConstant update = ParserUtils.toConst(as, s.UPDATE_RIGHT, vars);
//                                if(update == null)
//                                    throw new CompilerException(s.UPDATE_RIGHT.getStreamPos(), CompilerException.Code.ILLEGAL,
//                                        "could not evaluate");
//                                if(update.NAME == null)
//                                    throw new CompilerException(s.UPDATE_RIGHT.getStreamPos(), CompilerException.Code.ILLEGAL,
//                                        getName() + " expects a named constant here");
//                                out.append(update.NAME);
//                            }
//                            out.append(" if" + (alwaysTrue ? "" : " " + stateStr + " and") +
//                                    " Action = " + stripIndex(module, AsLabel.getKey(s.LABEL)));
//                            List<AsStatement> synced = as.getSynced(s.LABEL, module);
//                            for(AsStatement sSynced : synced)
//                                out.append(getSync(sSynced));
//                            if(s.LABEL != null && !as.synchronous) {
//                                SCAN_NEXT_MODULE:
//                                for(AsModule mOther : as.moduleList)
//                                    if(mOther != module) {
//                                        for(AsStatement sSynced : synced)
//                                            if(mOther == sSynced.OWNER)
//                                                continue SCAN_NEXT_MODULE;
//                                        for(AsStatement a : mOther.STATEMENTS)
//                                            if(a.LABEL == null)
//                                                out.append(getSync(a));
//                                    }
//                            }
//                            out.append(";\n");
//                        }
//                    }
//                } catch(ParseException e) {
//                        //e.addReport(new CompilerException.Report(
//                        //        s.GUARD.getStreamPos(), CompilerException.Code.ILLEGAL,
//                        //        "could not evaluate"));
//                        throw e;
//                }
//            }
//            if(protocol) {
//                first = true;
//                for(String l : labels) {
//                    if(!first)
//                        out.append(", ");
//                    out.append(stripIndex(module, l));
//                    first = false;
//                }
//                out.append("};\n");
//            }
//        }
//        out.append("end " + sectionName + "\n");
//    }
    /**
     * Appends the contents of the section <code>Evaluation</code>.
     * This is otherwise a set of formulas.
     * 
     * @param a agent system
     * @param out output stream to append to
     */
    protected void printEvaluation(AgentSystem as, OutputStreamWriter out)
            throws IOException, CompilerException {
        out.append("Evaluation\n");
        for(AsFormula f : as.formulaList) {
            out.append(INDENT + f.NAME + " if ");
            out.append(toString(f.EXPR, false));
            out.append(";\n");
        }
        out.append("end Evaluation\n");
    }
    private String getQualifiedName(AsVariable v) {
        String q = v.getQualifiedName();
        int pos = q.indexOf('.');
        return getAgentName(q.substring(0, pos)) +
                q.substring(pos);
    }
    /**
     * Appends the contents of the section <code>InitStates</code>.
     * 
     * @param a agent system
     * @param out output stream to append to
     */
    protected void printInitStates(AgentSystem as, OutputStreamWriter out)
            throws IOException, CompilerException {
        out.append("InitStates\n");
        StringBuilder expr = new StringBuilder();
        boolean first = true;
        for(AsModule m : AS.moduleList) {
            for(AsVariable v : m.variableList) {
                List<AsConstant> cList = v.INIT.get(v.RANGE, 0);
                if(!cList.isEmpty()) {
                    if(!first)
                        expr.append(" and\n");
                    String q = getQualifiedName(v);
                    expr.append(INDENT);
                    if(cList.size() > 1)
                        expr.append("( ");
    //                AsConstant c = v.INIT.VALUE.values().iterator().next().iterator().next(); // !!!
                    boolean firstC = true;
                    for(AsConstant c : cList) {
                        if(!firstC)
                            expr.append(" or ");
                        expr.append(q + "=" + c.NAME);
                        firstC = false;
                    }
                    if(cList.size() > 1)
                        expr.append(" )");
                    first = false;
                }
            }
        }
        Literal filterConstant = as.initFilter.getSimpleConstant();
        if(filterConstant == null || !filterConstant.getBoolean()) {
            String s;
            if(expr.length() != 0)
                s = "( " + expr.toString() + " ) and (\n";
            else
                s = "";
            s += toString(as.initFilter, true);
            if(expr.length() != 0)
                s += " )";
            expr = new StringBuilder(s);
        }
        out.append(expr.toString());
        out.append(";\n");
        out.append("end InitStates\n");
    }
    /**
     * Appends the contents of the section <code>Formulae</code>.
     * 
     * @param a agent system
     * @param out output stream to append to
     */
    protected void printFormulae(AgentSystem as, OutputStreamWriter out)
            throws IOException, CompilerException {
        out.append("Formulae\n");
        boolean first = true;
        for(AsProperty p : AS.propertyList) {
            out.append(INDENT + toString(p.EXPR, true) + ";\n");
        }
        out.append("end Formulae\n");
    }
    @Override
    protected void generate(OutputStreamWriter out) throws CompilerException {
        try {
            printVisitor = new TextPrintExpression(AS, null, FileType.MCMAS);
            switch(AS.type) {
                case MDP:
                    break;

                default:
                    throw new CompilerException(null, CompilerException.Code.NOT_IMPLEMENTED,
                            "model type not supported by the MCMS backend: " +
                            AS.type.toString());
            }
            for(AsModule m : AS.moduleList) {
                printVisitor = new TextPrintExpression(AS, m, FileType.MCMAS);
                out.append("Agent " + getAgentName(m.NAME) + "\n");
                printObservableVars(m, out);
                printVars(m, out);
                printActions(m, out);
                printProtocol(AS, m, out);
                printStatements(AS, m, out);
                out.append("end Agent\n");
            }
            printVisitor = new TextPrintExpression(AS, null, FileType.MCMAS);
            printEvaluation(AS, out);
            printInitStates(AS, out);
            printFormulae(AS, out);
        } catch(IOException e) {
            throw new CompilerException(null,
                        CompilerException.Code.IO,
                        "could not write to output file: " + e.getMessage());
        }
    }
}
