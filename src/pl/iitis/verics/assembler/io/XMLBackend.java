/*
 * XMLBackend.java
 *
 * Created on Mar 29 2016
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.io;

import java.util.*;
import java.io.*;

import org.jdom2.*;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.output.XMLOutputter;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.AbstractExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.ConstantExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;

import pl.iitis.verics.agent.*;
import pl.iitis.verics.agent.property.AsProperty;
import pl.iitis.verics.assembler.expr.XMLPrintExpression;

/**
 * A backend for generating XML files.
 * 
 * @author Artur Rataj
 */
public class XMLBackend extends AbstractBackend {
    /**
     * Model.
     */
    public final static String E_MODEL = "model";
    /**
     * Class.
     */
    public final static String A_CLASS = "class";
    /**
     * If observable; "true" for MDP, "partial" for POMDP
     */
    public final static String A_OBSERVABLE = "observable";
    /**
     * If synchronised. True for IS, false for IIS.
     */
    public final static String A_SYNC = "sync";
    /**
     * Constant.
     */
    public final static String A_CONST = "const";
    /**
     * Module.
     */
    public final static String E_MODULE = "module";
    /**
     * Formula.
     */
    public final static String E_FORMULA = "formula";
    /**
     * Global constants.
     */
    public final static String E_CONSTANTS = "constants";
    /**
     * Module variables.
     */
    public final static String E_VARS = "variables";
    /**
     * Observables, i.e. variables read within expressions.
     */
    public final static String E_OBSERVABLE = "observable";
    /**
     * Local variables.
     */
    public final static String E_LOCAL = "local";
    /**
     * Variables external for a module.
     */
    public final static String E_EXTERNAL = "external";
    /**
     * Module actions.
     */
    public final static String E_ACTIONS = "actions";
    /**
     * Module protocol.
     */
    public final static String E_PROTOCOL = "protocol";
    /**
     * Transitions.
     */
    public final static String E_TRANSITIONS = "transitions";
    /**
     * A single statement.
     */
    public final static String E_STATEMENT = "statement";
    /**
     * A guard within a statement.
     */
    public final static String E_GUARD = "guard";
    /**
     * An update within a statement.
     */
    public final static String E_UPDATE = "update";
    /**
     * Formulas, or valuation.
     */
    public final static String E_FORMULAS = "formulas";
    /**
     * Init states.
     */
    public final static String E_INIT_STATES = "init_states";
    /**
     * Properties.
     */
    public final static String E_PROPERTY = "property";
    /**
     * A synchronising label.
     */
    public final static String A_LABEL = "label";
    /**
     * A set of actions.
     */
    public final static String A_ACTIONS = "actions";
    /**
     * A declaration.
     */
    public final static String E_DECLARATION = "declaration";
    /**
     * A domain of a variable.
     */
    public final static String E_DOMAIN = "domain";
    /**
     * Initial values, with conjunction with the init expression..
     */
    public final static String E_INIT = "init";
    /**
     * Source. Used in the case of the init_states equation.
     */
    public final static String A_SOURCE = "source";
    /**
     * A type.
     */
    public final static String A_TYPE = "type";
    
    /**
     * Indent size.
     */
    final String INDENT = "    ";
    /**
     * Creates a new MCMAS backend.
     * 
     * @param as the agent system to serialize
     * @param fileName name of the MCMAS file to create
     */
    public XMLBackend(AgentSystem as, String fileName) throws CompilerException {
        super(as, fileName);
    }
    @Override
    public String getName() {
        return "XML";
    }
    /**
     * Returns an element which represents a variable declaration.
     * 
     * @param v variable
     * @return element
     */
    protected Element getVar(AsVariable v) throws CompilerException {
        Element var = new Element(E_DECLARATION);
        var.setAttribute(new Attribute(XMLPrintExpression.A_NAME, v.getQualifiedName()));
        Set<AsConstant> set = v.RANGE.set;
        if(set == null)
            throw new CompilerException(v.getStreamPos(), CompilerException.Code.NOT_IMPLEMENTED,
                    getName() + " only supports variables defined with discrete values");
        Element domain = new Element(E_DOMAIN);
        for(AsConstant c : set) {
            if(c.NAME == null)
                throw new CompilerException(v.getStreamPos(), CompilerException.Code.NOT_IMPLEMENTED,
                        getName() + " only supports named constants");
            Element e = new Element(XMLPrintExpression.E_CONSTANT_EXPR);
            e.setText(c.NAME);
            domain.addContent(e);
        }
        var.addContent(domain);
        Element init = new Element(E_INIT);
        if(v.INIT.SPACE.size() != 1)
            throw new CompilerException(v.getStreamPos(), CompilerException.Code.NOT_IMPLEMENTED,
                    getName() + " only supports a single init space");
        for(AsConstant c : v.INIT.VALUE.values().iterator().next()) {
            if(c.NAME == null)
                throw new CompilerException(v.getStreamPos(), CompilerException.Code.NOT_IMPLEMENTED,
                        getName() + " only supports named constants");
            Element e = new Element(XMLPrintExpression.E_CONSTANT_EXPR);
            e.setText(c.NAME);
            init.addContent(e);
        }
        var.addContent(init);
        return var;
    }
    /**
     * Appends the contents of the section <code>Vars</code>.
     * 
     * @param m module
     * @return element
     */
    protected Element printObservableVars(AsModule m) throws CompilerException {
        Element vars = new Element(E_OBSERVABLE);
        List<AsVariable> local = new LinkedList<>();
        List<AsVariable> external = new LinkedList<>();
        for(AsVariable v : m.usedVars)
            if(v.OWNER == m)
                local.add(v);
            else
                external.add(v);
        Element locals = new Element(E_LOCAL);
        for(AsVariable v : local) {
            Element var = new Element(XMLPrintExpression.E_VAR);
            var.addContent(v.getQualifiedName());
            locals.addContent(var);
        }
        Element externals = new Element(E_EXTERNAL);
        for(AsVariable v : external) {
            Element var = new Element(XMLPrintExpression.E_VAR);
            var.addContent(v.getQualifiedName());
            externals.addContent(var);
        }
        vars.addContent(locals);
        vars.addContent(externals);
        return vars;
    }
    /**
     * Appends the contents of the section <code>Vars</code>.
     * 
     * @param m module
     * @return element
     */
    protected Element printVars(AsModule m) throws CompilerException {
        Element vars = new Element(E_VARS);
        for(AsVariable v : m.variableList) {
            vars.addContent(getVar(v));
        }
        return vars;
    }
    /**
     * Appends the contents of the section <code>Actions</code>.
     * 
     * @param m module
     * @return element
     */
    protected Element printActions(AsModule m) {
        Element actions = new Element(E_ACTIONS);
        for(String l : m.ACTIONS) {
            Element action = new Element(XMLPrintExpression.E_ACTION_EXPR);
            action.setText(l);
            actions.addContent(action);
        }
        return actions;
    }
    /**
     * Returns a representation of an expression current print visitor--style.
     * 
     * @param expr expression
     * @param restoreFormula if to restore formulas
     * @return textual form
     */
    protected Element toElement(AbstractExpression expr, boolean restoreFormula)
            throws CompilerException {
        boolean prevRestoreFormula = AS.toStringRestoreFormulaEnabled;
        AS.toStringRestoreFormulaEnabled = restoreFormula;
        Element node = (Element)printVisitor.print(expr);
        AS.toStringRestoreFormulaEnabled = prevRestoreFormula;
        return node;
    }
    /**
     * Appends the contents of the section <code>Protocol</code>.
     * 
     * @param as agent system
     * @param module module
     * @return element
     */
    protected Element printProtocol(AgentSystem as, AsModule module)
            throws CompilerException {
        Element protocol = new Element(E_PROTOCOL);
        Iterator<AbstractExpression> gI = module.PROTOCOL.guards.iterator();
        Iterator<List<String>> aI = module.PROTOCOL.actions.iterator();
        while(gI.hasNext()) {
            AbstractExpression g = gI.next();
            List<String> actionNames = aI.next();
            Element set = new Element(E_STATEMENT);
            Element guard = new Element(E_GUARD);
            Element h;
            if(g instanceof ConstantExpression &&
                    g.getSimpleConstant().type.isBoolean() &&
                    g.getSimpleConstant().getBoolean()) {
                h = new Element("const");
                h.setText("1");
            } else
                h = toElement(g, true);
            guard.addContent(h);
            set.addContent(guard);
            Element actions = new Element(E_ACTIONS);
            for(String a : actionNames) {
                Element action = new Element(XMLPrintExpression.E_ACTION_EXPR);
                action.setText(a);
                actions.addContent(action);
            }
            set.addContent(actions);
            protocol.addContent(set);
        }
        return protocol;
    }
    /**
     * Appends the contents of the section <code>Transitions</code>.
     * 
     * @param as agent system
     * @param module module
     * @return element
     */
    protected Element printStatements(AgentSystem as, AsModule module)
            throws CompilerException {
        Element statements = new Element(E_TRANSITIONS);
        for(AsStatement s : module.STATEMENTS) {
            Element statement = new Element(E_STATEMENT);
            if(s.LABEL != null)
                statement.setAttribute(new Attribute(A_LABEL, s.LABEL.NAME));
            Literal simple = s.GUARD.getSimpleConstant();
            Element guard;
//            if(simple != null && simple.getBoolean()) {
//                guard = new Element(XMLPrintExpression.E_ACTION_EXPR);
//                guard.setText(AsLabel.SYNC_KEY_NONE);
//            } else
                guard = toElement(s.GUARD, true);
            statement.addContent(new Element(E_GUARD).addContent(guard));
            if(s.UPDATE_LEFT != null) {
                Element update = new Element(XMLPrintExpression.E_ASSIGNMENT_EXPR);
                Element lvalue = new Element(XMLPrintExpression.E_VAR);
                lvalue.setText(s.UPDATE_LEFT.getQualifiedName());
                update.addContent(lvalue);
                update.addContent(toElement(s.UPDATE_RIGHT, true));
                statement.addContent(new Element(E_UPDATE).addContent(update));
            }
            statements.addContent(statement);
        }
        return statements;
    }
    /**
     * Appends the contents of the section <code>Formulas</code>.
     * 
     * @param a agent system
     * @return element
     */
    protected Element printFormulas(AgentSystem as) throws CompilerException {
        Element formulas = new Element(E_FORMULAS);
        for(AsFormula f : as.formulaList) {
            Element formula = new Element(E_FORMULA);
            formula.setAttribute(new Attribute(XMLPrintExpression.A_NAME, f.NAME));
            formula.addContent(toElement(f.EXPR, false));
            formulas.addContent(formula);
        }
        return formulas;
    }
    private String getQualifiedName(AsVariable v) {
        String q = v.getQualifiedName();
        int pos = q.indexOf('.');
        return q.substring(0, pos) + q.substring(pos);
    }
    /**
     * Appends the contents of the section <code>InitStates</code>.
     * 
     * @param a agent system
     * @return element
     */
    protected Element printInitStates(AgentSystem as) throws CompilerException {
        Element initStates = new Element(E_INIT_STATES);
        List<Element> constraints = new LinkedList<>();
        for(AsModule m : AS.moduleList) {
            for(AsVariable v : m.variableList) {
                String q = getQualifiedName(v);
                //AsConstant c = v.INIT.VALUE.values().iterator().next().iterator().next(); // !!!
                Element alternative = null;
                List<AsConstant> cList = v.INIT.get(v.RANGE, 0);
                for(AsConstant c : cList) {
                    Element equation = new Element(XMLPrintExpression.E_BINARY_EXPR);
                    equation.setAttribute(new Attribute(XMLPrintExpression.A_OP,
                            XMLPrintExpression.translateOpName(
                                "==", true)));
                    Element lvalue = new Element(XMLPrintExpression.E_VAR);
                    lvalue.setText(q);
                    equation.addContent(lvalue);
                    Element rvalue = new Element(XMLPrintExpression.E_CONSTANT_EXPR);
                    rvalue.setText(c.NAME);
                    equation.addContent(rvalue);
                    if(alternative == null)
                        alternative = equation;
                    else {
                        Element newAlternative = new Element(XMLPrintExpression.E_BINARY_EXPR);
                        newAlternative.setAttribute(new Attribute(XMLPrintExpression.A_OP,
                                XMLPrintExpression.translateOpName(
                                    "||", true)));
                        newAlternative.addContent(alternative);
                        newAlternative.addContent(equation);
                        alternative = newAlternative;
                    }
                }
                constraints.add(alternative);
            }
        }
        Literal filterConstant = as.initFilter.getSimpleConstant();
        boolean customInitEquation = filterConstant == null || !filterConstant.getBoolean();
        initStates.setAttribute(A_SOURCE, customInitEquation ?
                "declaration,initEq" : "declaration");
        if(customInitEquation)
            constraints.add(toElement(as.initFilter, true));
        Element conjunction = null;
        for(Element c : constraints) {
            if(conjunction == null)
                conjunction = c;
            else {
                Element newConjunction = new Element(XMLPrintExpression.E_BINARY_EXPR);
                newConjunction.setAttribute(new Attribute(XMLPrintExpression.A_OP,
                        XMLPrintExpression.translateOpName(
                            "&&", true)));
                newConjunction.addContent(conjunction);
                newConjunction.addContent(c);
                conjunction = newConjunction;
            }
        }
        if(conjunction != null)
            initStates.addContent(conjunction);
        return initStates;
    }
    /**
     * Appends the contents of the sections <code>Property</code>.
     * 
     * @param a agent system
     * @return elements
     */
    protected List<Element> printProperties(AgentSystem as) throws CompilerException {
        List<Element> out = new LinkedList<>();
        for(AsProperty p : AS.propertyList) {
            Element property = new Element(E_PROPERTY);
            property.setAttribute(new Attribute(A_CLASS, p.TYPE.toString()));
            property.setAttribute(new Attribute(XMLPrintExpression.A_NAME, p.NAME));
            property.addContent(toElement(p.EXPR, true));
            out.add(property);
        }
        return out;
    }
    @Override
    protected void generate(OutputStreamWriter out) throws CompilerException {
        Document doc = new Document();
        printVisitor = new XMLPrintExpression(AS, null);
        switch(AS.type) {
            case MDP:
                break;

            default:
                throw new CompilerException(null, CompilerException.Code.NOT_IMPLEMENTED,
                        "model type not supported by the XML backend: " +
                        AS.type.toString());
        }
        Element root = new Element(E_MODEL);
        root.setAttribute(new Attribute(A_CLASS, AS.type.toString().toLowerCase()));
        root.setAttribute(new Attribute(XMLPrintExpression.A_NAME, AS.NAME));
        root.setAttribute(new Attribute(A_OBSERVABLE, AS.fullyObservable ? "true" : "partial"));
        root.setAttribute(new Attribute(A_SYNC, "" + AS.synchronous));
        doc.addContent(root);
        printConstants(root);
        printModules(root);
        printVisitor = new XMLPrintExpression(AS, null);
        root.addContent(printFormulas(AS));
        root.addContent(printInitStates(AS));
        root.addContent(printProperties(AS));
        try {
            XMLOutputter outp = new XMLOutputter(Format.getPrettyFormat().
                    setTextMode(TextMode.PRESERVE));
            outp.setFormat(Format.getPrettyFormat());
            outp.output(doc, out);
        } catch(IOException e) {
            throw new CompilerException(null,
                        CompilerException.Code.IO,
                        "could not write to output file: " + e.getMessage());
        }
    }
    /**
     * Prints all constants.
     * 
     * @param root root element of the document to print
     */
    void printConstants(Element root) throws CompilerException {
        Element constants = new Element(E_CONSTANTS);
        for(AsConstant c : AS.constantList) {
            Element constant = new Element(E_DECLARATION);
            constant.setAttribute(XMLPrintExpression.A_NAME, c.NAME);
            constant.setAttribute(XMLPrintExpression.A_TYPE, c.getTypeId());
            constant.setAttribute(XMLPrintExpression.A_VALUE, c.getValueStr());
            constants.addContent(constant);
        }
        root.addContent(constants);
    }
    void printModules(Element root) throws CompilerException {
        for(AsModule m : AS.moduleList) {
            printVisitor = new XMLPrintExpression(AS, m);
            Element module = new Element(E_MODULE);
            module.setAttribute(XMLPrintExpression.A_NAME, m.NAME);
            module.addContent(printVars(m));
            module.addContent(printObservableVars(m));
            module.addContent(printActions(m));
            module.addContent(printProtocol(AS, m));
            module.addContent(printStatements(AS, m));
            root.addContent(module);
        }
    }
}
