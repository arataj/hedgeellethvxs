/*
 * PropertyPreprocessor.java
 *
 * Created on Oct 18, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler;

import java.util.*;
import java.util.regex.*;

/**
 * Divides combos of operators.
 * 
 * @author Artur Rataj
 */
public class PropertyPreprocessor {
    /**
     * Original VXSP, without the property operators modified.
     */
    final String VXSP;
    
    /**
     * Converts properties.
     * 
     * @param vxsp a VXSP code
     */
    public PropertyPreprocessor(String vxsp) {
        VXSP = vxsp;
    }
    /**
     * Attemps to split a group of letters into whitespace--separated operators.
     * 
     * @param id identifier to analyze
     * @return a set of whitespace--separated operators or null if the split has been
     * unsuccesfull
     */
    protected String split(String id) {
        StringBuilder out = new StringBuilder();
        int size = id.length();
        if((size&1) != 0)
            return null;
        for(int i = 0; i < size; i += 2) {
            char c1 = id.charAt(i);
            char c2 = id.charAt(i + 1);
            if(!((c1 == 'A' || c1 == 'E') && (c2 == 'X' || c2 == 'F' || c2 == 'G')))
                return null;
            if(i != 0)
                out.append(" ");
            out.append(c1 + " " + c2);
        }
        return out.toString();
    }
    /**
     * Returns a string with modified properties.
     * 
     * @return a VXSP code ready for parsing
     */
    public String process() {
        final String PROPERTY = "property";
        StringBuilder out = new StringBuilder();
        int pos = 0;
        while((pos = VXSP.indexOf(PROPERTY, pos)) != -1) {
            if(
                    (pos == 0 || !Character.isAlphabetic(VXSP.charAt(pos - 1))) &&
                    (pos == VXSP.length() - 1 || !Character.isAlphabetic(VXSP.charAt(pos + PROPERTY.length())))) {
                out.append(VXSP.substring(0, pos));
                break;
            }
            pos = pos + 1;
        }
        if(out.length() == 0)
            return VXSP;
        else {
            String p = VXSP.substring(pos);
            int prevPos = 0;
            Pattern re = Pattern.compile("\\w+");
            Matcher matcher = re.matcher(p);
            while(matcher.find()) {
                String id = matcher.group();
                String split = split(id);
                if(split == null)
                    split = id;
                pos = matcher.start();
                out.append(p.substring(prevPos, pos));
                out.append(split);
                prevPos = pos + id.length();
            }
            out.append(p.substring(prevPos));
            return out.toString();
        }
    }
}
