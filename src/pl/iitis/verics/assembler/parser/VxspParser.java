// Generated from Vxsp.g4 by ANTLR 4.5

    package pl.iitis.verics.assembler.parser;

    import java.util.*;
    import java.io.*;

    import org.antlr.v4.runtime.Token;
    
    import pl.gliwice.iitis.hedgeelleth.compiler.util.CompilerUtils;
    import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
    import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
    import pl.gliwice.iitis.hedgeelleth.compiler.tree.*;
    import pl.gliwice.iitis.hedgeelleth.compiler.tree.statement.expression.*;
    import pl.iitis.verics.agent.*;
    import pl.iitis.verics.agent.property.*;
    import pl.iitis.verics.assembler.ParserUtils;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class VxspParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, AGENT=5, CLASS=6, CONST=7, COMMON=8, DEF=9, 
		ENVIRONMENT=10, FALSE=11, INIT=12, MODULE=13, PROPERTY=14, PROTOCOL=15, 
		R=16, TRUE=17, Z=18, PLUS=19, MINUS=20, MULT=21, DIV=22, PERCENT=23, EQUAL=24, 
		LESS=25, GREATER=26, LESS_EQ=27, GREATER_EQ=28, INEQUAL=29, RARROW=30, 
		DARROW=31, ASSIGN=32, LAND=33, LOR=34, LNOT=35, COMMA=36, COLON=37, SEMICOLON=38, 
		DOT=39, LPAREN=40, RPAREN=41, LBRACE=42, RBRACE=43, LSQUARE=44, RSQUARE=45, 
		ELLIPSIS=46, LTL=47, LTLK=48, LTLKD=49, ELTL=50, ELTLK=51, ELTLKD=52, 
		ECTL=53, ECTLK=54, ECTLKD=55, ACTL=56, ACTLK=57, ACTLKD=58, CTL=59, KD=60, 
		A=61, E=62, F=63, G=64, U=65, X=66, AF=67, EF=68, AG=69, EG=70, AX=71, 
		EX=72, Eg=73, Dg=74, Cg=75, Kh=76, K=77, Oc=78, IntegerLiteral=79, FloatingPointLiteral=80, 
		BooleanLiteral=81, CharacterLiteral=82, StringLiteral=83, Identifier=84, 
		WS=85, COMMENT=86, LINE_COMMENT=87, ErrorCharacter=88;
	public static final int
		RULE_compilationUnit = 0, RULE_clazz = 1, RULE_globalConstants = 2, RULE_globalConstant = 3, 
		RULE_modulesOrFormulas = 4, RULE_module = 5, RULE_stateVarDeclaration = 6, 
		RULE_commonDef = 7, RULE_constRangeList = 8, RULE_constRange = 9, RULE_constBoundary = 10, 
		RULE_initValueSpaces = 11, RULE_initValueSpace = 12, RULE_initValueSet = 13, 
		RULE_initValueRange = 14, RULE_protocolDeclaration = 15, RULE_statement = 16, 
		RULE_variableRef = 17, RULE_initFilter = 18, RULE_constExpression = 19, 
		RULE_expression = 20, RULE_booleanExpression = 21, RULE_relationalExpression = 22, 
		RULE_addExpression = 23, RULE_multExpression = 24, RULE_unaryExpression = 25, 
		RULE_primaryExpression = 26, RULE_formula = 27, RULE_properties = 28, 
		RULE_valueOrFormula = 29, RULE_coalitionGroup = 30, RULE_property = 31, 
		RULE_propertyLTLStatement = 32, RULE_propertyLTL = 33, RULE_propertyLTL_Xi = 34, 
		RULE_propertyLTL_Xi_sub = 35, RULE_propertyLTLKStatement = 36, RULE_propertyLTLK = 37, 
		RULE_propertyLTLK_Xi = 38, RULE_propertyLTLK_Xi_sub = 39, RULE_propertyLTLK_Xi_super = 40, 
		RULE_propertyLTLK_Xi_sub_super = 41, RULE_propertyLTLKDStatement = 42, 
		RULE_propertyLTLKD = 43, RULE_propertyLTLKD_Xi = 44, RULE_propertyLTLKD_Xi_sub = 45, 
		RULE_propertyLTLKD_Xi_super = 46, RULE_propertyLTLKD_Xi_sub_super = 47, 
		RULE_propertyACTLStatement = 48, RULE_propertyACTL = 49, RULE_propertyACTL_sub = 50, 
		RULE_propertyACTLKStatement = 51, RULE_propertyACTLK = 52, RULE_propertyACTLK_sub = 53, 
		RULE_propertyACTLKDStatement = 54, RULE_propertyACTLKD = 55, RULE_propertyACTLKD_sub = 56, 
		RULE_propertyACTLAStatement = 57, RULE_propertyACTLA = 58, RULE_propertyACTLA_sub = 59, 
		RULE_propertyACTLA_Xi = 60, RULE_propertyACTLA_Psi = 61, RULE_propertyACTLA_Psi_sub = 62, 
		RULE_propertyACTLAKStatement = 63, RULE_propertyACTLAK = 64, RULE_propertyACTLAK_sub = 65, 
		RULE_propertyACTLAK_Xi = 66, RULE_propertyACTLAK_Xi_super = 67, RULE_propertyACTLAK_Psi = 68, 
		RULE_propertyACTLAK_Psi_sub = 69, RULE_propertyACTLAKDStatement = 70, 
		RULE_propertyACTLAKD = 71, RULE_propertyACTLAKD_sub = 72, RULE_propertyACTLAKD_Xi = 73, 
		RULE_propertyACTLAKD_Xi_super = 74, RULE_propertyACTLAKD_Psi = 75, RULE_propertyACTLAKD_Psi_sub = 76, 
		RULE_propertyELTLStatement = 77, RULE_propertyELTL = 78, RULE_propertyELTL_Xi = 79, 
		RULE_propertyELTL_Xi_sub = 80, RULE_propertyELTLKStatement = 81, RULE_propertyELTLK = 82, 
		RULE_propertyELTLK_Xi = 83, RULE_propertyELTLK_Xi_sub = 84, RULE_propertyELTLK_Xi_super = 85, 
		RULE_propertyELTLK_Xi_sub_super = 86, RULE_propertyELTLKDStatement = 87, 
		RULE_propertyELTLKD = 88, RULE_propertyELTLKD_Xi = 89, RULE_propertyELTLKD_Xi_sub = 90, 
		RULE_propertyELTLKD_Xi_super = 91, RULE_propertyELTLKD_Xi_sub_super = 92, 
		RULE_propertyECTLStatement = 93, RULE_propertyECTL = 94, RULE_propertyECTL_sub = 95, 
		RULE_propertyECTLKStatement = 96, RULE_propertyECTLK = 97, RULE_propertyECTLK_sub = 98, 
		RULE_propertyECTLKDStatement = 99, RULE_propertyECTLKD = 100, RULE_propertyECTLKD_sub = 101, 
		RULE_propertyECTLAStatement = 102, RULE_propertyECTLA = 103, RULE_propertyECTLA_sub = 104, 
		RULE_propertyECTLA_Xi = 105, RULE_propertyECTLA_Psi = 106, RULE_propertyECTLA_Psi_sub = 107, 
		RULE_propertyECTLAKStatement = 108, RULE_propertyECTLAK = 109, RULE_propertyECTLAK_sub = 110, 
		RULE_propertyECTLAK_Xi = 111, RULE_propertyECTLAK_Xi_super = 112, RULE_propertyECTLAK_Psi = 113, 
		RULE_propertyECTLAK_Psi_sub = 114, RULE_propertyECTLAKDStatement = 115, 
		RULE_propertyECTLAKD = 116, RULE_propertyECTLAKD_sub = 117, RULE_propertyECTLAKD_Xi = 118, 
		RULE_propertyECTLAKD_Xi_super = 119, RULE_propertyECTLAKD_Psi = 120, RULE_propertyECTLAKD_Psi_sub = 121, 
		RULE_propertyCTLAStatement = 122, RULE_propertyCTLA = 123, RULE_propertyCTLA_sub = 124, 
		RULE_propertyCTLA_Xi = 125, RULE_propertyCTLA_Psi = 126, RULE_propertyCTLA_Psi_sub = 127, 
		RULE_propertyCTLAKStatement = 128, RULE_propertyCTLAK = 129, RULE_propertyCTLAK_sub = 130, 
		RULE_propertyCTLAK_Xi = 131, RULE_propertyCTLAK_Xi_super = 132, RULE_propertyCTLAK_Psi = 133, 
		RULE_propertyCTLAK_Psi_sub = 134, RULE_propertyCTLAKDStatement = 135, 
		RULE_propertyCTLAKD = 136, RULE_propertyCTLAKD_sub = 137, RULE_propertyCTLAKD_Xi = 138, 
		RULE_propertyCTLAKD_Xi_super = 139, RULE_propertyCTLAKD_Psi = 140, RULE_propertyCTLAKD_Psi_sub = 141, 
		RULE_qualifiedIdentifier = 142, RULE_number = 143, RULE_integerValue = 144, 
		RULE_doubleValue = 145, RULE_literal = 146, RULE_identifierList = 147, 
		RULE_qualifiedIdentifierEnv = 148, RULE_expressionList = 149;
	public static final String[] ruleNames = {
		"compilationUnit", "clazz", "globalConstants", "globalConstant", "modulesOrFormulas", 
		"module", "stateVarDeclaration", "commonDef", "constRangeList", "constRange", 
		"constBoundary", "initValueSpaces", "initValueSpace", "initValueSet", 
		"initValueRange", "protocolDeclaration", "statement", "variableRef", "initFilter", 
		"constExpression", "expression", "booleanExpression", "relationalExpression", 
		"addExpression", "multExpression", "unaryExpression", "primaryExpression", 
		"formula", "properties", "valueOrFormula", "coalitionGroup", "property", 
		"propertyLTLStatement", "propertyLTL", "propertyLTL_Xi", "propertyLTL_Xi_sub", 
		"propertyLTLKStatement", "propertyLTLK", "propertyLTLK_Xi", "propertyLTLK_Xi_sub", 
		"propertyLTLK_Xi_super", "propertyLTLK_Xi_sub_super", "propertyLTLKDStatement", 
		"propertyLTLKD", "propertyLTLKD_Xi", "propertyLTLKD_Xi_sub", "propertyLTLKD_Xi_super", 
		"propertyLTLKD_Xi_sub_super", "propertyACTLStatement", "propertyACTL", 
		"propertyACTL_sub", "propertyACTLKStatement", "propertyACTLK", "propertyACTLK_sub", 
		"propertyACTLKDStatement", "propertyACTLKD", "propertyACTLKD_sub", "propertyACTLAStatement", 
		"propertyACTLA", "propertyACTLA_sub", "propertyACTLA_Xi", "propertyACTLA_Psi", 
		"propertyACTLA_Psi_sub", "propertyACTLAKStatement", "propertyACTLAK", 
		"propertyACTLAK_sub", "propertyACTLAK_Xi", "propertyACTLAK_Xi_super", 
		"propertyACTLAK_Psi", "propertyACTLAK_Psi_sub", "propertyACTLAKDStatement", 
		"propertyACTLAKD", "propertyACTLAKD_sub", "propertyACTLAKD_Xi", "propertyACTLAKD_Xi_super", 
		"propertyACTLAKD_Psi", "propertyACTLAKD_Psi_sub", "propertyELTLStatement", 
		"propertyELTL", "propertyELTL_Xi", "propertyELTL_Xi_sub", "propertyELTLKStatement", 
		"propertyELTLK", "propertyELTLK_Xi", "propertyELTLK_Xi_sub", "propertyELTLK_Xi_super", 
		"propertyELTLK_Xi_sub_super", "propertyELTLKDStatement", "propertyELTLKD", 
		"propertyELTLKD_Xi", "propertyELTLKD_Xi_sub", "propertyELTLKD_Xi_super", 
		"propertyELTLKD_Xi_sub_super", "propertyECTLStatement", "propertyECTL", 
		"propertyECTL_sub", "propertyECTLKStatement", "propertyECTLK", "propertyECTLK_sub", 
		"propertyECTLKDStatement", "propertyECTLKD", "propertyECTLKD_sub", "propertyECTLAStatement", 
		"propertyECTLA", "propertyECTLA_sub", "propertyECTLA_Xi", "propertyECTLA_Psi", 
		"propertyECTLA_Psi_sub", "propertyECTLAKStatement", "propertyECTLAK", 
		"propertyECTLAK_sub", "propertyECTLAK_Xi", "propertyECTLAK_Xi_super", 
		"propertyECTLAK_Psi", "propertyECTLAK_Psi_sub", "propertyECTLAKDStatement", 
		"propertyECTLAKD", "propertyECTLAKD_sub", "propertyECTLAKD_Xi", "propertyECTLAKD_Xi_super", 
		"propertyECTLAKD_Psi", "propertyECTLAKD_Psi_sub", "propertyCTLAStatement", 
		"propertyCTLA", "propertyCTLA_sub", "propertyCTLA_Xi", "propertyCTLA_Psi", 
		"propertyCTLA_Psi_sub", "propertyCTLAKStatement", "propertyCTLAK", "propertyCTLAK_sub", 
		"propertyCTLAK_Xi", "propertyCTLAK_Xi_super", "propertyCTLAK_Psi", "propertyCTLAK_Psi_sub", 
		"propertyCTLAKDStatement", "propertyCTLAKD", "propertyCTLAKD_sub", "propertyCTLAKD_Xi", 
		"propertyCTLAKD_Xi_super", "propertyCTLAKD_Psi", "propertyCTLAKD_Psi_sub", 
		"qualifiedIdentifier", "number", "integerValue", "doubleValue", "literal", 
		"identifierList", "qualifiedIdentifierEnv", "expressionList"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'sync'", "'other'", "'<>'", "'%+'", "'agent'", "'class'", "'const'", 
		"'common'", "'def'", "'environment'", "'false'", "'init'", "'module'", 
		"'property'", "'protocol'", "'R'", "'true'", "'Z'", "'+'", "'-'", "'*'", 
		"'/'", "'%'", "'=='", "'<'", "'>'", "'<='", "'>='", "'!='", "'->'", "'<->'", 
		"':='", "'&&'", "'||'", "'!'", "','", "':'", "';'", "'.'", "'('", "')'", 
		"'{'", "'}'", "'['", "']'", "'...'", "'LTL'", "'LTLK'", "'LTLKD'", "'ELTL'", 
		"'ELTLK'", "'ELTLKD'", "'ECTL'", "'ECTLK'", "'ECTLKD'", "'ACTL'", "'ACTLK'", 
		"'ACTLKD'", "'CTL'", "'KD'", "'A'", "'E'", "'F'", "'G'", "'U'", "'X'", 
		"'AF'", "'EF'", "'AG'", "'EG'", "'AX'", "'EX'", "'Eg'", "'Dg'", "'Cg'", 
		"'Kh'", "'K'", "'Oc'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, "AGENT", "CLASS", "CONST", "COMMON", "DEF", 
		"ENVIRONMENT", "FALSE", "INIT", "MODULE", "PROPERTY", "PROTOCOL", "R", 
		"TRUE", "Z", "PLUS", "MINUS", "MULT", "DIV", "PERCENT", "EQUAL", "LESS", 
		"GREATER", "LESS_EQ", "GREATER_EQ", "INEQUAL", "RARROW", "DARROW", "ASSIGN", 
		"LAND", "LOR", "LNOT", "COMMA", "COLON", "SEMICOLON", "DOT", "LPAREN", 
		"RPAREN", "LBRACE", "RBRACE", "LSQUARE", "RSQUARE", "ELLIPSIS", "LTL", 
		"LTLK", "LTLKD", "ELTL", "ELTLK", "ELTLKD", "ECTL", "ECTLK", "ECTLKD", 
		"ACTL", "ACTLK", "ACTLKD", "CTL", "KD", "A", "E", "F", "G", "U", "X", 
		"AF", "EF", "AG", "EG", "AX", "EX", "Eg", "Dg", "Cg", "Kh", "K", "Oc", 
		"IntegerLiteral", "FloatingPointLiteral", "BooleanLiteral", "CharacterLiteral", 
		"StringLiteral", "Identifier", "WS", "COMMENT", "LINE_COMMENT", "ErrorCharacter"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Vxsp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	    /**
	     * Name of current parsed stream.
	     */
	    String streamName;
	    /**
	     * Errors. After the parsing, the parser's own parse exceptions are
	     * appended here.
	     */
	    public ParseException errors = new ParseException();
	    /**
	     * If the latest (top--most is more recent) constExpression declared a
	     * new global constant, then here is its value. Otherwise null.
	     */
	    AsConstant newlyDeclaredConstant = null;

	    public void setStreamName(String streamName) {
	        this.streamName = streamName;
	    }
	    public String getStreamName() {
	        return streamName;
	    }
	    public StreamPos getStreamPos(Token token) {
	        return new StreamPos(streamName, token);
	    }
	    public StreamPos getStreamPos() {
	        return new StreamPos(streamName,
	                        getCurrentToken().getLine(), getCurrentToken().getCharPositionInLine() + 1);
	    }
	    public void add(ParseException e) {
	        errors.addAllReports(e);
	    }

	public VxspParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class CompilationUnitContext extends ParserRuleContext {
		public String streamName;
		public AgentSystem result;
		public AgentSystem as;
		public ClazzContext clazz() {
			return getRuleContext(ClazzContext.class,0);
		}
		public GlobalConstantsContext globalConstants() {
			return getRuleContext(GlobalConstantsContext.class,0);
		}
		public ModulesOrFormulasContext modulesOrFormulas() {
			return getRuleContext(ModulesOrFormulasContext.class,0);
		}
		public InitFilterContext initFilter() {
			return getRuleContext(InitFilterContext.class,0);
		}
		public PropertiesContext properties() {
			return getRuleContext(PropertiesContext.class,0);
		}
		public CompilationUnitContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public CompilationUnitContext(ParserRuleContext parent, int invokingState, String streamName) {
			super(parent, invokingState);
			this.streamName = streamName;
		}
		@Override public int getRuleIndex() { return RULE_compilationUnit; }
	}

	public final CompilationUnitContext compilationUnit(String streamName) throws RecognitionException {
		CompilationUnitContext _localctx = new CompilationUnitContext(_ctx, getState(), streamName);
		enterRule(_localctx, 0, RULE_compilationUnit);
		try {
			enterOuterAlt(_localctx, 1);
			{

			    setStreamName(streamName);
			    ((CompilationUnitContext)_localctx).as =  new AgentSystem(streamName);

			setState(301);
			clazz(_localctx.as);
			setState(302);
			globalConstants(_localctx.as);
			setState(303);
			modulesOrFormulas(_localctx.as);
			setState(304);
			initFilter(_localctx.as);
			setState(305);
			properties(_localctx.as);

			    ((CompilationUnitContext)_localctx).result =  _localctx.as;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClazzContext extends ParserRuleContext {
		public AgentSystem as;
		public ParseException error =  new ParseException();
		public Token tType;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public ClazzContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ClazzContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_clazz; }
	}

	public final ClazzContext clazz(AgentSystem as) throws RecognitionException {
		ClazzContext _localctx = new ClazzContext(_ctx, getState(), as);
		enterRule(_localctx, 2, RULE_clazz);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(308);
			match(CLASS);
			setState(309);
			((ClazzContext)_localctx).tType = match(Identifier);

			        try {
			            AgentSystem.Type.parseType((((ClazzContext)_localctx).tType!=null?((ClazzContext)_localctx).tType.getText():null), _localctx.as);
			        } catch(ParseException e) {
			            e.completePos(getStreamPos());
			            add(e);
			        }
			    
			setState(313);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(311);
				match(T__0);
				 _localctx.as.synchronous = true; 
				}
			}

			setState(315);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GlobalConstantsContext extends ParserRuleContext {
		public AgentSystem as;
		public List<GlobalConstantContext> globalConstant() {
			return getRuleContexts(GlobalConstantContext.class);
		}
		public GlobalConstantContext globalConstant(int i) {
			return getRuleContext(GlobalConstantContext.class,i);
		}
		public GlobalConstantsContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public GlobalConstantsContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_globalConstants; }
	}

	public final GlobalConstantsContext globalConstants(AgentSystem as) throws RecognitionException {
		GlobalConstantsContext _localctx = new GlobalConstantsContext(_ctx, getState(), as);
		enterRule(_localctx, 4, RULE_globalConstants);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(320);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMON) {
				{
				{
				setState(317);
				globalConstant(_localctx.as);
				}
				}
				setState(322);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GlobalConstantContext extends ParserRuleContext {
		public AgentSystem as;
		public StreamPos pos =  null;
		public boolean floating;
		public String name;
		public AsConstant constant =  null;
		public Token tName;
		public ConstExpressionContext c;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public ConstExpressionContext constExpression() {
			return getRuleContext(ConstExpressionContext.class,0);
		}
		public GlobalConstantContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public GlobalConstantContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_globalConstant; }
	}

	public final GlobalConstantContext globalConstant(AgentSystem as) throws RecognitionException {
		GlobalConstantContext _localctx = new GlobalConstantContext(_ctx, getState(), as);
		enterRule(_localctx, 6, RULE_globalConstant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			match(COMMON);
			 ((GlobalConstantContext)_localctx).pos =  getStreamPos(); 
			setState(329);
			switch (_input.LA(1)) {
			case Z:
				{
				setState(325);
				match(Z);
				 ((GlobalConstantContext)_localctx).floating =  false; 
				}
				break;
			case R:
				{
				setState(327);
				match(R);
				 ((GlobalConstantContext)_localctx).floating =  true; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(331);
			((GlobalConstantContext)_localctx).tName = match(Identifier);
			 ((GlobalConstantContext)_localctx).name =  (((GlobalConstantContext)_localctx).tName!=null?((GlobalConstantContext)_localctx).tName.getText():null);
			        /*
			        try {
			            CompilerUtils.checkSymbolName(_localctx.pos, _localctx.name,
			                true);
			        } catch(ParseException e) {
			            add(e);
			        }
			         */
			    
			setState(333);
			match(ASSIGN);
			setState(334);
			((GlobalConstantContext)_localctx).c = constExpression(_localctx.as, false);

			        if(!_localctx.floating && ((GlobalConstantContext)_localctx).c.result != null && ((GlobalConstantContext)_localctx).c.result.isFloating())
			            add(new ParseException(((GlobalConstantContext)_localctx).c.result.getStreamPos(),
			                ParseException.Code.INVALID,
			                "loss of precision: R to Z"));
			        AsConstant d;
			        if(_localctx.floating && ((GlobalConstantContext)_localctx).c.result != null && !((GlobalConstantContext)_localctx).c.result.isFloating())
			           d = new AsConstant(_localctx.pos, _localctx.name, new Literal(((GlobalConstantContext)_localctx).c.result.getDouble()));
			        else
			           d = new AsConstant(_localctx.pos, _localctx.name, ((GlobalConstantContext)_localctx).c.result);
			        _localctx.as.addConstant(d);
			    
			setState(336);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException e) {

			    add(new ParseException(getStreamPos(), ParseException.Code.INVALID,
			        "invalid constant declaration: " + e.getMessage()));
			    throw e;

		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModulesOrFormulasContext extends ParserRuleContext {
		public AgentSystem as;
		public List<ModuleContext> module() {
			return getRuleContexts(ModuleContext.class);
		}
		public ModuleContext module(int i) {
			return getRuleContext(ModuleContext.class,i);
		}
		public List<FormulaContext> formula() {
			return getRuleContexts(FormulaContext.class);
		}
		public FormulaContext formula(int i) {
			return getRuleContext(FormulaContext.class,i);
		}
		public ModulesOrFormulasContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ModulesOrFormulasContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_modulesOrFormulas; }
	}

	public final ModulesOrFormulasContext modulesOrFormulas(AgentSystem as) throws RecognitionException {
		ModulesOrFormulasContext _localctx = new ModulesOrFormulasContext(_ctx, getState(), as);
		enterRule(_localctx, 8, RULE_modulesOrFormulas);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(344);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AGENT) | (1L << DEF) | (1L << ENVIRONMENT) | (1L << MODULE))) != 0)) {
				{
				setState(342);
				switch (_input.LA(1)) {
				case AGENT:
				case ENVIRONMENT:
				case MODULE:
					{
					setState(338);
					module(_localctx.as);
					 _localctx.as.inModule = false; 
					}
					break;
				case DEF:
					{
					setState(341);
					formula(_localctx.as);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModuleContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean forcePrivateVariables =  false;
		public String name;
		public AsModule m =  null;
		public ParseException errors =  new ParseException();
		public Token tName;
		public StateVarDeclarationContext variable;
		public ProtocolDeclarationContext protocol;
		public StatementContext stat;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public List<StateVarDeclarationContext> stateVarDeclaration() {
			return getRuleContexts(StateVarDeclarationContext.class);
		}
		public StateVarDeclarationContext stateVarDeclaration(int i) {
			return getRuleContext(StateVarDeclarationContext.class,i);
		}
		public ProtocolDeclarationContext protocolDeclaration() {
			return getRuleContext(ProtocolDeclarationContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public ModuleContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ModuleContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_module; }
	}

	public final ModuleContext module(AgentSystem as) throws RecognitionException {
		ModuleContext _localctx = new ModuleContext(_ctx, getState(), as);
		enterRule(_localctx, 10, RULE_module);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(355);
			switch (_input.LA(1)) {
			case AGENT:
				{
				setState(347);
				match(AGENT);
				 ((ModuleContext)_localctx).forcePrivateVariables =  true; 
				setState(349);
				((ModuleContext)_localctx).tName = match(Identifier);
				}
				break;
			case MODULE:
				{
				setState(350);
				match(MODULE);
				 ((ModuleContext)_localctx).forcePrivateVariables =  false; 
				setState(352);
				((ModuleContext)_localctx).tName = match(Identifier);
				}
				break;
			case ENVIRONMENT:
				{
				setState(353);
				match(ENVIRONMENT);
				 ((ModuleContext)_localctx).forcePrivateVariables =  false;
				        switch(_localctx.as.type) {
				            case MDP:
				                if(_localctx.as.fullyObservable)
				                    add(new ParseException(getStreamPos(),
				                        ParseException.Code.INVALID,
				                        "non-PO MDP does not allow an environment"));
				                break;

				            default:
				                throw new RuntimeException("unknown model class");
				        }
				     
				}
				break;
			default:
				throw new NoViableAltException(this);
			}

			      if(((ModuleContext)_localctx).tName == null)
			          ((ModuleContext)_localctx).name =  "environment";
			      else
			          ((ModuleContext)_localctx).name =  (((ModuleContext)_localctx).tName!=null?((ModuleContext)_localctx).tName.getText():null);
			      ((ModuleContext)_localctx).m =  new AsModule(getStreamPos(), _localctx.forcePrivateVariables, _localctx.name);
			      _localctx.as.addModule(_localctx.m);
			      _localctx.as.inModule = true;

			setState(358);
			match(LBRACE);
			setState(364);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMON || _la==Z) {
				{
				{
				setState(359);
				((ModuleContext)_localctx).variable = stateVarDeclaration(_localctx.as);

				        if(((ModuleContext)_localctx).variable.result != null) {
				            /*
				            if(_localctx.forcePrivateVariables && ((ModuleContext)_localctx).variable.result.COMMON.knownProto == null)
				                ((ModuleContext)_localctx).variable.result.COMMON.knownProto = new LinkedList<>();
				             */
				            _localctx.m.addVariable(((ModuleContext)_localctx).variable.result);
				        }
				    
				}
				}
				setState(366);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(370);
			_la = _input.LA(1);
			if (_la==PROTOCOL) {
				{
				setState(367);
				((ModuleContext)_localctx).protocol = protocolDeclaration(_localctx.as);

				        if(((ModuleContext)_localctx).protocol.result != null) {
				            _localctx.m.PROTOCOL.setStreamPos(((ModuleContext)_localctx).protocol.result.getStreamPos());
				            _localctx.m.PROTOCOL.add(((ModuleContext)_localctx).protocol.result);
				        }
				    
				}
			}

			setState(377);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LSQUARE) {
				{
				{
				setState(372);
				((ModuleContext)_localctx).stat = statement(_localctx.as);

				        if(((ModuleContext)_localctx).stat.result != null)
				            _localctx.m.STATEMENTS.add(((ModuleContext)_localctx).stat.result);
				    
				}
				}
				setState(379);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(380);
			match(RBRACE);
			setState(381);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StateVarDeclarationContext extends ParserRuleContext {
		public AgentSystem as;
		public AsVariable result;
		public StreamPos pos =  null;
		public String name;
		public List<AsConstant> constants =  null;
		public CommonDefContext common;
		public ConstRangeListContext range;
		public Token tName;
		public InitValueSpacesContext init;
		public CommonDefContext commonDef() {
			return getRuleContext(CommonDefContext.class,0);
		}
		public ConstRangeListContext constRangeList() {
			return getRuleContext(ConstRangeListContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public InitValueSpacesContext initValueSpaces() {
			return getRuleContext(InitValueSpacesContext.class,0);
		}
		public StateVarDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public StateVarDeclarationContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_stateVarDeclaration; }
	}

	public final StateVarDeclarationContext stateVarDeclaration(AgentSystem as) throws RecognitionException {
		StateVarDeclarationContext _localctx = new StateVarDeclarationContext(_ctx, getState(), as);
		enterRule(_localctx, 12, RULE_stateVarDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			((StateVarDeclarationContext)_localctx).common = commonDef();
			setState(384);
			match(Z);


			setState(386);
			match(LBRACE);
			setState(387);
			((StateVarDeclarationContext)_localctx).range = constRangeList(_localctx.as);
			setState(388);
			match(RBRACE);
			setState(389);
			((StateVarDeclarationContext)_localctx).tName = match(Identifier);
			 ((StateVarDeclarationContext)_localctx).pos =  getStreamPos(); ((StateVarDeclarationContext)_localctx).name =  (((StateVarDeclarationContext)_localctx).tName!=null?((StateVarDeclarationContext)_localctx).tName.getText():null); 
			setState(393);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(391);
				match(ASSIGN);
				setState(392);
				((StateVarDeclarationContext)_localctx).init = initValueSpaces(_localctx.as);
				}
			}

			setState(395);
			match(SEMICOLON);

			    if(((StateVarDeclarationContext)_localctx).common.result != null) {
			        switch(_localctx.as.type) {
			            case MDP:
			                if(_localctx.as.fullyObservable) {
			                    if(((StateVarDeclarationContext)_localctx).common.result.knownProto != null)
			                        add(new ParseException(new StreamPos(null, (((StateVarDeclarationContext)_localctx).common!=null?(((StateVarDeclarationContext)_localctx).common.start):null)),
			                            ParseException.Code.INVALID,
			                            "non-PO MDP allows only a default common definition"));
			                }
			                break;

			            default:
			                throw new RuntimeException("unknown model class");
			        }
			        AsRange r = new AsRange(((StateVarDeclarationContext)_localctx).range.result);
			        AsInitValue i;
			        if(((StateVarDeclarationContext)_localctx).init == null) {
			            i = new AsInitValue(false, false);
			            i.add(0, r.set);
			        } else
			            i = ((StateVarDeclarationContext)_localctx).init.result;
			        AsVariable v = new AsVariable(_localctx.pos, _localctx.as.getParsedModule(), _localctx.name,
			                ((StateVarDeclarationContext)_localctx).common.result, r, i);
			        ((StateVarDeclarationContext)_localctx).result =  v;
			    }
			    /*
			    List<String> names = ((StateVarDeclarationContext)_localctx).common.result.knownProto;
			    String owner = _localctx.as.getParsedModule().NAME;
			    if(names != null && !names.contains(owner))
			        names.add(owner);
			     */

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommonDefContext extends ParserRuleContext {
		public AsCommon result;
		public StreamPos pos;
		public List<String> moduleNames =  null;
		public IdentifierListContext list;
		public IdentifierListContext identifierList() {
			return getRuleContext(IdentifierListContext.class,0);
		}
		public CommonDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_commonDef; }
	}

	public final CommonDefContext commonDef() throws RecognitionException {
		CommonDefContext _localctx = new CommonDefContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_commonDef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{

			    ((CommonDefContext)_localctx).result =  null;

			setState(409);
			_la = _input.LA(1);
			if (_la==COMMON) {
				{
				setState(399);
				match(COMMON);
				 ((CommonDefContext)_localctx).pos =  getStreamPos(); 
				setState(406);
				_la = _input.LA(1);
				if (_la==LBRACE) {
					{
					setState(401);
					match(LBRACE);
					setState(402);
					((CommonDefContext)_localctx).list = identifierList();
					 ((CommonDefContext)_localctx).moduleNames =  ((CommonDefContext)_localctx).list.result; 
					setState(404);
					match(RBRACE);
					}
				}


				        ((CommonDefContext)_localctx).result =  new AsCommon(_localctx.pos, _localctx.moduleNames);
				    
				}
			}


			    if(_localctx.result == null)
			        ((CommonDefContext)_localctx).result =  new AsCommon(getStreamPos());

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstRangeListContext extends ParserRuleContext {
		public AgentSystem as;
		public List<AsConstant> result;
		public List<AsConstant> out =  new ArrayList<AsConstant>();
		public ConstRangeContext cr;
		public List<ConstRangeContext> constRange() {
			return getRuleContexts(ConstRangeContext.class);
		}
		public ConstRangeContext constRange(int i) {
			return getRuleContext(ConstRangeContext.class,i);
		}
		public ConstRangeListContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConstRangeListContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_constRangeList; }
	}

	public final ConstRangeListContext constRangeList(AgentSystem as) throws RecognitionException {
		ConstRangeListContext _localctx = new ConstRangeListContext(_ctx, getState(), as);
		enterRule(_localctx, 16, RULE_constRangeList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(413);
			((ConstRangeListContext)_localctx).cr = constRange(_localctx.as);

			     if(((ConstRangeListContext)_localctx).cr.result != null)
			        _localctx.out.addAll(((ConstRangeListContext)_localctx).cr.result);

			setState(421);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(415);
					match(COMMA);
					setState(416);
					((ConstRangeListContext)_localctx).cr = constRange(_localctx.as);

					        if(((ConstRangeListContext)_localctx).cr.result != null)
					               _localctx.out.addAll(((ConstRangeListContext)_localctx).cr.result);
					     
					}
					} 
				}
				setState(423);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			setState(425);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(424);
				match(COMMA);
				}
			}


			     ((ConstRangeListContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstRangeContext extends ParserRuleContext {
		public AgentSystem as;
		public List<AsConstant> result;
		public ConstBoundaryContext left;
		public ConstBoundaryContext right;
		public List<ConstBoundaryContext> constBoundary() {
			return getRuleContexts(ConstBoundaryContext.class);
		}
		public ConstBoundaryContext constBoundary(int i) {
			return getRuleContext(ConstBoundaryContext.class,i);
		}
		public ConstRangeContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConstRangeContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_constRange; }
	}

	public final ConstRangeContext constRange(AgentSystem as) throws RecognitionException {
		ConstRangeContext _localctx = new ConstRangeContext(_ctx, getState(), as);
		enterRule(_localctx, 18, RULE_constRange);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(429);
			((ConstRangeContext)_localctx).left = constBoundary(_localctx.as);
			setState(432);
			_la = _input.LA(1);
			if (_la==ELLIPSIS) {
				{
				setState(430);
				match(ELLIPSIS);
				setState(431);
				((ConstRangeContext)_localctx).right = constBoundary(_localctx.as);
				}
			}


			    if(((ConstRangeContext)_localctx).left.result != null) {
			        try {
			            if(((ConstRangeContext)_localctx).right == null || ((ConstRangeContext)_localctx).right.result == null)
			                ((ConstRangeContext)_localctx).result =  ParserUtils.iterate(((ConstRangeContext)_localctx).left.result, ((ConstRangeContext)_localctx).left.result);
			            else
			                ((ConstRangeContext)_localctx).result =  ParserUtils.iterate(((ConstRangeContext)_localctx).left.result, ((ConstRangeContext)_localctx).right.result);
			        } catch(ParseException e) {
			            e.completePos(getStreamPos());
			            add(e);
			            ((ConstRangeContext)_localctx).result =  null;
			        }
			    }

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstBoundaryContext extends ParserRuleContext {
		public AgentSystem as;
		public AsConstant result;
		public ConstExpressionContext left;
		public ConstExpressionContext right;
		public List<ConstExpressionContext> constExpression() {
			return getRuleContexts(ConstExpressionContext.class);
		}
		public ConstExpressionContext constExpression(int i) {
			return getRuleContext(ConstExpressionContext.class,i);
		}
		public ConstBoundaryContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConstBoundaryContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_constBoundary; }
	}

	public final ConstBoundaryContext constBoundary(AgentSystem as) throws RecognitionException {
		ConstBoundaryContext _localctx = new ConstBoundaryContext(_ctx, getState(), as);
		enterRule(_localctx, 20, RULE_constBoundary);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(436);
			((ConstBoundaryContext)_localctx).left = constExpression(_localctx.as, true);
			setState(439);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(437);
				match(ASSIGN);
				setState(438);
				((ConstBoundaryContext)_localctx).right = constExpression(_localctx.as, false);
				}
			}


			    ((ConstBoundaryContext)_localctx).result =  ((ConstBoundaryContext)_localctx).left.result;
			    if(((ConstBoundaryContext)_localctx).right != null) {
			        if(newlyDeclaredConstant != null) {
			            AsConstant initializedConstant = new AsConstant(
			                        newlyDeclaredConstant.getStreamPos(), newlyDeclaredConstant.NAME,
			                        ((ConstBoundaryContext)_localctx).right.result);
			            _localctx.as.replaceConstant(newlyDeclaredConstant, initializedConstant);
			        } else
			            add(new ParseException(new StreamPos(null, (((ConstBoundaryContext)_localctx).right!=null?(((ConstBoundaryContext)_localctx).right.start):null)),
			                ParseException.Code.INVALID,
			                "can initialize only a newly declared constant"));
			    }

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitValueSpacesContext extends ParserRuleContext {
		public AgentSystem as;
		public AsInitValue result;
		public InitValueSpaceContext out;
		public InitValueSpaceContext next;
		public List<InitValueSpaceContext> initValueSpace() {
			return getRuleContexts(InitValueSpaceContext.class);
		}
		public InitValueSpaceContext initValueSpace(int i) {
			return getRuleContext(InitValueSpaceContext.class,i);
		}
		public InitValueSpacesContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public InitValueSpacesContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_initValueSpaces; }
	}

	public final InitValueSpacesContext initValueSpaces(AgentSystem as) throws RecognitionException {
		InitValueSpacesContext _localctx = new InitValueSpacesContext(_ctx, getState(), as);
		enterRule(_localctx, 22, RULE_initValueSpaces);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(443);
			((InitValueSpacesContext)_localctx).out = initValueSpace(_localctx.as);
			setState(450);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(444);
					match(COMMA);
					setState(445);
					((InitValueSpacesContext)_localctx).next = initValueSpace(_localctx.as);

					        if(((InitValueSpacesContext)_localctx).out.result != null)
					            ((InitValueSpacesContext)_localctx).out.result.add(((InitValueSpacesContext)_localctx).next.result);
					    
					}
					} 
				}
				setState(452);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			setState(454);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(453);
				match(COMMA);
				}
			}


			    ((InitValueSpacesContext)_localctx).result =  ((InitValueSpacesContext)_localctx).out.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitValueSpaceContext extends ParserRuleContext {
		public AgentSystem as;
		public AsInitValue result;
		public StreamPos pos;
		public InitValueSetContext set;
		public InitValueSetContext initValueSet() {
			return getRuleContext(InitValueSetContext.class,0);
		}
		public InitValueSpaceContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public InitValueSpaceContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_initValueSpace; }
	}

	public final InitValueSpaceContext initValueSpace(AgentSystem as) throws RecognitionException {
		InitValueSpaceContext _localctx = new InitValueSpaceContext(_ctx, getState(), as);
		enterRule(_localctx, 24, RULE_initValueSpace);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(458);
			((InitValueSpaceContext)_localctx).set = initValueSet(_localctx.as);

			        ((InitValueSpaceContext)_localctx).pos =  ((InitValueSpaceContext)_localctx).set.result.get(0).getStreamPos();
			    

			    boolean floating = false;
			    boolean booleaN = true;
			    for(AsConstant c : ((InitValueSpaceContext)_localctx).set.result) {
			        if(c.isFloating())
			            floating = true;
			        if(!c.isBoolean())
			            booleaN = false;
			    }
			    boolean mixed = false;
			    for(AsConstant c : ((InitValueSpaceContext)_localctx).set.result) {
			        if(c.isBoolean() && !booleaN) {
			            add(new ParseException(c.getStreamPos(),
			                ParseException.Code.INVALID,
			                "mixed integer and boolean constants"));
			            mixed = true;
			            break;
			        }
			    }
			    if(!mixed) {
			        if(floating)
			            add(new ParseException(_localctx.pos,
			                ParseException.Code.INVALID,
			                "state variables must be integer"));
			        else {
			            AsInitValue init =  new AsInitValue(floating, booleaN);
			            Set<Integer> values = new HashSet<Integer>();
			            init.add(0, ((InitValueSpaceContext)_localctx).set.result);
			            ((InitValueSpaceContext)_localctx).result =  init;
			        }
			    }

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitValueSetContext extends ParserRuleContext {
		public AgentSystem as;
		public List<AsConstant> result;
		public StreamPos pos;
		public List<AsConstant> all =  new ArrayList<AsConstant>();
		public InitValueRangeContext s;
		public List<InitValueRangeContext> initValueRange() {
			return getRuleContexts(InitValueRangeContext.class);
		}
		public InitValueRangeContext initValueRange(int i) {
			return getRuleContext(InitValueRangeContext.class,i);
		}
		public InitValueSetContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public InitValueSetContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_initValueSet; }
	}

	public final InitValueSetContext initValueSet(AgentSystem as) throws RecognitionException {
		InitValueSetContext _localctx = new InitValueSetContext(_ctx, getState(), as);
		enterRule(_localctx, 26, RULE_initValueSet);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(462);
			((InitValueSetContext)_localctx).s = initValueRange(_localctx.as);

			    if(((InitValueSetContext)_localctx).s.result != null)
			        _localctx.all.addAll(((InitValueSetContext)_localctx).s.result);

			setState(470);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(464);
					match(COMMA);
					setState(465);
					((InitValueSetContext)_localctx).s = initValueRange(_localctx.as);

					    if(((InitValueSetContext)_localctx).s.result != null)
					        _localctx.all.addAll(((InitValueSetContext)_localctx).s.result);

					}
					} 
				}
				setState(472);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			setState(474);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				{
				setState(473);
				match(COMMA);
				}
				break;
			}

			    ((InitValueSetContext)_localctx).result =  _localctx.all;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitValueRangeContext extends ParserRuleContext {
		public AgentSystem as;
		public List<AsConstant> result;
		public ConstExpressionContext low;
		public ConstExpressionContext high;
		public List<ConstExpressionContext> constExpression() {
			return getRuleContexts(ConstExpressionContext.class);
		}
		public ConstExpressionContext constExpression(int i) {
			return getRuleContext(ConstExpressionContext.class,i);
		}
		public InitValueRangeContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public InitValueRangeContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_initValueRange; }
	}

	public final InitValueRangeContext initValueRange(AgentSystem as) throws RecognitionException {
		InitValueRangeContext _localctx = new InitValueRangeContext(_ctx, getState(), as);
		enterRule(_localctx, 28, RULE_initValueRange);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(478);
			((InitValueRangeContext)_localctx).low = constExpression(_localctx.as, false);
			setState(481);
			_la = _input.LA(1);
			if (_la==ELLIPSIS) {
				{
				setState(479);
				match(ELLIPSIS);
				setState(480);
				((InitValueRangeContext)_localctx).high = constExpression(_localctx.as, false);
				}
			}


			    if(((InitValueRangeContext)_localctx).low.result != null) {
			         try {
			            if(((InitValueRangeContext)_localctx).high == null || ((InitValueRangeContext)_localctx).high.result == null)
			                ((InitValueRangeContext)_localctx).result =  ParserUtils.iterate(((InitValueRangeContext)_localctx).low.result, ((InitValueRangeContext)_localctx).low.result);
			            else
			                ((InitValueRangeContext)_localctx).result =  ParserUtils.iterate(((InitValueRangeContext)_localctx).low.result, ((InitValueRangeContext)_localctx).high.result);
			        } catch(ParseException e) {
			            e.completePos(getStreamPos());
			            add(e);
			            ((InitValueRangeContext)_localctx).result =  null;
			        }
			    }

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProtocolDeclarationContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProtocol result;
		public StreamPos pos =  null;
		public AbstractExpression other =  null;;
		public IdentifierListContext actions;
		public ExpressionContext guard;
		public List<IdentifierListContext> identifierList() {
			return getRuleContexts(IdentifierListContext.class);
		}
		public IdentifierListContext identifierList(int i) {
			return getRuleContext(IdentifierListContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ProtocolDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ProtocolDeclarationContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_protocolDeclaration; }
	}

	public final ProtocolDeclarationContext protocolDeclaration(AgentSystem as) throws RecognitionException {
		ProtocolDeclarationContext _localctx = new ProtocolDeclarationContext(_ctx, getState(), as);
		enterRule(_localctx, 30, RULE_protocolDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(485);
			match(PROTOCOL);
			 ((ProtocolDeclarationContext)_localctx).pos =  getStreamPos(); 
			setState(487);
			match(LBRACE);

			    ((ProtocolDeclarationContext)_localctx).result =  new AsProtocol(_localctx.pos, _localctx.as.getParsedModule());

			setState(502);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ENVIRONMENT || _la==ASSIGN || _la==Identifier) {
				{
				{
				setState(489);
				((ProtocolDeclarationContext)_localctx).actions = identifierList();
				setState(490);
				match(ASSIGN);
				setState(495);
				switch (_input.LA(1)) {
				case ENVIRONMENT:
				case FALSE:
				case TRUE:
				case MINUS:
				case LNOT:
				case LPAREN:
				case IntegerLiteral:
				case FloatingPointLiteral:
				case Identifier:
					{
					setState(491);
					((ProtocolDeclarationContext)_localctx).guard = expression(_localctx.as, false);
					}
					break;
				case T__1:
					{
					 ((ProtocolDeclarationContext)_localctx).pos =  getStreamPos(); 
					setState(493);
					match(T__1);
					 ((ProtocolDeclarationContext)_localctx).other =  new AsOtherExpression(_localctx.pos, _localctx.as.SCOPE); 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}

				        if(((ProtocolDeclarationContext)_localctx).actions != null && (((ProtocolDeclarationContext)_localctx).guard != null || _localctx.other != null))
				            _localctx.result.add(_localctx.other != null ? _localctx.other : ((ProtocolDeclarationContext)_localctx).guard.result, ((ProtocolDeclarationContext)_localctx).actions.result, false);
				    
				setState(498);
				match(SEMICOLON);
				}
				}
				setState(504);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(505);
			match(RBRACE);
			setState(506);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsStatement result;
		public StreamPos pos;
		public AsLabel label;
		public AbstractExpression other;
		public Token tLabel;
		public ExpressionContext guard;
		public VariableRefContext updateLeft;
		public ExpressionContext updateRight;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public VariableRefContext variableRef() {
			return getRuleContext(VariableRefContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public StatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public StatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	}

	public final StatementContext statement(AgentSystem as) throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState(), as);
		enterRule(_localctx, 32, RULE_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(508);
			match(LSQUARE);
			 ((StatementContext)_localctx).pos =  getStreamPos(); 
			setState(511);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(510);
				((StatementContext)_localctx).tLabel = match(Identifier);
				}
			}

			setState(513);
			match(RSQUARE);

			    if(((StatementContext)_localctx).tLabel == null)
			        ((StatementContext)_localctx).label =  null;
			    else {
			        String n = (((StatementContext)_localctx).tLabel!=null?((StatementContext)_localctx).tLabel.getText():null);
			        if(n.equals(AsLabel.SYNC_KEY_NONE)) {
			            add(new ParseException(getStreamPos(),
			                ParseException.Code.ILLEGAL,
			                AsLabel.SYNC_KEY_NONE + " can not be used as a label name"));
			            ((StatementContext)_localctx).label =  null;
			        } else
			            ((StatementContext)_localctx).label =  new AsLabel(_localctx.pos, n);
			    }

			setState(521);
			switch (_input.LA(1)) {
			case ENVIRONMENT:
			case FALSE:
			case TRUE:
			case MINUS:
			case LNOT:
			case LPAREN:
			case IntegerLiteral:
			case FloatingPointLiteral:
			case Identifier:
				{
				setState(515);
				((StatementContext)_localctx).guard = expression(_localctx.as, false);
				 ((StatementContext)_localctx).other =  null; 
				}
				break;
			case T__1:
				{
				 ((StatementContext)_localctx).pos =  getStreamPos(); 
				setState(519);
				match(T__1);
				 ((StatementContext)_localctx).other =  new AsOtherExpression(_localctx.pos, _localctx.as.SCOPE); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(523);
			match(RARROW);
			setState(529);
			switch (_input.LA(1)) {
			case ENVIRONMENT:
			case Identifier:
				{
				setState(524);
				((StatementContext)_localctx).updateLeft = variableRef(_localctx.as);
				setState(525);
				match(ASSIGN);
				setState(526);
				((StatementContext)_localctx).updateRight = expression(_localctx.as, false);
				}
				break;
			case TRUE:
				{
				setState(528);
				match(TRUE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(531);
			match(SEMICOLON);

			    ((StatementContext)_localctx).result =  new AsStatement(_localctx.pos, _localctx.as.getParsedModule(),
			        _localctx.label, _localctx.other != null ? _localctx.other : ((StatementContext)_localctx).guard.result,
			        ((StatementContext)_localctx).updateLeft == null ? null : ((StatementContext)_localctx).updateLeft.result,
			        ((StatementContext)_localctx).updateRight == null ? null : ((StatementContext)_localctx).updateRight.result);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableRefContext extends ParserRuleContext {
		public AgentSystem as;
		public AsVariable result;
		public QualifiedIdentifierEnvContext name;
		public QualifiedIdentifierEnvContext qualifiedIdentifierEnv() {
			return getRuleContext(QualifiedIdentifierEnvContext.class,0);
		}
		public VariableRefContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public VariableRefContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_variableRef; }
	}

	public final VariableRefContext variableRef(AgentSystem as) throws RecognitionException {
		VariableRefContext _localctx = new VariableRefContext(_ctx, getState(), as);
		enterRule(_localctx, 34, RULE_variableRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(534);
			((VariableRefContext)_localctx).name = qualifiedIdentifierEnv();

			        String name = (((VariableRefContext)_localctx).name!=null?_input.getText(((VariableRefContext)_localctx).name.start,((VariableRefContext)_localctx).name.stop):null);
			        if(name.indexOf(".") == -1)
			            if(_localctx.as.inModule)
			                name = _localctx.as.getParsedModule().NAME + "." + name;
			            else
			                add(new ParseException(getStreamPos(),
			                    ParseException.Code.INVALID,
			                    "unqualified variable outside of a module"));
			        ((VariableRefContext)_localctx).result =  new AsVariable(new StreamPos(null, (((VariableRefContext)_localctx).name!=null?(((VariableRefContext)_localctx).name.start):null)), null,
			            name, null, null, null);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitFilterContext extends ParserRuleContext {
		public AgentSystem as;
		public ExpressionContext filter;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public InitFilterContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public InitFilterContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_initFilter; }
	}

	public final InitFilterContext initFilter(AgentSystem as) throws RecognitionException {
		InitFilterContext _localctx = new InitFilterContext(_ctx, getState(), as);
		enterRule(_localctx, 36, RULE_initFilter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(542);
			_la = _input.LA(1);
			if (_la==INIT) {
				{
				setState(537);
				match(INIT);
				setState(538);
				((InitFilterContext)_localctx).filter = expression(_localctx.as, false);

				        if(((InitFilterContext)_localctx).filter.result != null)
				            _localctx.as.initFilter = ((InitFilterContext)_localctx).filter.result;
				    
				setState(540);
				match(SEMICOLON);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AsConstant result;
		public AsConstant c;
		public ExpressionContext expr;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConstExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConstExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_constExpression; }
	}

	public final ConstExpressionContext constExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		ConstExpressionContext _localctx = new ConstExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 38, RULE_constExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(544);
			((ConstExpressionContext)_localctx).expr = expression(_localctx.as, _localctx.allowDeclaration);

			    try {
			        if(((ConstExpressionContext)_localctx).expr.result == null)
			            add(new ParseException(getStreamPos(),
			                ParseException.Code.INVALID,
			                "constant expression expected"));
			        else if((((ConstExpressionContext)_localctx).c =  ParserUtils.toConst(_localctx.as, ((ConstExpressionContext)_localctx).expr.result, null, null)) == null)
			            add(new ParseException(((ConstExpressionContext)_localctx).expr.result.getStreamPos(),
			                ParseException.Code.INVALID,
			                "constant expression expected"));
			        ((ConstExpressionContext)_localctx).result =  _localctx.c;
			    } catch(ParseException e) {
			        e.completePos(((ConstExpressionContext)_localctx).expr.result.getStreamPos());
			        add(e);
			    } 

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public BooleanExpressionContext pass;
		public BooleanExpressionContext booleanExpression() {
			return getRuleContext(BooleanExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	}

	public final ExpressionContext expression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 40, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{

			   newlyDeclaredConstant = null;

			setState(548);
			((ExpressionContext)_localctx).pass = booleanExpression(_localctx.as, _localctx.allowDeclaration);

			    ((ExpressionContext)_localctx).result =  ((ExpressionContext)_localctx).pass.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public BinaryExpression.Op op;
		public StreamPos pos;
		public RelationalExpressionContext left;
		public RelationalExpressionContext right;
		public List<RelationalExpressionContext> relationalExpression() {
			return getRuleContexts(RelationalExpressionContext.class);
		}
		public RelationalExpressionContext relationalExpression(int i) {
			return getRuleContext(RelationalExpressionContext.class,i);
		}
		public BooleanExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public BooleanExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_booleanExpression; }
	}

	public final BooleanExpressionContext booleanExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		BooleanExpressionContext _localctx = new BooleanExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 42, RULE_booleanExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(551);
			((BooleanExpressionContext)_localctx).left = relationalExpression(_localctx.as, _localctx.allowDeclaration);
			setState(564);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LAND || _la==LOR) {
				{
				{
				 ((BooleanExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(557);
				switch (_input.LA(1)) {
				case LAND:
					{
					setState(553);
					match(LAND);
					 ((BooleanExpressionContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
					}
					break;
				case LOR:
					{
					setState(555);
					match(LOR);
					 ((BooleanExpressionContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(559);
				((BooleanExpressionContext)_localctx).right = relationalExpression(_localctx.as, _localctx.allowDeclaration);

				        if(((BooleanExpressionContext)_localctx).left.result != null && ((BooleanExpressionContext)_localctx).right.result != null)
				            ((BooleanExpressionContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((BooleanExpressionContext)_localctx).left.result, ((BooleanExpressionContext)_localctx).right.result);
				    
				}
				}
				setState(566);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

			   ((BooleanExpressionContext)_localctx).result =  ((BooleanExpressionContext)_localctx).left.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public BinaryExpression.Op op;
		public StreamPos pos;
		public AddExpressionContext left;
		public AddExpressionContext right;
		public List<AddExpressionContext> addExpression() {
			return getRuleContexts(AddExpressionContext.class);
		}
		public AddExpressionContext addExpression(int i) {
			return getRuleContext(AddExpressionContext.class,i);
		}
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
	}

	public final RelationalExpressionContext relationalExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 44, RULE_relationalExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(569);
			((RelationalExpressionContext)_localctx).left = addExpression(_localctx.as, _localctx.allowDeclaration);
			setState(590);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << EQUAL) | (1L << LESS) | (1L << GREATER) | (1L << LESS_EQ) | (1L << GREATER_EQ))) != 0)) {
				{
				{
				 ((RelationalExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(583);
				switch (_input.LA(1)) {
				case EQUAL:
					{
					setState(571);
					match(EQUAL);
					 ((RelationalExpressionContext)_localctx).op =  BinaryExpression.Op.EQUAL; 
					}
					break;
				case T__2:
					{
					setState(573);
					match(T__2);
					 ((RelationalExpressionContext)_localctx).op =  BinaryExpression.Op.INEQUAL; 
					}
					break;
				case LESS:
					{
					setState(575);
					match(LESS);
					 ((RelationalExpressionContext)_localctx).op =  BinaryExpression.Op.LESS; 
					}
					break;
				case GREATER:
					{
					setState(577);
					match(GREATER);
					 ((RelationalExpressionContext)_localctx).op =  BinaryExpression.Op.GREATER; 
					}
					break;
				case LESS_EQ:
					{
					setState(579);
					match(LESS_EQ);
					 ((RelationalExpressionContext)_localctx).op =  BinaryExpression.Op.LESS_OR_EQUAL; 
					}
					break;
				case GREATER_EQ:
					{
					setState(581);
					match(GREATER_EQ);
					 ((RelationalExpressionContext)_localctx).op =  BinaryExpression.Op.GREATER_OR_EQUAL; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(585);
				((RelationalExpressionContext)_localctx).right = addExpression(_localctx.as, _localctx.allowDeclaration);

				        if(((RelationalExpressionContext)_localctx).left.result != null && ((RelationalExpressionContext)_localctx).right.result != null)
				            ((RelationalExpressionContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((RelationalExpressionContext)_localctx).left.result, ((RelationalExpressionContext)_localctx).right.result);
				    
				}
				}
				setState(592);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

			   ((RelationalExpressionContext)_localctx).result =  ((RelationalExpressionContext)_localctx).left.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public BinaryExpression.Op op;
		public StreamPos pos;
		public MultExpressionContext left;
		public MultExpressionContext right;
		public List<MultExpressionContext> multExpression() {
			return getRuleContexts(MultExpressionContext.class);
		}
		public MultExpressionContext multExpression(int i) {
			return getRuleContext(MultExpressionContext.class,i);
		}
		public AddExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public AddExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_addExpression; }
	}

	public final AddExpressionContext addExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		AddExpressionContext _localctx = new AddExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 46, RULE_addExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(595);
			((AddExpressionContext)_localctx).left = multExpression(_localctx.as, _localctx.allowDeclaration);
			setState(608);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PLUS || _la==MINUS) {
				{
				{
				 ((AddExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(601);
				switch (_input.LA(1)) {
				case PLUS:
					{
					setState(597);
					match(PLUS);
					 ((AddExpressionContext)_localctx).op =  BinaryExpression.Op.PLUS; 
					}
					break;
				case MINUS:
					{
					setState(599);
					match(MINUS);
					 ((AddExpressionContext)_localctx).op =  BinaryExpression.Op.MINUS; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(603);
				((AddExpressionContext)_localctx).right = multExpression(_localctx.as, _localctx.allowDeclaration);

				        if(((AddExpressionContext)_localctx).left.result != null && ((AddExpressionContext)_localctx).right.result != null)
				            ((AddExpressionContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((AddExpressionContext)_localctx).left.result, ((AddExpressionContext)_localctx).right.result);
				    
				}
				}
				setState(610);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

			   ((AddExpressionContext)_localctx).result =  ((AddExpressionContext)_localctx).left.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public BinaryExpression.Op op;
		public StreamPos pos;
		public UnaryExpressionContext left;
		public UnaryExpressionContext right;
		public List<UnaryExpressionContext> unaryExpression() {
			return getRuleContexts(UnaryExpressionContext.class);
		}
		public UnaryExpressionContext unaryExpression(int i) {
			return getRuleContext(UnaryExpressionContext.class,i);
		}
		public MultExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MultExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_multExpression; }
	}

	public final MultExpressionContext multExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		MultExpressionContext _localctx = new MultExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 48, RULE_multExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(613);
			((MultExpressionContext)_localctx).left = unaryExpression(_localctx.as, _localctx.allowDeclaration);
			setState(630);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__3) | (1L << MULT) | (1L << DIV) | (1L << PERCENT))) != 0)) {
				{
				{
				 ((MultExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(623);
				switch (_input.LA(1)) {
				case MULT:
					{
					setState(615);
					match(MULT);
					 ((MultExpressionContext)_localctx).op =  BinaryExpression.Op.MULTIPLY; 
					}
					break;
				case DIV:
					{
					setState(617);
					match(DIV);
					 ((MultExpressionContext)_localctx).op =  BinaryExpression.Op.DIVIDE; 
					}
					break;
				case PERCENT:
					{
					setState(619);
					match(PERCENT);
					 ((MultExpressionContext)_localctx).op =  BinaryExpression.Op.MODULUS; 
					}
					break;
				case T__3:
					{
					setState(621);
					match(T__3);
					 ((MultExpressionContext)_localctx).op =  BinaryExpression.Op.MODULUS_POS; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(625);
				((MultExpressionContext)_localctx).right = unaryExpression(_localctx.as, _localctx.allowDeclaration);

				        if(((MultExpressionContext)_localctx).left.result != null && ((MultExpressionContext)_localctx).right.result != null)
				            ((MultExpressionContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((MultExpressionContext)_localctx).left.result, ((MultExpressionContext)_localctx).right.result);
				    
				}
				}
				setState(632);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

			   ((MultExpressionContext)_localctx).result =  ((MultExpressionContext)_localctx).left.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public UnaryExpression.Op op;
		public StreamPos pos;
		public PrimaryExpressionContext sub;
		public PrimaryExpressionContext primaryExpression() {
			return getRuleContext(PrimaryExpressionContext.class,0);
		}
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
	}

	public final UnaryExpressionContext unaryExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 50, RULE_unaryExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(646);
			switch (_input.LA(1)) {
			case MINUS:
			case LNOT:
				{
				{
				 ((UnaryExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(640);
				switch (_input.LA(1)) {
				case MINUS:
					{
					setState(636);
					match(MINUS);
					 ((UnaryExpressionContext)_localctx).op =  UnaryExpression.Op.NEGATION; 
					}
					break;
				case LNOT:
					{
					setState(638);
					match(LNOT);
					 ((UnaryExpressionContext)_localctx).op =  UnaryExpression.Op.CONDITIONAL_NEGATION; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(642);
				((UnaryExpressionContext)_localctx).sub = primaryExpression(_localctx.as, _localctx.allowDeclaration);

				             if(((UnaryExpressionContext)_localctx).sub.result != null)
				                 ((UnaryExpressionContext)_localctx).sub.result = new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((UnaryExpressionContext)_localctx).sub.result);
				         
				}
				}
				break;
			case ENVIRONMENT:
			case FALSE:
			case TRUE:
			case LPAREN:
			case IntegerLiteral:
			case FloatingPointLiteral:
			case Identifier:
				{
				setState(645);
				((UnaryExpressionContext)_localctx).sub = primaryExpression(_localctx.as, _localctx.allowDeclaration);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}

			    ((UnaryExpressionContext)_localctx).result =  ((UnaryExpressionContext)_localctx).sub.result;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryExpressionContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public AbstractExpression result;
		public StreamPos pos;
		public Method function =  null;
		public ValueOrFormulaContext vf;
		public ExpressionContext expr;
		public Token tName;
		public ExpressionListContext exprList;
		public ValueOrFormulaContext valueOrFormula() {
			return getRuleContext(ValueOrFormulaContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public PrimaryExpressionContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PrimaryExpressionContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_primaryExpression; }
	}

	public final PrimaryExpressionContext primaryExpression(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		PrimaryExpressionContext _localctx = new PrimaryExpressionContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 52, RULE_primaryExpression);
		try {
			setState(666);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				 ((PrimaryExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(651);
				((PrimaryExpressionContext)_localctx).vf = valueOrFormula(_localctx.as, _localctx.allowDeclaration);

				    if(((PrimaryExpressionContext)_localctx).vf.result != null) {
				        ValueOrFormula v = ((PrimaryExpressionContext)_localctx).vf.result;
				        if(v.VALUE != null) {
				            if(v.VALUE instanceof AsLabel) {
				                AsLabel label = (AsLabel)v.VALUE;
				                ((PrimaryExpressionContext)_localctx).result =  new AsActionExpression(_localctx.as, label.getStreamPos(), _localctx.as.SCOPE,
				                        null, label.NAME);
				            } else
				                ((PrimaryExpressionContext)_localctx).result =  ParserUtils.newPrimary(as, _localctx.pos, v.VALUE);
				        } else {
				            ((PrimaryExpressionContext)_localctx).result =  v.FORMULA.EXPR;
				            _localctx.as.register(_localctx.result, v.FORMULA);
				        }
				    }

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(654);
				match(LPAREN);
				setState(655);
				((PrimaryExpressionContext)_localctx).expr = expression(_localctx.as, _localctx.allowDeclaration);
				setState(656);
				match(RPAREN);

				    newlyDeclaredConstant = null;
				    ((PrimaryExpressionContext)_localctx).result =  ((PrimaryExpressionContext)_localctx).expr.result;

				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(659);
				((PrimaryExpressionContext)_localctx).tName = match(Identifier);
				 ((PrimaryExpressionContext)_localctx).pos =  getStreamPos(); 
				setState(661);
				match(LPAREN);
				setState(662);
				((PrimaryExpressionContext)_localctx).exprList = expressionList(_localctx.as, _localctx.allowDeclaration);
				setState(663);
				match(RPAREN);

				     int numArgs = -1;
				     switch((((PrimaryExpressionContext)_localctx).tName!=null?((PrimaryExpressionContext)_localctx).tName.getText():null)) {
				         case "abs":
				             ((PrimaryExpressionContext)_localctx).function =  ParserUtils.METHOD_ABS;
				             numArgs = 1;
				             break;

				         case "mod":
				             ((PrimaryExpressionContext)_localctx).function =  ParserUtils.METHOD_MOD;
				             numArgs = 2;
				             break;

				         default:
				             add(new ParseException(getStreamPos(),
				                     ParseException.Code.UNKNOWN,
				                     "unknown function: " + (((PrimaryExpressionContext)_localctx).tName!=null?((PrimaryExpressionContext)_localctx).tName.getText():null)));
				     }
				     newlyDeclaredConstant = null;
				     if(_localctx.function != null) {
				         if(((PrimaryExpressionContext)_localctx).exprList.result.size() != numArgs)
				             add(new ParseException(getStreamPos(),
				                     ParseException.Code.INVALID,
				                     "expected " + numArgs + " arguments"));
				         ((PrimaryExpressionContext)_localctx).result =  new CallExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.function, ((PrimaryExpressionContext)_localctx).exprList.result);
				     } else
				         ((PrimaryExpressionContext)_localctx).result =  null;

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormulaContext extends ParserRuleContext {
		public AgentSystem as;
		public StreamPos pos;
		public Token tName;
		public ExpressionContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FormulaContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FormulaContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_formula; }
	}

	public final FormulaContext formula(AgentSystem as) throws RecognitionException {
		FormulaContext _localctx = new FormulaContext(_ctx, getState(), as);
		enterRule(_localctx, 54, RULE_formula);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(668);
			match(DEF);
			 ((FormulaContext)_localctx).pos =  getStreamPos(); 
			setState(670);
			((FormulaContext)_localctx).tName = match(Identifier);
			setState(671);
			match(ASSIGN);
			setState(672);
			((FormulaContext)_localctx).expr = expression(_localctx.as, false);
			setState(673);
			match(SEMICOLON);

			    if(((FormulaContext)_localctx).expr.result != null) {
			        AsFormula f = new AsFormula(_localctx.pos, (((FormulaContext)_localctx).tName!=null?((FormulaContext)_localctx).tName.getText():null), ((FormulaContext)_localctx).expr.result);
			        _localctx.as.addFormula(f);
			    }

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertiesContext extends ParserRuleContext {
		public AgentSystem as;
		public List<PropertyContext> property() {
			return getRuleContexts(PropertyContext.class);
		}
		public PropertyContext property(int i) {
			return getRuleContext(PropertyContext.class,i);
		}
		public PropertiesContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertiesContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_properties; }
	}

	public final PropertiesContext properties(AgentSystem as) throws RecognitionException {
		PropertiesContext _localctx = new PropertiesContext(_ctx, getState(), as);
		enterRule(_localctx, 56, RULE_properties);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(681);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PROPERTY) {
				{
				{
				setState(676);
				property(_localctx.as);

				  
				}
				}
				setState(683);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueOrFormulaContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public ValueOrFormula result;
		public StreamPos pos;
		public NumberContext t;
		public QualifiedIdentifierEnvContext name;
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public QualifiedIdentifierEnvContext qualifiedIdentifierEnv() {
			return getRuleContext(QualifiedIdentifierEnvContext.class,0);
		}
		public ValueOrFormulaContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ValueOrFormulaContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_valueOrFormula; }
	}

	public final ValueOrFormulaContext valueOrFormula(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		ValueOrFormulaContext _localctx = new ValueOrFormulaContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 58, RULE_valueOrFormula);
		try {
			enterOuterAlt(_localctx, 1);
			{

			    ((ValueOrFormulaContext)_localctx).pos =  getStreamPos();
			    newlyDeclaredConstant = null;

			setState(691);
			switch (_input.LA(1)) {
			case FALSE:
			case TRUE:
			case IntegerLiteral:
			case FloatingPointLiteral:
				{
				setState(685);
				((ValueOrFormulaContext)_localctx).t = number();

				        ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula((AsConstant)((ValueOrFormulaContext)_localctx).t.result);
				    
				}
				break;
			case ENVIRONMENT:
			case Identifier:
				{
				setState(688);
				((ValueOrFormulaContext)_localctx).name = qualifiedIdentifierEnv();

				        // local variables and (global) constants have priority before formulas
				        String varName = ((ValueOrFormulaContext)_localctx).name.result;
				        if(varName.indexOf(".") == -1 && _localctx.as.inModule) {
				            varName = _localctx.as.getParsedModule().NAME + "." + varName;
				        }
				        AsTyped typed = _localctx.as.lookupValue(varName);
				        if(typed == null)
				            typed = _localctx.as.lookupValue(((ValueOrFormulaContext)_localctx).name.result);
				        if(typed == null) {
				            // formulas have priority before new constants
				            AsFormula f = as.lookupFormula(((ValueOrFormulaContext)_localctx).name.result);
				            if(f == null) {
				                boolean qualified = ((ValueOrFormulaContext)_localctx).name.result.indexOf('.') != -1;
				                if(allowDeclaration && !qualified) {
				                    double max = 0;
				                    for(AsConstant c : _localctx.as.constantList)
				                        if(!c.isBoolean()) {
				                            double v = c.getMaxPrecisionFloatingPoint();
				                            if(max < v)
				                                max = v;
				                        }
				                    AsConstant newConstant = new AsConstant(_localctx.pos, ((ValueOrFormulaContext)_localctx).name.result,
				                        new Literal((int)(max + 1)));
				                    _localctx.as.addConstant(newConstant);
				                    newlyDeclaredConstant = newConstant;
				                    ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(newConstant);
				                } else {
				                    if(!qualified) {
				                        /* //; to be assigned later
				                        // during the semantic check
				                        ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(new AsConstant(_localctx.pos, ((ValueOrFormulaContext)_localctx).name.result));
				                          */
				                        String name = ((ValueOrFormulaContext)_localctx).name.result;
				                        // a global constant has a priority
				                        AsConstant constant = _localctx.as.lookupConstant(name);
				                        if(constant != null)
				                            ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(constant);
				                        else {
				                            //if(name.indexOf('.') != -1)
				                            //    throw new RuntimeException("expected an unqualified identifier");
				                            if(_localctx.as.inModule)
				                                name = _localctx.as.getParsedModule().NAME + "." + name;
				                            else
				                                add(new ParseException(_localctx.pos,
				                                    ParseException.Code.INVALID,
				                                    "unknown constant or unqualified symbol outside of a module: " + name));
				                            ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(new AsLabel(_localctx.pos, name));
				                        }
				                    } else {
				                        // can mean a variable or an action; to be assigned/changed to a protocol
				                        // action later during the semantic check
				                        ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(new AsVariable(getStreamPos(), null,
				                            ((ValueOrFormulaContext)_localctx).name.result, null, null, null));
				                    }
				                }
				             } else
				                ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(f);
				        } else
				            ((ValueOrFormulaContext)_localctx).result =  new ValueOrFormula(typed);
				    
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CoalitionGroupContext extends ParserRuleContext {
		public AgentSystem as;
		public AsCoalition result;
		public Token tName;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public CoalitionGroupContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public CoalitionGroupContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_coalitionGroup; }
	}

	public final CoalitionGroupContext coalitionGroup(AgentSystem as) throws RecognitionException {
		CoalitionGroupContext _localctx = new CoalitionGroupContext(_ctx, getState(), as);
		enterRule(_localctx, 60, RULE_coalitionGroup);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(693);
			((CoalitionGroupContext)_localctx).tName = match(Identifier);

			    AsModule m = as.lookupModule((((CoalitionGroupContext)_localctx).tName!=null?((CoalitionGroupContext)_localctx).tName.getText():null));
			    if(m == null)
			        add(new ParseException(getStreamPos(),
			                ParseException.Code.UNKNOWN,
			                "unknown module: " + (((CoalitionGroupContext)_localctx).tName!=null?((CoalitionGroupContext)_localctx).tName.getText():null)));
			    else
			        ((CoalitionGroupContext)_localctx).result =  new AsCoalition(m);

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyContext extends ParserRuleContext {
		public AgentSystem as;
		public PropertyLTLStatementContext propertyLTLStatement;
		public PropertyLTLKStatementContext propertyLTLKStatement;
		public PropertyLTLKDStatementContext propertyLTLKDStatement;
		public PropertyACTLStatementContext propertyACTLStatement;
		public PropertyACTLKStatementContext propertyACTLKStatement;
		public PropertyACTLKDStatementContext propertyACTLKDStatement;
		public PropertyACTLAStatementContext propertyACTLAStatement;
		public PropertyACTLAKStatementContext propertyACTLAKStatement;
		public PropertyACTLAKDStatementContext propertyACTLAKDStatement;
		public PropertyELTLStatementContext propertyELTLStatement;
		public PropertyELTLKStatementContext propertyELTLKStatement;
		public PropertyELTLKDStatementContext propertyELTLKDStatement;
		public PropertyECTLStatementContext propertyECTLStatement;
		public PropertyECTLKStatementContext propertyECTLKStatement;
		public PropertyECTLKDStatementContext propertyECTLKDStatement;
		public PropertyECTLAStatementContext propertyECTLAStatement;
		public PropertyECTLAKStatementContext propertyECTLAKStatement;
		public PropertyECTLAKDStatementContext propertyECTLAKDStatement;
		public PropertyCTLAStatementContext propertyCTLAStatement;
		public PropertyCTLAKStatementContext propertyCTLAKStatement;
		public PropertyCTLAKDStatementContext propertyCTLAKDStatement;
		public PropertyLTLStatementContext propertyLTLStatement() {
			return getRuleContext(PropertyLTLStatementContext.class,0);
		}
		public PropertyLTLKStatementContext propertyLTLKStatement() {
			return getRuleContext(PropertyLTLKStatementContext.class,0);
		}
		public PropertyLTLKDStatementContext propertyLTLKDStatement() {
			return getRuleContext(PropertyLTLKDStatementContext.class,0);
		}
		public PropertyACTLStatementContext propertyACTLStatement() {
			return getRuleContext(PropertyACTLStatementContext.class,0);
		}
		public PropertyACTLKStatementContext propertyACTLKStatement() {
			return getRuleContext(PropertyACTLKStatementContext.class,0);
		}
		public PropertyACTLKDStatementContext propertyACTLKDStatement() {
			return getRuleContext(PropertyACTLKDStatementContext.class,0);
		}
		public PropertyACTLAStatementContext propertyACTLAStatement() {
			return getRuleContext(PropertyACTLAStatementContext.class,0);
		}
		public PropertyACTLAKStatementContext propertyACTLAKStatement() {
			return getRuleContext(PropertyACTLAKStatementContext.class,0);
		}
		public PropertyACTLAKDStatementContext propertyACTLAKDStatement() {
			return getRuleContext(PropertyACTLAKDStatementContext.class,0);
		}
		public PropertyELTLStatementContext propertyELTLStatement() {
			return getRuleContext(PropertyELTLStatementContext.class,0);
		}
		public PropertyELTLKStatementContext propertyELTLKStatement() {
			return getRuleContext(PropertyELTLKStatementContext.class,0);
		}
		public PropertyELTLKDStatementContext propertyELTLKDStatement() {
			return getRuleContext(PropertyELTLKDStatementContext.class,0);
		}
		public PropertyECTLStatementContext propertyECTLStatement() {
			return getRuleContext(PropertyECTLStatementContext.class,0);
		}
		public PropertyECTLKStatementContext propertyECTLKStatement() {
			return getRuleContext(PropertyECTLKStatementContext.class,0);
		}
		public PropertyECTLKDStatementContext propertyECTLKDStatement() {
			return getRuleContext(PropertyECTLKDStatementContext.class,0);
		}
		public PropertyECTLAStatementContext propertyECTLAStatement() {
			return getRuleContext(PropertyECTLAStatementContext.class,0);
		}
		public PropertyECTLAKStatementContext propertyECTLAKStatement() {
			return getRuleContext(PropertyECTLAKStatementContext.class,0);
		}
		public PropertyECTLAKDStatementContext propertyECTLAKDStatement() {
			return getRuleContext(PropertyECTLAKDStatementContext.class,0);
		}
		public PropertyCTLAStatementContext propertyCTLAStatement() {
			return getRuleContext(PropertyCTLAStatementContext.class,0);
		}
		public PropertyCTLAKStatementContext propertyCTLAKStatement() {
			return getRuleContext(PropertyCTLAKStatementContext.class,0);
		}
		public PropertyCTLAKDStatementContext propertyCTLAKDStatement() {
			return getRuleContext(PropertyCTLAKDStatementContext.class,0);
		}
		public PropertyContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_property; }
	}

	public final PropertyContext property(AgentSystem as) throws RecognitionException {
		PropertyContext _localctx = new PropertyContext(_ctx, getState(), as);
		enterRule(_localctx, 62, RULE_property);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(696);
			match(PROPERTY);
			setState(760);
			switch ( getInterpreter().adaptivePredict(_input,40,_ctx) ) {
			case 1:
				{
				setState(697);
				((PropertyContext)_localctx).propertyLTLStatement = propertyLTLStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyLTLStatement.result); 
				}
				break;
			case 2:
				{
				setState(700);
				((PropertyContext)_localctx).propertyLTLKStatement = propertyLTLKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyLTLKStatement.result); 
				}
				break;
			case 3:
				{
				setState(703);
				((PropertyContext)_localctx).propertyLTLKDStatement = propertyLTLKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyLTLKDStatement.result); 
				}
				break;
			case 4:
				{
				setState(706);
				((PropertyContext)_localctx).propertyACTLStatement = propertyACTLStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyACTLStatement.result); 
				}
				break;
			case 5:
				{
				setState(709);
				((PropertyContext)_localctx).propertyACTLKStatement = propertyACTLKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyACTLKStatement.result); 
				}
				break;
			case 6:
				{
				setState(712);
				((PropertyContext)_localctx).propertyACTLKDStatement = propertyACTLKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyACTLKDStatement.result); 
				}
				break;
			case 7:
				{
				setState(715);
				((PropertyContext)_localctx).propertyACTLAStatement = propertyACTLAStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyACTLAStatement.result); 
				}
				break;
			case 8:
				{
				setState(718);
				((PropertyContext)_localctx).propertyACTLAKStatement = propertyACTLAKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyACTLAKStatement.result); 
				}
				break;
			case 9:
				{
				setState(721);
				((PropertyContext)_localctx).propertyACTLAKDStatement = propertyACTLAKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyACTLAKDStatement.result); 
				}
				break;
			case 10:
				{
				setState(724);
				((PropertyContext)_localctx).propertyELTLStatement = propertyELTLStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyELTLStatement.result); 
				}
				break;
			case 11:
				{
				setState(727);
				((PropertyContext)_localctx).propertyELTLKStatement = propertyELTLKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyELTLKStatement.result); 
				}
				break;
			case 12:
				{
				setState(730);
				((PropertyContext)_localctx).propertyELTLKDStatement = propertyELTLKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyELTLKDStatement.result); 
				}
				break;
			case 13:
				{
				setState(733);
				((PropertyContext)_localctx).propertyECTLStatement = propertyECTLStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyECTLStatement.result); 
				}
				break;
			case 14:
				{
				setState(736);
				((PropertyContext)_localctx).propertyECTLKStatement = propertyECTLKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyECTLKStatement.result); 
				}
				break;
			case 15:
				{
				setState(739);
				((PropertyContext)_localctx).propertyECTLKDStatement = propertyECTLKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyECTLKDStatement.result); 
				}
				break;
			case 16:
				{
				setState(742);
				((PropertyContext)_localctx).propertyECTLAStatement = propertyECTLAStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyECTLAStatement.result); 
				}
				break;
			case 17:
				{
				setState(745);
				((PropertyContext)_localctx).propertyECTLAKStatement = propertyECTLAKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyECTLAKStatement.result); 
				}
				break;
			case 18:
				{
				setState(748);
				((PropertyContext)_localctx).propertyECTLAKDStatement = propertyECTLAKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyECTLAKDStatement.result); 
				}
				break;
			case 19:
				{
				setState(751);
				((PropertyContext)_localctx).propertyCTLAStatement = propertyCTLAStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyCTLAStatement.result); 
				}
				break;
			case 20:
				{
				setState(754);
				((PropertyContext)_localctx).propertyCTLAKStatement = propertyCTLAKStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyCTLAKStatement.result); 
				}
				break;
			case 21:
				{
				setState(757);
				((PropertyContext)_localctx).propertyCTLAKDStatement = propertyCTLAKDStatement(_localctx.as);
				 _localctx.as.addProperty(((PropertyContext)_localctx).propertyCTLAKDStatement.result); 
				}
				break;
			}
			setState(762);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyLTLContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyLTLContext propertyLTL() {
			return getRuleContext(PropertyLTLContext.class,0);
		}
		public PropertyLTLStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLStatement; }
	}

	public final PropertyLTLStatementContext propertyLTLStatement(AgentSystem as) throws RecognitionException {
		PropertyLTLStatementContext _localctx = new PropertyLTLStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 64, RULE_propertyLTLStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(764);
			match(LTL);
			 ((PropertyLTLStatementContext)_localctx).pos =  getStreamPos(); 
			setState(766);
			((PropertyLTLStatementContext)_localctx).tName = match(Identifier);
			setState(767);
			match(ASSIGN);

				((PropertyLTLStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.LTL, (((PropertyLTLStatementContext)_localctx).tName!=null?((PropertyLTLStatementContext)_localctx).tName.getText():null));

			setState(769);
			((PropertyLTLStatementContext)_localctx).expr = propertyLTL(_localctx.as);

				_localctx.out.EXPR = ((PropertyLTLStatementContext)_localctx).expr.result;
				((PropertyLTLStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTL_XiContext sub;
		public PropertyLTL_XiContext propertyLTL_Xi() {
			return getRuleContext(PropertyLTL_XiContext.class,0);
		}
		public PropertyLTLContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTL; }
	}

	public final PropertyLTLContext propertyLTL(AgentSystem as) throws RecognitionException {
		PropertyLTLContext _localctx = new PropertyLTLContext(_ctx, getState(), as);
		enterRule(_localctx, 66, RULE_propertyLTL);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(772);
			((PropertyLTLContext)_localctx).sub = propertyLTL_Xi(_localctx.as);

					((PropertyLTLContext)_localctx).result =  new UnaryExpression(((PropertyLTLContext)_localctx).sub.result.getStreamPos(), _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyLTLContext)_localctx).sub.result);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTL_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTL_Xi_subContext left;
		public PropertyLTL_Xi_subContext right;
		public List<PropertyLTL_Xi_subContext> propertyLTL_Xi_sub() {
			return getRuleContexts(PropertyLTL_Xi_subContext.class);
		}
		public PropertyLTL_Xi_subContext propertyLTL_Xi_sub(int i) {
			return getRuleContext(PropertyLTL_Xi_subContext.class,i);
		}
		public PropertyLTL_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTL_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTL_Xi; }
	}

	public final PropertyLTL_XiContext propertyLTL_Xi(AgentSystem as) throws RecognitionException {
		PropertyLTL_XiContext _localctx = new PropertyLTL_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 68, RULE_propertyLTL_Xi);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(775);
			((PropertyLTL_XiContext)_localctx).left = propertyLTL_Xi_sub(_localctx.as);
			setState(796);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(788);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(776);
						match(LAND);
						 ((PropertyLTL_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(778);
						match(LOR);
						 ((PropertyLTL_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(780);
						match(RARROW);
						 ((PropertyLTL_XiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(782);
						match(DARROW);
						 ((PropertyLTL_XiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					case U:
						{
						setState(784);
						match(U);
						 ((PropertyLTL_XiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(786);
						match(R);
						 ((PropertyLTL_XiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyLTL_XiContext)_localctx).pos =  getStreamPos(); 
					setState(791);
					((PropertyLTL_XiContext)_localctx).right = propertyLTL_Xi_sub(_localctx.as);

								    ((PropertyLTL_XiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyLTL_XiContext)_localctx).left.result, ((PropertyLTL_XiContext)_localctx).right.result); 
					}
					} 
				}
				setState(798);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,42,_ctx);
			}

					((PropertyLTL_XiContext)_localctx).result =  ((PropertyLTL_XiContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTL_Xi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTL_XiContext subParenthesized;
		public PropertyLTL_XiContext subPhi;
		public PropertyLTL_XiContext sub;
		public RelationalExpressionContext expr;
		public PropertyLTL_XiContext propertyLTL_Xi() {
			return getRuleContext(PropertyLTL_XiContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyLTL_Xi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTL_Xi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTL_Xi_sub; }
	}

	public final PropertyLTL_Xi_subContext propertyLTL_Xi_sub(AgentSystem as) throws RecognitionException {
		PropertyLTL_Xi_subContext _localctx = new PropertyLTL_Xi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 70, RULE_propertyLTL_Xi_sub);
		try {
			setState(829);
			switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(801);
				match(LPAREN);
				setState(802);
				((PropertyLTL_Xi_subContext)_localctx).subParenthesized = propertyLTL_Xi(_localctx.as);
				setState(803);
				match(RPAREN);

						((PropertyLTL_Xi_subContext)_localctx).result =  ((PropertyLTL_Xi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(806);
				match(LNOT);
				 ((PropertyLTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(808);
				((PropertyLTL_Xi_subContext)_localctx).subPhi = propertyLTL_Xi(_localctx.as);

						((PropertyLTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyLTL_Xi_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(811);
				match(X);
				 ((PropertyLTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(813);
				((PropertyLTL_Xi_subContext)_localctx).sub = propertyLTL_Xi(_localctx.as);

						((PropertyLTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyLTL_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(816);
				match(F);
				 ((PropertyLTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(818);
				((PropertyLTL_Xi_subContext)_localctx).sub = propertyLTL_Xi(_localctx.as);

						((PropertyLTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyLTL_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(821);
				match(G);
				 ((PropertyLTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(823);
				((PropertyLTL_Xi_subContext)_localctx).sub = propertyLTL_Xi(_localctx.as);

						((PropertyLTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyLTL_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(826);
				((PropertyLTL_Xi_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyLTL_Xi_subContext)_localctx).result =  ((PropertyLTL_Xi_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyLTLKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyLTLKContext propertyLTLK() {
			return getRuleContext(PropertyLTLKContext.class,0);
		}
		public PropertyLTLKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKStatement; }
	}

	public final PropertyLTLKStatementContext propertyLTLKStatement(AgentSystem as) throws RecognitionException {
		PropertyLTLKStatementContext _localctx = new PropertyLTLKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 72, RULE_propertyLTLKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(831);
			match(LTLK);
			 ((PropertyLTLKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(833);
			((PropertyLTLKStatementContext)_localctx).tName = match(Identifier);
			setState(834);
			match(ASSIGN);

				((PropertyLTLKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.LTLK, (((PropertyLTLKStatementContext)_localctx).tName!=null?((PropertyLTLKStatementContext)_localctx).tName.getText():null));

			setState(836);
			((PropertyLTLKStatementContext)_localctx).expr = propertyLTLK(_localctx.as);

				_localctx.out.EXPR = ((PropertyLTLKStatementContext)_localctx).expr.result;
				((PropertyLTLKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLK_XiContext sub;
		public PropertyLTLK_XiContext propertyLTLK_Xi() {
			return getRuleContext(PropertyLTLK_XiContext.class,0);
		}
		public PropertyLTLKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLK; }
	}

	public final PropertyLTLKContext propertyLTLK(AgentSystem as) throws RecognitionException {
		PropertyLTLKContext _localctx = new PropertyLTLKContext(_ctx, getState(), as);
		enterRule(_localctx, 74, RULE_propertyLTLK);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(839);
			((PropertyLTLKContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

					((PropertyLTLKContext)_localctx).result =  new UnaryExpression(((PropertyLTLKContext)_localctx).sub.result.getStreamPos(), _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyLTLKContext)_localctx).sub.result);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLK_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLK_Xi_subContext left;
		public PropertyLTLK_Xi_subContext right;
		public List<PropertyLTLK_Xi_subContext> propertyLTLK_Xi_sub() {
			return getRuleContexts(PropertyLTLK_Xi_subContext.class);
		}
		public PropertyLTLK_Xi_subContext propertyLTLK_Xi_sub(int i) {
			return getRuleContext(PropertyLTLK_Xi_subContext.class,i);
		}
		public PropertyLTLK_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLK_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLK_Xi; }
	}

	public final PropertyLTLK_XiContext propertyLTLK_Xi(AgentSystem as) throws RecognitionException {
		PropertyLTLK_XiContext _localctx = new PropertyLTLK_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 76, RULE_propertyLTLK_Xi);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(842);
			((PropertyLTLK_XiContext)_localctx).left = propertyLTLK_Xi_sub(_localctx.as);
			setState(863);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(855);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(843);
						match(LAND);
						 ((PropertyLTLK_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(845);
						match(LOR);
						 ((PropertyLTLK_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(847);
						match(RARROW);
						 ((PropertyLTLK_XiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(849);
						match(DARROW);
						 ((PropertyLTLK_XiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					case U:
						{
						setState(851);
						match(U);
						 ((PropertyLTLK_XiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(853);
						match(R);
						 ((PropertyLTLK_XiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyLTLK_XiContext)_localctx).pos =  getStreamPos(); 
					setState(858);
					((PropertyLTLK_XiContext)_localctx).right = propertyLTLK_Xi_sub(_localctx.as);

								    ((PropertyLTLK_XiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyLTLK_XiContext)_localctx).left.result, ((PropertyLTLK_XiContext)_localctx).right.result); 
					}
					} 
				}
				setState(865);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,45,_ctx);
			}

					((PropertyLTLK_XiContext)_localctx).result =  ((PropertyLTLK_XiContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLK_Xi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLK_XiContext subParenthesized;
		public PropertyLTLK_XiContext subPhi;
		public PropertyLTLK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyLTLK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyLTLK_XiContext propertyLTLK_Xi() {
			return getRuleContext(PropertyLTLK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyLTLK_Xi_superContext propertyLTLK_Xi_super() {
			return getRuleContext(PropertyLTLK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyLTLK_Xi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLK_Xi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLK_Xi_sub; }
	}

	public final PropertyLTLK_Xi_subContext propertyLTLK_Xi_sub(AgentSystem as) throws RecognitionException {
		PropertyLTLK_Xi_subContext _localctx = new PropertyLTLK_Xi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 78, RULE_propertyLTLK_Xi_sub);
		try {
			setState(972);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(868);
				match(LPAREN);
				setState(869);
				((PropertyLTLK_Xi_subContext)_localctx).subParenthesized = propertyLTLK_Xi(_localctx.as);
				setState(870);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  ((PropertyLTLK_Xi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(873);
				match(LNOT);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(875);
				((PropertyLTLK_Xi_subContext)_localctx).subPhi = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyLTLK_Xi_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(878);
				match(X);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(880);
				((PropertyLTLK_Xi_subContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyLTLK_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(883);
				match(F);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(885);
				((PropertyLTLK_Xi_subContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyLTLK_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(888);
				match(G);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(890);
				((PropertyLTLK_Xi_subContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyLTLK_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(893);
				match(K);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(895);
				match(LPAREN);
				setState(896);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(897);
				match(COMMA);
				setState(898);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(899);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(902);
				match(Eg);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(904);
				match(LPAREN);
				setState(905);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(906);
				match(COMMA);
				setState(907);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(908);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(911);
				match(Dg);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(913);
				match(LPAREN);
				setState(914);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(915);
				match(COMMA);
				setState(916);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(917);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(920);
				match(Cg);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(922);
				match(LPAREN);
				setState(923);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(924);
				match(COMMA);
				setState(925);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(926);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(929);
				match(LNOT);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(931);
				match(K);
				setState(932);
				match(LPAREN);
				setState(933);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(934);
				match(COMMA);
				setState(935);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(936);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(939);
				match(LNOT);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(941);
				match(Eg);
				setState(942);
				match(LPAREN);
				setState(943);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(944);
				match(COMMA);
				setState(945);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(946);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(949);
				match(LNOT);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(951);
				match(Dg);
				setState(952);
				match(LPAREN);
				setState(953);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(954);
				match(COMMA);
				setState(955);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(956);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(959);
				match(LNOT);
				 ((PropertyLTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(961);
				match(Cg);
				setState(962);
				match(LPAREN);
				setState(963);
				((PropertyLTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(964);
				match(COMMA);
				setState(965);
				((PropertyLTLK_Xi_subContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(966);
				match(RPAREN);

						((PropertyLTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyLTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(969);
				((PropertyLTLK_Xi_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyLTLK_Xi_subContext)_localctx).result =  ((PropertyLTLK_Xi_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLK_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLK_Xi_subContext left;
		public PropertyLTLK_Xi_subContext right;
		public List<PropertyLTLK_Xi_subContext> propertyLTLK_Xi_sub() {
			return getRuleContexts(PropertyLTLK_Xi_subContext.class);
		}
		public PropertyLTLK_Xi_subContext propertyLTLK_Xi_sub(int i) {
			return getRuleContext(PropertyLTLK_Xi_subContext.class,i);
		}
		public PropertyLTLK_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLK_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLK_Xi_super; }
	}

	public final PropertyLTLK_Xi_superContext propertyLTLK_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyLTLK_Xi_superContext _localctx = new PropertyLTLK_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 80, RULE_propertyLTLK_Xi_super);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(974);
			((PropertyLTLK_Xi_superContext)_localctx).left = propertyLTLK_Xi_sub(_localctx.as);
			setState(995);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 16)) & ~0x3f) == 0 && ((1L << (_la - 16)) & ((1L << (R - 16)) | (1L << (RARROW - 16)) | (1L << (DARROW - 16)) | (1L << (LAND - 16)) | (1L << (LOR - 16)) | (1L << (U - 16)))) != 0)) {
				{
				{
				setState(987);
				switch (_input.LA(1)) {
				case LAND:
					{
					setState(975);
					match(LAND);
					 ((PropertyLTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
					}
					break;
				case LOR:
					{
					setState(977);
					match(LOR);
					 ((PropertyLTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
					}
					break;
				case RARROW:
					{
					setState(979);
					match(RARROW);
					 ((PropertyLTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
					}
					break;
				case DARROW:
					{
					setState(981);
					match(DARROW);
					 ((PropertyLTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
					}
					break;
				case U:
					{
					setState(983);
					match(U);
					 ((PropertyLTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.U; 
					}
					break;
				case R:
					{
					setState(985);
					match(R);
					 ((PropertyLTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.R; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				 ((PropertyLTLK_Xi_superContext)_localctx).pos =  getStreamPos(); 
				setState(990);
				((PropertyLTLK_Xi_superContext)_localctx).right = propertyLTLK_Xi_sub(_localctx.as);

							    ((PropertyLTLK_Xi_superContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyLTLK_Xi_superContext)_localctx).left.result, ((PropertyLTLK_Xi_superContext)_localctx).right.result); 
				}
				}
				setState(997);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

					((PropertyLTLK_Xi_superContext)_localctx).result =  ((PropertyLTLK_Xi_superContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLK_Xi_sub_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLK_XiContext subParenthesized;
		public PropertyLTLK_XiContext subPhi;
		public PropertyLTLK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyLTLK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyLTLK_XiContext propertyLTLK_Xi() {
			return getRuleContext(PropertyLTLK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyLTLK_Xi_superContext propertyLTLK_Xi_super() {
			return getRuleContext(PropertyLTLK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyLTLK_Xi_sub_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLK_Xi_sub_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLK_Xi_sub_super; }
	}

	public final PropertyLTLK_Xi_sub_superContext propertyLTLK_Xi_sub_super(AgentSystem as) throws RecognitionException {
		PropertyLTLK_Xi_sub_superContext _localctx = new PropertyLTLK_Xi_sub_superContext(_ctx, getState(), as);
		enterRule(_localctx, 82, RULE_propertyLTLK_Xi_sub_super);
		try {
			setState(1104);
			switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1000);
				match(LPAREN);
				setState(1001);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subParenthesized = propertyLTLK_Xi(_localctx.as);
				setState(1002);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  ((PropertyLTLK_Xi_sub_superContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1005);
				match(LNOT);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1007);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subPhi = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyLTLK_Xi_sub_superContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1010);
				match(X);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1012);
				((PropertyLTLK_Xi_sub_superContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyLTLK_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1015);
				match(F);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1017);
				((PropertyLTLK_Xi_sub_superContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyLTLK_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1020);
				match(G);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1022);
				((PropertyLTLK_Xi_sub_superContext)_localctx).sub = propertyLTLK_Xi(_localctx.as);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyLTLK_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1025);
				match(K);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1027);
				match(LPAREN);
				setState(1028);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1029);
				match(COMMA);
				setState(1030);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1031);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1034);
				match(Eg);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1036);
				match(LPAREN);
				setState(1037);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1038);
				match(COMMA);
				setState(1039);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1040);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1043);
				match(Dg);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1045);
				match(LPAREN);
				setState(1046);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1047);
				match(COMMA);
				setState(1048);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1049);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1052);
				match(Cg);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1054);
				match(LPAREN);
				setState(1055);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1056);
				match(COMMA);
				setState(1057);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1058);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1061);
				match(LNOT);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1063);
				match(K);
				setState(1064);
				match(LPAREN);
				setState(1065);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1066);
				match(COMMA);
				setState(1067);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1068);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1071);
				match(LNOT);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1073);
				match(Eg);
				setState(1074);
				match(LPAREN);
				setState(1075);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1076);
				match(COMMA);
				setState(1077);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1078);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1081);
				match(LNOT);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1083);
				match(Dg);
				setState(1084);
				match(LPAREN);
				setState(1085);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1086);
				match(COMMA);
				setState(1087);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1088);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(1091);
				match(LNOT);
				 ((PropertyLTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1093);
				match(Cg);
				setState(1094);
				match(LPAREN);
				setState(1095);
				((PropertyLTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1096);
				match(COMMA);
				setState(1097);
				((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper = propertyLTLK_Xi_super(_localctx.as);
				setState(1098);
				match(RPAREN);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyLTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(1101);
				((PropertyLTLK_Xi_sub_superContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyLTLK_Xi_sub_superContext)_localctx).result =  ((PropertyLTLK_Xi_sub_superContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyLTLKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyLTLKDContext propertyLTLKD() {
			return getRuleContext(PropertyLTLKDContext.class,0);
		}
		public PropertyLTLKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKDStatement; }
	}

	public final PropertyLTLKDStatementContext propertyLTLKDStatement(AgentSystem as) throws RecognitionException {
		PropertyLTLKDStatementContext _localctx = new PropertyLTLKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 84, RULE_propertyLTLKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1106);
			match(LTLKD);
			 ((PropertyLTLKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(1108);
			((PropertyLTLKDStatementContext)_localctx).tName = match(Identifier);
			setState(1109);
			match(ASSIGN);

				((PropertyLTLKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.LTLKD, (((PropertyLTLKDStatementContext)_localctx).tName!=null?((PropertyLTLKDStatementContext)_localctx).tName.getText():null));

			setState(1111);
			((PropertyLTLKDStatementContext)_localctx).expr = propertyLTLKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyLTLKDStatementContext)_localctx).expr.result;
				((PropertyLTLKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLKD_XiContext sub;
		public PropertyLTLKD_XiContext propertyLTLKD_Xi() {
			return getRuleContext(PropertyLTLKD_XiContext.class,0);
		}
		public PropertyLTLKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKD; }
	}

	public final PropertyLTLKDContext propertyLTLKD(AgentSystem as) throws RecognitionException {
		PropertyLTLKDContext _localctx = new PropertyLTLKDContext(_ctx, getState(), as);
		enterRule(_localctx, 86, RULE_propertyLTLKD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1114);
			((PropertyLTLKDContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

					((PropertyLTLKDContext)_localctx).result =  new UnaryExpression(((PropertyLTLKDContext)_localctx).sub.result.getStreamPos(), _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyLTLKDContext)_localctx).sub.result);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKD_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLKD_Xi_subContext left;
		public PropertyLTLKD_Xi_subContext right;
		public List<PropertyLTLKD_Xi_subContext> propertyLTLKD_Xi_sub() {
			return getRuleContexts(PropertyLTLKD_Xi_subContext.class);
		}
		public PropertyLTLKD_Xi_subContext propertyLTLKD_Xi_sub(int i) {
			return getRuleContext(PropertyLTLKD_Xi_subContext.class,i);
		}
		public PropertyLTLKD_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKD_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKD_Xi; }
	}

	public final PropertyLTLKD_XiContext propertyLTLKD_Xi(AgentSystem as) throws RecognitionException {
		PropertyLTLKD_XiContext _localctx = new PropertyLTLKD_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 88, RULE_propertyLTLKD_Xi);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1117);
			((PropertyLTLKD_XiContext)_localctx).left = propertyLTLKD_Xi_sub(_localctx.as);
			setState(1138);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1130);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1118);
						match(LAND);
						 ((PropertyLTLKD_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1120);
						match(LOR);
						 ((PropertyLTLKD_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1122);
						match(RARROW);
						 ((PropertyLTLKD_XiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1124);
						match(DARROW);
						 ((PropertyLTLKD_XiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					case U:
						{
						setState(1126);
						match(U);
						 ((PropertyLTLKD_XiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(1128);
						match(R);
						 ((PropertyLTLKD_XiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyLTLKD_XiContext)_localctx).pos =  getStreamPos(); 
					setState(1133);
					((PropertyLTLKD_XiContext)_localctx).right = propertyLTLKD_Xi_sub(_localctx.as);

								    ((PropertyLTLKD_XiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyLTLKD_XiContext)_localctx).left.result, ((PropertyLTLKD_XiContext)_localctx).right.result); 
					}
					} 
				}
				setState(1140);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,51,_ctx);
			}

					((PropertyLTLKD_XiContext)_localctx).result =  ((PropertyLTLKD_XiContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKD_Xi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLKD_XiContext subParenthesized;
		public PropertyLTLKD_XiContext subPhi;
		public PropertyLTLKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyLTLKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyLTLKD_XiContext propertyLTLKD_Xi() {
			return getRuleContext(PropertyLTLKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyLTLKD_Xi_superContext propertyLTLKD_Xi_super() {
			return getRuleContext(PropertyLTLKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyLTLKD_Xi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKD_Xi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKD_Xi_sub; }
	}

	public final PropertyLTLKD_Xi_subContext propertyLTLKD_Xi_sub(AgentSystem as) throws RecognitionException {
		PropertyLTLKD_Xi_subContext _localctx = new PropertyLTLKD_Xi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 90, RULE_propertyLTLKD_Xi_sub);
		try {
			setState(1285);
			switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1143);
				match(LPAREN);
				setState(1144);
				((PropertyLTLKD_Xi_subContext)_localctx).subParenthesized = propertyLTLKD_Xi(_localctx.as);
				setState(1145);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  ((PropertyLTLKD_Xi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1148);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1150);
				((PropertyLTLKD_Xi_subContext)_localctx).subPhi = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyLTLKD_Xi_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1153);
				match(X);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1155);
				((PropertyLTLKD_Xi_subContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyLTLKD_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1158);
				match(F);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1160);
				((PropertyLTLKD_Xi_subContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyLTLKD_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1163);
				match(G);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1165);
				((PropertyLTLKD_Xi_subContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyLTLKD_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1168);
				match(K);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1170);
				match(LPAREN);
				setState(1171);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1172);
				match(COMMA);
				setState(1173);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1174);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1177);
				match(Eg);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1179);
				match(LPAREN);
				setState(1180);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1181);
				match(COMMA);
				setState(1182);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1183);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1186);
				match(Dg);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1188);
				match(LPAREN);
				setState(1189);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1190);
				match(COMMA);
				setState(1191);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1192);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1195);
				match(Cg);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1197);
				match(LPAREN);
				setState(1198);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1199);
				match(COMMA);
				setState(1200);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1201);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1204);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1206);
				match(K);
				setState(1207);
				match(LPAREN);
				setState(1208);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1209);
				match(COMMA);
				setState(1210);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1211);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1214);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1216);
				match(Eg);
				setState(1217);
				match(LPAREN);
				setState(1218);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1219);
				match(COMMA);
				setState(1220);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1221);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1224);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1226);
				match(Dg);
				setState(1227);
				match(LPAREN);
				setState(1228);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1229);
				match(COMMA);
				setState(1230);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1231);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(1234);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1236);
				match(Cg);
				setState(1237);
				match(LPAREN);
				setState(1238);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1239);
				match(COMMA);
				setState(1240);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1241);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(1244);
				match(Oc);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1246);
				match(LPAREN);
				setState(1247);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1248);
				match(COMMA);
				setState(1249);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1250);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Oc, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(1253);
				match(Kh);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1255);
				match(LPAREN);
				setState(1256);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1257);
				match(COMMA);
				setState(1258);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1259);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Khc, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(1262);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1264);
				match(Oc);
				setState(1265);
				match(LPAREN);
				setState(1266);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1267);
				match(COMMA);
				setState(1268);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1269);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(1272);
				match(LNOT);
				 ((PropertyLTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1274);
				match(Kh);
				setState(1275);
				match(LPAREN);
				setState(1276);
				((PropertyLTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1277);
				match(COMMA);
				setState(1278);
				((PropertyLTLKD_Xi_subContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1279);
				match(RPAREN);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyLTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(1282);
				((PropertyLTLKD_Xi_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyLTLKD_Xi_subContext)_localctx).result =  ((PropertyLTLKD_Xi_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKD_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLKD_Xi_subContext left;
		public PropertyLTLKD_Xi_subContext right;
		public List<PropertyLTLKD_Xi_subContext> propertyLTLKD_Xi_sub() {
			return getRuleContexts(PropertyLTLKD_Xi_subContext.class);
		}
		public PropertyLTLKD_Xi_subContext propertyLTLKD_Xi_sub(int i) {
			return getRuleContext(PropertyLTLKD_Xi_subContext.class,i);
		}
		public PropertyLTLKD_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKD_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKD_Xi_super; }
	}

	public final PropertyLTLKD_Xi_superContext propertyLTLKD_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyLTLKD_Xi_superContext _localctx = new PropertyLTLKD_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 92, RULE_propertyLTLKD_Xi_super);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1287);
			((PropertyLTLKD_Xi_superContext)_localctx).left = propertyLTLKD_Xi_sub(_localctx.as);
			setState(1308);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 16)) & ~0x3f) == 0 && ((1L << (_la - 16)) & ((1L << (R - 16)) | (1L << (RARROW - 16)) | (1L << (DARROW - 16)) | (1L << (LAND - 16)) | (1L << (LOR - 16)) | (1L << (U - 16)))) != 0)) {
				{
				{
				setState(1300);
				switch (_input.LA(1)) {
				case LAND:
					{
					setState(1288);
					match(LAND);
					 ((PropertyLTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
					}
					break;
				case LOR:
					{
					setState(1290);
					match(LOR);
					 ((PropertyLTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
					}
					break;
				case RARROW:
					{
					setState(1292);
					match(RARROW);
					 ((PropertyLTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
					}
					break;
				case DARROW:
					{
					setState(1294);
					match(DARROW);
					 ((PropertyLTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
					}
					break;
				case U:
					{
					setState(1296);
					match(U);
					 ((PropertyLTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.U; 
					}
					break;
				case R:
					{
					setState(1298);
					match(R);
					 ((PropertyLTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.R; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				 ((PropertyLTLKD_Xi_superContext)_localctx).pos =  getStreamPos(); 
				setState(1303);
				((PropertyLTLKD_Xi_superContext)_localctx).right = propertyLTLKD_Xi_sub(_localctx.as);

							    ((PropertyLTLKD_Xi_superContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyLTLKD_Xi_superContext)_localctx).left.result, ((PropertyLTLKD_Xi_superContext)_localctx).right.result); 
				}
				}
				setState(1310);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

					((PropertyLTLKD_Xi_superContext)_localctx).result =  ((PropertyLTLKD_Xi_superContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyLTLKD_Xi_sub_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyLTLKD_XiContext subParenthesized;
		public PropertyLTLKD_XiContext subPhi;
		public PropertyLTLKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyLTLKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyLTLKD_XiContext propertyLTLKD_Xi() {
			return getRuleContext(PropertyLTLKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyLTLKD_Xi_superContext propertyLTLKD_Xi_super() {
			return getRuleContext(PropertyLTLKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyLTLKD_Xi_sub_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyLTLKD_Xi_sub_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyLTLKD_Xi_sub_super; }
	}

	public final PropertyLTLKD_Xi_sub_superContext propertyLTLKD_Xi_sub_super(AgentSystem as) throws RecognitionException {
		PropertyLTLKD_Xi_sub_superContext _localctx = new PropertyLTLKD_Xi_sub_superContext(_ctx, getState(), as);
		enterRule(_localctx, 94, RULE_propertyLTLKD_Xi_sub_super);
		try {
			setState(1455);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1313);
				match(LPAREN);
				setState(1314);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subParenthesized = propertyLTLKD_Xi(_localctx.as);
				setState(1315);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  ((PropertyLTLKD_Xi_sub_superContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1318);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1320);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subPhi = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1323);
				match(X);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1325);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyLTLKD_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1328);
				match(F);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1330);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyLTLKD_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1333);
				match(G);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1335);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).sub = propertyLTLKD_Xi(_localctx.as);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyLTLKD_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1338);
				match(K);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1340);
				match(LPAREN);
				setState(1341);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1342);
				match(COMMA);
				setState(1343);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1344);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1347);
				match(Eg);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1349);
				match(LPAREN);
				setState(1350);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1351);
				match(COMMA);
				setState(1352);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1353);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1356);
				match(Dg);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1358);
				match(LPAREN);
				setState(1359);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1360);
				match(COMMA);
				setState(1361);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1362);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1365);
				match(Cg);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1367);
				match(LPAREN);
				setState(1368);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1369);
				match(COMMA);
				setState(1370);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1371);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1374);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1376);
				match(K);
				setState(1377);
				match(LPAREN);
				setState(1378);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1379);
				match(COMMA);
				setState(1380);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1381);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1384);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1386);
				match(Eg);
				setState(1387);
				match(LPAREN);
				setState(1388);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1389);
				match(COMMA);
				setState(1390);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1391);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1394);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1396);
				match(Dg);
				setState(1397);
				match(LPAREN);
				setState(1398);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1399);
				match(COMMA);
				setState(1400);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1401);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(1404);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1406);
				match(Cg);
				setState(1407);
				match(LPAREN);
				setState(1408);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1409);
				match(COMMA);
				setState(1410);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1411);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(1414);
				match(Oc);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1416);
				match(LPAREN);
				setState(1417);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1418);
				match(COMMA);
				setState(1419);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1420);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Oc, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(1423);
				match(Kh);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1425);
				match(LPAREN);
				setState(1426);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1427);
				match(COMMA);
				setState(1428);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1429);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Khc, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(1432);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1434);
				match(Oc);
				setState(1435);
				match(LPAREN);
				setState(1436);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1437);
				match(COMMA);
				setState(1438);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1439);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(1442);
				match(LNOT);
				 ((PropertyLTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(1444);
				match(Kh);
				setState(1445);
				match(LPAREN);
				setState(1446);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1447);
				match(COMMA);
				setState(1448);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper = propertyLTLKD_Xi_super(_localctx.as);
				setState(1449);
				match(RPAREN);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyLTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyLTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(1452);
				((PropertyLTLKD_Xi_sub_superContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyLTLKD_Xi_sub_superContext)_localctx).result =  ((PropertyLTLKD_Xi_sub_superContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyACTLContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyACTLContext propertyACTL() {
			return getRuleContext(PropertyACTLContext.class,0);
		}
		public PropertyACTLStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLStatement; }
	}

	public final PropertyACTLStatementContext propertyACTLStatement(AgentSystem as) throws RecognitionException {
		PropertyACTLStatementContext _localctx = new PropertyACTLStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 96, RULE_propertyACTLStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1457);
			match(ACTL);
			 ((PropertyACTLStatementContext)_localctx).pos =  getStreamPos(); 
			setState(1459);
			((PropertyACTLStatementContext)_localctx).tName = match(Identifier);
			setState(1460);
			match(ASSIGN);

				((PropertyACTLStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ACTL, (((PropertyACTLStatementContext)_localctx).tName!=null?((PropertyACTLStatementContext)_localctx).tName.getText():null));

			setState(1462);
			((PropertyACTLStatementContext)_localctx).expr = propertyACTL(_localctx.as);

				_localctx.out.EXPR = ((PropertyACTLStatementContext)_localctx).expr.result;
				((PropertyACTLStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyACTL_subContext left;
		public PropertyACTL_subContext right;
		public List<PropertyACTL_subContext> propertyACTL_sub() {
			return getRuleContexts(PropertyACTL_subContext.class);
		}
		public PropertyACTL_subContext propertyACTL_sub(int i) {
			return getRuleContext(PropertyACTL_subContext.class,i);
		}
		public PropertyACTLContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTL; }
	}

	public final PropertyACTLContext propertyACTL(AgentSystem as) throws RecognitionException {
		PropertyACTLContext _localctx = new PropertyACTLContext(_ctx, getState(), as);
		enterRule(_localctx, 98, RULE_propertyACTL);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1465);
			((PropertyACTLContext)_localctx).left = propertyACTL_sub(_localctx.as);
			setState(1482);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1474);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1466);
						match(LAND);
						 ((PropertyACTLContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1468);
						match(LOR);
						 ((PropertyACTLContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1470);
						match(RARROW);
						 ((PropertyACTLContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1472);
						match(DARROW);
						 ((PropertyACTLContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLContext)_localctx).pos =  getStreamPos(); 
					setState(1477);
					((PropertyACTLContext)_localctx).right = propertyACTL_sub(_localctx.as);

								    ((PropertyACTLContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLContext)_localctx).left.result, ((PropertyACTLContext)_localctx).right.result); 
					}
					} 
				}
				setState(1484);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,57,_ctx);
			}

					((PropertyACTLContext)_localctx).result =  ((PropertyACTLContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTL_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyACTLContext subParenthesized;
		public PropertyACTLContext subPhi;
		public PropertyACTLContext sub;
		public PropertyACTLContext left;
		public PropertyACTLContext right;
		public RelationalExpressionContext expr;
		public List<PropertyACTLContext> propertyACTL() {
			return getRuleContexts(PropertyACTLContext.class);
		}
		public PropertyACTLContext propertyACTL(int i) {
			return getRuleContext(PropertyACTLContext.class,i);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyACTL_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTL_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTL_sub; }
	}

	public final PropertyACTL_subContext propertyACTL_sub(AgentSystem as) throws RecognitionException {
		PropertyACTL_subContext _localctx = new PropertyACTL_subContext(_ctx, getState(), as);
		enterRule(_localctx, 100, RULE_propertyACTL_sub);
		try {
			setState(1544);
			switch ( getInterpreter().adaptivePredict(_input,58,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1487);
				match(LPAREN);
				setState(1488);
				((PropertyACTL_subContext)_localctx).subParenthesized = propertyACTL(_localctx.as);
				setState(1489);
				match(RPAREN);

						((PropertyACTL_subContext)_localctx).result =  ((PropertyACTL_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1492);
				match(LNOT);
				 ((PropertyACTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(1494);
				((PropertyACTL_subContext)_localctx).subPhi = propertyACTL(_localctx.as);

						((PropertyACTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyACTL_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(1497);
				match(A);
				 ((PropertyACTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(1499);
				match(X);
				 ((PropertyACTL_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1502);
				((PropertyACTL_subContext)_localctx).sub = propertyACTL(_localctx.as);

						((PropertyACTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyACTL_subContext)_localctx).sub.result));
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(1505);
				match(A);
				 ((PropertyACTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(1507);
				match(F);
				 ((PropertyACTL_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1510);
				((PropertyACTL_subContext)_localctx).sub = propertyACTL(_localctx.as);

						((PropertyACTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyACTL_subContext)_localctx).sub.result));
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(1513);
				match(A);
				 ((PropertyACTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(1515);
				match(G);
				 ((PropertyACTL_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1518);
				((PropertyACTL_subContext)_localctx).sub = propertyACTL(_localctx.as);

						((PropertyACTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyACTL_subContext)_localctx).sub.result));
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1521);
				match(A);
				 ((PropertyACTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(1523);
				match(LPAREN);
				setState(1524);
				((PropertyACTL_subContext)_localctx).left = propertyACTL(_localctx.as);
				setState(1525);
				match(U);
				 ((PropertyACTL_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(1527);
				((PropertyACTL_subContext)_localctx).right = propertyACTL(_localctx.as);
				setState(1528);
				match(RPAREN);

						((PropertyACTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.U, ((PropertyACTL_subContext)_localctx).left.result, ((PropertyACTL_subContext)_localctx).right.result));
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1531);
				match(A);
				 ((PropertyACTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(1533);
				match(LPAREN);
				setState(1534);
				((PropertyACTL_subContext)_localctx).left = propertyACTL(_localctx.as);
				setState(1535);
				match(R);
				 ((PropertyACTL_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(1537);
				((PropertyACTL_subContext)_localctx).right = propertyACTL(_localctx.as);
				setState(1538);
				match(RPAREN);

						((PropertyACTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.R, ((PropertyACTL_subContext)_localctx).left.result, ((PropertyACTL_subContext)_localctx).right.result));
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1541);
				((PropertyACTL_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyACTL_subContext)_localctx).result =  ((PropertyACTL_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyACTLKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyACTLKContext propertyACTLK() {
			return getRuleContext(PropertyACTLKContext.class,0);
		}
		public PropertyACTLKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLKStatement; }
	}

	public final PropertyACTLKStatementContext propertyACTLKStatement(AgentSystem as) throws RecognitionException {
		PropertyACTLKStatementContext _localctx = new PropertyACTLKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 102, RULE_propertyACTLKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1546);
			match(ACTLK);
			 ((PropertyACTLKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(1548);
			((PropertyACTLKStatementContext)_localctx).tName = match(Identifier);
			setState(1549);
			match(ASSIGN);

				((PropertyACTLKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ACTLK, (((PropertyACTLKStatementContext)_localctx).tName!=null?((PropertyACTLKStatementContext)_localctx).tName.getText():null));

			setState(1551);
			((PropertyACTLKStatementContext)_localctx).expr = propertyACTLK(_localctx.as);

				_localctx.out.EXPR = ((PropertyACTLKStatementContext)_localctx).expr.result;
				((PropertyACTLKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyACTLK_subContext left;
		public PropertyACTLK_subContext right;
		public List<PropertyACTLK_subContext> propertyACTLK_sub() {
			return getRuleContexts(PropertyACTLK_subContext.class);
		}
		public PropertyACTLK_subContext propertyACTLK_sub(int i) {
			return getRuleContext(PropertyACTLK_subContext.class,i);
		}
		public PropertyACTLKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLK; }
	}

	public final PropertyACTLKContext propertyACTLK(AgentSystem as) throws RecognitionException {
		PropertyACTLKContext _localctx = new PropertyACTLKContext(_ctx, getState(), as);
		enterRule(_localctx, 104, RULE_propertyACTLK);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1554);
			((PropertyACTLKContext)_localctx).left = propertyACTLK_sub(_localctx.as);
			setState(1571);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1563);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1555);
						match(LAND);
						 ((PropertyACTLKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1557);
						match(LOR);
						 ((PropertyACTLKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1559);
						match(RARROW);
						 ((PropertyACTLKContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1561);
						match(DARROW);
						 ((PropertyACTLKContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLKContext)_localctx).pos =  getStreamPos(); 
					setState(1566);
					((PropertyACTLKContext)_localctx).right = propertyACTLK_sub(_localctx.as);

								    ((PropertyACTLKContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLKContext)_localctx).left.result, ((PropertyACTLKContext)_localctx).right.result); 
					}
					} 
				}
				setState(1573);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,60,_ctx);
			}

					((PropertyACTLKContext)_localctx).result =  ((PropertyACTLKContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLK_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyACTLKContext subParenthesized;
		public PropertyACTLKContext subPhi;
		public PropertyACTLKContext sub;
		public PropertyACTLKContext left;
		public PropertyACTLKContext right;
		public CoalitionGroupContext coalition;
		public PropertyACTLKContext subSuper;
		public RelationalExpressionContext expr;
		public List<PropertyACTLKContext> propertyACTLK() {
			return getRuleContexts(PropertyACTLKContext.class);
		}
		public PropertyACTLKContext propertyACTLK(int i) {
			return getRuleContext(PropertyACTLKContext.class,i);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyACTLK_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLK_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLK_sub; }
	}

	public final PropertyACTLK_subContext propertyACTLK_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLK_subContext _localctx = new PropertyACTLK_subContext(_ctx, getState(), as);
		enterRule(_localctx, 106, RULE_propertyACTLK_sub);
		try {
			setState(1669);
			switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1576);
				match(LPAREN);
				setState(1577);
				((PropertyACTLK_subContext)_localctx).subParenthesized = propertyACTLK(_localctx.as);
				setState(1578);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  ((PropertyACTLK_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1581);
				match(LNOT);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1583);
				((PropertyACTLK_subContext)_localctx).subPhi = propertyACTLK(_localctx.as);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyACTLK_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(1586);
				match(A);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1588);
				match(X);
				 ((PropertyACTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1591);
				((PropertyACTLK_subContext)_localctx).sub = propertyACTLK(_localctx.as);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyACTLK_subContext)_localctx).sub.result));
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(1594);
				match(A);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1596);
				match(F);
				 ((PropertyACTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1599);
				((PropertyACTLK_subContext)_localctx).sub = propertyACTLK(_localctx.as);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyACTLK_subContext)_localctx).sub.result));
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(1602);
				match(A);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1604);
				match(G);
				 ((PropertyACTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1607);
				((PropertyACTLK_subContext)_localctx).sub = propertyACTLK(_localctx.as);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyACTLK_subContext)_localctx).sub.result));
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1610);
				match(A);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1612);
				match(LPAREN);
				setState(1613);
				((PropertyACTLK_subContext)_localctx).left = propertyACTLK(_localctx.as);
				setState(1614);
				match(U);
				 ((PropertyACTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(1616);
				((PropertyACTLK_subContext)_localctx).right = propertyACTLK(_localctx.as);
				setState(1617);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.U, ((PropertyACTLK_subContext)_localctx).left.result, ((PropertyACTLK_subContext)_localctx).right.result));
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1620);
				match(A);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1622);
				match(LPAREN);
				setState(1623);
				((PropertyACTLK_subContext)_localctx).left = propertyACTLK(_localctx.as);
				setState(1624);
				match(R);
				 ((PropertyACTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(1626);
				((PropertyACTLK_subContext)_localctx).right = propertyACTLK(_localctx.as);
				setState(1627);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.R, ((PropertyACTLK_subContext)_localctx).left.result, ((PropertyACTLK_subContext)_localctx).right.result));
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1630);
				match(K);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1632);
				match(LPAREN);
				setState(1633);
				((PropertyACTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1634);
				match(COMMA);
				setState(1635);
				((PropertyACTLK_subContext)_localctx).subSuper = propertyACTLK(_localctx.as);
				setState(1636);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyACTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1639);
				match(Eg);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1641);
				match(LPAREN);
				setState(1642);
				((PropertyACTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1643);
				match(COMMA);
				setState(1644);
				((PropertyACTLK_subContext)_localctx).subSuper = propertyACTLK(_localctx.as);
				setState(1645);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyACTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1648);
				match(Dg);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1650);
				match(LPAREN);
				setState(1651);
				((PropertyACTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1652);
				match(COMMA);
				setState(1653);
				((PropertyACTLK_subContext)_localctx).subSuper = propertyACTLK(_localctx.as);
				setState(1654);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyACTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1657);
				match(Cg);
				 ((PropertyACTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1659);
				match(LPAREN);
				setState(1660);
				((PropertyACTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1661);
				match(COMMA);
				setState(1662);
				((PropertyACTLK_subContext)_localctx).subSuper = propertyACTLK(_localctx.as);
				setState(1663);
				match(RPAREN);

						((PropertyACTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyACTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1666);
				((PropertyACTLK_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyACTLK_subContext)_localctx).result =  ((PropertyACTLK_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyACTLKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyACTLKDContext propertyACTLKD() {
			return getRuleContext(PropertyACTLKDContext.class,0);
		}
		public PropertyACTLKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLKDStatement; }
	}

	public final PropertyACTLKDStatementContext propertyACTLKDStatement(AgentSystem as) throws RecognitionException {
		PropertyACTLKDStatementContext _localctx = new PropertyACTLKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 108, RULE_propertyACTLKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1671);
			match(ACTLKD);
			 ((PropertyACTLKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(1673);
			((PropertyACTLKDStatementContext)_localctx).tName = match(Identifier);
			setState(1674);
			match(ASSIGN);

				((PropertyACTLKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ACTLKD, (((PropertyACTLKDStatementContext)_localctx).tName!=null?((PropertyACTLKDStatementContext)_localctx).tName.getText():null));

			setState(1676);
			((PropertyACTLKDStatementContext)_localctx).expr = propertyACTLKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyACTLKDStatementContext)_localctx).expr.result;
				((PropertyACTLKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyACTLKD_subContext left;
		public PropertyACTLKD_subContext right;
		public List<PropertyACTLKD_subContext> propertyACTLKD_sub() {
			return getRuleContexts(PropertyACTLKD_subContext.class);
		}
		public PropertyACTLKD_subContext propertyACTLKD_sub(int i) {
			return getRuleContext(PropertyACTLKD_subContext.class,i);
		}
		public PropertyACTLKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLKD; }
	}

	public final PropertyACTLKDContext propertyACTLKD(AgentSystem as) throws RecognitionException {
		PropertyACTLKDContext _localctx = new PropertyACTLKDContext(_ctx, getState(), as);
		enterRule(_localctx, 110, RULE_propertyACTLKD);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1679);
			((PropertyACTLKDContext)_localctx).left = propertyACTLKD_sub(_localctx.as);
			setState(1696);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,63,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1688);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1680);
						match(LAND);
						 ((PropertyACTLKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1682);
						match(LOR);
						 ((PropertyACTLKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1684);
						match(RARROW);
						 ((PropertyACTLKDContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1686);
						match(DARROW);
						 ((PropertyACTLKDContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLKDContext)_localctx).pos =  getStreamPos(); 
					setState(1691);
					((PropertyACTLKDContext)_localctx).right = propertyACTLKD_sub(_localctx.as);

								    ((PropertyACTLKDContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLKDContext)_localctx).left.result, ((PropertyACTLKDContext)_localctx).right.result); 
					}
					} 
				}
				setState(1698);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,63,_ctx);
			}

					((PropertyACTLKDContext)_localctx).result =  ((PropertyACTLKDContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLKD_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyACTLKDContext subParenthesized;
		public PropertyACTLKDContext subPhi;
		public PropertyACTLKDContext sub;
		public PropertyACTLKDContext left;
		public PropertyACTLKDContext right;
		public CoalitionGroupContext coalition;
		public PropertyACTLKDContext subSuper;
		public RelationalExpressionContext expr;
		public List<PropertyACTLKDContext> propertyACTLKD() {
			return getRuleContexts(PropertyACTLKDContext.class);
		}
		public PropertyACTLKDContext propertyACTLKD(int i) {
			return getRuleContext(PropertyACTLKDContext.class,i);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyACTLKD_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLKD_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLKD_sub; }
	}

	public final PropertyACTLKD_subContext propertyACTLKD_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLKD_subContext _localctx = new PropertyACTLKD_subContext(_ctx, getState(), as);
		enterRule(_localctx, 112, RULE_propertyACTLKD_sub);
		try {
			setState(1812);
			switch ( getInterpreter().adaptivePredict(_input,64,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1701);
				match(LPAREN);
				setState(1702);
				((PropertyACTLKD_subContext)_localctx).subParenthesized = propertyACTLKD(_localctx.as);
				setState(1703);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  ((PropertyACTLKD_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1706);
				match(LNOT);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1708);
				((PropertyACTLKD_subContext)_localctx).subPhi = propertyACTLKD(_localctx.as);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyACTLKD_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(1711);
				match(A);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1713);
				match(X);
				 ((PropertyACTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1716);
				((PropertyACTLKD_subContext)_localctx).sub = propertyACTLKD(_localctx.as);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyACTLKD_subContext)_localctx).sub.result));
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(1719);
				match(A);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1721);
				match(F);
				 ((PropertyACTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1724);
				((PropertyACTLKD_subContext)_localctx).sub = propertyACTLKD(_localctx.as);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyACTLKD_subContext)_localctx).sub.result));
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(1727);
				match(A);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1729);
				match(G);
				 ((PropertyACTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(1732);
				((PropertyACTLKD_subContext)_localctx).sub = propertyACTLKD(_localctx.as);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyACTLKD_subContext)_localctx).sub.result));
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1735);
				match(A);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1737);
				match(LPAREN);
				setState(1738);
				((PropertyACTLKD_subContext)_localctx).left = propertyACTLKD(_localctx.as);
				setState(1739);
				match(U);
				 ((PropertyACTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(1741);
				((PropertyACTLKD_subContext)_localctx).right = propertyACTLKD(_localctx.as);
				setState(1742);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.U, ((PropertyACTLKD_subContext)_localctx).left.result, ((PropertyACTLKD_subContext)_localctx).right.result));
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1745);
				match(A);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1747);
				match(LPAREN);
				setState(1748);
				((PropertyACTLKD_subContext)_localctx).left = propertyACTLKD(_localctx.as);
				setState(1749);
				match(R);
				 ((PropertyACTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(1751);
				((PropertyACTLKD_subContext)_localctx).right = propertyACTLKD(_localctx.as);
				setState(1752);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.R, ((PropertyACTLKD_subContext)_localctx).left.result, ((PropertyACTLKD_subContext)_localctx).right.result));
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1755);
				match(K);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1757);
				match(LPAREN);
				setState(1758);
				((PropertyACTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1759);
				match(COMMA);
				setState(1760);
				((PropertyACTLKD_subContext)_localctx).subSuper = propertyACTLKD(_localctx.as);
				setState(1761);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyACTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1764);
				match(Eg);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1766);
				match(LPAREN);
				setState(1767);
				((PropertyACTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1768);
				match(COMMA);
				setState(1769);
				((PropertyACTLKD_subContext)_localctx).subSuper = propertyACTLKD(_localctx.as);
				setState(1770);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyACTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1773);
				match(Dg);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1775);
				match(LPAREN);
				setState(1776);
				((PropertyACTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1777);
				match(COMMA);
				setState(1778);
				((PropertyACTLKD_subContext)_localctx).subSuper = propertyACTLKD(_localctx.as);
				setState(1779);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyACTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1782);
				match(Cg);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1784);
				match(LPAREN);
				setState(1785);
				((PropertyACTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1786);
				match(COMMA);
				setState(1787);
				((PropertyACTLKD_subContext)_localctx).subSuper = propertyACTLKD(_localctx.as);
				setState(1788);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyACTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1791);
				match(Oc);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1793);
				match(LPAREN);
				setState(1794);
				((PropertyACTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1795);
				match(COMMA);
				setState(1796);
				((PropertyACTLKD_subContext)_localctx).subSuper = propertyACTLKD(_localctx.as);
				setState(1797);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Oc, ((PropertyACTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(1800);
				match(Kh);
				 ((PropertyACTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(1802);
				match(LPAREN);
				setState(1803);
				((PropertyACTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1804);
				match(COMMA);
				setState(1805);
				((PropertyACTLKD_subContext)_localctx).subSuper = propertyACTLKD(_localctx.as);
				setState(1806);
				match(RPAREN);

						((PropertyACTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Khc, ((PropertyACTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(1809);
				((PropertyACTLKD_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyACTLKD_subContext)_localctx).result =  ((PropertyACTLKD_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyACTLAContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyACTLAContext propertyACTLA() {
			return getRuleContext(PropertyACTLAContext.class,0);
		}
		public PropertyACTLAStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAStatement; }
	}

	public final PropertyACTLAStatementContext propertyACTLAStatement(AgentSystem as) throws RecognitionException {
		PropertyACTLAStatementContext _localctx = new PropertyACTLAStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 114, RULE_propertyACTLAStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1814);
			match(ACTL);
			 ((PropertyACTLAStatementContext)_localctx).pos =  getStreamPos(); 
			setState(1816);
			match(MULT);
			setState(1817);
			((PropertyACTLAStatementContext)_localctx).tName = match(Identifier);
			setState(1818);
			match(ASSIGN);

				((PropertyACTLAStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ACTLA, (((PropertyACTLAStatementContext)_localctx).tName!=null?((PropertyACTLAStatementContext)_localctx).tName.getText():null));

			setState(1820);
			((PropertyACTLAStatementContext)_localctx).expr = propertyACTLA(_localctx.as);

				_localctx.out.EXPR = ((PropertyACTLAStatementContext)_localctx).expr.result;
				((PropertyACTLAStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLA_subContext left;
		public PropertyACTLA_subContext right;
		public List<PropertyACTLA_subContext> propertyACTLA_sub() {
			return getRuleContexts(PropertyACTLA_subContext.class);
		}
		public PropertyACTLA_subContext propertyACTLA_sub(int i) {
			return getRuleContext(PropertyACTLA_subContext.class,i);
		}
		public PropertyACTLAContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLA; }
	}

	public final PropertyACTLAContext propertyACTLA(AgentSystem as) throws RecognitionException {
		PropertyACTLAContext _localctx = new PropertyACTLAContext(_ctx, getState(), as);
		enterRule(_localctx, 116, RULE_propertyACTLA);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1823);
			((PropertyACTLAContext)_localctx).left = propertyACTLA_sub(_localctx.as);
			setState(1840);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1832);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1824);
						match(LAND);
						 ((PropertyACTLAContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1826);
						match(LOR);
						 ((PropertyACTLAContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1828);
						match(RARROW);
						 ((PropertyACTLAContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1830);
						match(DARROW);
						 ((PropertyACTLAContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAContext)_localctx).pos =  getStreamPos(); 
					setState(1835);
					((PropertyACTLAContext)_localctx).right = propertyACTLA_sub(_localctx.as);

								    ((PropertyACTLAContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAContext)_localctx).left.result, ((PropertyACTLAContext)_localctx).right.result); 
					}
					} 
				}
				setState(1842);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
			}

					((PropertyACTLAContext)_localctx).result =  ((PropertyACTLAContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLA_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAContext subParenthesized;
		public PropertyACTLAContext subPhi;
		public PropertyACTLA_XiContext sub;
		public RelationalExpressionContext expr;
		public PropertyACTLAContext propertyACTLA() {
			return getRuleContext(PropertyACTLAContext.class,0);
		}
		public PropertyACTLA_XiContext propertyACTLA_Xi() {
			return getRuleContext(PropertyACTLA_XiContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyACTLA_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLA_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLA_sub; }
	}

	public final PropertyACTLA_subContext propertyACTLA_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLA_subContext _localctx = new PropertyACTLA_subContext(_ctx, getState(), as);
		enterRule(_localctx, 118, RULE_propertyACTLA_sub);
		try {
			setState(1863);
			switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1845);
				match(LPAREN);
				setState(1846);
				((PropertyACTLA_subContext)_localctx).subParenthesized = propertyACTLA(_localctx.as);
				setState(1847);
				match(RPAREN);

						((PropertyACTLA_subContext)_localctx).result =  ((PropertyACTLA_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1850);
				match(LNOT);
				 ((PropertyACTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(1852);
				((PropertyACTLA_subContext)_localctx).subPhi = propertyACTLA(_localctx.as);

						((PropertyACTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyACTLA_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1855);
				match(A);
				 ((PropertyACTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(1857);
				((PropertyACTLA_subContext)_localctx).sub = propertyACTLA_Xi(_localctx.as);

						((PropertyACTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyACTLA_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1860);
				((PropertyACTLA_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyACTLA_subContext)_localctx).result =  ((PropertyACTLA_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLA_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLA_subContext subPhi;
		public PropertyACTLA_Psi_subContext subPsi;
		public PropertyACTLA_subContext propertyACTLA_sub() {
			return getRuleContext(PropertyACTLA_subContext.class,0);
		}
		public PropertyACTLA_Psi_subContext propertyACTLA_Psi_sub() {
			return getRuleContext(PropertyACTLA_Psi_subContext.class,0);
		}
		public PropertyACTLA_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLA_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLA_Xi; }
	}

	public final PropertyACTLA_XiContext propertyACTLA_Xi(AgentSystem as) throws RecognitionException {
		PropertyACTLA_XiContext _localctx = new PropertyACTLA_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 120, RULE_propertyACTLA_Xi);
		try {
			setState(1871);
			switch ( getInterpreter().adaptivePredict(_input,68,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1865);
				((PropertyACTLA_XiContext)_localctx).subPhi = propertyACTLA_sub(_localctx.as);

						((PropertyACTLA_XiContext)_localctx).result =  ((PropertyACTLA_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1868);
				((PropertyACTLA_XiContext)_localctx).subPsi = propertyACTLA_Psi_sub(_localctx.as);

						((PropertyACTLA_XiContext)_localctx).result =  ((PropertyACTLA_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLA_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLA_Psi_subContext left;
		public PropertyACTLA_Psi_subContext right;
		public PropertyACTLA_XiContext leftXi;
		public PropertyACTLA_XiContext rightXi;
		public List<PropertyACTLA_Psi_subContext> propertyACTLA_Psi_sub() {
			return getRuleContexts(PropertyACTLA_Psi_subContext.class);
		}
		public PropertyACTLA_Psi_subContext propertyACTLA_Psi_sub(int i) {
			return getRuleContext(PropertyACTLA_Psi_subContext.class,i);
		}
		public List<PropertyACTLA_XiContext> propertyACTLA_Xi() {
			return getRuleContexts(PropertyACTLA_XiContext.class);
		}
		public PropertyACTLA_XiContext propertyACTLA_Xi(int i) {
			return getRuleContext(PropertyACTLA_XiContext.class,i);
		}
		public PropertyACTLA_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLA_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLA_Psi; }
	}

	public final PropertyACTLA_PsiContext propertyACTLA_Psi(AgentSystem as) throws RecognitionException {
		PropertyACTLA_PsiContext _localctx = new PropertyACTLA_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 122, RULE_propertyACTLA_Psi);
		int _la;
		try {
			setState(1913);
			switch ( getInterpreter().adaptivePredict(_input,73,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1873);
				((PropertyACTLA_PsiContext)_localctx).left = propertyACTLA_Psi_sub(_localctx.as);
				setState(1890);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(1882);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1874);
						match(LAND);
						 ((PropertyACTLA_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1876);
						match(LOR);
						 ((PropertyACTLA_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1878);
						match(RARROW);
						 ((PropertyACTLA_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1880);
						match(DARROW);
						 ((PropertyACTLA_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLA_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(1885);
					((PropertyACTLA_PsiContext)_localctx).right = propertyACTLA_Psi_sub(_localctx.as);

								    ((PropertyACTLA_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLA_PsiContext)_localctx).left.result, ((PropertyACTLA_PsiContext)_localctx).right.result); 
					}
					}
					setState(1892);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyACTLA_PsiContext)_localctx).result =  ((PropertyACTLA_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1895);
				((PropertyACTLA_PsiContext)_localctx).leftXi = propertyACTLA_Xi(_localctx.as);
				setState(1908);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(1900);
					switch (_input.LA(1)) {
					case U:
						{
						setState(1896);
						match(U);
						 ((PropertyACTLA_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(1898);
						match(R);
						 ((PropertyACTLA_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLA_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(1903);
					((PropertyACTLA_PsiContext)_localctx).rightXi = propertyACTLA_Xi(_localctx.as);

								    ((PropertyACTLA_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLA_PsiContext)_localctx).leftXi.result, ((PropertyACTLA_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(1910);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyACTLA_PsiContext)_localctx).result =  ((PropertyACTLA_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLA_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLA_PsiContext subParenthesized;
		public PropertyACTLA_XiContext sub;
		public PropertyACTLA_PsiContext propertyACTLA_Psi() {
			return getRuleContext(PropertyACTLA_PsiContext.class,0);
		}
		public PropertyACTLA_XiContext propertyACTLA_Xi() {
			return getRuleContext(PropertyACTLA_XiContext.class,0);
		}
		public PropertyACTLA_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLA_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLA_Psi_sub; }
	}

	public final PropertyACTLA_Psi_subContext propertyACTLA_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLA_Psi_subContext _localctx = new PropertyACTLA_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 124, RULE_propertyACTLA_Psi_sub);
		try {
			setState(1935);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(1915);
				match(LPAREN);
				setState(1916);
				((PropertyACTLA_Psi_subContext)_localctx).subParenthesized = propertyACTLA_Psi(_localctx.as);
				setState(1917);
				match(RPAREN);

						((PropertyACTLA_Psi_subContext)_localctx).result =  ((PropertyACTLA_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(1920);
				match(X);
				 ((PropertyACTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1922);
				((PropertyACTLA_Psi_subContext)_localctx).sub = propertyACTLA_Xi(_localctx.as);

						((PropertyACTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyACTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(1925);
				match(F);
				 ((PropertyACTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1927);
				((PropertyACTLA_Psi_subContext)_localctx).sub = propertyACTLA_Xi(_localctx.as);

						((PropertyACTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyACTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(1930);
				match(G);
				 ((PropertyACTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(1932);
				((PropertyACTLA_Psi_subContext)_localctx).sub = propertyACTLA_Xi(_localctx.as);

						((PropertyACTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyACTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyACTLAKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyACTLAKContext propertyACTLAK() {
			return getRuleContext(PropertyACTLAKContext.class,0);
		}
		public PropertyACTLAKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKStatement; }
	}

	public final PropertyACTLAKStatementContext propertyACTLAKStatement(AgentSystem as) throws RecognitionException {
		PropertyACTLAKStatementContext _localctx = new PropertyACTLAKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 126, RULE_propertyACTLAKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1937);
			match(ACTL);
			 ((PropertyACTLAKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(1939);
			match(MULT);
			setState(1940);
			match(K);
			setState(1941);
			((PropertyACTLAKStatementContext)_localctx).tName = match(Identifier);
			setState(1942);
			match(ASSIGN);

				((PropertyACTLAKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ACTLAK, (((PropertyACTLAKStatementContext)_localctx).tName!=null?((PropertyACTLAKStatementContext)_localctx).tName.getText():null));

			setState(1944);
			((PropertyACTLAKStatementContext)_localctx).expr = propertyACTLAK(_localctx.as);

				_localctx.out.EXPR = ((PropertyACTLAKStatementContext)_localctx).expr.result;
				((PropertyACTLAKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAK_subContext left;
		public PropertyACTLAK_subContext right;
		public List<PropertyACTLAK_subContext> propertyACTLAK_sub() {
			return getRuleContexts(PropertyACTLAK_subContext.class);
		}
		public PropertyACTLAK_subContext propertyACTLAK_sub(int i) {
			return getRuleContext(PropertyACTLAK_subContext.class,i);
		}
		public PropertyACTLAKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAK; }
	}

	public final PropertyACTLAKContext propertyACTLAK(AgentSystem as) throws RecognitionException {
		PropertyACTLAKContext _localctx = new PropertyACTLAKContext(_ctx, getState(), as);
		enterRule(_localctx, 128, RULE_propertyACTLAK);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1947);
			((PropertyACTLAKContext)_localctx).left = propertyACTLAK_sub(_localctx.as);
			setState(1964);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1956);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(1948);
						match(LAND);
						 ((PropertyACTLAKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(1950);
						match(LOR);
						 ((PropertyACTLAKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(1952);
						match(RARROW);
						 ((PropertyACTLAKContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(1954);
						match(DARROW);
						 ((PropertyACTLAKContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAKContext)_localctx).pos =  getStreamPos(); 
					setState(1959);
					((PropertyACTLAKContext)_localctx).right = propertyACTLAK_sub(_localctx.as);

								    ((PropertyACTLAKContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAKContext)_localctx).left.result, ((PropertyACTLAKContext)_localctx).right.result); 
					}
					} 
				}
				setState(1966);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,76,_ctx);
			}

					((PropertyACTLAKContext)_localctx).result =  ((PropertyACTLAKContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAK_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKContext subParenthesized;
		public PropertyACTLAKContext subPhi;
		public PropertyACTLAK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyACTLAK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyACTLAKContext propertyACTLAK() {
			return getRuleContext(PropertyACTLAKContext.class,0);
		}
		public PropertyACTLAK_XiContext propertyACTLAK_Xi() {
			return getRuleContext(PropertyACTLAK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyACTLAK_Xi_superContext propertyACTLAK_Xi_super() {
			return getRuleContext(PropertyACTLAK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyACTLAK_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAK_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAK_sub; }
	}

	public final PropertyACTLAK_subContext propertyACTLAK_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLAK_subContext _localctx = new PropertyACTLAK_subContext(_ctx, getState(), as);
		enterRule(_localctx, 130, RULE_propertyACTLAK_sub);
		try {
			setState(2023);
			switch ( getInterpreter().adaptivePredict(_input,77,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1969);
				match(LPAREN);
				setState(1970);
				((PropertyACTLAK_subContext)_localctx).subParenthesized = propertyACTLAK(_localctx.as);
				setState(1971);
				match(RPAREN);

						((PropertyACTLAK_subContext)_localctx).result =  ((PropertyACTLAK_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1974);
				match(LNOT);
				 ((PropertyACTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1976);
				((PropertyACTLAK_subContext)_localctx).subPhi = propertyACTLAK(_localctx.as);

						((PropertyACTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyACTLAK_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1979);
				match(A);
				 ((PropertyACTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1981);
				((PropertyACTLAK_subContext)_localctx).sub = propertyACTLAK_Xi(_localctx.as);

						((PropertyACTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyACTLAK_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1984);
				match(K);
				 ((PropertyACTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1986);
				match(LPAREN);
				setState(1987);
				((PropertyACTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1988);
				match(COMMA);
				setState(1989);
				((PropertyACTLAK_subContext)_localctx).subSuper = propertyACTLAK_Xi_super(_localctx.as);
				setState(1990);
				match(RPAREN);

						((PropertyACTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyACTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1993);
				match(Eg);
				 ((PropertyACTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(1995);
				match(LPAREN);
				setState(1996);
				((PropertyACTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(1997);
				match(COMMA);
				setState(1998);
				((PropertyACTLAK_subContext)_localctx).subSuper = propertyACTLAK_Xi_super(_localctx.as);
				setState(1999);
				match(RPAREN);

						((PropertyACTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyACTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2002);
				match(Dg);
				 ((PropertyACTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2004);
				match(LPAREN);
				setState(2005);
				((PropertyACTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2006);
				match(COMMA);
				setState(2007);
				((PropertyACTLAK_subContext)_localctx).subSuper = propertyACTLAK_Xi_super(_localctx.as);
				setState(2008);
				match(RPAREN);

						((PropertyACTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyACTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2011);
				match(Cg);
				 ((PropertyACTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2013);
				match(LPAREN);
				setState(2014);
				((PropertyACTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2015);
				match(COMMA);
				setState(2016);
				((PropertyACTLAK_subContext)_localctx).subSuper = propertyACTLAK_Xi_super(_localctx.as);
				setState(2017);
				match(RPAREN);

						((PropertyACTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyACTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2020);
				((PropertyACTLAK_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyACTLAK_subContext)_localctx).result =  ((PropertyACTLAK_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAK_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAK_subContext subPhi;
		public PropertyACTLAK_Psi_subContext subPsi;
		public PropertyACTLAK_subContext propertyACTLAK_sub() {
			return getRuleContext(PropertyACTLAK_subContext.class,0);
		}
		public PropertyACTLAK_Psi_subContext propertyACTLAK_Psi_sub() {
			return getRuleContext(PropertyACTLAK_Psi_subContext.class,0);
		}
		public PropertyACTLAK_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAK_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAK_Xi; }
	}

	public final PropertyACTLAK_XiContext propertyACTLAK_Xi(AgentSystem as) throws RecognitionException {
		PropertyACTLAK_XiContext _localctx = new PropertyACTLAK_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 132, RULE_propertyACTLAK_Xi);
		try {
			setState(2031);
			switch ( getInterpreter().adaptivePredict(_input,78,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2025);
				((PropertyACTLAK_XiContext)_localctx).subPhi = propertyACTLAK_sub(_localctx.as);

						((PropertyACTLAK_XiContext)_localctx).result =  ((PropertyACTLAK_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2028);
				((PropertyACTLAK_XiContext)_localctx).subPsi = propertyACTLAK_Psi_sub(_localctx.as);

						((PropertyACTLAK_XiContext)_localctx).result =  ((PropertyACTLAK_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAK_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKContext subPhi;
		public PropertyACTLAK_PsiContext subPsi;
		public PropertyACTLAKContext propertyACTLAK() {
			return getRuleContext(PropertyACTLAKContext.class,0);
		}
		public PropertyACTLAK_PsiContext propertyACTLAK_Psi() {
			return getRuleContext(PropertyACTLAK_PsiContext.class,0);
		}
		public PropertyACTLAK_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAK_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAK_Xi_super; }
	}

	public final PropertyACTLAK_Xi_superContext propertyACTLAK_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyACTLAK_Xi_superContext _localctx = new PropertyACTLAK_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 134, RULE_propertyACTLAK_Xi_super);
		try {
			setState(2039);
			switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2033);
				((PropertyACTLAK_Xi_superContext)_localctx).subPhi = propertyACTLAK(_localctx.as);

						((PropertyACTLAK_Xi_superContext)_localctx).result =  ((PropertyACTLAK_Xi_superContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2036);
				((PropertyACTLAK_Xi_superContext)_localctx).subPsi = propertyACTLAK_Psi(_localctx.as);

						((PropertyACTLAK_Xi_superContext)_localctx).result =  ((PropertyACTLAK_Xi_superContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAK_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAK_Psi_subContext left;
		public PropertyACTLAK_Psi_subContext right;
		public PropertyACTLAK_XiContext leftXi;
		public PropertyACTLAK_XiContext rightXi;
		public List<PropertyACTLAK_Psi_subContext> propertyACTLAK_Psi_sub() {
			return getRuleContexts(PropertyACTLAK_Psi_subContext.class);
		}
		public PropertyACTLAK_Psi_subContext propertyACTLAK_Psi_sub(int i) {
			return getRuleContext(PropertyACTLAK_Psi_subContext.class,i);
		}
		public List<PropertyACTLAK_XiContext> propertyACTLAK_Xi() {
			return getRuleContexts(PropertyACTLAK_XiContext.class);
		}
		public PropertyACTLAK_XiContext propertyACTLAK_Xi(int i) {
			return getRuleContext(PropertyACTLAK_XiContext.class,i);
		}
		public PropertyACTLAK_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAK_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAK_Psi; }
	}

	public final PropertyACTLAK_PsiContext propertyACTLAK_Psi(AgentSystem as) throws RecognitionException {
		PropertyACTLAK_PsiContext _localctx = new PropertyACTLAK_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 136, RULE_propertyACTLAK_Psi);
		int _la;
		try {
			setState(2081);
			switch ( getInterpreter().adaptivePredict(_input,84,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2041);
				((PropertyACTLAK_PsiContext)_localctx).left = propertyACTLAK_Psi_sub(_localctx.as);
				setState(2058);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(2050);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2042);
						match(LAND);
						 ((PropertyACTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2044);
						match(LOR);
						 ((PropertyACTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2046);
						match(RARROW);
						 ((PropertyACTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2048);
						match(DARROW);
						 ((PropertyACTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAK_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(2053);
					((PropertyACTLAK_PsiContext)_localctx).right = propertyACTLAK_Psi_sub(_localctx.as);

								    ((PropertyACTLAK_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAK_PsiContext)_localctx).left.result, ((PropertyACTLAK_PsiContext)_localctx).right.result); 
					}
					}
					setState(2060);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyACTLAK_PsiContext)_localctx).result =  ((PropertyACTLAK_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2063);
				((PropertyACTLAK_PsiContext)_localctx).leftXi = propertyACTLAK_Xi(_localctx.as);
				setState(2076);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(2068);
					switch (_input.LA(1)) {
					case U:
						{
						setState(2064);
						match(U);
						 ((PropertyACTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(2066);
						match(R);
						 ((PropertyACTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAK_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(2071);
					((PropertyACTLAK_PsiContext)_localctx).rightXi = propertyACTLAK_Xi(_localctx.as);

								    ((PropertyACTLAK_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAK_PsiContext)_localctx).leftXi.result, ((PropertyACTLAK_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(2078);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyACTLAK_PsiContext)_localctx).result =  ((PropertyACTLAK_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAK_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAK_PsiContext subParenthesized;
		public PropertyACTLAK_XiContext sub;
		public PropertyACTLAK_PsiContext propertyACTLAK_Psi() {
			return getRuleContext(PropertyACTLAK_PsiContext.class,0);
		}
		public PropertyACTLAK_XiContext propertyACTLAK_Xi() {
			return getRuleContext(PropertyACTLAK_XiContext.class,0);
		}
		public PropertyACTLAK_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAK_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAK_Psi_sub; }
	}

	public final PropertyACTLAK_Psi_subContext propertyACTLAK_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLAK_Psi_subContext _localctx = new PropertyACTLAK_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 138, RULE_propertyACTLAK_Psi_sub);
		try {
			setState(2103);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(2083);
				match(LPAREN);
				setState(2084);
				((PropertyACTLAK_Psi_subContext)_localctx).subParenthesized = propertyACTLAK_Psi(_localctx.as);
				setState(2085);
				match(RPAREN);

						((PropertyACTLAK_Psi_subContext)_localctx).result =  ((PropertyACTLAK_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(2088);
				match(X);
				 ((PropertyACTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2090);
				((PropertyACTLAK_Psi_subContext)_localctx).sub = propertyACTLAK_Xi(_localctx.as);

						((PropertyACTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyACTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(2093);
				match(F);
				 ((PropertyACTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2095);
				((PropertyACTLAK_Psi_subContext)_localctx).sub = propertyACTLAK_Xi(_localctx.as);

						((PropertyACTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyACTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(2098);
				match(G);
				 ((PropertyACTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2100);
				((PropertyACTLAK_Psi_subContext)_localctx).sub = propertyACTLAK_Xi(_localctx.as);

						((PropertyACTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyACTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyACTLAKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyACTLAKDContext propertyACTLAKD() {
			return getRuleContext(PropertyACTLAKDContext.class,0);
		}
		public PropertyACTLAKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKDStatement; }
	}

	public final PropertyACTLAKDStatementContext propertyACTLAKDStatement(AgentSystem as) throws RecognitionException {
		PropertyACTLAKDStatementContext _localctx = new PropertyACTLAKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 140, RULE_propertyACTLAKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2105);
			match(ACTL);
			 ((PropertyACTLAKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(2107);
			match(MULT);
			setState(2108);
			match(KD);
			setState(2109);
			((PropertyACTLAKDStatementContext)_localctx).tName = match(Identifier);
			setState(2110);
			match(ASSIGN);

				((PropertyACTLAKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ACTLAKD, (((PropertyACTLAKDStatementContext)_localctx).tName!=null?((PropertyACTLAKDStatementContext)_localctx).tName.getText():null));

			setState(2112);
			((PropertyACTLAKDStatementContext)_localctx).expr = propertyACTLAKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyACTLAKDStatementContext)_localctx).expr.result;
				((PropertyACTLAKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKD_subContext left;
		public PropertyACTLAKD_subContext right;
		public List<PropertyACTLAKD_subContext> propertyACTLAKD_sub() {
			return getRuleContexts(PropertyACTLAKD_subContext.class);
		}
		public PropertyACTLAKD_subContext propertyACTLAKD_sub(int i) {
			return getRuleContext(PropertyACTLAKD_subContext.class,i);
		}
		public PropertyACTLAKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKD; }
	}

	public final PropertyACTLAKDContext propertyACTLAKD(AgentSystem as) throws RecognitionException {
		PropertyACTLAKDContext _localctx = new PropertyACTLAKDContext(_ctx, getState(), as);
		enterRule(_localctx, 142, RULE_propertyACTLAKD);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2115);
			((PropertyACTLAKDContext)_localctx).left = propertyACTLAKD_sub(_localctx.as);
			setState(2132);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,87,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2124);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2116);
						match(LAND);
						 ((PropertyACTLAKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2118);
						match(LOR);
						 ((PropertyACTLAKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2120);
						match(RARROW);
						 ((PropertyACTLAKDContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2122);
						match(DARROW);
						 ((PropertyACTLAKDContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAKDContext)_localctx).pos =  getStreamPos(); 
					setState(2127);
					((PropertyACTLAKDContext)_localctx).right = propertyACTLAKD_sub(_localctx.as);

								    ((PropertyACTLAKDContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAKDContext)_localctx).left.result, ((PropertyACTLAKDContext)_localctx).right.result); 
					}
					} 
				}
				setState(2134);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,87,_ctx);
			}

					((PropertyACTLAKDContext)_localctx).result =  ((PropertyACTLAKDContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKD_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKDContext subParenthesized;
		public PropertyACTLAKDContext subPhi;
		public PropertyACTLAKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyACTLAKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyACTLAKDContext propertyACTLAKD() {
			return getRuleContext(PropertyACTLAKDContext.class,0);
		}
		public PropertyACTLAKD_XiContext propertyACTLAKD_Xi() {
			return getRuleContext(PropertyACTLAKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyACTLAKD_Xi_superContext propertyACTLAKD_Xi_super() {
			return getRuleContext(PropertyACTLAKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyACTLAKD_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKD_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKD_sub; }
	}

	public final PropertyACTLAKD_subContext propertyACTLAKD_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLAKD_subContext _localctx = new PropertyACTLAKD_subContext(_ctx, getState(), as);
		enterRule(_localctx, 144, RULE_propertyACTLAKD_sub);
		try {
			setState(2209);
			switch ( getInterpreter().adaptivePredict(_input,88,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2137);
				match(LPAREN);
				setState(2138);
				((PropertyACTLAKD_subContext)_localctx).subParenthesized = propertyACTLAKD(_localctx.as);
				setState(2139);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  ((PropertyACTLAKD_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2142);
				match(LNOT);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2144);
				((PropertyACTLAKD_subContext)_localctx).subPhi = propertyACTLAKD(_localctx.as);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyACTLAKD_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2147);
				match(A);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2149);
				((PropertyACTLAKD_subContext)_localctx).sub = propertyACTLAKD_Xi(_localctx.as);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyACTLAKD_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(2152);
				match(K);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2154);
				match(LPAREN);
				setState(2155);
				((PropertyACTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2156);
				match(COMMA);
				setState(2157);
				((PropertyACTLAKD_subContext)_localctx).subSuper = propertyACTLAKD_Xi_super(_localctx.as);
				setState(2158);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyACTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(2161);
				match(Eg);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2163);
				match(LPAREN);
				setState(2164);
				((PropertyACTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2165);
				match(COMMA);
				setState(2166);
				((PropertyACTLAKD_subContext)_localctx).subSuper = propertyACTLAKD_Xi_super(_localctx.as);
				setState(2167);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyACTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2170);
				match(Dg);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2172);
				match(LPAREN);
				setState(2173);
				((PropertyACTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2174);
				match(COMMA);
				setState(2175);
				((PropertyACTLAKD_subContext)_localctx).subSuper = propertyACTLAKD_Xi_super(_localctx.as);
				setState(2176);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyACTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2179);
				match(Cg);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2181);
				match(LPAREN);
				setState(2182);
				((PropertyACTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2183);
				match(COMMA);
				setState(2184);
				((PropertyACTLAKD_subContext)_localctx).subSuper = propertyACTLAKD_Xi_super(_localctx.as);
				setState(2185);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyACTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2188);
				match(Oc);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2190);
				match(LPAREN);
				setState(2191);
				((PropertyACTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2192);
				match(COMMA);
				setState(2193);
				((PropertyACTLAKD_subContext)_localctx).subSuper = propertyACTLAKD_Xi_super(_localctx.as);
				setState(2194);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Oc, ((PropertyACTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(2197);
				match(Kh);
				 ((PropertyACTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(2199);
				match(LPAREN);
				setState(2200);
				((PropertyACTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2201);
				match(COMMA);
				setState(2202);
				((PropertyACTLAKD_subContext)_localctx).subSuper = propertyACTLAKD_Xi_super(_localctx.as);
				setState(2203);
				match(RPAREN);

						((PropertyACTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Khc, ((PropertyACTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyACTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(2206);
				((PropertyACTLAKD_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyACTLAKD_subContext)_localctx).result =  ((PropertyACTLAKD_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKD_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKD_subContext subPhi;
		public PropertyACTLAKD_Psi_subContext subPsi;
		public PropertyACTLAKD_subContext propertyACTLAKD_sub() {
			return getRuleContext(PropertyACTLAKD_subContext.class,0);
		}
		public PropertyACTLAKD_Psi_subContext propertyACTLAKD_Psi_sub() {
			return getRuleContext(PropertyACTLAKD_Psi_subContext.class,0);
		}
		public PropertyACTLAKD_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKD_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKD_Xi; }
	}

	public final PropertyACTLAKD_XiContext propertyACTLAKD_Xi(AgentSystem as) throws RecognitionException {
		PropertyACTLAKD_XiContext _localctx = new PropertyACTLAKD_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 146, RULE_propertyACTLAKD_Xi);
		try {
			setState(2217);
			switch ( getInterpreter().adaptivePredict(_input,89,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2211);
				((PropertyACTLAKD_XiContext)_localctx).subPhi = propertyACTLAKD_sub(_localctx.as);

						((PropertyACTLAKD_XiContext)_localctx).result =  ((PropertyACTLAKD_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2214);
				((PropertyACTLAKD_XiContext)_localctx).subPsi = propertyACTLAKD_Psi_sub(_localctx.as);

						((PropertyACTLAKD_XiContext)_localctx).result =  ((PropertyACTLAKD_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKD_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKDContext subPhi;
		public PropertyACTLAKD_PsiContext subPsi;
		public PropertyACTLAKDContext propertyACTLAKD() {
			return getRuleContext(PropertyACTLAKDContext.class,0);
		}
		public PropertyACTLAKD_PsiContext propertyACTLAKD_Psi() {
			return getRuleContext(PropertyACTLAKD_PsiContext.class,0);
		}
		public PropertyACTLAKD_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKD_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKD_Xi_super; }
	}

	public final PropertyACTLAKD_Xi_superContext propertyACTLAKD_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyACTLAKD_Xi_superContext _localctx = new PropertyACTLAKD_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 148, RULE_propertyACTLAKD_Xi_super);
		try {
			setState(2225);
			switch ( getInterpreter().adaptivePredict(_input,90,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2219);
				((PropertyACTLAKD_Xi_superContext)_localctx).subPhi = propertyACTLAKD(_localctx.as);

						((PropertyACTLAKD_Xi_superContext)_localctx).result =  ((PropertyACTLAKD_Xi_superContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2222);
				((PropertyACTLAKD_Xi_superContext)_localctx).subPsi = propertyACTLAKD_Psi(_localctx.as);

						((PropertyACTLAKD_Xi_superContext)_localctx).result =  ((PropertyACTLAKD_Xi_superContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKD_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKD_Psi_subContext left;
		public PropertyACTLAKD_Psi_subContext right;
		public PropertyACTLAKD_XiContext leftXi;
		public PropertyACTLAKD_XiContext rightXi;
		public List<PropertyACTLAKD_Psi_subContext> propertyACTLAKD_Psi_sub() {
			return getRuleContexts(PropertyACTLAKD_Psi_subContext.class);
		}
		public PropertyACTLAKD_Psi_subContext propertyACTLAKD_Psi_sub(int i) {
			return getRuleContext(PropertyACTLAKD_Psi_subContext.class,i);
		}
		public List<PropertyACTLAKD_XiContext> propertyACTLAKD_Xi() {
			return getRuleContexts(PropertyACTLAKD_XiContext.class);
		}
		public PropertyACTLAKD_XiContext propertyACTLAKD_Xi(int i) {
			return getRuleContext(PropertyACTLAKD_XiContext.class,i);
		}
		public PropertyACTLAKD_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKD_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKD_Psi; }
	}

	public final PropertyACTLAKD_PsiContext propertyACTLAKD_Psi(AgentSystem as) throws RecognitionException {
		PropertyACTLAKD_PsiContext _localctx = new PropertyACTLAKD_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 150, RULE_propertyACTLAKD_Psi);
		int _la;
		try {
			setState(2267);
			switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2227);
				((PropertyACTLAKD_PsiContext)_localctx).left = propertyACTLAKD_Psi_sub(_localctx.as);
				setState(2244);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(2236);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2228);
						match(LAND);
						 ((PropertyACTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2230);
						match(LOR);
						 ((PropertyACTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2232);
						match(RARROW);
						 ((PropertyACTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2234);
						match(DARROW);
						 ((PropertyACTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAKD_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(2239);
					((PropertyACTLAKD_PsiContext)_localctx).right = propertyACTLAKD_Psi_sub(_localctx.as);

								    ((PropertyACTLAKD_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAKD_PsiContext)_localctx).left.result, ((PropertyACTLAKD_PsiContext)_localctx).right.result); 
					}
					}
					setState(2246);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyACTLAKD_PsiContext)_localctx).result =  ((PropertyACTLAKD_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2249);
				((PropertyACTLAKD_PsiContext)_localctx).leftXi = propertyACTLAKD_Xi(_localctx.as);
				setState(2262);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(2254);
					switch (_input.LA(1)) {
					case U:
						{
						setState(2250);
						match(U);
						 ((PropertyACTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(2252);
						match(R);
						 ((PropertyACTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyACTLAKD_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(2257);
					((PropertyACTLAKD_PsiContext)_localctx).rightXi = propertyACTLAKD_Xi(_localctx.as);

								    ((PropertyACTLAKD_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyACTLAKD_PsiContext)_localctx).leftXi.result, ((PropertyACTLAKD_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(2264);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyACTLAKD_PsiContext)_localctx).result =  ((PropertyACTLAKD_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyACTLAKD_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyACTLAKD_PsiContext subParenthesized;
		public PropertyACTLAKD_XiContext sub;
		public PropertyACTLAKD_PsiContext propertyACTLAKD_Psi() {
			return getRuleContext(PropertyACTLAKD_PsiContext.class,0);
		}
		public PropertyACTLAKD_XiContext propertyACTLAKD_Xi() {
			return getRuleContext(PropertyACTLAKD_XiContext.class,0);
		}
		public PropertyACTLAKD_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyACTLAKD_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyACTLAKD_Psi_sub; }
	}

	public final PropertyACTLAKD_Psi_subContext propertyACTLAKD_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyACTLAKD_Psi_subContext _localctx = new PropertyACTLAKD_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 152, RULE_propertyACTLAKD_Psi_sub);
		try {
			setState(2289);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(2269);
				match(LPAREN);
				setState(2270);
				((PropertyACTLAKD_Psi_subContext)_localctx).subParenthesized = propertyACTLAKD_Psi(_localctx.as);
				setState(2271);
				match(RPAREN);

						((PropertyACTLAKD_Psi_subContext)_localctx).result =  ((PropertyACTLAKD_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(2274);
				match(X);
				 ((PropertyACTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2276);
				((PropertyACTLAKD_Psi_subContext)_localctx).sub = propertyACTLAKD_Xi(_localctx.as);

						((PropertyACTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyACTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(2279);
				match(F);
				 ((PropertyACTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2281);
				((PropertyACTLAKD_Psi_subContext)_localctx).sub = propertyACTLAKD_Xi(_localctx.as);

						((PropertyACTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyACTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(2284);
				match(G);
				 ((PropertyACTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2286);
				((PropertyACTLAKD_Psi_subContext)_localctx).sub = propertyACTLAKD_Xi(_localctx.as);

						((PropertyACTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyACTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyELTLContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyELTLContext propertyELTL() {
			return getRuleContext(PropertyELTLContext.class,0);
		}
		public PropertyELTLStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLStatement; }
	}

	public final PropertyELTLStatementContext propertyELTLStatement(AgentSystem as) throws RecognitionException {
		PropertyELTLStatementContext _localctx = new PropertyELTLStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 154, RULE_propertyELTLStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2291);
			match(ELTL);
			 ((PropertyELTLStatementContext)_localctx).pos =  getStreamPos(); 
			setState(2293);
			((PropertyELTLStatementContext)_localctx).tName = match(Identifier);
			setState(2294);
			match(ASSIGN);

				((PropertyELTLStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ELTL, (((PropertyELTLStatementContext)_localctx).tName!=null?((PropertyELTLStatementContext)_localctx).tName.getText():null));

			setState(2296);
			((PropertyELTLStatementContext)_localctx).expr = propertyELTL(_localctx.as);

				_localctx.out.EXPR = ((PropertyELTLStatementContext)_localctx).expr.result;
				((PropertyELTLStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTL_XiContext sub;
		public PropertyELTL_XiContext propertyELTL_Xi() {
			return getRuleContext(PropertyELTL_XiContext.class,0);
		}
		public PropertyELTLContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTL; }
	}

	public final PropertyELTLContext propertyELTL(AgentSystem as) throws RecognitionException {
		PropertyELTLContext _localctx = new PropertyELTLContext(_ctx, getState(), as);
		enterRule(_localctx, 156, RULE_propertyELTL);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2299);
			((PropertyELTLContext)_localctx).sub = propertyELTL_Xi(_localctx.as);

					((PropertyELTLContext)_localctx).result =  new UnaryExpression(((PropertyELTLContext)_localctx).sub.result.getStreamPos(), _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyELTLContext)_localctx).sub.result);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTL_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTL_Xi_subContext left;
		public PropertyELTL_Xi_subContext right;
		public List<PropertyELTL_Xi_subContext> propertyELTL_Xi_sub() {
			return getRuleContexts(PropertyELTL_Xi_subContext.class);
		}
		public PropertyELTL_Xi_subContext propertyELTL_Xi_sub(int i) {
			return getRuleContext(PropertyELTL_Xi_subContext.class,i);
		}
		public PropertyELTL_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTL_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTL_Xi; }
	}

	public final PropertyELTL_XiContext propertyELTL_Xi(AgentSystem as) throws RecognitionException {
		PropertyELTL_XiContext _localctx = new PropertyELTL_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 158, RULE_propertyELTL_Xi);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2302);
			((PropertyELTL_XiContext)_localctx).left = propertyELTL_Xi_sub(_localctx.as);
			setState(2323);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,98,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2315);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2303);
						match(LAND);
						 ((PropertyELTL_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2305);
						match(LOR);
						 ((PropertyELTL_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2307);
						match(RARROW);
						 ((PropertyELTL_XiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2309);
						match(DARROW);
						 ((PropertyELTL_XiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					case U:
						{
						setState(2311);
						match(U);
						 ((PropertyELTL_XiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(2313);
						match(R);
						 ((PropertyELTL_XiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyELTL_XiContext)_localctx).pos =  getStreamPos(); 
					setState(2318);
					((PropertyELTL_XiContext)_localctx).right = propertyELTL_Xi_sub(_localctx.as);

								    ((PropertyELTL_XiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyELTL_XiContext)_localctx).left.result, ((PropertyELTL_XiContext)_localctx).right.result); 
					}
					} 
				}
				setState(2325);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,98,_ctx);
			}

					((PropertyELTL_XiContext)_localctx).result =  ((PropertyELTL_XiContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTL_Xi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTL_XiContext subParenthesized;
		public PropertyELTL_XiContext subPhi;
		public PropertyELTL_XiContext sub;
		public RelationalExpressionContext expr;
		public PropertyELTL_XiContext propertyELTL_Xi() {
			return getRuleContext(PropertyELTL_XiContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyELTL_Xi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTL_Xi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTL_Xi_sub; }
	}

	public final PropertyELTL_Xi_subContext propertyELTL_Xi_sub(AgentSystem as) throws RecognitionException {
		PropertyELTL_Xi_subContext _localctx = new PropertyELTL_Xi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 160, RULE_propertyELTL_Xi_sub);
		try {
			setState(2356);
			switch ( getInterpreter().adaptivePredict(_input,99,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2328);
				match(LPAREN);
				setState(2329);
				((PropertyELTL_Xi_subContext)_localctx).subParenthesized = propertyELTL_Xi(_localctx.as);
				setState(2330);
				match(RPAREN);

						((PropertyELTL_Xi_subContext)_localctx).result =  ((PropertyELTL_Xi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2333);
				match(LNOT);
				 ((PropertyELTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2335);
				((PropertyELTL_Xi_subContext)_localctx).subPhi = propertyELTL_Xi(_localctx.as);

						((PropertyELTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyELTL_Xi_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2338);
				match(X);
				 ((PropertyELTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2340);
				((PropertyELTL_Xi_subContext)_localctx).sub = propertyELTL_Xi(_localctx.as);

						((PropertyELTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyELTL_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(2343);
				match(F);
				 ((PropertyELTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2345);
				((PropertyELTL_Xi_subContext)_localctx).sub = propertyELTL_Xi(_localctx.as);

						((PropertyELTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyELTL_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(2348);
				match(G);
				 ((PropertyELTL_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2350);
				((PropertyELTL_Xi_subContext)_localctx).sub = propertyELTL_Xi(_localctx.as);

						((PropertyELTL_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyELTL_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2353);
				((PropertyELTL_Xi_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyELTL_Xi_subContext)_localctx).result =  ((PropertyELTL_Xi_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyELTLKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyELTLKContext propertyELTLK() {
			return getRuleContext(PropertyELTLKContext.class,0);
		}
		public PropertyELTLKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKStatement; }
	}

	public final PropertyELTLKStatementContext propertyELTLKStatement(AgentSystem as) throws RecognitionException {
		PropertyELTLKStatementContext _localctx = new PropertyELTLKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 162, RULE_propertyELTLKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2358);
			match(ELTLK);
			 ((PropertyELTLKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(2360);
			((PropertyELTLKStatementContext)_localctx).tName = match(Identifier);
			setState(2361);
			match(ASSIGN);

				((PropertyELTLKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ELTLK, (((PropertyELTLKStatementContext)_localctx).tName!=null?((PropertyELTLKStatementContext)_localctx).tName.getText():null));

			setState(2363);
			((PropertyELTLKStatementContext)_localctx).expr = propertyELTLK(_localctx.as);

				_localctx.out.EXPR = ((PropertyELTLKStatementContext)_localctx).expr.result;
				((PropertyELTLKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLK_XiContext sub;
		public PropertyELTLK_XiContext propertyELTLK_Xi() {
			return getRuleContext(PropertyELTLK_XiContext.class,0);
		}
		public PropertyELTLKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLK; }
	}

	public final PropertyELTLKContext propertyELTLK(AgentSystem as) throws RecognitionException {
		PropertyELTLKContext _localctx = new PropertyELTLKContext(_ctx, getState(), as);
		enterRule(_localctx, 164, RULE_propertyELTLK);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2366);
			((PropertyELTLKContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

					((PropertyELTLKContext)_localctx).result =  new UnaryExpression(((PropertyELTLKContext)_localctx).sub.result.getStreamPos(), _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyELTLKContext)_localctx).sub.result);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLK_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLK_Xi_subContext left;
		public PropertyELTLK_Xi_subContext right;
		public List<PropertyELTLK_Xi_subContext> propertyELTLK_Xi_sub() {
			return getRuleContexts(PropertyELTLK_Xi_subContext.class);
		}
		public PropertyELTLK_Xi_subContext propertyELTLK_Xi_sub(int i) {
			return getRuleContext(PropertyELTLK_Xi_subContext.class,i);
		}
		public PropertyELTLK_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLK_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLK_Xi; }
	}

	public final PropertyELTLK_XiContext propertyELTLK_Xi(AgentSystem as) throws RecognitionException {
		PropertyELTLK_XiContext _localctx = new PropertyELTLK_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 166, RULE_propertyELTLK_Xi);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2369);
			((PropertyELTLK_XiContext)_localctx).left = propertyELTLK_Xi_sub(_localctx.as);
			setState(2390);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,101,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2382);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2370);
						match(LAND);
						 ((PropertyELTLK_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2372);
						match(LOR);
						 ((PropertyELTLK_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2374);
						match(RARROW);
						 ((PropertyELTLK_XiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2376);
						match(DARROW);
						 ((PropertyELTLK_XiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					case U:
						{
						setState(2378);
						match(U);
						 ((PropertyELTLK_XiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(2380);
						match(R);
						 ((PropertyELTLK_XiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyELTLK_XiContext)_localctx).pos =  getStreamPos(); 
					setState(2385);
					((PropertyELTLK_XiContext)_localctx).right = propertyELTLK_Xi_sub(_localctx.as);

								    ((PropertyELTLK_XiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyELTLK_XiContext)_localctx).left.result, ((PropertyELTLK_XiContext)_localctx).right.result); 
					}
					} 
				}
				setState(2392);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,101,_ctx);
			}

					((PropertyELTLK_XiContext)_localctx).result =  ((PropertyELTLK_XiContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLK_Xi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLK_XiContext subParenthesized;
		public PropertyELTLK_XiContext subPhi;
		public PropertyELTLK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyELTLK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyELTLK_XiContext propertyELTLK_Xi() {
			return getRuleContext(PropertyELTLK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyELTLK_Xi_superContext propertyELTLK_Xi_super() {
			return getRuleContext(PropertyELTLK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyELTLK_Xi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLK_Xi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLK_Xi_sub; }
	}

	public final PropertyELTLK_Xi_subContext propertyELTLK_Xi_sub(AgentSystem as) throws RecognitionException {
		PropertyELTLK_Xi_subContext _localctx = new PropertyELTLK_Xi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 168, RULE_propertyELTLK_Xi_sub);
		try {
			setState(2463);
			switch ( getInterpreter().adaptivePredict(_input,102,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2395);
				match(LPAREN);
				setState(2396);
				((PropertyELTLK_Xi_subContext)_localctx).subParenthesized = propertyELTLK_Xi(_localctx.as);
				setState(2397);
				match(RPAREN);

						((PropertyELTLK_Xi_subContext)_localctx).result =  ((PropertyELTLK_Xi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2400);
				match(LNOT);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2402);
				((PropertyELTLK_Xi_subContext)_localctx).subPhi = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyELTLK_Xi_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2405);
				match(X);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2407);
				((PropertyELTLK_Xi_subContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyELTLK_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(2410);
				match(F);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2412);
				((PropertyELTLK_Xi_subContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyELTLK_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(2415);
				match(G);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2417);
				((PropertyELTLK_Xi_subContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyELTLK_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2420);
				match(LNOT);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2422);
				match(K);
				setState(2423);
				match(LPAREN);
				setState(2424);
				((PropertyELTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2425);
				match(COMMA);
				setState(2426);
				((PropertyELTLK_Xi_subContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2427);
				match(RPAREN);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyELTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2430);
				match(LNOT);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2432);
				match(Eg);
				setState(2433);
				match(LPAREN);
				setState(2434);
				((PropertyELTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2435);
				match(COMMA);
				setState(2436);
				((PropertyELTLK_Xi_subContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2437);
				match(RPAREN);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyELTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2440);
				match(LNOT);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2442);
				match(Dg);
				setState(2443);
				match(LPAREN);
				setState(2444);
				((PropertyELTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2445);
				match(COMMA);
				setState(2446);
				((PropertyELTLK_Xi_subContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2447);
				match(RPAREN);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyELTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(2450);
				match(LNOT);
				 ((PropertyELTLK_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2452);
				match(Cg);
				setState(2453);
				match(LPAREN);
				setState(2454);
				((PropertyELTLK_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2455);
				match(COMMA);
				setState(2456);
				((PropertyELTLK_Xi_subContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2457);
				match(RPAREN);

						((PropertyELTLK_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyELTLK_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(2460);
				((PropertyELTLK_Xi_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyELTLK_Xi_subContext)_localctx).result =  ((PropertyELTLK_Xi_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLK_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLK_Xi_subContext left;
		public PropertyELTLK_Xi_subContext right;
		public List<PropertyELTLK_Xi_subContext> propertyELTLK_Xi_sub() {
			return getRuleContexts(PropertyELTLK_Xi_subContext.class);
		}
		public PropertyELTLK_Xi_subContext propertyELTLK_Xi_sub(int i) {
			return getRuleContext(PropertyELTLK_Xi_subContext.class,i);
		}
		public PropertyELTLK_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLK_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLK_Xi_super; }
	}

	public final PropertyELTLK_Xi_superContext propertyELTLK_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyELTLK_Xi_superContext _localctx = new PropertyELTLK_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 170, RULE_propertyELTLK_Xi_super);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2465);
			((PropertyELTLK_Xi_superContext)_localctx).left = propertyELTLK_Xi_sub(_localctx.as);
			setState(2486);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 16)) & ~0x3f) == 0 && ((1L << (_la - 16)) & ((1L << (R - 16)) | (1L << (RARROW - 16)) | (1L << (DARROW - 16)) | (1L << (LAND - 16)) | (1L << (LOR - 16)) | (1L << (U - 16)))) != 0)) {
				{
				{
				setState(2478);
				switch (_input.LA(1)) {
				case LAND:
					{
					setState(2466);
					match(LAND);
					 ((PropertyELTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
					}
					break;
				case LOR:
					{
					setState(2468);
					match(LOR);
					 ((PropertyELTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
					}
					break;
				case RARROW:
					{
					setState(2470);
					match(RARROW);
					 ((PropertyELTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
					}
					break;
				case DARROW:
					{
					setState(2472);
					match(DARROW);
					 ((PropertyELTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
					}
					break;
				case U:
					{
					setState(2474);
					match(U);
					 ((PropertyELTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.U; 
					}
					break;
				case R:
					{
					setState(2476);
					match(R);
					 ((PropertyELTLK_Xi_superContext)_localctx).op =  BinaryExpression.Op.R; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				 ((PropertyELTLK_Xi_superContext)_localctx).pos =  getStreamPos(); 
				setState(2481);
				((PropertyELTLK_Xi_superContext)_localctx).right = propertyELTLK_Xi_sub(_localctx.as);

							    ((PropertyELTLK_Xi_superContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyELTLK_Xi_superContext)_localctx).left.result, ((PropertyELTLK_Xi_superContext)_localctx).right.result); 
				}
				}
				setState(2488);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

					((PropertyELTLK_Xi_superContext)_localctx).result =  ((PropertyELTLK_Xi_superContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLK_Xi_sub_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLK_XiContext subParenthesized;
		public PropertyELTLK_XiContext subPhi;
		public PropertyELTLK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyELTLK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyELTLK_XiContext propertyELTLK_Xi() {
			return getRuleContext(PropertyELTLK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyELTLK_Xi_superContext propertyELTLK_Xi_super() {
			return getRuleContext(PropertyELTLK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyELTLK_Xi_sub_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLK_Xi_sub_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLK_Xi_sub_super; }
	}

	public final PropertyELTLK_Xi_sub_superContext propertyELTLK_Xi_sub_super(AgentSystem as) throws RecognitionException {
		PropertyELTLK_Xi_sub_superContext _localctx = new PropertyELTLK_Xi_sub_superContext(_ctx, getState(), as);
		enterRule(_localctx, 172, RULE_propertyELTLK_Xi_sub_super);
		try {
			setState(2559);
			switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2491);
				match(LPAREN);
				setState(2492);
				((PropertyELTLK_Xi_sub_superContext)_localctx).subParenthesized = propertyELTLK_Xi(_localctx.as);
				setState(2493);
				match(RPAREN);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  ((PropertyELTLK_Xi_sub_superContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2496);
				match(LNOT);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2498);
				((PropertyELTLK_Xi_sub_superContext)_localctx).subPhi = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyELTLK_Xi_sub_superContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2501);
				match(X);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2503);
				((PropertyELTLK_Xi_sub_superContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyELTLK_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(2506);
				match(F);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2508);
				((PropertyELTLK_Xi_sub_superContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyELTLK_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(2511);
				match(G);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2513);
				((PropertyELTLK_Xi_sub_superContext)_localctx).sub = propertyELTLK_Xi(_localctx.as);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyELTLK_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2516);
				match(LNOT);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2518);
				match(K);
				setState(2519);
				match(LPAREN);
				setState(2520);
				((PropertyELTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2521);
				match(COMMA);
				setState(2522);
				((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2523);
				match(RPAREN);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2526);
				match(LNOT);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2528);
				match(Eg);
				setState(2529);
				match(LPAREN);
				setState(2530);
				((PropertyELTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2531);
				match(COMMA);
				setState(2532);
				((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2533);
				match(RPAREN);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2536);
				match(LNOT);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2538);
				match(Dg);
				setState(2539);
				match(LPAREN);
				setState(2540);
				((PropertyELTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2541);
				match(COMMA);
				setState(2542);
				((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2543);
				match(RPAREN);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(2546);
				match(LNOT);
				 ((PropertyELTLK_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2548);
				match(Cg);
				setState(2549);
				match(LPAREN);
				setState(2550);
				((PropertyELTLK_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2551);
				match(COMMA);
				setState(2552);
				((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper = propertyELTLK_Xi_super(_localctx.as);
				setState(2553);
				match(RPAREN);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyELTLK_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLK_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(2556);
				((PropertyELTLK_Xi_sub_superContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyELTLK_Xi_sub_superContext)_localctx).result =  ((PropertyELTLK_Xi_sub_superContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyELTLKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyELTLKDContext propertyELTLKD() {
			return getRuleContext(PropertyELTLKDContext.class,0);
		}
		public PropertyELTLKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKDStatement; }
	}

	public final PropertyELTLKDStatementContext propertyELTLKDStatement(AgentSystem as) throws RecognitionException {
		PropertyELTLKDStatementContext _localctx = new PropertyELTLKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 174, RULE_propertyELTLKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2561);
			match(ELTLKD);
			 ((PropertyELTLKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(2563);
			((PropertyELTLKDStatementContext)_localctx).tName = match(Identifier);
			setState(2564);
			match(ASSIGN);

				((PropertyELTLKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ELTLKD, (((PropertyELTLKDStatementContext)_localctx).tName!=null?((PropertyELTLKDStatementContext)_localctx).tName.getText():null));

			setState(2566);
			((PropertyELTLKDStatementContext)_localctx).expr = propertyELTLKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyELTLKDStatementContext)_localctx).expr.result;
				((PropertyELTLKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLKD_XiContext sub;
		public PropertyELTLKD_XiContext propertyELTLKD_Xi() {
			return getRuleContext(PropertyELTLKD_XiContext.class,0);
		}
		public PropertyELTLKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKD; }
	}

	public final PropertyELTLKDContext propertyELTLKD(AgentSystem as) throws RecognitionException {
		PropertyELTLKDContext _localctx = new PropertyELTLKDContext(_ctx, getState(), as);
		enterRule(_localctx, 176, RULE_propertyELTLKD);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2569);
			((PropertyELTLKDContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

					((PropertyELTLKDContext)_localctx).result =  new UnaryExpression(((PropertyELTLKDContext)_localctx).sub.result.getStreamPos(), _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyELTLKDContext)_localctx).sub.result);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKD_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLKD_Xi_subContext left;
		public PropertyELTLKD_Xi_subContext right;
		public List<PropertyELTLKD_Xi_subContext> propertyELTLKD_Xi_sub() {
			return getRuleContexts(PropertyELTLKD_Xi_subContext.class);
		}
		public PropertyELTLKD_Xi_subContext propertyELTLKD_Xi_sub(int i) {
			return getRuleContext(PropertyELTLKD_Xi_subContext.class,i);
		}
		public PropertyELTLKD_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKD_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKD_Xi; }
	}

	public final PropertyELTLKD_XiContext propertyELTLKD_Xi(AgentSystem as) throws RecognitionException {
		PropertyELTLKD_XiContext _localctx = new PropertyELTLKD_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 178, RULE_propertyELTLKD_Xi);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2572);
			((PropertyELTLKD_XiContext)_localctx).left = propertyELTLKD_Xi_sub(_localctx.as);
			setState(2593);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,107,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2585);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2573);
						match(LAND);
						 ((PropertyELTLKD_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2575);
						match(LOR);
						 ((PropertyELTLKD_XiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2577);
						match(RARROW);
						 ((PropertyELTLKD_XiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2579);
						match(DARROW);
						 ((PropertyELTLKD_XiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					case U:
						{
						setState(2581);
						match(U);
						 ((PropertyELTLKD_XiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(2583);
						match(R);
						 ((PropertyELTLKD_XiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyELTLKD_XiContext)_localctx).pos =  getStreamPos(); 
					setState(2588);
					((PropertyELTLKD_XiContext)_localctx).right = propertyELTLKD_Xi_sub(_localctx.as);

								    ((PropertyELTLKD_XiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyELTLKD_XiContext)_localctx).left.result, ((PropertyELTLKD_XiContext)_localctx).right.result); 
					}
					} 
				}
				setState(2595);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,107,_ctx);
			}

					((PropertyELTLKD_XiContext)_localctx).result =  ((PropertyELTLKD_XiContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKD_Xi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLKD_XiContext subParenthesized;
		public PropertyELTLKD_XiContext subPhi;
		public PropertyELTLKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyELTLKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyELTLKD_XiContext propertyELTLKD_Xi() {
			return getRuleContext(PropertyELTLKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyELTLKD_Xi_superContext propertyELTLKD_Xi_super() {
			return getRuleContext(PropertyELTLKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyELTLKD_Xi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKD_Xi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKD_Xi_sub; }
	}

	public final PropertyELTLKD_Xi_subContext propertyELTLKD_Xi_sub(AgentSystem as) throws RecognitionException {
		PropertyELTLKD_Xi_subContext _localctx = new PropertyELTLKD_Xi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 180, RULE_propertyELTLKD_Xi_sub);
		try {
			setState(2686);
			switch ( getInterpreter().adaptivePredict(_input,108,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2598);
				match(LPAREN);
				setState(2599);
				((PropertyELTLKD_Xi_subContext)_localctx).subParenthesized = propertyELTLKD_Xi(_localctx.as);
				setState(2600);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  ((PropertyELTLKD_Xi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2603);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2605);
				((PropertyELTLKD_Xi_subContext)_localctx).subPhi = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyELTLKD_Xi_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2608);
				match(X);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2610);
				((PropertyELTLKD_Xi_subContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyELTLKD_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(2613);
				match(F);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2615);
				((PropertyELTLKD_Xi_subContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyELTLKD_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(2618);
				match(G);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2620);
				((PropertyELTLKD_Xi_subContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyELTLKD_Xi_subContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2623);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2625);
				match(K);
				setState(2626);
				match(LPAREN);
				setState(2627);
				((PropertyELTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2628);
				match(COMMA);
				setState(2629);
				((PropertyELTLKD_Xi_subContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2630);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyELTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2633);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2635);
				match(Eg);
				setState(2636);
				match(LPAREN);
				setState(2637);
				((PropertyELTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2638);
				match(COMMA);
				setState(2639);
				((PropertyELTLKD_Xi_subContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2640);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyELTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2643);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2645);
				match(Dg);
				setState(2646);
				match(LPAREN);
				setState(2647);
				((PropertyELTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2648);
				match(COMMA);
				setState(2649);
				((PropertyELTLKD_Xi_subContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2650);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyELTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(2653);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2655);
				match(Cg);
				setState(2656);
				match(LPAREN);
				setState(2657);
				((PropertyELTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2658);
				match(COMMA);
				setState(2659);
				((PropertyELTLKD_Xi_subContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2660);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyELTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(2663);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2665);
				match(Oc);
				setState(2666);
				match(LPAREN);
				setState(2667);
				((PropertyELTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2668);
				match(COMMA);
				setState(2669);
				((PropertyELTLKD_Xi_subContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2670);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyELTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(2673);
				match(LNOT);
				 ((PropertyELTLKD_Xi_subContext)_localctx).pos =  getStreamPos(); 
				setState(2675);
				match(Kh);
				setState(2676);
				match(LPAREN);
				setState(2677);
				((PropertyELTLKD_Xi_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2678);
				match(COMMA);
				setState(2679);
				((PropertyELTLKD_Xi_subContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2680);
				match(RPAREN);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyELTLKD_Xi_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(2683);
				((PropertyELTLKD_Xi_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyELTLKD_Xi_subContext)_localctx).result =  ((PropertyELTLKD_Xi_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKD_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLKD_Xi_subContext left;
		public PropertyELTLKD_Xi_subContext right;
		public List<PropertyELTLKD_Xi_subContext> propertyELTLKD_Xi_sub() {
			return getRuleContexts(PropertyELTLKD_Xi_subContext.class);
		}
		public PropertyELTLKD_Xi_subContext propertyELTLKD_Xi_sub(int i) {
			return getRuleContext(PropertyELTLKD_Xi_subContext.class,i);
		}
		public PropertyELTLKD_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKD_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKD_Xi_super; }
	}

	public final PropertyELTLKD_Xi_superContext propertyELTLKD_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyELTLKD_Xi_superContext _localctx = new PropertyELTLKD_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 182, RULE_propertyELTLKD_Xi_super);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2688);
			((PropertyELTLKD_Xi_superContext)_localctx).left = propertyELTLKD_Xi_sub(_localctx.as);
			setState(2709);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 16)) & ~0x3f) == 0 && ((1L << (_la - 16)) & ((1L << (R - 16)) | (1L << (RARROW - 16)) | (1L << (DARROW - 16)) | (1L << (LAND - 16)) | (1L << (LOR - 16)) | (1L << (U - 16)))) != 0)) {
				{
				{
				setState(2701);
				switch (_input.LA(1)) {
				case LAND:
					{
					setState(2689);
					match(LAND);
					 ((PropertyELTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
					}
					break;
				case LOR:
					{
					setState(2691);
					match(LOR);
					 ((PropertyELTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
					}
					break;
				case RARROW:
					{
					setState(2693);
					match(RARROW);
					 ((PropertyELTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
					}
					break;
				case DARROW:
					{
					setState(2695);
					match(DARROW);
					 ((PropertyELTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
					}
					break;
				case U:
					{
					setState(2697);
					match(U);
					 ((PropertyELTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.U; 
					}
					break;
				case R:
					{
					setState(2699);
					match(R);
					 ((PropertyELTLKD_Xi_superContext)_localctx).op =  BinaryExpression.Op.R; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				 ((PropertyELTLKD_Xi_superContext)_localctx).pos =  getStreamPos(); 
				setState(2704);
				((PropertyELTLKD_Xi_superContext)_localctx).right = propertyELTLKD_Xi_sub(_localctx.as);

							    ((PropertyELTLKD_Xi_superContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyELTLKD_Xi_superContext)_localctx).left.result, ((PropertyELTLKD_Xi_superContext)_localctx).right.result); 
				}
				}
				setState(2711);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}

					((PropertyELTLKD_Xi_superContext)_localctx).result =  ((PropertyELTLKD_Xi_superContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyELTLKD_Xi_sub_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyELTLKD_XiContext subParenthesized;
		public PropertyELTLKD_XiContext subPhi;
		public PropertyELTLKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyELTLKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyELTLKD_XiContext propertyELTLKD_Xi() {
			return getRuleContext(PropertyELTLKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyELTLKD_Xi_superContext propertyELTLKD_Xi_super() {
			return getRuleContext(PropertyELTLKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyELTLKD_Xi_sub_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyELTLKD_Xi_sub_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyELTLKD_Xi_sub_super; }
	}

	public final PropertyELTLKD_Xi_sub_superContext propertyELTLKD_Xi_sub_super(AgentSystem as) throws RecognitionException {
		PropertyELTLKD_Xi_sub_superContext _localctx = new PropertyELTLKD_Xi_sub_superContext(_ctx, getState(), as);
		enterRule(_localctx, 184, RULE_propertyELTLKD_Xi_sub_super);
		try {
			setState(2802);
			switch ( getInterpreter().adaptivePredict(_input,111,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2714);
				match(LPAREN);
				setState(2715);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subParenthesized = propertyELTLKD_Xi(_localctx.as);
				setState(2716);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  ((PropertyELTLKD_Xi_sub_superContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2719);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2721);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subPhi = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2724);
				match(X);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2726);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyELTLKD_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(2729);
				match(F);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2731);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyELTLKD_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(2734);
				match(G);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2736);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).sub = propertyELTLKD_Xi(_localctx.as);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyELTLKD_Xi_sub_superContext)_localctx).sub.result);
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2739);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2741);
				match(K);
				setState(2742);
				match(LPAREN);
				setState(2743);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2744);
				match(COMMA);
				setState(2745);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2746);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2749);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2751);
				match(Eg);
				setState(2752);
				match(LPAREN);
				setState(2753);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2754);
				match(COMMA);
				setState(2755);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2756);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2759);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2761);
				match(Dg);
				setState(2762);
				match(LPAREN);
				setState(2763);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2764);
				match(COMMA);
				setState(2765);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2766);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(2769);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2771);
				match(Cg);
				setState(2772);
				match(LPAREN);
				setState(2773);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2774);
				match(COMMA);
				setState(2775);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2776);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(2779);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2781);
				match(Oc);
				setState(2782);
				match(LPAREN);
				setState(2783);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2784);
				match(COMMA);
				setState(2785);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2786);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(2789);
				match(LNOT);
				 ((PropertyELTLKD_Xi_sub_superContext)_localctx).pos =  getStreamPos(); 
				setState(2791);
				match(Kh);
				setState(2792);
				match(LPAREN);
				setState(2793);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2794);
				match(COMMA);
				setState(2795);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper = propertyELTLKD_Xi_super(_localctx.as);
				setState(2796);
				match(RPAREN);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyELTLKD_Xi_sub_superContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyELTLKD_Xi_sub_superContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(2799);
				((PropertyELTLKD_Xi_sub_superContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyELTLKD_Xi_sub_superContext)_localctx).result =  ((PropertyELTLKD_Xi_sub_superContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyECTLContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyECTLContext propertyECTL() {
			return getRuleContext(PropertyECTLContext.class,0);
		}
		public PropertyECTLStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLStatement; }
	}

	public final PropertyECTLStatementContext propertyECTLStatement(AgentSystem as) throws RecognitionException {
		PropertyECTLStatementContext _localctx = new PropertyECTLStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 186, RULE_propertyECTLStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2804);
			match(ECTL);
			 ((PropertyECTLStatementContext)_localctx).pos =  getStreamPos(); 
			setState(2806);
			((PropertyECTLStatementContext)_localctx).tName = match(Identifier);
			setState(2807);
			match(ASSIGN);

				((PropertyECTLStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ECTL, (((PropertyECTLStatementContext)_localctx).tName!=null?((PropertyECTLStatementContext)_localctx).tName.getText():null));

			setState(2809);
			((PropertyECTLStatementContext)_localctx).expr = propertyECTL(_localctx.as);

				_localctx.out.EXPR = ((PropertyECTLStatementContext)_localctx).expr.result;
				((PropertyECTLStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyECTL_subContext left;
		public PropertyECTL_subContext right;
		public List<PropertyECTL_subContext> propertyECTL_sub() {
			return getRuleContexts(PropertyECTL_subContext.class);
		}
		public PropertyECTL_subContext propertyECTL_sub(int i) {
			return getRuleContext(PropertyECTL_subContext.class,i);
		}
		public PropertyECTLContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTL; }
	}

	public final PropertyECTLContext propertyECTL(AgentSystem as) throws RecognitionException {
		PropertyECTLContext _localctx = new PropertyECTLContext(_ctx, getState(), as);
		enterRule(_localctx, 188, RULE_propertyECTL);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2812);
			((PropertyECTLContext)_localctx).left = propertyECTL_sub(_localctx.as);
			setState(2829);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,113,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2821);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2813);
						match(LAND);
						 ((PropertyECTLContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2815);
						match(LOR);
						 ((PropertyECTLContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2817);
						match(RARROW);
						 ((PropertyECTLContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2819);
						match(DARROW);
						 ((PropertyECTLContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLContext)_localctx).pos =  getStreamPos(); 
					setState(2824);
					((PropertyECTLContext)_localctx).right = propertyECTL_sub(_localctx.as);

								    ((PropertyECTLContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLContext)_localctx).left.result, ((PropertyECTLContext)_localctx).right.result); 
					}
					} 
				}
				setState(2831);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,113,_ctx);
			}

					((PropertyECTLContext)_localctx).result =  ((PropertyECTLContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTL_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyECTLContext subParenthesized;
		public PropertyECTLContext subPhi;
		public PropertyECTLContext sub;
		public PropertyECTLContext left;
		public PropertyECTLContext right;
		public RelationalExpressionContext expr;
		public List<PropertyECTLContext> propertyECTL() {
			return getRuleContexts(PropertyECTLContext.class);
		}
		public PropertyECTLContext propertyECTL(int i) {
			return getRuleContext(PropertyECTLContext.class,i);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyECTL_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTL_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTL_sub; }
	}

	public final PropertyECTL_subContext propertyECTL_sub(AgentSystem as) throws RecognitionException {
		PropertyECTL_subContext _localctx = new PropertyECTL_subContext(_ctx, getState(), as);
		enterRule(_localctx, 190, RULE_propertyECTL_sub);
		try {
			setState(2891);
			switch ( getInterpreter().adaptivePredict(_input,114,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2834);
				match(LPAREN);
				setState(2835);
				((PropertyECTL_subContext)_localctx).subParenthesized = propertyECTL(_localctx.as);
				setState(2836);
				match(RPAREN);

						((PropertyECTL_subContext)_localctx).result =  ((PropertyECTL_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2839);
				match(LNOT);
				 ((PropertyECTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(2841);
				((PropertyECTL_subContext)_localctx).subPhi = propertyECTL(_localctx.as);

						((PropertyECTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyECTL_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(2844);
				match(E);
				 ((PropertyECTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(2846);
				match(X);
				 ((PropertyECTL_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(2849);
				((PropertyECTL_subContext)_localctx).sub = propertyECTL(_localctx.as);

						((PropertyECTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyECTL_subContext)_localctx).sub.result));
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(2852);
				match(E);
				 ((PropertyECTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(2854);
				match(F);
				 ((PropertyECTL_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(2857);
				((PropertyECTL_subContext)_localctx).sub = propertyECTL(_localctx.as);

						((PropertyECTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyECTL_subContext)_localctx).sub.result));
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(2860);
				match(E);
				 ((PropertyECTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(2862);
				match(G);
				 ((PropertyECTL_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(2865);
				((PropertyECTL_subContext)_localctx).sub = propertyECTL(_localctx.as);

						((PropertyECTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyECTL_subContext)_localctx).sub.result));
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2868);
				match(E);
				 ((PropertyECTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(2870);
				match(LPAREN);
				setState(2871);
				((PropertyECTL_subContext)_localctx).left = propertyECTL(_localctx.as);
				setState(2872);
				match(U);
				 ((PropertyECTL_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(2874);
				((PropertyECTL_subContext)_localctx).right = propertyECTL(_localctx.as);
				setState(2875);
				match(RPAREN);

						((PropertyECTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.U, ((PropertyECTL_subContext)_localctx).left.result, ((PropertyECTL_subContext)_localctx).right.result));
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2878);
				match(E);
				 ((PropertyECTL_subContext)_localctx).pos =  getStreamPos(); 
				setState(2880);
				match(LPAREN);
				setState(2881);
				((PropertyECTL_subContext)_localctx).left = propertyECTL(_localctx.as);
				setState(2882);
				match(R);
				 ((PropertyECTL_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(2884);
				((PropertyECTL_subContext)_localctx).right = propertyECTL(_localctx.as);
				setState(2885);
				match(RPAREN);

						((PropertyECTL_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.R, ((PropertyECTL_subContext)_localctx).left.result, ((PropertyECTL_subContext)_localctx).right.result));
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2888);
				((PropertyECTL_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyECTL_subContext)_localctx).result =  ((PropertyECTL_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyECTLKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyECTLKContext propertyECTLK() {
			return getRuleContext(PropertyECTLKContext.class,0);
		}
		public PropertyECTLKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLKStatement; }
	}

	public final PropertyECTLKStatementContext propertyECTLKStatement(AgentSystem as) throws RecognitionException {
		PropertyECTLKStatementContext _localctx = new PropertyECTLKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 192, RULE_propertyECTLKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2893);
			match(ECTLK);
			 ((PropertyECTLKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(2895);
			((PropertyECTLKStatementContext)_localctx).tName = match(Identifier);
			setState(2896);
			match(ASSIGN);

				((PropertyECTLKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ECTLK, (((PropertyECTLKStatementContext)_localctx).tName!=null?((PropertyECTLKStatementContext)_localctx).tName.getText():null));

			setState(2898);
			((PropertyECTLKStatementContext)_localctx).expr = propertyECTLK(_localctx.as);

				_localctx.out.EXPR = ((PropertyECTLKStatementContext)_localctx).expr.result;
				((PropertyECTLKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyECTLK_subContext left;
		public PropertyECTLK_subContext right;
		public List<PropertyECTLK_subContext> propertyECTLK_sub() {
			return getRuleContexts(PropertyECTLK_subContext.class);
		}
		public PropertyECTLK_subContext propertyECTLK_sub(int i) {
			return getRuleContext(PropertyECTLK_subContext.class,i);
		}
		public PropertyECTLKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLK; }
	}

	public final PropertyECTLKContext propertyECTLK(AgentSystem as) throws RecognitionException {
		PropertyECTLKContext _localctx = new PropertyECTLKContext(_ctx, getState(), as);
		enterRule(_localctx, 194, RULE_propertyECTLK);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2901);
			((PropertyECTLKContext)_localctx).left = propertyECTLK_sub(_localctx.as);
			setState(2918);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,116,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2910);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(2902);
						match(LAND);
						 ((PropertyECTLKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(2904);
						match(LOR);
						 ((PropertyECTLKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(2906);
						match(RARROW);
						 ((PropertyECTLKContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(2908);
						match(DARROW);
						 ((PropertyECTLKContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLKContext)_localctx).pos =  getStreamPos(); 
					setState(2913);
					((PropertyECTLKContext)_localctx).right = propertyECTLK_sub(_localctx.as);

								    ((PropertyECTLKContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLKContext)_localctx).left.result, ((PropertyECTLKContext)_localctx).right.result); 
					}
					} 
				}
				setState(2920);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,116,_ctx);
			}

					((PropertyECTLKContext)_localctx).result =  ((PropertyECTLKContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLK_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyECTLKContext subParenthesized;
		public PropertyECTLKContext subPhi;
		public PropertyECTLKContext sub;
		public PropertyECTLKContext left;
		public PropertyECTLKContext right;
		public CoalitionGroupContext coalition;
		public PropertyECTLKContext subSuper;
		public RelationalExpressionContext expr;
		public List<PropertyECTLKContext> propertyECTLK() {
			return getRuleContexts(PropertyECTLKContext.class);
		}
		public PropertyECTLKContext propertyECTLK(int i) {
			return getRuleContext(PropertyECTLKContext.class,i);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyECTLK_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLK_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLK_sub; }
	}

	public final PropertyECTLK_subContext propertyECTLK_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLK_subContext _localctx = new PropertyECTLK_subContext(_ctx, getState(), as);
		enterRule(_localctx, 196, RULE_propertyECTLK_sub);
		try {
			setState(3020);
			switch ( getInterpreter().adaptivePredict(_input,117,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2923);
				match(LPAREN);
				setState(2924);
				((PropertyECTLK_subContext)_localctx).subParenthesized = propertyECTLK(_localctx.as);
				setState(2925);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  ((PropertyECTLK_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2928);
				match(LNOT);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2930);
				((PropertyECTLK_subContext)_localctx).subPhi = propertyECTLK(_localctx.as);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyECTLK_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(2933);
				match(E);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2935);
				match(X);
				 ((PropertyECTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(2938);
				((PropertyECTLK_subContext)_localctx).sub = propertyECTLK(_localctx.as);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyECTLK_subContext)_localctx).sub.result));
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(2941);
				match(E);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2943);
				match(F);
				 ((PropertyECTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(2946);
				((PropertyECTLK_subContext)_localctx).sub = propertyECTLK(_localctx.as);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyECTLK_subContext)_localctx).sub.result));
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(2949);
				match(E);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2951);
				match(G);
				 ((PropertyECTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(2954);
				((PropertyECTLK_subContext)_localctx).sub = propertyECTLK(_localctx.as);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyECTLK_subContext)_localctx).sub.result));
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(2957);
				match(E);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2959);
				match(LPAREN);
				setState(2960);
				((PropertyECTLK_subContext)_localctx).left = propertyECTLK(_localctx.as);
				setState(2961);
				match(U);
				 ((PropertyECTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(2963);
				((PropertyECTLK_subContext)_localctx).right = propertyECTLK(_localctx.as);
				setState(2964);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.U, ((PropertyECTLK_subContext)_localctx).left.result, ((PropertyECTLK_subContext)_localctx).right.result));
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(2967);
				match(E);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2969);
				match(LPAREN);
				setState(2970);
				((PropertyECTLK_subContext)_localctx).left = propertyECTLK(_localctx.as);
				setState(2971);
				match(R);
				 ((PropertyECTLK_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(2973);
				((PropertyECTLK_subContext)_localctx).right = propertyECTLK(_localctx.as);
				setState(2974);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.R, ((PropertyECTLK_subContext)_localctx).left.result, ((PropertyECTLK_subContext)_localctx).right.result));
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(2977);
				match(LNOT);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2979);
				match(K);
				setState(2980);
				match(LPAREN);
				setState(2981);
				((PropertyECTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2982);
				match(COMMA);
				setState(2983);
				((PropertyECTLK_subContext)_localctx).subSuper = propertyECTLK(_localctx.as);
				setState(2984);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyECTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(2987);
				match(LNOT);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2989);
				match(Eg);
				setState(2990);
				match(LPAREN);
				setState(2991);
				((PropertyECTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(2992);
				match(COMMA);
				setState(2993);
				((PropertyECTLK_subContext)_localctx).subSuper = propertyECTLK(_localctx.as);
				setState(2994);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyECTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(2997);
				match(LNOT);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(2999);
				match(Dg);
				setState(3000);
				match(LPAREN);
				setState(3001);
				((PropertyECTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3002);
				match(COMMA);
				setState(3003);
				((PropertyECTLK_subContext)_localctx).subSuper = propertyECTLK(_localctx.as);
				setState(3004);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyECTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(3007);
				match(LNOT);
				 ((PropertyECTLK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3009);
				match(Cg);
				setState(3010);
				match(LPAREN);
				setState(3011);
				((PropertyECTLK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3012);
				match(COMMA);
				setState(3013);
				((PropertyECTLK_subContext)_localctx).subSuper = propertyECTLK(_localctx.as);
				setState(3014);
				match(RPAREN);

						((PropertyECTLK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyECTLK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(3017);
				((PropertyECTLK_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyECTLK_subContext)_localctx).result =  ((PropertyECTLK_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyECTLKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyECTLKDContext propertyECTLKD() {
			return getRuleContext(PropertyECTLKDContext.class,0);
		}
		public PropertyECTLKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLKDStatement; }
	}

	public final PropertyECTLKDStatementContext propertyECTLKDStatement(AgentSystem as) throws RecognitionException {
		PropertyECTLKDStatementContext _localctx = new PropertyECTLKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 198, RULE_propertyECTLKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3022);
			match(ECTLKD);
			 ((PropertyECTLKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(3024);
			((PropertyECTLKDStatementContext)_localctx).tName = match(Identifier);
			setState(3025);
			match(ASSIGN);

				((PropertyECTLKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ECTLKD, (((PropertyECTLKDStatementContext)_localctx).tName!=null?((PropertyECTLKDStatementContext)_localctx).tName.getText():null));

			setState(3027);
			((PropertyECTLKDStatementContext)_localctx).expr = propertyECTLKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyECTLKDStatementContext)_localctx).expr.result;
				((PropertyECTLKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyECTLKD_subContext left;
		public PropertyECTLKD_subContext right;
		public List<PropertyECTLKD_subContext> propertyECTLKD_sub() {
			return getRuleContexts(PropertyECTLKD_subContext.class);
		}
		public PropertyECTLKD_subContext propertyECTLKD_sub(int i) {
			return getRuleContext(PropertyECTLKD_subContext.class,i);
		}
		public PropertyECTLKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLKD; }
	}

	public final PropertyECTLKDContext propertyECTLKD(AgentSystem as) throws RecognitionException {
		PropertyECTLKDContext _localctx = new PropertyECTLKDContext(_ctx, getState(), as);
		enterRule(_localctx, 200, RULE_propertyECTLKD);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(3030);
			((PropertyECTLKDContext)_localctx).left = propertyECTLKD_sub(_localctx.as);
			setState(3047);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,119,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(3039);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3031);
						match(LAND);
						 ((PropertyECTLKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3033);
						match(LOR);
						 ((PropertyECTLKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3035);
						match(RARROW);
						 ((PropertyECTLKDContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3037);
						match(DARROW);
						 ((PropertyECTLKDContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLKDContext)_localctx).pos =  getStreamPos(); 
					setState(3042);
					((PropertyECTLKDContext)_localctx).right = propertyECTLKD_sub(_localctx.as);

								    ((PropertyECTLKDContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLKDContext)_localctx).left.result, ((PropertyECTLKDContext)_localctx).right.result); 
					}
					} 
				}
				setState(3049);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,119,_ctx);
			}

					((PropertyECTLKDContext)_localctx).result =  ((PropertyECTLKDContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLKD_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public StreamPos pos2;
		public PropertyECTLKDContext subParenthesized;
		public PropertyECTLKDContext subPhi;
		public PropertyECTLKDContext sub;
		public PropertyECTLKDContext left;
		public PropertyECTLKDContext right;
		public CoalitionGroupContext coalition;
		public PropertyECTLKDContext subSuper;
		public RelationalExpressionContext expr;
		public List<PropertyECTLKDContext> propertyECTLKD() {
			return getRuleContexts(PropertyECTLKDContext.class);
		}
		public PropertyECTLKDContext propertyECTLKD(int i) {
			return getRuleContext(PropertyECTLKDContext.class,i);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyECTLKD_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLKD_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLKD_sub; }
	}

	public final PropertyECTLKD_subContext propertyECTLKD_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLKD_subContext _localctx = new PropertyECTLKD_subContext(_ctx, getState(), as);
		enterRule(_localctx, 202, RULE_propertyECTLKD_sub);
		try {
			setState(3169);
			switch ( getInterpreter().adaptivePredict(_input,120,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3052);
				match(LPAREN);
				setState(3053);
				((PropertyECTLKD_subContext)_localctx).subParenthesized = propertyECTLKD(_localctx.as);
				setState(3054);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  ((PropertyECTLKD_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3057);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3059);
				((PropertyECTLKD_subContext)_localctx).subPhi = propertyECTLKD(_localctx.as);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyECTLKD_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(3062);
				match(E);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3064);
				match(X);
				 ((PropertyECTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(3067);
				((PropertyECTLKD_subContext)_localctx).sub = propertyECTLKD(_localctx.as);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyECTLKD_subContext)_localctx).sub.result));
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(3070);
				match(E);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3072);
				match(F);
				 ((PropertyECTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(3075);
				((PropertyECTLKD_subContext)_localctx).sub = propertyECTLKD(_localctx.as);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyECTLKD_subContext)_localctx).sub.result));
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				{
				setState(3078);
				match(E);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3080);
				match(G);
				 ((PropertyECTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				}
				setState(3083);
				((PropertyECTLKD_subContext)_localctx).sub = propertyECTLKD(_localctx.as);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new UnaryExpression(_localctx.pos2, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyECTLKD_subContext)_localctx).sub.result));
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(3086);
				match(E);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3088);
				match(LPAREN);
				setState(3089);
				((PropertyECTLKD_subContext)_localctx).left = propertyECTLKD(_localctx.as);
				setState(3090);
				match(U);
				 ((PropertyECTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(3092);
				((PropertyECTLKD_subContext)_localctx).right = propertyECTLKD(_localctx.as);
				setState(3093);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.U, ((PropertyECTLKD_subContext)_localctx).left.result, ((PropertyECTLKD_subContext)_localctx).right.result));
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(3096);
				match(E);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3098);
				match(LPAREN);
				setState(3099);
				((PropertyECTLKD_subContext)_localctx).left = propertyECTLKD(_localctx.as);
				setState(3100);
				match(R);
				 ((PropertyECTLKD_subContext)_localctx).pos2 =  getStreamPos(); 
				setState(3102);
				((PropertyECTLKD_subContext)_localctx).right = propertyECTLKD(_localctx.as);
				setState(3103);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, new BinaryExpression(_localctx.pos2, _localctx.as.SCOPE, BinaryExpression.Op.R, ((PropertyECTLKD_subContext)_localctx).left.result, ((PropertyECTLKD_subContext)_localctx).right.result));
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(3106);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3108);
				match(K);
				setState(3109);
				match(LPAREN);
				setState(3110);
				((PropertyECTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3111);
				match(COMMA);
				setState(3112);
				((PropertyECTLKD_subContext)_localctx).subSuper = propertyECTLKD(_localctx.as);
				setState(3113);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyECTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(3116);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3118);
				match(Eg);
				setState(3119);
				match(LPAREN);
				setState(3120);
				((PropertyECTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3121);
				match(COMMA);
				setState(3122);
				((PropertyECTLKD_subContext)_localctx).subSuper = propertyECTLKD(_localctx.as);
				setState(3123);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyECTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(3126);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3128);
				match(Dg);
				setState(3129);
				match(LPAREN);
				setState(3130);
				((PropertyECTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3131);
				match(COMMA);
				setState(3132);
				((PropertyECTLKD_subContext)_localctx).subSuper = propertyECTLKD(_localctx.as);
				setState(3133);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyECTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(3136);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3138);
				match(Cg);
				setState(3139);
				match(LPAREN);
				setState(3140);
				((PropertyECTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3141);
				match(COMMA);
				setState(3142);
				((PropertyECTLKD_subContext)_localctx).subSuper = propertyECTLKD(_localctx.as);
				setState(3143);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyECTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(3146);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3148);
				match(Oc);
				setState(3149);
				match(LPAREN);
				setState(3150);
				((PropertyECTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3151);
				match(COMMA);
				setState(3152);
				((PropertyECTLKD_subContext)_localctx).subSuper = propertyECTLKD(_localctx.as);
				setState(3153);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyECTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(3156);
				match(LNOT);
				 ((PropertyECTLKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3158);
				match(Kh);
				setState(3159);
				match(LPAREN);
				setState(3160);
				((PropertyECTLKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3161);
				match(COMMA);
				setState(3162);
				((PropertyECTLKD_subContext)_localctx).subSuper = propertyECTLKD(_localctx.as);
				setState(3163);
				match(RPAREN);

						((PropertyECTLKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyECTLKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(3166);
				((PropertyECTLKD_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyECTLKD_subContext)_localctx).result =  ((PropertyECTLKD_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyECTLAContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyECTLAContext propertyECTLA() {
			return getRuleContext(PropertyECTLAContext.class,0);
		}
		public PropertyECTLAStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAStatement; }
	}

	public final PropertyECTLAStatementContext propertyECTLAStatement(AgentSystem as) throws RecognitionException {
		PropertyECTLAStatementContext _localctx = new PropertyECTLAStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 204, RULE_propertyECTLAStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3171);
			match(ECTL);
			 ((PropertyECTLAStatementContext)_localctx).pos =  getStreamPos(); 
			setState(3173);
			match(MULT);
			setState(3174);
			((PropertyECTLAStatementContext)_localctx).tName = match(Identifier);
			setState(3175);
			match(ASSIGN);

				((PropertyECTLAStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ECTLA, (((PropertyECTLAStatementContext)_localctx).tName!=null?((PropertyECTLAStatementContext)_localctx).tName.getText():null));

			setState(3177);
			((PropertyECTLAStatementContext)_localctx).expr = propertyECTLA(_localctx.as);

				_localctx.out.EXPR = ((PropertyECTLAStatementContext)_localctx).expr.result;
				((PropertyECTLAStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLA_subContext left;
		public PropertyECTLA_subContext right;
		public List<PropertyECTLA_subContext> propertyECTLA_sub() {
			return getRuleContexts(PropertyECTLA_subContext.class);
		}
		public PropertyECTLA_subContext propertyECTLA_sub(int i) {
			return getRuleContext(PropertyECTLA_subContext.class,i);
		}
		public PropertyECTLAContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLA; }
	}

	public final PropertyECTLAContext propertyECTLA(AgentSystem as) throws RecognitionException {
		PropertyECTLAContext _localctx = new PropertyECTLAContext(_ctx, getState(), as);
		enterRule(_localctx, 206, RULE_propertyECTLA);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(3180);
			((PropertyECTLAContext)_localctx).left = propertyECTLA_sub(_localctx.as);
			setState(3197);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,122,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(3189);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3181);
						match(LAND);
						 ((PropertyECTLAContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3183);
						match(LOR);
						 ((PropertyECTLAContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3185);
						match(RARROW);
						 ((PropertyECTLAContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3187);
						match(DARROW);
						 ((PropertyECTLAContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAContext)_localctx).pos =  getStreamPos(); 
					setState(3192);
					((PropertyECTLAContext)_localctx).right = propertyECTLA_sub(_localctx.as);

								    ((PropertyECTLAContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAContext)_localctx).left.result, ((PropertyECTLAContext)_localctx).right.result); 
					}
					} 
				}
				setState(3199);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,122,_ctx);
			}

					((PropertyECTLAContext)_localctx).result =  ((PropertyECTLAContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLA_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAContext subParenthesized;
		public PropertyECTLAContext subPhi;
		public PropertyECTLA_XiContext sub;
		public RelationalExpressionContext expr;
		public PropertyECTLAContext propertyECTLA() {
			return getRuleContext(PropertyECTLAContext.class,0);
		}
		public PropertyECTLA_XiContext propertyECTLA_Xi() {
			return getRuleContext(PropertyECTLA_XiContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyECTLA_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLA_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLA_sub; }
	}

	public final PropertyECTLA_subContext propertyECTLA_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLA_subContext _localctx = new PropertyECTLA_subContext(_ctx, getState(), as);
		enterRule(_localctx, 208, RULE_propertyECTLA_sub);
		try {
			setState(3220);
			switch ( getInterpreter().adaptivePredict(_input,123,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3202);
				match(LPAREN);
				setState(3203);
				((PropertyECTLA_subContext)_localctx).subParenthesized = propertyECTLA(_localctx.as);
				setState(3204);
				match(RPAREN);

						((PropertyECTLA_subContext)_localctx).result =  ((PropertyECTLA_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3207);
				match(LNOT);
				 ((PropertyECTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(3209);
				((PropertyECTLA_subContext)_localctx).subPhi = propertyECTLA(_localctx.as);

						((PropertyECTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyECTLA_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(3212);
				match(E);
				 ((PropertyECTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(3214);
				((PropertyECTLA_subContext)_localctx).sub = propertyECTLA_Xi(_localctx.as);

						((PropertyECTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyECTLA_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(3217);
				((PropertyECTLA_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyECTLA_subContext)_localctx).result =  ((PropertyECTLA_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLA_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLA_subContext subPhi;
		public PropertyECTLA_Psi_subContext subPsi;
		public PropertyECTLA_subContext propertyECTLA_sub() {
			return getRuleContext(PropertyECTLA_subContext.class,0);
		}
		public PropertyECTLA_Psi_subContext propertyECTLA_Psi_sub() {
			return getRuleContext(PropertyECTLA_Psi_subContext.class,0);
		}
		public PropertyECTLA_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLA_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLA_Xi; }
	}

	public final PropertyECTLA_XiContext propertyECTLA_Xi(AgentSystem as) throws RecognitionException {
		PropertyECTLA_XiContext _localctx = new PropertyECTLA_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 210, RULE_propertyECTLA_Xi);
		try {
			setState(3228);
			switch ( getInterpreter().adaptivePredict(_input,124,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3222);
				((PropertyECTLA_XiContext)_localctx).subPhi = propertyECTLA_sub(_localctx.as);

						((PropertyECTLA_XiContext)_localctx).result =  ((PropertyECTLA_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3225);
				((PropertyECTLA_XiContext)_localctx).subPsi = propertyECTLA_Psi_sub(_localctx.as);

						((PropertyECTLA_XiContext)_localctx).result =  ((PropertyECTLA_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLA_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLA_Psi_subContext left;
		public PropertyECTLA_Psi_subContext right;
		public PropertyECTLA_XiContext leftXi;
		public PropertyECTLA_XiContext rightXi;
		public List<PropertyECTLA_Psi_subContext> propertyECTLA_Psi_sub() {
			return getRuleContexts(PropertyECTLA_Psi_subContext.class);
		}
		public PropertyECTLA_Psi_subContext propertyECTLA_Psi_sub(int i) {
			return getRuleContext(PropertyECTLA_Psi_subContext.class,i);
		}
		public List<PropertyECTLA_XiContext> propertyECTLA_Xi() {
			return getRuleContexts(PropertyECTLA_XiContext.class);
		}
		public PropertyECTLA_XiContext propertyECTLA_Xi(int i) {
			return getRuleContext(PropertyECTLA_XiContext.class,i);
		}
		public PropertyECTLA_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLA_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLA_Psi; }
	}

	public final PropertyECTLA_PsiContext propertyECTLA_Psi(AgentSystem as) throws RecognitionException {
		PropertyECTLA_PsiContext _localctx = new PropertyECTLA_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 212, RULE_propertyECTLA_Psi);
		int _la;
		try {
			setState(3270);
			switch ( getInterpreter().adaptivePredict(_input,129,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3230);
				((PropertyECTLA_PsiContext)_localctx).left = propertyECTLA_Psi_sub(_localctx.as);
				setState(3247);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(3239);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3231);
						match(LAND);
						 ((PropertyECTLA_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3233);
						match(LOR);
						 ((PropertyECTLA_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3235);
						match(RARROW);
						 ((PropertyECTLA_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3237);
						match(DARROW);
						 ((PropertyECTLA_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLA_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3242);
					((PropertyECTLA_PsiContext)_localctx).right = propertyECTLA_Psi_sub(_localctx.as);

								    ((PropertyECTLA_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLA_PsiContext)_localctx).left.result, ((PropertyECTLA_PsiContext)_localctx).right.result); 
					}
					}
					setState(3249);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyECTLA_PsiContext)_localctx).result =  ((PropertyECTLA_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3252);
				((PropertyECTLA_PsiContext)_localctx).leftXi = propertyECTLA_Xi(_localctx.as);
				setState(3265);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(3257);
					switch (_input.LA(1)) {
					case U:
						{
						setState(3253);
						match(U);
						 ((PropertyECTLA_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(3255);
						match(R);
						 ((PropertyECTLA_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLA_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3260);
					((PropertyECTLA_PsiContext)_localctx).rightXi = propertyECTLA_Xi(_localctx.as);

								    ((PropertyECTLA_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLA_PsiContext)_localctx).leftXi.result, ((PropertyECTLA_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(3267);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyECTLA_PsiContext)_localctx).result =  ((PropertyECTLA_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLA_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLA_PsiContext subParenthesized;
		public PropertyECTLA_XiContext sub;
		public PropertyECTLA_PsiContext propertyECTLA_Psi() {
			return getRuleContext(PropertyECTLA_PsiContext.class,0);
		}
		public PropertyECTLA_XiContext propertyECTLA_Xi() {
			return getRuleContext(PropertyECTLA_XiContext.class,0);
		}
		public PropertyECTLA_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLA_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLA_Psi_sub; }
	}

	public final PropertyECTLA_Psi_subContext propertyECTLA_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLA_Psi_subContext _localctx = new PropertyECTLA_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 214, RULE_propertyECTLA_Psi_sub);
		try {
			setState(3292);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(3272);
				match(LPAREN);
				setState(3273);
				((PropertyECTLA_Psi_subContext)_localctx).subParenthesized = propertyECTLA_Psi(_localctx.as);
				setState(3274);
				match(RPAREN);

						((PropertyECTLA_Psi_subContext)_localctx).result =  ((PropertyECTLA_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(3277);
				match(X);
				 ((PropertyECTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3279);
				((PropertyECTLA_Psi_subContext)_localctx).sub = propertyECTLA_Xi(_localctx.as);

						((PropertyECTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyECTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(3282);
				match(F);
				 ((PropertyECTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3284);
				((PropertyECTLA_Psi_subContext)_localctx).sub = propertyECTLA_Xi(_localctx.as);

						((PropertyECTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyECTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(3287);
				match(G);
				 ((PropertyECTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3289);
				((PropertyECTLA_Psi_subContext)_localctx).sub = propertyECTLA_Xi(_localctx.as);

						((PropertyECTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyECTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyECTLAKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyECTLAKContext propertyECTLAK() {
			return getRuleContext(PropertyECTLAKContext.class,0);
		}
		public PropertyECTLAKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKStatement; }
	}

	public final PropertyECTLAKStatementContext propertyECTLAKStatement(AgentSystem as) throws RecognitionException {
		PropertyECTLAKStatementContext _localctx = new PropertyECTLAKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 216, RULE_propertyECTLAKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3294);
			match(ECTL);
			 ((PropertyECTLAKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(3296);
			match(MULT);
			setState(3297);
			match(K);
			setState(3298);
			((PropertyECTLAKStatementContext)_localctx).tName = match(Identifier);
			setState(3299);
			match(ASSIGN);

				((PropertyECTLAKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ECTLAK, (((PropertyECTLAKStatementContext)_localctx).tName!=null?((PropertyECTLAKStatementContext)_localctx).tName.getText():null));

			setState(3301);
			((PropertyECTLAKStatementContext)_localctx).expr = propertyECTLAK(_localctx.as);

				_localctx.out.EXPR = ((PropertyECTLAKStatementContext)_localctx).expr.result;
				((PropertyECTLAKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAK_subContext left;
		public PropertyECTLAK_subContext right;
		public List<PropertyECTLAK_subContext> propertyECTLAK_sub() {
			return getRuleContexts(PropertyECTLAK_subContext.class);
		}
		public PropertyECTLAK_subContext propertyECTLAK_sub(int i) {
			return getRuleContext(PropertyECTLAK_subContext.class,i);
		}
		public PropertyECTLAKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAK; }
	}

	public final PropertyECTLAKContext propertyECTLAK(AgentSystem as) throws RecognitionException {
		PropertyECTLAKContext _localctx = new PropertyECTLAKContext(_ctx, getState(), as);
		enterRule(_localctx, 218, RULE_propertyECTLAK);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(3304);
			((PropertyECTLAKContext)_localctx).left = propertyECTLAK_sub(_localctx.as);
			setState(3321);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,132,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(3313);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3305);
						match(LAND);
						 ((PropertyECTLAKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3307);
						match(LOR);
						 ((PropertyECTLAKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3309);
						match(RARROW);
						 ((PropertyECTLAKContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3311);
						match(DARROW);
						 ((PropertyECTLAKContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAKContext)_localctx).pos =  getStreamPos(); 
					setState(3316);
					((PropertyECTLAKContext)_localctx).right = propertyECTLAK_sub(_localctx.as);

								    ((PropertyECTLAKContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAKContext)_localctx).left.result, ((PropertyECTLAKContext)_localctx).right.result); 
					}
					} 
				}
				setState(3323);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,132,_ctx);
			}

					((PropertyECTLAKContext)_localctx).result =  ((PropertyECTLAKContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAK_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKContext subParenthesized;
		public PropertyECTLAKContext subPhi;
		public PropertyECTLAK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyECTLAK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyECTLAKContext propertyECTLAK() {
			return getRuleContext(PropertyECTLAKContext.class,0);
		}
		public PropertyECTLAK_XiContext propertyECTLAK_Xi() {
			return getRuleContext(PropertyECTLAK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyECTLAK_Xi_superContext propertyECTLAK_Xi_super() {
			return getRuleContext(PropertyECTLAK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyECTLAK_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAK_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAK_sub; }
	}

	public final PropertyECTLAK_subContext propertyECTLAK_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLAK_subContext _localctx = new PropertyECTLAK_subContext(_ctx, getState(), as);
		enterRule(_localctx, 220, RULE_propertyECTLAK_sub);
		try {
			setState(3384);
			switch ( getInterpreter().adaptivePredict(_input,133,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3326);
				match(LPAREN);
				setState(3327);
				((PropertyECTLAK_subContext)_localctx).subParenthesized = propertyECTLAK(_localctx.as);
				setState(3328);
				match(RPAREN);

						((PropertyECTLAK_subContext)_localctx).result =  ((PropertyECTLAK_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3331);
				match(LNOT);
				 ((PropertyECTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3333);
				((PropertyECTLAK_subContext)_localctx).subPhi = propertyECTLAK(_localctx.as);

						((PropertyECTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyECTLAK_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(3336);
				match(E);
				 ((PropertyECTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3338);
				((PropertyECTLAK_subContext)_localctx).sub = propertyECTLAK_Xi(_localctx.as);

						((PropertyECTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyECTLAK_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(3341);
				match(LNOT);
				 ((PropertyECTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3343);
				match(K);
				setState(3344);
				match(LPAREN);
				setState(3345);
				((PropertyECTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3346);
				match(COMMA);
				setState(3347);
				((PropertyECTLAK_subContext)_localctx).subSuper = propertyECTLAK_Xi_super(_localctx.as);
				setState(3348);
				match(RPAREN);

						((PropertyECTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyECTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(3351);
				match(LNOT);
				 ((PropertyECTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3353);
				match(Eg);
				setState(3354);
				match(LPAREN);
				setState(3355);
				((PropertyECTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3356);
				match(COMMA);
				setState(3357);
				((PropertyECTLAK_subContext)_localctx).subSuper = propertyECTLAK_Xi_super(_localctx.as);
				setState(3358);
				match(RPAREN);

						((PropertyECTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyECTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(3361);
				match(LNOT);
				 ((PropertyECTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3363);
				match(Dg);
				setState(3364);
				match(LPAREN);
				setState(3365);
				((PropertyECTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3366);
				match(COMMA);
				setState(3367);
				((PropertyECTLAK_subContext)_localctx).subSuper = propertyECTLAK_Xi_super(_localctx.as);
				setState(3368);
				match(RPAREN);

						((PropertyECTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyECTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(3371);
				match(LNOT);
				 ((PropertyECTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3373);
				match(Cg);
				setState(3374);
				match(LPAREN);
				setState(3375);
				((PropertyECTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3376);
				match(COMMA);
				setState(3377);
				((PropertyECTLAK_subContext)_localctx).subSuper = propertyECTLAK_Xi_super(_localctx.as);
				setState(3378);
				match(RPAREN);

						((PropertyECTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyECTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(3381);
				((PropertyECTLAK_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyECTLAK_subContext)_localctx).result =  ((PropertyECTLAK_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAK_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAK_subContext subPhi;
		public PropertyECTLAK_Psi_subContext subPsi;
		public PropertyECTLAK_subContext propertyECTLAK_sub() {
			return getRuleContext(PropertyECTLAK_subContext.class,0);
		}
		public PropertyECTLAK_Psi_subContext propertyECTLAK_Psi_sub() {
			return getRuleContext(PropertyECTLAK_Psi_subContext.class,0);
		}
		public PropertyECTLAK_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAK_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAK_Xi; }
	}

	public final PropertyECTLAK_XiContext propertyECTLAK_Xi(AgentSystem as) throws RecognitionException {
		PropertyECTLAK_XiContext _localctx = new PropertyECTLAK_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 222, RULE_propertyECTLAK_Xi);
		try {
			setState(3392);
			switch ( getInterpreter().adaptivePredict(_input,134,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3386);
				((PropertyECTLAK_XiContext)_localctx).subPhi = propertyECTLAK_sub(_localctx.as);

						((PropertyECTLAK_XiContext)_localctx).result =  ((PropertyECTLAK_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3389);
				((PropertyECTLAK_XiContext)_localctx).subPsi = propertyECTLAK_Psi_sub(_localctx.as);

						((PropertyECTLAK_XiContext)_localctx).result =  ((PropertyECTLAK_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAK_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKContext subPhi;
		public PropertyECTLAK_PsiContext subPsi;
		public PropertyECTLAKContext propertyECTLAK() {
			return getRuleContext(PropertyECTLAKContext.class,0);
		}
		public PropertyECTLAK_PsiContext propertyECTLAK_Psi() {
			return getRuleContext(PropertyECTLAK_PsiContext.class,0);
		}
		public PropertyECTLAK_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAK_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAK_Xi_super; }
	}

	public final PropertyECTLAK_Xi_superContext propertyECTLAK_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyECTLAK_Xi_superContext _localctx = new PropertyECTLAK_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 224, RULE_propertyECTLAK_Xi_super);
		try {
			setState(3400);
			switch ( getInterpreter().adaptivePredict(_input,135,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3394);
				((PropertyECTLAK_Xi_superContext)_localctx).subPhi = propertyECTLAK(_localctx.as);

						((PropertyECTLAK_Xi_superContext)_localctx).result =  ((PropertyECTLAK_Xi_superContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3397);
				((PropertyECTLAK_Xi_superContext)_localctx).subPsi = propertyECTLAK_Psi(_localctx.as);

						((PropertyECTLAK_Xi_superContext)_localctx).result =  ((PropertyECTLAK_Xi_superContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAK_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAK_Psi_subContext left;
		public PropertyECTLAK_Psi_subContext right;
		public PropertyECTLAK_XiContext leftXi;
		public PropertyECTLAK_XiContext rightXi;
		public List<PropertyECTLAK_Psi_subContext> propertyECTLAK_Psi_sub() {
			return getRuleContexts(PropertyECTLAK_Psi_subContext.class);
		}
		public PropertyECTLAK_Psi_subContext propertyECTLAK_Psi_sub(int i) {
			return getRuleContext(PropertyECTLAK_Psi_subContext.class,i);
		}
		public List<PropertyECTLAK_XiContext> propertyECTLAK_Xi() {
			return getRuleContexts(PropertyECTLAK_XiContext.class);
		}
		public PropertyECTLAK_XiContext propertyECTLAK_Xi(int i) {
			return getRuleContext(PropertyECTLAK_XiContext.class,i);
		}
		public PropertyECTLAK_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAK_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAK_Psi; }
	}

	public final PropertyECTLAK_PsiContext propertyECTLAK_Psi(AgentSystem as) throws RecognitionException {
		PropertyECTLAK_PsiContext _localctx = new PropertyECTLAK_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 226, RULE_propertyECTLAK_Psi);
		int _la;
		try {
			setState(3442);
			switch ( getInterpreter().adaptivePredict(_input,140,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3402);
				((PropertyECTLAK_PsiContext)_localctx).left = propertyECTLAK_Psi_sub(_localctx.as);
				setState(3419);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(3411);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3403);
						match(LAND);
						 ((PropertyECTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3405);
						match(LOR);
						 ((PropertyECTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3407);
						match(RARROW);
						 ((PropertyECTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3409);
						match(DARROW);
						 ((PropertyECTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAK_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3414);
					((PropertyECTLAK_PsiContext)_localctx).right = propertyECTLAK_Psi_sub(_localctx.as);

								    ((PropertyECTLAK_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAK_PsiContext)_localctx).left.result, ((PropertyECTLAK_PsiContext)_localctx).right.result); 
					}
					}
					setState(3421);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyECTLAK_PsiContext)_localctx).result =  ((PropertyECTLAK_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3424);
				((PropertyECTLAK_PsiContext)_localctx).leftXi = propertyECTLAK_Xi(_localctx.as);
				setState(3437);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(3429);
					switch (_input.LA(1)) {
					case U:
						{
						setState(3425);
						match(U);
						 ((PropertyECTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(3427);
						match(R);
						 ((PropertyECTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAK_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3432);
					((PropertyECTLAK_PsiContext)_localctx).rightXi = propertyECTLAK_Xi(_localctx.as);

								    ((PropertyECTLAK_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAK_PsiContext)_localctx).leftXi.result, ((PropertyECTLAK_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(3439);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyECTLAK_PsiContext)_localctx).result =  ((PropertyECTLAK_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAK_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAK_PsiContext subParenthesized;
		public PropertyECTLAK_XiContext sub;
		public PropertyECTLAK_PsiContext propertyECTLAK_Psi() {
			return getRuleContext(PropertyECTLAK_PsiContext.class,0);
		}
		public PropertyECTLAK_XiContext propertyECTLAK_Xi() {
			return getRuleContext(PropertyECTLAK_XiContext.class,0);
		}
		public PropertyECTLAK_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAK_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAK_Psi_sub; }
	}

	public final PropertyECTLAK_Psi_subContext propertyECTLAK_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLAK_Psi_subContext _localctx = new PropertyECTLAK_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 228, RULE_propertyECTLAK_Psi_sub);
		try {
			setState(3464);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(3444);
				match(LPAREN);
				setState(3445);
				((PropertyECTLAK_Psi_subContext)_localctx).subParenthesized = propertyECTLAK_Psi(_localctx.as);
				setState(3446);
				match(RPAREN);

						((PropertyECTLAK_Psi_subContext)_localctx).result =  ((PropertyECTLAK_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(3449);
				match(X);
				 ((PropertyECTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3451);
				((PropertyECTLAK_Psi_subContext)_localctx).sub = propertyECTLAK_Xi(_localctx.as);

						((PropertyECTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyECTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(3454);
				match(F);
				 ((PropertyECTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3456);
				((PropertyECTLAK_Psi_subContext)_localctx).sub = propertyECTLAK_Xi(_localctx.as);

						((PropertyECTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyECTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(3459);
				match(G);
				 ((PropertyECTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3461);
				((PropertyECTLAK_Psi_subContext)_localctx).sub = propertyECTLAK_Xi(_localctx.as);

						((PropertyECTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyECTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyECTLAKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyECTLAKDContext propertyECTLAKD() {
			return getRuleContext(PropertyECTLAKDContext.class,0);
		}
		public PropertyECTLAKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKDStatement; }
	}

	public final PropertyECTLAKDStatementContext propertyECTLAKDStatement(AgentSystem as) throws RecognitionException {
		PropertyECTLAKDStatementContext _localctx = new PropertyECTLAKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 230, RULE_propertyECTLAKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3466);
			match(ECTL);
			 ((PropertyECTLAKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(3468);
			match(MULT);
			setState(3469);
			match(KD);
			setState(3470);
			((PropertyECTLAKDStatementContext)_localctx).tName = match(Identifier);
			setState(3471);
			match(ASSIGN);

				((PropertyECTLAKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.ECTLAKD, (((PropertyECTLAKDStatementContext)_localctx).tName!=null?((PropertyECTLAKDStatementContext)_localctx).tName.getText():null));

			setState(3473);
			((PropertyECTLAKDStatementContext)_localctx).expr = propertyECTLAKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyECTLAKDStatementContext)_localctx).expr.result;
				((PropertyECTLAKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKD_subContext left;
		public PropertyECTLAKD_subContext right;
		public List<PropertyECTLAKD_subContext> propertyECTLAKD_sub() {
			return getRuleContexts(PropertyECTLAKD_subContext.class);
		}
		public PropertyECTLAKD_subContext propertyECTLAKD_sub(int i) {
			return getRuleContext(PropertyECTLAKD_subContext.class,i);
		}
		public PropertyECTLAKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKD; }
	}

	public final PropertyECTLAKDContext propertyECTLAKD(AgentSystem as) throws RecognitionException {
		PropertyECTLAKDContext _localctx = new PropertyECTLAKDContext(_ctx, getState(), as);
		enterRule(_localctx, 232, RULE_propertyECTLAKD);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(3476);
			((PropertyECTLAKDContext)_localctx).left = propertyECTLAKD_sub(_localctx.as);
			setState(3493);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,143,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(3485);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3477);
						match(LAND);
						 ((PropertyECTLAKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3479);
						match(LOR);
						 ((PropertyECTLAKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3481);
						match(RARROW);
						 ((PropertyECTLAKDContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3483);
						match(DARROW);
						 ((PropertyECTLAKDContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAKDContext)_localctx).pos =  getStreamPos(); 
					setState(3488);
					((PropertyECTLAKDContext)_localctx).right = propertyECTLAKD_sub(_localctx.as);

								    ((PropertyECTLAKDContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAKDContext)_localctx).left.result, ((PropertyECTLAKDContext)_localctx).right.result); 
					}
					} 
				}
				setState(3495);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,143,_ctx);
			}

					((PropertyECTLAKDContext)_localctx).result =  ((PropertyECTLAKDContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKD_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKDContext subParenthesized;
		public PropertyECTLAKDContext subPhi;
		public PropertyECTLAKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyECTLAKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyECTLAKDContext propertyECTLAKD() {
			return getRuleContext(PropertyECTLAKDContext.class,0);
		}
		public PropertyECTLAKD_XiContext propertyECTLAKD_Xi() {
			return getRuleContext(PropertyECTLAKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyECTLAKD_Xi_superContext propertyECTLAKD_Xi_super() {
			return getRuleContext(PropertyECTLAKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyECTLAKD_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKD_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKD_sub; }
	}

	public final PropertyECTLAKD_subContext propertyECTLAKD_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLAKD_subContext _localctx = new PropertyECTLAKD_subContext(_ctx, getState(), as);
		enterRule(_localctx, 234, RULE_propertyECTLAKD_sub);
		try {
			setState(3576);
			switch ( getInterpreter().adaptivePredict(_input,144,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3498);
				match(LPAREN);
				setState(3499);
				((PropertyECTLAKD_subContext)_localctx).subParenthesized = propertyECTLAKD(_localctx.as);
				setState(3500);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  ((PropertyECTLAKD_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3503);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3505);
				((PropertyECTLAKD_subContext)_localctx).subPhi = propertyECTLAKD(_localctx.as);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyECTLAKD_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(3508);
				match(E);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3510);
				((PropertyECTLAKD_subContext)_localctx).sub = propertyECTLAKD_Xi(_localctx.as);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyECTLAKD_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(3513);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3515);
				match(K);
				setState(3516);
				match(LPAREN);
				setState(3517);
				((PropertyECTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3518);
				match(COMMA);
				setState(3519);
				((PropertyECTLAKD_subContext)_localctx).subSuper = propertyECTLAKD_Xi_super(_localctx.as);
				setState(3520);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyECTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(3523);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3525);
				match(Eg);
				setState(3526);
				match(LPAREN);
				setState(3527);
				((PropertyECTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3528);
				match(COMMA);
				setState(3529);
				((PropertyECTLAKD_subContext)_localctx).subSuper = propertyECTLAKD_Xi_super(_localctx.as);
				setState(3530);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyECTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(3533);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3535);
				match(Dg);
				setState(3536);
				match(LPAREN);
				setState(3537);
				((PropertyECTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3538);
				match(COMMA);
				setState(3539);
				((PropertyECTLAKD_subContext)_localctx).subSuper = propertyECTLAKD_Xi_super(_localctx.as);
				setState(3540);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyECTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(3543);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3545);
				match(Cg);
				setState(3546);
				match(LPAREN);
				setState(3547);
				((PropertyECTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3548);
				match(COMMA);
				setState(3549);
				((PropertyECTLAKD_subContext)_localctx).subSuper = propertyECTLAKD_Xi_super(_localctx.as);
				setState(3550);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyECTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(3553);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3555);
				match(Oc);
				setState(3556);
				match(LPAREN);
				setState(3557);
				((PropertyECTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3558);
				match(COMMA);
				setState(3559);
				((PropertyECTLAKD_subContext)_localctx).subSuper = propertyECTLAKD_Xi_super(_localctx.as);
				setState(3560);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyECTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(3563);
				match(LNOT);
				 ((PropertyECTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(3565);
				match(Kh);
				setState(3566);
				match(LPAREN);
				setState(3567);
				((PropertyECTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3568);
				match(COMMA);
				setState(3569);
				((PropertyECTLAKD_subContext)_localctx).subSuper = propertyECTLAKD_Xi_super(_localctx.as);
				setState(3570);
				match(RPAREN);

						((PropertyECTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyECTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyECTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(3573);
				((PropertyECTLAKD_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyECTLAKD_subContext)_localctx).result =  ((PropertyECTLAKD_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKD_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKD_subContext subPhi;
		public PropertyECTLAKD_Psi_subContext subPsi;
		public PropertyECTLAKD_subContext propertyECTLAKD_sub() {
			return getRuleContext(PropertyECTLAKD_subContext.class,0);
		}
		public PropertyECTLAKD_Psi_subContext propertyECTLAKD_Psi_sub() {
			return getRuleContext(PropertyECTLAKD_Psi_subContext.class,0);
		}
		public PropertyECTLAKD_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKD_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKD_Xi; }
	}

	public final PropertyECTLAKD_XiContext propertyECTLAKD_Xi(AgentSystem as) throws RecognitionException {
		PropertyECTLAKD_XiContext _localctx = new PropertyECTLAKD_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 236, RULE_propertyECTLAKD_Xi);
		try {
			setState(3584);
			switch ( getInterpreter().adaptivePredict(_input,145,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3578);
				((PropertyECTLAKD_XiContext)_localctx).subPhi = propertyECTLAKD_sub(_localctx.as);

						((PropertyECTLAKD_XiContext)_localctx).result =  ((PropertyECTLAKD_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3581);
				((PropertyECTLAKD_XiContext)_localctx).subPsi = propertyECTLAKD_Psi_sub(_localctx.as);

						((PropertyECTLAKD_XiContext)_localctx).result =  ((PropertyECTLAKD_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKD_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKDContext subPhi;
		public PropertyECTLAKD_PsiContext subPsi;
		public PropertyECTLAKDContext propertyECTLAKD() {
			return getRuleContext(PropertyECTLAKDContext.class,0);
		}
		public PropertyECTLAKD_PsiContext propertyECTLAKD_Psi() {
			return getRuleContext(PropertyECTLAKD_PsiContext.class,0);
		}
		public PropertyECTLAKD_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKD_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKD_Xi_super; }
	}

	public final PropertyECTLAKD_Xi_superContext propertyECTLAKD_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyECTLAKD_Xi_superContext _localctx = new PropertyECTLAKD_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 238, RULE_propertyECTLAKD_Xi_super);
		try {
			setState(3592);
			switch ( getInterpreter().adaptivePredict(_input,146,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3586);
				((PropertyECTLAKD_Xi_superContext)_localctx).subPhi = propertyECTLAKD(_localctx.as);

						((PropertyECTLAKD_Xi_superContext)_localctx).result =  ((PropertyECTLAKD_Xi_superContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3589);
				((PropertyECTLAKD_Xi_superContext)_localctx).subPsi = propertyECTLAKD_Psi(_localctx.as);

						((PropertyECTLAKD_Xi_superContext)_localctx).result =  ((PropertyECTLAKD_Xi_superContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKD_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKD_Psi_subContext left;
		public PropertyECTLAKD_Psi_subContext right;
		public PropertyECTLAKD_XiContext leftXi;
		public PropertyECTLAKD_XiContext rightXi;
		public List<PropertyECTLAKD_Psi_subContext> propertyECTLAKD_Psi_sub() {
			return getRuleContexts(PropertyECTLAKD_Psi_subContext.class);
		}
		public PropertyECTLAKD_Psi_subContext propertyECTLAKD_Psi_sub(int i) {
			return getRuleContext(PropertyECTLAKD_Psi_subContext.class,i);
		}
		public List<PropertyECTLAKD_XiContext> propertyECTLAKD_Xi() {
			return getRuleContexts(PropertyECTLAKD_XiContext.class);
		}
		public PropertyECTLAKD_XiContext propertyECTLAKD_Xi(int i) {
			return getRuleContext(PropertyECTLAKD_XiContext.class,i);
		}
		public PropertyECTLAKD_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKD_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKD_Psi; }
	}

	public final PropertyECTLAKD_PsiContext propertyECTLAKD_Psi(AgentSystem as) throws RecognitionException {
		PropertyECTLAKD_PsiContext _localctx = new PropertyECTLAKD_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 240, RULE_propertyECTLAKD_Psi);
		int _la;
		try {
			setState(3634);
			switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3594);
				((PropertyECTLAKD_PsiContext)_localctx).left = propertyECTLAKD_Psi_sub(_localctx.as);
				setState(3611);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(3603);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3595);
						match(LAND);
						 ((PropertyECTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3597);
						match(LOR);
						 ((PropertyECTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3599);
						match(RARROW);
						 ((PropertyECTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3601);
						match(DARROW);
						 ((PropertyECTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAKD_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3606);
					((PropertyECTLAKD_PsiContext)_localctx).right = propertyECTLAKD_Psi_sub(_localctx.as);

								    ((PropertyECTLAKD_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAKD_PsiContext)_localctx).left.result, ((PropertyECTLAKD_PsiContext)_localctx).right.result); 
					}
					}
					setState(3613);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyECTLAKD_PsiContext)_localctx).result =  ((PropertyECTLAKD_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3616);
				((PropertyECTLAKD_PsiContext)_localctx).leftXi = propertyECTLAKD_Xi(_localctx.as);
				setState(3629);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(3621);
					switch (_input.LA(1)) {
					case U:
						{
						setState(3617);
						match(U);
						 ((PropertyECTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(3619);
						match(R);
						 ((PropertyECTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyECTLAKD_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3624);
					((PropertyECTLAKD_PsiContext)_localctx).rightXi = propertyECTLAKD_Xi(_localctx.as);

								    ((PropertyECTLAKD_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyECTLAKD_PsiContext)_localctx).leftXi.result, ((PropertyECTLAKD_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(3631);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyECTLAKD_PsiContext)_localctx).result =  ((PropertyECTLAKD_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyECTLAKD_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyECTLAKD_PsiContext subParenthesized;
		public PropertyECTLAKD_XiContext sub;
		public PropertyECTLAKD_PsiContext propertyECTLAKD_Psi() {
			return getRuleContext(PropertyECTLAKD_PsiContext.class,0);
		}
		public PropertyECTLAKD_XiContext propertyECTLAKD_Xi() {
			return getRuleContext(PropertyECTLAKD_XiContext.class,0);
		}
		public PropertyECTLAKD_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyECTLAKD_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyECTLAKD_Psi_sub; }
	}

	public final PropertyECTLAKD_Psi_subContext propertyECTLAKD_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyECTLAKD_Psi_subContext _localctx = new PropertyECTLAKD_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 242, RULE_propertyECTLAKD_Psi_sub);
		try {
			setState(3656);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(3636);
				match(LPAREN);
				setState(3637);
				((PropertyECTLAKD_Psi_subContext)_localctx).subParenthesized = propertyECTLAKD_Psi(_localctx.as);
				setState(3638);
				match(RPAREN);

						((PropertyECTLAKD_Psi_subContext)_localctx).result =  ((PropertyECTLAKD_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(3641);
				match(X);
				 ((PropertyECTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3643);
				((PropertyECTLAKD_Psi_subContext)_localctx).sub = propertyECTLAKD_Xi(_localctx.as);

						((PropertyECTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyECTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(3646);
				match(F);
				 ((PropertyECTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3648);
				((PropertyECTLAKD_Psi_subContext)_localctx).sub = propertyECTLAKD_Xi(_localctx.as);

						((PropertyECTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyECTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(3651);
				match(G);
				 ((PropertyECTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3653);
				((PropertyECTLAKD_Psi_subContext)_localctx).sub = propertyECTLAKD_Xi(_localctx.as);

						((PropertyECTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyECTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyCTLAContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyCTLAContext propertyCTLA() {
			return getRuleContext(PropertyCTLAContext.class,0);
		}
		public PropertyCTLAStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAStatement; }
	}

	public final PropertyCTLAStatementContext propertyCTLAStatement(AgentSystem as) throws RecognitionException {
		PropertyCTLAStatementContext _localctx = new PropertyCTLAStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 244, RULE_propertyCTLAStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3658);
			match(CTL);
			 ((PropertyCTLAStatementContext)_localctx).pos =  getStreamPos(); 
			setState(3660);
			match(MULT);
			setState(3661);
			((PropertyCTLAStatementContext)_localctx).tName = match(Identifier);
			setState(3662);
			match(ASSIGN);

				((PropertyCTLAStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.CTLA, (((PropertyCTLAStatementContext)_localctx).tName!=null?((PropertyCTLAStatementContext)_localctx).tName.getText():null));

			setState(3664);
			((PropertyCTLAStatementContext)_localctx).expr = propertyCTLA(_localctx.as);

				_localctx.out.EXPR = ((PropertyCTLAStatementContext)_localctx).expr.result;
				((PropertyCTLAStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLA_subContext left;
		public PropertyCTLA_subContext right;
		public List<PropertyCTLA_subContext> propertyCTLA_sub() {
			return getRuleContexts(PropertyCTLA_subContext.class);
		}
		public PropertyCTLA_subContext propertyCTLA_sub(int i) {
			return getRuleContext(PropertyCTLA_subContext.class,i);
		}
		public PropertyCTLAContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLA; }
	}

	public final PropertyCTLAContext propertyCTLA(AgentSystem as) throws RecognitionException {
		PropertyCTLAContext _localctx = new PropertyCTLAContext(_ctx, getState(), as);
		enterRule(_localctx, 246, RULE_propertyCTLA);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(3667);
			((PropertyCTLAContext)_localctx).left = propertyCTLA_sub(_localctx.as);
			setState(3684);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,154,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(3676);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3668);
						match(LAND);
						 ((PropertyCTLAContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3670);
						match(LOR);
						 ((PropertyCTLAContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3672);
						match(RARROW);
						 ((PropertyCTLAContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3674);
						match(DARROW);
						 ((PropertyCTLAContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAContext)_localctx).pos =  getStreamPos(); 
					setState(3679);
					((PropertyCTLAContext)_localctx).right = propertyCTLA_sub(_localctx.as);

								    ((PropertyCTLAContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAContext)_localctx).left.result, ((PropertyCTLAContext)_localctx).right.result); 
					}
					} 
				}
				setState(3686);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,154,_ctx);
			}

					((PropertyCTLAContext)_localctx).result =  ((PropertyCTLAContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLA_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAContext subParenthesized;
		public PropertyCTLAContext subPhi;
		public PropertyCTLA_XiContext sub;
		public RelationalExpressionContext expr;
		public PropertyCTLAContext propertyCTLA() {
			return getRuleContext(PropertyCTLAContext.class,0);
		}
		public PropertyCTLA_XiContext propertyCTLA_Xi() {
			return getRuleContext(PropertyCTLA_XiContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyCTLA_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLA_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLA_sub; }
	}

	public final PropertyCTLA_subContext propertyCTLA_sub(AgentSystem as) throws RecognitionException {
		PropertyCTLA_subContext _localctx = new PropertyCTLA_subContext(_ctx, getState(), as);
		enterRule(_localctx, 248, RULE_propertyCTLA_sub);
		try {
			setState(3712);
			switch ( getInterpreter().adaptivePredict(_input,155,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3689);
				match(LPAREN);
				setState(3690);
				((PropertyCTLA_subContext)_localctx).subParenthesized = propertyCTLA(_localctx.as);
				setState(3691);
				match(RPAREN);

						((PropertyCTLA_subContext)_localctx).result =  ((PropertyCTLA_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3694);
				match(LNOT);
				 ((PropertyCTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(3696);
				((PropertyCTLA_subContext)_localctx).subPhi = propertyCTLA(_localctx.as);

						((PropertyCTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyCTLA_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(3699);
				match(E);
				 ((PropertyCTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(3701);
				((PropertyCTLA_subContext)_localctx).sub = propertyCTLA_Xi(_localctx.as);

						((PropertyCTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyCTLA_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(3704);
				match(A);
				 ((PropertyCTLA_subContext)_localctx).pos =  getStreamPos(); 
				setState(3706);
				((PropertyCTLA_subContext)_localctx).sub = propertyCTLA_Xi(_localctx.as);

						((PropertyCTLA_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyCTLA_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(3709);
				((PropertyCTLA_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyCTLA_subContext)_localctx).result =  ((PropertyCTLA_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLA_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLA_subContext subPhi;
		public PropertyCTLA_Psi_subContext subPsi;
		public PropertyCTLA_subContext propertyCTLA_sub() {
			return getRuleContext(PropertyCTLA_subContext.class,0);
		}
		public PropertyCTLA_Psi_subContext propertyCTLA_Psi_sub() {
			return getRuleContext(PropertyCTLA_Psi_subContext.class,0);
		}
		public PropertyCTLA_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLA_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLA_Xi; }
	}

	public final PropertyCTLA_XiContext propertyCTLA_Xi(AgentSystem as) throws RecognitionException {
		PropertyCTLA_XiContext _localctx = new PropertyCTLA_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 250, RULE_propertyCTLA_Xi);
		try {
			setState(3720);
			switch ( getInterpreter().adaptivePredict(_input,156,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3714);
				((PropertyCTLA_XiContext)_localctx).subPhi = propertyCTLA_sub(_localctx.as);

						((PropertyCTLA_XiContext)_localctx).result =  ((PropertyCTLA_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3717);
				((PropertyCTLA_XiContext)_localctx).subPsi = propertyCTLA_Psi_sub(_localctx.as);

						((PropertyCTLA_XiContext)_localctx).result =  ((PropertyCTLA_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLA_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLA_Psi_subContext left;
		public PropertyCTLA_Psi_subContext right;
		public PropertyCTLA_XiContext leftXi;
		public PropertyCTLA_XiContext rightXi;
		public List<PropertyCTLA_Psi_subContext> propertyCTLA_Psi_sub() {
			return getRuleContexts(PropertyCTLA_Psi_subContext.class);
		}
		public PropertyCTLA_Psi_subContext propertyCTLA_Psi_sub(int i) {
			return getRuleContext(PropertyCTLA_Psi_subContext.class,i);
		}
		public List<PropertyCTLA_XiContext> propertyCTLA_Xi() {
			return getRuleContexts(PropertyCTLA_XiContext.class);
		}
		public PropertyCTLA_XiContext propertyCTLA_Xi(int i) {
			return getRuleContext(PropertyCTLA_XiContext.class,i);
		}
		public PropertyCTLA_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLA_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLA_Psi; }
	}

	public final PropertyCTLA_PsiContext propertyCTLA_Psi(AgentSystem as) throws RecognitionException {
		PropertyCTLA_PsiContext _localctx = new PropertyCTLA_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 252, RULE_propertyCTLA_Psi);
		int _la;
		try {
			setState(3762);
			switch ( getInterpreter().adaptivePredict(_input,161,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3722);
				((PropertyCTLA_PsiContext)_localctx).left = propertyCTLA_Psi_sub(_localctx.as);
				setState(3739);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(3731);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3723);
						match(LAND);
						 ((PropertyCTLA_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3725);
						match(LOR);
						 ((PropertyCTLA_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3727);
						match(RARROW);
						 ((PropertyCTLA_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3729);
						match(DARROW);
						 ((PropertyCTLA_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLA_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3734);
					((PropertyCTLA_PsiContext)_localctx).right = propertyCTLA_Psi_sub(_localctx.as);

								    ((PropertyCTLA_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLA_PsiContext)_localctx).left.result, ((PropertyCTLA_PsiContext)_localctx).right.result); 
					}
					}
					setState(3741);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyCTLA_PsiContext)_localctx).result =  ((PropertyCTLA_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3744);
				((PropertyCTLA_PsiContext)_localctx).leftXi = propertyCTLA_Xi(_localctx.as);
				setState(3757);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(3749);
					switch (_input.LA(1)) {
					case U:
						{
						setState(3745);
						match(U);
						 ((PropertyCTLA_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(3747);
						match(R);
						 ((PropertyCTLA_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLA_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3752);
					((PropertyCTLA_PsiContext)_localctx).rightXi = propertyCTLA_Xi(_localctx.as);

								    ((PropertyCTLA_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLA_PsiContext)_localctx).leftXi.result, ((PropertyCTLA_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(3759);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyCTLA_PsiContext)_localctx).result =  ((PropertyCTLA_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLA_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLA_PsiContext subParenthesized;
		public PropertyCTLA_XiContext sub;
		public PropertyCTLA_PsiContext propertyCTLA_Psi() {
			return getRuleContext(PropertyCTLA_PsiContext.class,0);
		}
		public PropertyCTLA_XiContext propertyCTLA_Xi() {
			return getRuleContext(PropertyCTLA_XiContext.class,0);
		}
		public PropertyCTLA_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLA_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLA_Psi_sub; }
	}

	public final PropertyCTLA_Psi_subContext propertyCTLA_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyCTLA_Psi_subContext _localctx = new PropertyCTLA_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 254, RULE_propertyCTLA_Psi_sub);
		try {
			setState(3784);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(3764);
				match(LPAREN);
				setState(3765);
				((PropertyCTLA_Psi_subContext)_localctx).subParenthesized = propertyCTLA_Psi(_localctx.as);
				setState(3766);
				match(RPAREN);

						((PropertyCTLA_Psi_subContext)_localctx).result =  ((PropertyCTLA_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(3769);
				match(X);
				 ((PropertyCTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3771);
				((PropertyCTLA_Psi_subContext)_localctx).sub = propertyCTLA_Xi(_localctx.as);

						((PropertyCTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyCTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(3774);
				match(F);
				 ((PropertyCTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3776);
				((PropertyCTLA_Psi_subContext)_localctx).sub = propertyCTLA_Xi(_localctx.as);

						((PropertyCTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyCTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(3779);
				match(G);
				 ((PropertyCTLA_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3781);
				((PropertyCTLA_Psi_subContext)_localctx).sub = propertyCTLA_Xi(_localctx.as);

						((PropertyCTLA_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyCTLA_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyCTLAKContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyCTLAKContext propertyCTLAK() {
			return getRuleContext(PropertyCTLAKContext.class,0);
		}
		public PropertyCTLAKStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKStatement; }
	}

	public final PropertyCTLAKStatementContext propertyCTLAKStatement(AgentSystem as) throws RecognitionException {
		PropertyCTLAKStatementContext _localctx = new PropertyCTLAKStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 256, RULE_propertyCTLAKStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3786);
			match(CTL);
			 ((PropertyCTLAKStatementContext)_localctx).pos =  getStreamPos(); 
			setState(3788);
			match(MULT);
			setState(3789);
			match(K);
			setState(3790);
			((PropertyCTLAKStatementContext)_localctx).tName = match(Identifier);
			setState(3791);
			match(ASSIGN);

				((PropertyCTLAKStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.CTLAK, (((PropertyCTLAKStatementContext)_localctx).tName!=null?((PropertyCTLAKStatementContext)_localctx).tName.getText():null));

			setState(3793);
			((PropertyCTLAKStatementContext)_localctx).expr = propertyCTLAK(_localctx.as);

				_localctx.out.EXPR = ((PropertyCTLAKStatementContext)_localctx).expr.result;
				((PropertyCTLAKStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAK_subContext left;
		public PropertyCTLAK_subContext right;
		public List<PropertyCTLAK_subContext> propertyCTLAK_sub() {
			return getRuleContexts(PropertyCTLAK_subContext.class);
		}
		public PropertyCTLAK_subContext propertyCTLAK_sub(int i) {
			return getRuleContext(PropertyCTLAK_subContext.class,i);
		}
		public PropertyCTLAKContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAK; }
	}

	public final PropertyCTLAKContext propertyCTLAK(AgentSystem as) throws RecognitionException {
		PropertyCTLAKContext _localctx = new PropertyCTLAKContext(_ctx, getState(), as);
		enterRule(_localctx, 258, RULE_propertyCTLAK);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(3796);
			((PropertyCTLAKContext)_localctx).left = propertyCTLAK_sub(_localctx.as);
			setState(3813);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,164,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(3805);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3797);
						match(LAND);
						 ((PropertyCTLAKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3799);
						match(LOR);
						 ((PropertyCTLAKContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3801);
						match(RARROW);
						 ((PropertyCTLAKContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3803);
						match(DARROW);
						 ((PropertyCTLAKContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAKContext)_localctx).pos =  getStreamPos(); 
					setState(3808);
					((PropertyCTLAKContext)_localctx).right = propertyCTLAK_sub(_localctx.as);

								    ((PropertyCTLAKContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAKContext)_localctx).left.result, ((PropertyCTLAKContext)_localctx).right.result); 
					}
					} 
				}
				setState(3815);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,164,_ctx);
			}

					((PropertyCTLAKContext)_localctx).result =  ((PropertyCTLAKContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAK_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKContext subParenthesized;
		public PropertyCTLAKContext subPhi;
		public PropertyCTLAK_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyCTLAK_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyCTLAKContext propertyCTLAK() {
			return getRuleContext(PropertyCTLAKContext.class,0);
		}
		public PropertyCTLAK_XiContext propertyCTLAK_Xi() {
			return getRuleContext(PropertyCTLAK_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyCTLAK_Xi_superContext propertyCTLAK_Xi_super() {
			return getRuleContext(PropertyCTLAK_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyCTLAK_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAK_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAK_sub; }
	}

	public final PropertyCTLAK_subContext propertyCTLAK_sub(AgentSystem as) throws RecognitionException {
		PropertyCTLAK_subContext _localctx = new PropertyCTLAK_subContext(_ctx, getState(), as);
		enterRule(_localctx, 260, RULE_propertyCTLAK_sub);
		try {
			setState(3917);
			switch ( getInterpreter().adaptivePredict(_input,165,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3818);
				match(LPAREN);
				setState(3819);
				((PropertyCTLAK_subContext)_localctx).subParenthesized = propertyCTLAK(_localctx.as);
				setState(3820);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  ((PropertyCTLAK_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3823);
				match(LNOT);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3825);
				((PropertyCTLAK_subContext)_localctx).subPhi = propertyCTLAK(_localctx.as);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyCTLAK_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(3828);
				match(E);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3830);
				((PropertyCTLAK_subContext)_localctx).sub = propertyCTLAK_Xi(_localctx.as);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyCTLAK_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(3833);
				match(A);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3835);
				((PropertyCTLAK_subContext)_localctx).sub = propertyCTLAK_Xi(_localctx.as);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyCTLAK_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(3838);
				match(K);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3840);
				match(LPAREN);
				setState(3841);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3842);
				match(COMMA);
				setState(3843);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3844);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(3847);
				match(Eg);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3849);
				match(LPAREN);
				setState(3850);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3851);
				match(COMMA);
				setState(3852);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3853);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(3856);
				match(Dg);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3858);
				match(LPAREN);
				setState(3859);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3860);
				match(COMMA);
				setState(3861);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3862);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(3865);
				match(Cg);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3867);
				match(LPAREN);
				setState(3868);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3869);
				match(COMMA);
				setState(3870);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3871);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(3874);
				match(LNOT);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3876);
				match(K);
				setState(3877);
				match(LPAREN);
				setState(3878);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3879);
				match(COMMA);
				setState(3880);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3881);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(3884);
				match(LNOT);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3886);
				match(Eg);
				setState(3887);
				match(LPAREN);
				setState(3888);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3889);
				match(COMMA);
				setState(3890);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3891);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(3894);
				match(LNOT);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3896);
				match(Dg);
				setState(3897);
				match(LPAREN);
				setState(3898);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3899);
				match(COMMA);
				setState(3900);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3901);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(3904);
				match(LNOT);
				 ((PropertyCTLAK_subContext)_localctx).pos =  getStreamPos(); 
				setState(3906);
				match(Cg);
				setState(3907);
				match(LPAREN);
				setState(3908);
				((PropertyCTLAK_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(3909);
				match(COMMA);
				setState(3910);
				((PropertyCTLAK_subContext)_localctx).subSuper = propertyCTLAK_Xi_super(_localctx.as);
				setState(3911);
				match(RPAREN);

						((PropertyCTLAK_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyCTLAK_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAK_subContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(3914);
				((PropertyCTLAK_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyCTLAK_subContext)_localctx).result =  ((PropertyCTLAK_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAK_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAK_subContext subPhi;
		public PropertyCTLAK_Psi_subContext subPsi;
		public PropertyCTLAK_subContext propertyCTLAK_sub() {
			return getRuleContext(PropertyCTLAK_subContext.class,0);
		}
		public PropertyCTLAK_Psi_subContext propertyCTLAK_Psi_sub() {
			return getRuleContext(PropertyCTLAK_Psi_subContext.class,0);
		}
		public PropertyCTLAK_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAK_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAK_Xi; }
	}

	public final PropertyCTLAK_XiContext propertyCTLAK_Xi(AgentSystem as) throws RecognitionException {
		PropertyCTLAK_XiContext _localctx = new PropertyCTLAK_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 262, RULE_propertyCTLAK_Xi);
		try {
			setState(3925);
			switch ( getInterpreter().adaptivePredict(_input,166,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3919);
				((PropertyCTLAK_XiContext)_localctx).subPhi = propertyCTLAK_sub(_localctx.as);

						((PropertyCTLAK_XiContext)_localctx).result =  ((PropertyCTLAK_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3922);
				((PropertyCTLAK_XiContext)_localctx).subPsi = propertyCTLAK_Psi_sub(_localctx.as);

						((PropertyCTLAK_XiContext)_localctx).result =  ((PropertyCTLAK_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAK_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKContext subPhi;
		public PropertyCTLAK_PsiContext subPsi;
		public PropertyCTLAKContext propertyCTLAK() {
			return getRuleContext(PropertyCTLAKContext.class,0);
		}
		public PropertyCTLAK_PsiContext propertyCTLAK_Psi() {
			return getRuleContext(PropertyCTLAK_PsiContext.class,0);
		}
		public PropertyCTLAK_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAK_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAK_Xi_super; }
	}

	public final PropertyCTLAK_Xi_superContext propertyCTLAK_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyCTLAK_Xi_superContext _localctx = new PropertyCTLAK_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 264, RULE_propertyCTLAK_Xi_super);
		try {
			setState(3933);
			switch ( getInterpreter().adaptivePredict(_input,167,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3927);
				((PropertyCTLAK_Xi_superContext)_localctx).subPhi = propertyCTLAK(_localctx.as);

						((PropertyCTLAK_Xi_superContext)_localctx).result =  ((PropertyCTLAK_Xi_superContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3930);
				((PropertyCTLAK_Xi_superContext)_localctx).subPsi = propertyCTLAK_Psi(_localctx.as);

						((PropertyCTLAK_Xi_superContext)_localctx).result =  ((PropertyCTLAK_Xi_superContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAK_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAK_Psi_subContext left;
		public PropertyCTLAK_Psi_subContext right;
		public PropertyCTLAK_XiContext leftXi;
		public PropertyCTLAK_XiContext rightXi;
		public List<PropertyCTLAK_Psi_subContext> propertyCTLAK_Psi_sub() {
			return getRuleContexts(PropertyCTLAK_Psi_subContext.class);
		}
		public PropertyCTLAK_Psi_subContext propertyCTLAK_Psi_sub(int i) {
			return getRuleContext(PropertyCTLAK_Psi_subContext.class,i);
		}
		public List<PropertyCTLAK_XiContext> propertyCTLAK_Xi() {
			return getRuleContexts(PropertyCTLAK_XiContext.class);
		}
		public PropertyCTLAK_XiContext propertyCTLAK_Xi(int i) {
			return getRuleContext(PropertyCTLAK_XiContext.class,i);
		}
		public PropertyCTLAK_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAK_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAK_Psi; }
	}

	public final PropertyCTLAK_PsiContext propertyCTLAK_Psi(AgentSystem as) throws RecognitionException {
		PropertyCTLAK_PsiContext _localctx = new PropertyCTLAK_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 266, RULE_propertyCTLAK_Psi);
		int _la;
		try {
			setState(3975);
			switch ( getInterpreter().adaptivePredict(_input,172,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(3935);
				((PropertyCTLAK_PsiContext)_localctx).left = propertyCTLAK_Psi_sub(_localctx.as);
				setState(3952);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(3944);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(3936);
						match(LAND);
						 ((PropertyCTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(3938);
						match(LOR);
						 ((PropertyCTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(3940);
						match(RARROW);
						 ((PropertyCTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(3942);
						match(DARROW);
						 ((PropertyCTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAK_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3947);
					((PropertyCTLAK_PsiContext)_localctx).right = propertyCTLAK_Psi_sub(_localctx.as);

								    ((PropertyCTLAK_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAK_PsiContext)_localctx).left.result, ((PropertyCTLAK_PsiContext)_localctx).right.result); 
					}
					}
					setState(3954);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyCTLAK_PsiContext)_localctx).result =  ((PropertyCTLAK_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(3957);
				((PropertyCTLAK_PsiContext)_localctx).leftXi = propertyCTLAK_Xi(_localctx.as);
				setState(3970);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(3962);
					switch (_input.LA(1)) {
					case U:
						{
						setState(3958);
						match(U);
						 ((PropertyCTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(3960);
						match(R);
						 ((PropertyCTLAK_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAK_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(3965);
					((PropertyCTLAK_PsiContext)_localctx).rightXi = propertyCTLAK_Xi(_localctx.as);

								    ((PropertyCTLAK_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAK_PsiContext)_localctx).leftXi.result, ((PropertyCTLAK_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(3972);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyCTLAK_PsiContext)_localctx).result =  ((PropertyCTLAK_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAK_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAK_PsiContext subParenthesized;
		public PropertyCTLAK_XiContext sub;
		public PropertyCTLAK_PsiContext propertyCTLAK_Psi() {
			return getRuleContext(PropertyCTLAK_PsiContext.class,0);
		}
		public PropertyCTLAK_XiContext propertyCTLAK_Xi() {
			return getRuleContext(PropertyCTLAK_XiContext.class,0);
		}
		public PropertyCTLAK_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAK_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAK_Psi_sub; }
	}

	public final PropertyCTLAK_Psi_subContext propertyCTLAK_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyCTLAK_Psi_subContext _localctx = new PropertyCTLAK_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 268, RULE_propertyCTLAK_Psi_sub);
		try {
			setState(3997);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(3977);
				match(LPAREN);
				setState(3978);
				((PropertyCTLAK_Psi_subContext)_localctx).subParenthesized = propertyCTLAK_Psi(_localctx.as);
				setState(3979);
				match(RPAREN);

						((PropertyCTLAK_Psi_subContext)_localctx).result =  ((PropertyCTLAK_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(3982);
				match(X);
				 ((PropertyCTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3984);
				((PropertyCTLAK_Psi_subContext)_localctx).sub = propertyCTLAK_Xi(_localctx.as);

						((PropertyCTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyCTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(3987);
				match(F);
				 ((PropertyCTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3989);
				((PropertyCTLAK_Psi_subContext)_localctx).sub = propertyCTLAK_Xi(_localctx.as);

						((PropertyCTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyCTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(3992);
				match(G);
				 ((PropertyCTLAK_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(3994);
				((PropertyCTLAK_Psi_subContext)_localctx).sub = propertyCTLAK_Xi(_localctx.as);

						((PropertyCTLAK_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyCTLAK_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKDStatementContext extends ParserRuleContext {
		public AgentSystem as;
		public AsProperty result;
		public StreamPos pos;
		public AsProperty out;
		public Token tName;
		public PropertyCTLAKDContext expr;
		public TerminalNode Identifier() { return getToken(VxspParser.Identifier, 0); }
		public PropertyCTLAKDContext propertyCTLAKD() {
			return getRuleContext(PropertyCTLAKDContext.class,0);
		}
		public PropertyCTLAKDStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKDStatementContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKDStatement; }
	}

	public final PropertyCTLAKDStatementContext propertyCTLAKDStatement(AgentSystem as) throws RecognitionException {
		PropertyCTLAKDStatementContext _localctx = new PropertyCTLAKDStatementContext(_ctx, getState(), as);
		enterRule(_localctx, 270, RULE_propertyCTLAKDStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(3999);
			match(CTL);
			 ((PropertyCTLAKDStatementContext)_localctx).pos =  getStreamPos(); 
			setState(4001);
			match(MULT);
			setState(4002);
			match(KD);
			setState(4003);
			((PropertyCTLAKDStatementContext)_localctx).tName = match(Identifier);
			setState(4004);
			match(ASSIGN);

				((PropertyCTLAKDStatementContext)_localctx).out =  new AsProperty(_localctx.pos, AsProperty.Type.CTLAKD, (((PropertyCTLAKDStatementContext)_localctx).tName!=null?((PropertyCTLAKDStatementContext)_localctx).tName.getText():null));

			setState(4006);
			((PropertyCTLAKDStatementContext)_localctx).expr = propertyCTLAKD(_localctx.as);

				_localctx.out.EXPR = ((PropertyCTLAKDStatementContext)_localctx).expr.result;
				((PropertyCTLAKDStatementContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKDContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKD_subContext left;
		public PropertyCTLAKD_subContext right;
		public List<PropertyCTLAKD_subContext> propertyCTLAKD_sub() {
			return getRuleContexts(PropertyCTLAKD_subContext.class);
		}
		public PropertyCTLAKD_subContext propertyCTLAKD_sub(int i) {
			return getRuleContext(PropertyCTLAKD_subContext.class,i);
		}
		public PropertyCTLAKDContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKDContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKD; }
	}

	public final PropertyCTLAKDContext propertyCTLAKD(AgentSystem as) throws RecognitionException {
		PropertyCTLAKDContext _localctx = new PropertyCTLAKDContext(_ctx, getState(), as);
		enterRule(_localctx, 272, RULE_propertyCTLAKD);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(4009);
			((PropertyCTLAKDContext)_localctx).left = propertyCTLAKD_sub(_localctx.as);
			setState(4026);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,175,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(4018);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(4010);
						match(LAND);
						 ((PropertyCTLAKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(4012);
						match(LOR);
						 ((PropertyCTLAKDContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(4014);
						match(RARROW);
						 ((PropertyCTLAKDContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(4016);
						match(DARROW);
						 ((PropertyCTLAKDContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAKDContext)_localctx).pos =  getStreamPos(); 
					setState(4021);
					((PropertyCTLAKDContext)_localctx).right = propertyCTLAKD_sub(_localctx.as);

								    ((PropertyCTLAKDContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAKDContext)_localctx).left.result, ((PropertyCTLAKDContext)_localctx).right.result); 
					}
					} 
				}
				setState(4028);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,175,_ctx);
			}

					((PropertyCTLAKDContext)_localctx).result =  ((PropertyCTLAKDContext)_localctx).left.result;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKD_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKDContext subParenthesized;
		public PropertyCTLAKDContext subPhi;
		public PropertyCTLAKD_XiContext sub;
		public CoalitionGroupContext coalition;
		public PropertyCTLAKD_Xi_superContext subSuper;
		public RelationalExpressionContext expr;
		public PropertyCTLAKDContext propertyCTLAKD() {
			return getRuleContext(PropertyCTLAKDContext.class,0);
		}
		public PropertyCTLAKD_XiContext propertyCTLAKD_Xi() {
			return getRuleContext(PropertyCTLAKD_XiContext.class,0);
		}
		public CoalitionGroupContext coalitionGroup() {
			return getRuleContext(CoalitionGroupContext.class,0);
		}
		public PropertyCTLAKD_Xi_superContext propertyCTLAKD_Xi_super() {
			return getRuleContext(PropertyCTLAKD_Xi_superContext.class,0);
		}
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public PropertyCTLAKD_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKD_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKD_sub; }
	}

	public final PropertyCTLAKD_subContext propertyCTLAKD_sub(AgentSystem as) throws RecognitionException {
		PropertyCTLAKD_subContext _localctx = new PropertyCTLAKD_subContext(_ctx, getState(), as);
		enterRule(_localctx, 274, RULE_propertyCTLAKD_sub);
		try {
			setState(4168);
			switch ( getInterpreter().adaptivePredict(_input,176,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(4031);
				match(LPAREN);
				setState(4032);
				((PropertyCTLAKD_subContext)_localctx).subParenthesized = propertyCTLAKD(_localctx.as);
				setState(4033);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  ((PropertyCTLAKD_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(4036);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4038);
				((PropertyCTLAKD_subContext)_localctx).subPhi = propertyCTLAKD(_localctx.as);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.CONDITIONAL_NEGATION, ((PropertyCTLAKD_subContext)_localctx).subPhi.result);
					
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(4041);
				match(E);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4043);
				((PropertyCTLAKD_subContext)_localctx).sub = propertyCTLAKD_Xi(_localctx.as);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.E, ((PropertyCTLAKD_subContext)_localctx).sub.result);
					
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(4046);
				match(A);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4048);
				((PropertyCTLAKD_subContext)_localctx).sub = propertyCTLAKD_Xi(_localctx.as);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.A, ((PropertyCTLAKD_subContext)_localctx).sub.result);
					
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(4051);
				match(K);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4053);
				match(LPAREN);
				setState(4054);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4055);
				match(COMMA);
				setState(4056);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4057);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Kc, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(4060);
				match(Eg);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4062);
				match(LPAREN);
				setState(4063);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4064);
				match(COMMA);
				setState(4065);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4066);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Eg, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(4069);
				match(Dg);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4071);
				match(LPAREN);
				setState(4072);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4073);
				match(COMMA);
				setState(4074);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4075);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Dg, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(4078);
				match(Cg);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4080);
				match(LPAREN);
				setState(4081);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4082);
				match(COMMA);
				setState(4083);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4084);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Cg, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(4087);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4089);
				match(K);
				setState(4090);
				match(LPAREN);
				setState(4091);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4092);
				match(COMMA);
				setState(4093);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4094);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Kc, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(4097);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4099);
				match(Eg);
				setState(4100);
				match(LPAREN);
				setState(4101);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4102);
				match(COMMA);
				setState(4103);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4104);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Eg, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(4107);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4109);
				match(Dg);
				setState(4110);
				match(LPAREN);
				setState(4111);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4112);
				match(COMMA);
				setState(4113);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4114);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Dg, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(4117);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4119);
				match(Cg);
				setState(4120);
				match(LPAREN);
				setState(4121);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4122);
				match(COMMA);
				setState(4123);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4124);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Cg, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(4127);
				match(Oc);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4129);
				match(LPAREN);
				setState(4130);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4131);
				match(COMMA);
				setState(4132);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4133);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Oc, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(4136);
				match(Kh);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4138);
				match(LPAREN);
				setState(4139);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4140);
				match(COMMA);
				setState(4141);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4142);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.Khc, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(4145);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4147);
				match(Oc);
				setState(4148);
				match(LPAREN);
				setState(4149);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4150);
				match(COMMA);
				setState(4151);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4152);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Oc, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(4155);
				match(LNOT);
				 ((PropertyCTLAKD_subContext)_localctx).pos =  getStreamPos(); 
				setState(4157);
				match(Kh);
				setState(4158);
				match(LPAREN);
				setState(4159);
				((PropertyCTLAKD_subContext)_localctx).coalition = coalitionGroup(_localctx.as);
				setState(4160);
				match(COMMA);
				setState(4161);
				((PropertyCTLAKD_subContext)_localctx).subSuper = propertyCTLAKD_Xi_super(_localctx.as);
				setState(4162);
				match(RPAREN);

						((PropertyCTLAKD_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.NEG_Khc, ((PropertyCTLAKD_subContext)_localctx).subSuper.result);
								((UnaryExpression)_localctx.result).coalition = ((PropertyCTLAKD_subContext)_localctx).coalition.result;
					
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(4165);
				((PropertyCTLAKD_subContext)_localctx).expr = relationalExpression(_localctx.as, false);

						((PropertyCTLAKD_subContext)_localctx).result =  ((PropertyCTLAKD_subContext)_localctx).expr.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKD_XiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKD_subContext subPhi;
		public PropertyCTLAKD_Psi_subContext subPsi;
		public PropertyCTLAKD_subContext propertyCTLAKD_sub() {
			return getRuleContext(PropertyCTLAKD_subContext.class,0);
		}
		public PropertyCTLAKD_Psi_subContext propertyCTLAKD_Psi_sub() {
			return getRuleContext(PropertyCTLAKD_Psi_subContext.class,0);
		}
		public PropertyCTLAKD_XiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKD_XiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKD_Xi; }
	}

	public final PropertyCTLAKD_XiContext propertyCTLAKD_Xi(AgentSystem as) throws RecognitionException {
		PropertyCTLAKD_XiContext _localctx = new PropertyCTLAKD_XiContext(_ctx, getState(), as);
		enterRule(_localctx, 276, RULE_propertyCTLAKD_Xi);
		try {
			setState(4176);
			switch ( getInterpreter().adaptivePredict(_input,177,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(4170);
				((PropertyCTLAKD_XiContext)_localctx).subPhi = propertyCTLAKD_sub(_localctx.as);

						((PropertyCTLAKD_XiContext)_localctx).result =  ((PropertyCTLAKD_XiContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(4173);
				((PropertyCTLAKD_XiContext)_localctx).subPsi = propertyCTLAKD_Psi_sub(_localctx.as);

						((PropertyCTLAKD_XiContext)_localctx).result =  ((PropertyCTLAKD_XiContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKD_Xi_superContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKDContext subPhi;
		public PropertyCTLAKD_PsiContext subPsi;
		public PropertyCTLAKDContext propertyCTLAKD() {
			return getRuleContext(PropertyCTLAKDContext.class,0);
		}
		public PropertyCTLAKD_PsiContext propertyCTLAKD_Psi() {
			return getRuleContext(PropertyCTLAKD_PsiContext.class,0);
		}
		public PropertyCTLAKD_Xi_superContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKD_Xi_superContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKD_Xi_super; }
	}

	public final PropertyCTLAKD_Xi_superContext propertyCTLAKD_Xi_super(AgentSystem as) throws RecognitionException {
		PropertyCTLAKD_Xi_superContext _localctx = new PropertyCTLAKD_Xi_superContext(_ctx, getState(), as);
		enterRule(_localctx, 278, RULE_propertyCTLAKD_Xi_super);
		try {
			setState(4184);
			switch ( getInterpreter().adaptivePredict(_input,178,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(4178);
				((PropertyCTLAKD_Xi_superContext)_localctx).subPhi = propertyCTLAKD(_localctx.as);

						((PropertyCTLAKD_Xi_superContext)_localctx).result =  ((PropertyCTLAKD_Xi_superContext)_localctx).subPhi.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(4181);
				((PropertyCTLAKD_Xi_superContext)_localctx).subPsi = propertyCTLAKD_Psi(_localctx.as);

						((PropertyCTLAKD_Xi_superContext)_localctx).result =  ((PropertyCTLAKD_Xi_superContext)_localctx).subPsi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKD_PsiContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKD_Psi_subContext left;
		public PropertyCTLAKD_Psi_subContext right;
		public PropertyCTLAKD_XiContext leftXi;
		public PropertyCTLAKD_XiContext rightXi;
		public List<PropertyCTLAKD_Psi_subContext> propertyCTLAKD_Psi_sub() {
			return getRuleContexts(PropertyCTLAKD_Psi_subContext.class);
		}
		public PropertyCTLAKD_Psi_subContext propertyCTLAKD_Psi_sub(int i) {
			return getRuleContext(PropertyCTLAKD_Psi_subContext.class,i);
		}
		public List<PropertyCTLAKD_XiContext> propertyCTLAKD_Xi() {
			return getRuleContexts(PropertyCTLAKD_XiContext.class);
		}
		public PropertyCTLAKD_XiContext propertyCTLAKD_Xi(int i) {
			return getRuleContext(PropertyCTLAKD_XiContext.class,i);
		}
		public PropertyCTLAKD_PsiContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKD_PsiContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKD_Psi; }
	}

	public final PropertyCTLAKD_PsiContext propertyCTLAKD_Psi(AgentSystem as) throws RecognitionException {
		PropertyCTLAKD_PsiContext _localctx = new PropertyCTLAKD_PsiContext(_ctx, getState(), as);
		enterRule(_localctx, 280, RULE_propertyCTLAKD_Psi);
		int _la;
		try {
			setState(4226);
			switch ( getInterpreter().adaptivePredict(_input,183,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(4186);
				((PropertyCTLAKD_PsiContext)_localctx).left = propertyCTLAKD_Psi_sub(_localctx.as);
				setState(4203);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << RARROW) | (1L << DARROW) | (1L << LAND) | (1L << LOR))) != 0)) {
					{
					{
					setState(4195);
					switch (_input.LA(1)) {
					case LAND:
						{
						setState(4187);
						match(LAND);
						 ((PropertyCTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_AND; 
						}
						break;
					case LOR:
						{
						setState(4189);
						match(LOR);
						 ((PropertyCTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.CONDITIONAL_OR; 
						}
						break;
					case RARROW:
						{
						setState(4191);
						match(RARROW);
						 ((PropertyCTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.IMPLICATION; 
						}
						break;
					case DARROW:
						{
						setState(4193);
						match(DARROW);
						 ((PropertyCTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.EQUIVALENCE; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAKD_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(4198);
					((PropertyCTLAKD_PsiContext)_localctx).right = propertyCTLAKD_Psi_sub(_localctx.as);

								    ((PropertyCTLAKD_PsiContext)_localctx).left.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAKD_PsiContext)_localctx).left.result, ((PropertyCTLAKD_PsiContext)_localctx).right.result); 
					}
					}
					setState(4205);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyCTLAKD_PsiContext)_localctx).result =  ((PropertyCTLAKD_PsiContext)_localctx).left.result;
					
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(4208);
				((PropertyCTLAKD_PsiContext)_localctx).leftXi = propertyCTLAKD_Xi(_localctx.as);
				setState(4221);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==R || _la==U) {
					{
					{
					setState(4213);
					switch (_input.LA(1)) {
					case U:
						{
						setState(4209);
						match(U);
						 ((PropertyCTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.U; 
						}
						break;
					case R:
						{
						setState(4211);
						match(R);
						 ((PropertyCTLAKD_PsiContext)_localctx).op =  BinaryExpression.Op.R; 
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					 ((PropertyCTLAKD_PsiContext)_localctx).pos =  getStreamPos(); 
					setState(4216);
					((PropertyCTLAKD_PsiContext)_localctx).rightXi = propertyCTLAKD_Xi(_localctx.as);

								    ((PropertyCTLAKD_PsiContext)_localctx).leftXi.result = new BinaryExpression(_localctx.pos, _localctx.as.SCOPE, _localctx.op, ((PropertyCTLAKD_PsiContext)_localctx).leftXi.result, ((PropertyCTLAKD_PsiContext)_localctx).rightXi.result); 
					}
					}
					setState(4223);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}

						((PropertyCTLAKD_PsiContext)_localctx).result =  ((PropertyCTLAKD_PsiContext)_localctx).leftXi.result;
					
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyCTLAKD_Psi_subContext extends ParserRuleContext {
		public AgentSystem as;
		public AbstractExpression result;
		public StreamPos pos;
		public BinaryExpression.Op op;
		public PropertyCTLAKD_PsiContext subParenthesized;
		public PropertyCTLAKD_XiContext sub;
		public PropertyCTLAKD_PsiContext propertyCTLAKD_Psi() {
			return getRuleContext(PropertyCTLAKD_PsiContext.class,0);
		}
		public PropertyCTLAKD_XiContext propertyCTLAKD_Xi() {
			return getRuleContext(PropertyCTLAKD_XiContext.class,0);
		}
		public PropertyCTLAKD_Psi_subContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PropertyCTLAKD_Psi_subContext(ParserRuleContext parent, int invokingState, AgentSystem as) {
			super(parent, invokingState);
			this.as = as;
		}
		@Override public int getRuleIndex() { return RULE_propertyCTLAKD_Psi_sub; }
	}

	public final PropertyCTLAKD_Psi_subContext propertyCTLAKD_Psi_sub(AgentSystem as) throws RecognitionException {
		PropertyCTLAKD_Psi_subContext _localctx = new PropertyCTLAKD_Psi_subContext(_ctx, getState(), as);
		enterRule(_localctx, 282, RULE_propertyCTLAKD_Psi_sub);
		try {
			setState(4248);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(4228);
				match(LPAREN);
				setState(4229);
				((PropertyCTLAKD_Psi_subContext)_localctx).subParenthesized = propertyCTLAKD_Psi(_localctx.as);
				setState(4230);
				match(RPAREN);

						((PropertyCTLAKD_Psi_subContext)_localctx).result =  ((PropertyCTLAKD_Psi_subContext)_localctx).subParenthesized.result;
					
				}
				break;
			case X:
				enterOuterAlt(_localctx, 2);
				{
				setState(4233);
				match(X);
				 ((PropertyCTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(4235);
				((PropertyCTLAKD_Psi_subContext)_localctx).sub = propertyCTLAKD_Xi(_localctx.as);

						((PropertyCTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.X, ((PropertyCTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case F:
				enterOuterAlt(_localctx, 3);
				{
				setState(4238);
				match(F);
				 ((PropertyCTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(4240);
				((PropertyCTLAKD_Psi_subContext)_localctx).sub = propertyCTLAKD_Xi(_localctx.as);

						((PropertyCTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.F, ((PropertyCTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			case G:
				enterOuterAlt(_localctx, 4);
				{
				setState(4243);
				match(G);
				 ((PropertyCTLAKD_Psi_subContext)_localctx).pos =  getStreamPos(); 
				setState(4245);
				((PropertyCTLAKD_Psi_subContext)_localctx).sub = propertyCTLAKD_Xi(_localctx.as);

						((PropertyCTLAKD_Psi_subContext)_localctx).result =  new UnaryExpression(_localctx.pos, _localctx.as.SCOPE, UnaryExpression.Op.G, ((PropertyCTLAKD_Psi_subContext)_localctx).sub.result);
					
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualifiedIdentifierContext extends ParserRuleContext {
		public String result;
		public StringBuilder out =  new StringBuilder();
		public Token tName;
		public List<TerminalNode> Identifier() { return getTokens(VxspParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(VxspParser.Identifier, i);
		}
		public QualifiedIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedIdentifier; }
	}

	public final QualifiedIdentifierContext qualifiedIdentifier() throws RecognitionException {
		QualifiedIdentifierContext _localctx = new QualifiedIdentifierContext(_ctx, getState());
		enterRule(_localctx, 284, RULE_qualifiedIdentifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4250);
			((QualifiedIdentifierContext)_localctx).tName = match(Identifier);
			 _localctx.out.append((((QualifiedIdentifierContext)_localctx).tName!=null?((QualifiedIdentifierContext)_localctx).tName.getText():null)); 
			setState(4255);
			_la = _input.LA(1);
			if (_la==DOT) {
				{
				setState(4252);
				match(DOT);
				setState(4253);
				((QualifiedIdentifierContext)_localctx).tName = match(Identifier);
				 _localctx.out.append('.' + (((QualifiedIdentifierContext)_localctx).tName!=null?((QualifiedIdentifierContext)_localctx).tName.getText():null)); 
				}
			}


			     ((QualifiedIdentifierContext)_localctx).result =  _localctx.out.toString();

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public AsConstant result;
		public IntegerValueContext i;
		public DoubleValueContext d;
		public IntegerValueContext integerValue() {
			return getRuleContext(IntegerValueContext.class,0);
		}
		public DoubleValueContext doubleValue() {
			return getRuleContext(DoubleValueContext.class,0);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 286, RULE_number);
		try {
			setState(4269);
			switch (_input.LA(1)) {
			case IntegerLiteral:
				enterOuterAlt(_localctx, 1);
				{
				setState(4259);
				((NumberContext)_localctx).i = integerValue();
				 ((NumberContext)_localctx).result =  new AsConstant(getStreamPos(), null, new Literal(((NumberContext)_localctx).i.result)); 
				}
				break;
			case FloatingPointLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(4262);
				((NumberContext)_localctx).d = doubleValue();
				 ((NumberContext)_localctx).result =  new AsConstant(getStreamPos(), null, new Literal(((NumberContext)_localctx).d.result)); 
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 3);
				{
				setState(4265);
				match(TRUE);
				 ((NumberContext)_localctx).result =  new AsConstant(getStreamPos(), null, new Literal(true)); 
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 4);
				{
				setState(4267);
				match(FALSE);
				 ((NumberContext)_localctx).result =  new AsConstant(getStreamPos(), null, new Literal(false)); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerValueContext extends ParserRuleContext {
		public int result;
		public Token t;
		public TerminalNode IntegerLiteral() { return getToken(VxspParser.IntegerLiteral, 0); }
		public IntegerValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integerValue; }
	}

	public final IntegerValueContext integerValue() throws RecognitionException {
		IntegerValueContext _localctx = new IntegerValueContext(_ctx, getState());
		enterRule(_localctx, 288, RULE_integerValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4271);
			((IntegerValueContext)_localctx).t = match(IntegerLiteral);

			        ((IntegerValueContext)_localctx).result =  Integer.valueOf((((IntegerValueContext)_localctx).t!=null?((IntegerValueContext)_localctx).t.getText():null));
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DoubleValueContext extends ParserRuleContext {
		public double result;
		public Token t;
		public TerminalNode FloatingPointLiteral() { return getToken(VxspParser.FloatingPointLiteral, 0); }
		public DoubleValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_doubleValue; }
	}

	public final DoubleValueContext doubleValue() throws RecognitionException {
		DoubleValueContext _localctx = new DoubleValueContext(_ctx, getState());
		enterRule(_localctx, 290, RULE_doubleValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4274);
			((DoubleValueContext)_localctx).t = match(FloatingPointLiteral);

			        ((DoubleValueContext)_localctx).result =   Double.valueOf((((DoubleValueContext)_localctx).t!=null?((DoubleValueContext)_localctx).t.getText():null));
			    
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode IntegerLiteral() { return getToken(VxspParser.IntegerLiteral, 0); }
		public TerminalNode FloatingPointLiteral() { return getToken(VxspParser.FloatingPointLiteral, 0); }
		public TerminalNode BooleanLiteral() { return getToken(VxspParser.BooleanLiteral, 0); }
		public TerminalNode CharacterLiteral() { return getToken(VxspParser.CharacterLiteral, 0); }
		public TerminalNode StringLiteral() { return getToken(VxspParser.StringLiteral, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 292, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4277);
			_la = _input.LA(1);
			if ( !(((((_la - 79)) & ~0x3f) == 0 && ((1L << (_la - 79)) & ((1L << (IntegerLiteral - 79)) | (1L << (FloatingPointLiteral - 79)) | (1L << (BooleanLiteral - 79)) | (1L << (CharacterLiteral - 79)) | (1L << (StringLiteral - 79)))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierListContext extends ParserRuleContext {
		public List<String> result;
		public List<String> out =  new ArrayList<String>();
		public Token cr;
		public List<TerminalNode> Identifier() { return getTokens(VxspParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(VxspParser.Identifier, i);
		}
		public IdentifierListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifierList; }
	}

	public final IdentifierListContext identifierList() throws RecognitionException {
		IdentifierListContext _localctx = new IdentifierListContext(_ctx, getState());
		enterRule(_localctx, 294, RULE_identifierList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(4300);
			_la = _input.LA(1);
			if (_la==ENVIRONMENT || _la==Identifier) {
				{
				setState(4283);
				switch (_input.LA(1)) {
				case Identifier:
					{
					setState(4279);
					((IdentifierListContext)_localctx).cr = match(Identifier);
					 _localctx.out.add((((IdentifierListContext)_localctx).cr!=null?((IdentifierListContext)_localctx).cr.getText():null)); 
					}
					break;
				case ENVIRONMENT:
					{
					setState(4281);
					match(ENVIRONMENT);
					 _localctx.out.add("environment"); 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(4294);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,189,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(4285);
						match(COMMA);
						setState(4290);
						switch (_input.LA(1)) {
						case Identifier:
							{
							setState(4286);
							((IdentifierListContext)_localctx).cr = match(Identifier);
							 _localctx.out.add((((IdentifierListContext)_localctx).cr!=null?((IdentifierListContext)_localctx).cr.getText():null)); 
							}
							break;
						case ENVIRONMENT:
							{
							setState(4288);
							match(ENVIRONMENT);
							 _localctx.out.add("environment"); 
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						}
						} 
					}
					setState(4296);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,189,_ctx);
				}
				setState(4298);
				_la = _input.LA(1);
				if (_la==COMMA) {
					{
					setState(4297);
					match(COMMA);
					}
				}

				}
			}


			     ((IdentifierListContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualifiedIdentifierEnvContext extends ParserRuleContext {
		public String result;
		public StringBuilder out =  new StringBuilder();
		public Token tName;
		public List<TerminalNode> Identifier() { return getTokens(VxspParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(VxspParser.Identifier, i);
		}
		public QualifiedIdentifierEnvContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedIdentifierEnv; }
	}

	public final QualifiedIdentifierEnvContext qualifiedIdentifierEnv() throws RecognitionException {
		QualifiedIdentifierEnvContext _localctx = new QualifiedIdentifierEnvContext(_ctx, getState());
		enterRule(_localctx, 296, RULE_qualifiedIdentifierEnv);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(4308);
			switch (_input.LA(1)) {
			case Identifier:
				{
				setState(4304);
				((QualifiedIdentifierEnvContext)_localctx).tName = match(Identifier);
				 _localctx.out.append((((QualifiedIdentifierEnvContext)_localctx).tName!=null?((QualifiedIdentifierEnvContext)_localctx).tName.getText():null)); 
				}
				break;
			case ENVIRONMENT:
				{
				setState(4306);
				match(ENVIRONMENT);
				 _localctx.out.append("environment"); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(4313);
			_la = _input.LA(1);
			if (_la==DOT) {
				{
				setState(4310);
				match(DOT);
				setState(4311);
				((QualifiedIdentifierEnvContext)_localctx).tName = match(Identifier);
				 _localctx.out.append('.' + (((QualifiedIdentifierEnvContext)_localctx).tName!=null?((QualifiedIdentifierEnvContext)_localctx).tName.getText():null)); 
				}
			}


			     ((QualifiedIdentifierEnvContext)_localctx).result =  _localctx.out.toString();

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public AgentSystem as;
		public boolean allowDeclaration;
		public List<AbstractExpression> result;
		public List<AbstractExpression> out =  new ArrayList<>();
		public ExpressionContext cr;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExpressionListContext(ParserRuleContext parent, int invokingState, AgentSystem as, boolean allowDeclaration) {
			super(parent, invokingState);
			this.as = as;
			this.allowDeclaration = allowDeclaration;
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
	}

	public final ExpressionListContext expressionList(AgentSystem as,boolean allowDeclaration) throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState(), as, allowDeclaration);
		enterRule(_localctx, 298, RULE_expressionList);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(4331);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ENVIRONMENT) | (1L << FALSE) | (1L << TRUE) | (1L << MINUS) | (1L << LNOT) | (1L << LPAREN))) != 0) || ((((_la - 79)) & ~0x3f) == 0 && ((1L << (_la - 79)) & ((1L << (IntegerLiteral - 79)) | (1L << (FloatingPointLiteral - 79)) | (1L << (Identifier - 79)))) != 0)) {
				{
				setState(4317);
				((ExpressionListContext)_localctx).cr = expression(_localctx.as, _localctx.allowDeclaration);
				 _localctx.out.add(((ExpressionListContext)_localctx).cr.result); 
				setState(4325);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,194,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(4319);
						match(COMMA);
						setState(4320);
						((ExpressionListContext)_localctx).cr = expression(_localctx.as, _localctx.allowDeclaration);
						 _localctx.out.add(((ExpressionListContext)_localctx).cr.result); 
						}
						} 
					}
					setState(4327);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,194,_ctx);
				}
				setState(4329);
				_la = _input.LA(1);
				if (_la==COMMA) {
					{
					setState(4328);
					match(COMMA);
					}
				}

				}
			}


			     ((ExpressionListContext)_localctx).result =  _localctx.out;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	private static final int _serializedATNSegments = 2;
	private static final String _serializedATNSegment0 =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3Z\u10f2\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080"+
		"\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084\t\u0084\4\u0085"+
		"\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087\4\u0088\t\u0088\4\u0089\t\u0089"+
		"\4\u008a\t\u008a\4\u008b\t\u008b\4\u008c\t\u008c\4\u008d\t\u008d\4\u008e"+
		"\t\u008e\4\u008f\t\u008f\4\u0090\t\u0090\4\u0091\t\u0091\4\u0092\t\u0092"+
		"\4\u0093\t\u0093\4\u0094\t\u0094\4\u0095\t\u0095\4\u0096\t\u0096\4\u0097"+
		"\t\u0097\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\5\3\u013c"+
		"\n\3\3\3\3\3\3\4\7\4\u0141\n\4\f\4\16\4\u0144\13\4\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\5\5\u014c\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\7\6\u0159"+
		"\n\6\f\6\16\6\u015c\13\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0166\n\7"+
		"\3\7\3\7\3\7\3\7\3\7\7\7\u016d\n\7\f\7\16\7\u0170\13\7\3\7\3\7\3\7\5\7"+
		"\u0175\n\7\3\7\3\7\3\7\7\7\u017a\n\7\f\7\16\7\u017d\13\7\3\7\3\7\3\7\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u018c\n\b\3\b\3\b\3\b\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u0199\n\t\3\t\5\t\u019c\n\t\3\t\3\t\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\7\n\u01a6\n\n\f\n\16\n\u01a9\13\n\3\n\5\n\u01ac\n"+
		"\n\3\n\3\n\3\13\3\13\3\13\5\13\u01b3\n\13\3\13\3\13\3\f\3\f\3\f\5\f\u01ba"+
		"\n\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\7\r\u01c3\n\r\f\r\16\r\u01c6\13\r\3\r"+
		"\5\r\u01c9\n\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\7\17\u01d7\n\17\f\17\16\17\u01da\13\17\3\17\5\17\u01dd\n\17\3\17\3"+
		"\17\3\20\3\20\3\20\5\20\u01e4\n\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\5\21\u01f2\n\21\3\21\3\21\3\21\7\21\u01f7\n"+
		"\21\f\21\16\21\u01fa\13\21\3\21\3\21\3\21\3\22\3\22\3\22\5\22\u0202\n"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u020c\n\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\5\22\u0214\n\22\3\22\3\22\3\22\3\23\3\23\3\23\3\24"+
		"\3\24\3\24\3\24\3\24\5\24\u0221\n\24\3\25\3\25\3\25\3\26\3\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\5\27\u0230\n\27\3\27\3\27\3\27\7\27\u0235"+
		"\n\27\f\27\16\27\u0238\13\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3"+
		"\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u024a\n\30\3\30\3\30\3\30"+
		"\7\30\u024f\n\30\f\30\16\30\u0252\13\30\3\30\3\30\3\31\3\31\3\31\3\31"+
		"\3\31\3\31\5\31\u025c\n\31\3\31\3\31\3\31\7\31\u0261\n\31\f\31\16\31\u0264"+
		"\13\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32"+
		"\u0272\n\32\3\32\3\32\3\32\7\32\u0277\n\32\f\32\16\32\u027a\13\32\3\32"+
		"\3\32\3\33\3\33\3\33\3\33\3\33\5\33\u0283\n\33\3\33\3\33\3\33\3\33\5\33"+
		"\u0289\n\33\3\33\3\33\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u029d\n\34\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\36\3\36\3\36\7\36\u02aa\n\36\f\36\16\36\u02ad\13\36"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u02b6\n\37\3 \3 \3 \3!\3!\3!"+
		"\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!"+
		"\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!"+
		"\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\3!\5!\u02fb\n!\3!\3!\3\"\3"+
		"\"\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3"+
		"$\3$\5$\u0317\n$\3$\3$\3$\3$\7$\u031d\n$\f$\16$\u0320\13$\3$\3$\3%\3%"+
		"\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%"+
		"\3%\3%\3%\5%\u0340\n%\3&\3&\3&\3&\3&\3&\3&\3&\3\'\3\'\3\'\3(\3(\3(\3("+
		"\3(\3(\3(\3(\3(\3(\3(\3(\3(\5(\u035a\n(\3(\3(\3(\3(\7(\u0360\n(\f(\16"+
		"(\u0363\13(\3(\3(\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3"+
		")\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\3)\5)\u03cf\n)\3*\3"+
		"*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\5*\u03de\n*\3*\3*\3*\3*\7*\u03e4\n"+
		"*\f*\16*\u03e7\13*\3*\3*\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+"+
		"\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+"+
		"\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+"+
		"\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+"+
		"\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\5+\u0453"+
		"\n+\3,\3,\3,\3,\3,\3,\3,\3,\3-\3-\3-\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3."+
		"\3.\3.\5.\u046d\n.\3.\3.\3.\3.\7.\u0473\n.\f.\16.\u0476\13.\3.\3.\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\5/\u0508\n/\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60"+
		"\3\60\3\60\3\60\5\60\u0517\n\60\3\60\3\60\3\60\3\60\7\60\u051d\n\60\f"+
		"\60\16\60\u0520\13\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61"+
		"\3\61\3\61\3\61\3\61\3\61\3\61\3\61\3\61\5\61\u05b2\n\61\3\62\3\62\3\62"+
		"\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63"+
		"\5\63\u05c5\n\63\3\63\3\63\3\63\3\63\7\63\u05cb\n\63\f\63\16\63\u05ce"+
		"\13\63\3\63\3\63\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64"+
		"\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64"+
		"\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64"+
		"\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64"+
		"\3\64\3\64\3\64\3\64\5\64\u060b\n\64\3\65\3\65\3\65\3\65\3\65\3\65\3\65"+
		"\3\65\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\5\66\u061e\n\66\3\66"+
		"\3\66\3\66\3\66\7\66\u0624\n\66\f\66\16\66\u0627\13\66\3\66\3\66\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u0688\n\67\38\38\38\38\3"+
		"8\38\38\38\39\39\39\39\39\39\39\39\39\59\u069b\n9\39\39\39\39\79\u06a1"+
		"\n9\f9\169\u06a4\139\39\39\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3"+
		":\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3"+
		":\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3"+
		":\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3"+
		":\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3:\3"+
		":\3:\3:\3:\3:\5:\u0717\n:\3;\3;\3;\3;\3;\3;\3;\3;\3;\3<\3<\3<\3<\3<\3"+
		"<\3<\3<\3<\5<\u072b\n<\3<\3<\3<\3<\7<\u0731\n<\f<\16<\u0734\13<\3<\3<"+
		"\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\5=\u074a\n=\3>"+
		"\3>\3>\3>\3>\3>\5>\u0752\n>\3?\3?\3?\3?\3?\3?\3?\3?\3?\5?\u075d\n?\3?"+
		"\3?\3?\3?\7?\u0763\n?\f?\16?\u0766\13?\3?\3?\3?\3?\3?\3?\3?\5?\u076f\n"+
		"?\3?\3?\3?\3?\7?\u0775\n?\f?\16?\u0778\13?\3?\3?\5?\u077c\n?\3@\3@\3@"+
		"\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\5@\u0792\n@\3A\3A"+
		"\3A\3A\3A\3A\3A\3A\3A\3A\3B\3B\3B\3B\3B\3B\3B\3B\3B\5B\u07a7\nB\3B\3B"+
		"\3B\3B\7B\u07ad\nB\fB\16B\u07b0\13B\3B\3B\3C\3C\3C\3C\3C\3C\3C\3C\3C\3"+
		"C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3"+
		"C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\5C\u07ea"+
		"\nC\3D\3D\3D\3D\3D\3D\5D\u07f2\nD\3E\3E\3E\3E\3E\3E\5E\u07fa\nE\3F\3F"+
		"\3F\3F\3F\3F\3F\3F\3F\5F\u0805\nF\3F\3F\3F\3F\7F\u080b\nF\fF\16F\u080e"+
		"\13F\3F\3F\3F\3F\3F\3F\3F\5F\u0817\nF\3F\3F\3F\3F\7F\u081d\nF\fF\16F\u0820"+
		"\13F\3F\3F\5F\u0824\nF\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3G\3"+
		"G\3G\3G\3G\3G\5G\u083a\nG\3H\3H\3H\3H\3H\3H\3H\3H\3H\3H\3I\3I\3I\3I\3"+
		"I\3I\3I\3I\3I\5I\u084f\nI\3I\3I\3I\3I\7I\u0855\nI\fI\16I\u0858\13I\3I"+
		"\3I\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J"+
		"\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J"+
		"\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J"+
		"\3J\3J\3J\3J\5J\u08a4\nJ\3K\3K\3K\3K\3K\3K\5K\u08ac\nK\3L\3L\3L\3L\3L"+
		"\3L\5L\u08b4\nL\3M\3M\3M\3M\3M\3M\3M\3M\3M\5M\u08bf\nM\3M\3M\3M\3M\7M"+
		"\u08c5\nM\fM\16M\u08c8\13M\3M\3M\3M\3M\3M\3M\3M\5M\u08d1\nM\3M\3M\3M\3"+
		"M\7M\u08d7\nM\fM\16M\u08da\13M\3M\3M\5M\u08de\nM\3N\3N\3N\3N\3N\3N\3N"+
		"\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\5N\u08f4\nN\3O\3O\3O\3O\3O\3O"+
		"\3O\3O\3P\3P\3P\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\3Q\5Q\u090e\nQ\3Q"+
		"\3Q\3Q\3Q\7Q\u0914\nQ\fQ\16Q\u0917\13Q\3Q\3Q\3R\3R\3R\3R\3R\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\3R\5R\u0937\n"+
		"R\3S\3S\3S\3S\3S\3S\3S\3S\3T\3T\3T\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3"+
		"U\3U\5U\u0951\nU\3U\3U\3U\3U\7U\u0957\nU\fU\16U\u095a\13U\3U\3U\3V\3V"+
		"\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V"+
		"\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V"+
		"\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\5V\u09a2"+
		"\nV\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\5W\u09b1\nW\3W\3W\3W\3W\7W"+
		"\u09b7\nW\fW\16W\u09ba\13W\3W\3W\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3"+
		"X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3"+
		"X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3X\3"+
		"X\3X\3X\3X\3X\3X\3X\3X\3X\3X\5X\u0a02\nX\3Y\3Y\3Y\3Y\3Y\3Y\3Y\3Y\3Z\3"+
		"Z\3Z\3[\3[\3[\3[\3[\3[\3[\3[\3[\3[\3[\3[\3[\5[\u0a1c\n[\3[\3[\3[\3[\7"+
		"[\u0a22\n[\f[\16[\u0a25\13[\3[\3[\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\"+
		"\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3"+
		"\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\"+
		"\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3"+
		"\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\"+
		"\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\3\\\5\\\u0a81\n\\\3]\3]\3]\3]\3]\3]\3"+
		"]\3]\3]\3]\3]\3]\3]\5]\u0a90\n]\3]\3]\3]\3]\7]\u0a96\n]\f]\16]\u0a99\13"+
		"]\3]\3]\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3"+
		"^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3"+
		"^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3"+
		"^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\3^\5^\u0af5"+
		"\n^\3_\3_\3_\3_\3_\3_\3_\3_\3`\3`\3`\3`\3`\3`\3`\3`\3`\5`\u0b08\n`\3`"+
		"\3`\3`\3`\7`\u0b0e\n`\f`\16`\u0b11\13`\3`\3`\3a\3a\3a\3a\3a\3a\3a\3a\3"+
		"a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3"+
		"a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\3"+
		"a\3a\3a\5a\u0b4e\na\3b\3b\3b\3b\3b\3b\3b\3b\3c\3c\3c\3c\3c\3c\3c\3c\3"+
		"c\5c\u0b61\nc\3c\3c\3c\3c\7c\u0b67\nc\fc\16c\u0b6a\13c\3c\3c\3d\3d\3d"+
		"\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d"+
		"\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d"+
		"\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d"+
		"\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d\3d"+
		"\3d\3d\5d\u0bcf\nd\3e\3e\3e\3e\3e\3e\3e\3e\3f\3f\3f\3f\3f\3f\3f\3f\3f"+
		"\5f\u0be2\nf\3f\3f\3f\3f\7f\u0be8\nf\ff\16f\u0beb\13f\3f\3f\3g\3g\3g\3"+
		"g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3"+
		"g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3"+
		"g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3"+
		"g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3"+
		"g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\3g\5g\u0c64"+
		"\ng\3h\3h\3h\3h\3h\3h\3h\3h\3h\3i\3i\3i\3i\3i\3i\3i\3i\3i\5i\u0c78\ni"+
		"\3i\3i\3i\3i\7i\u0c7e\ni\fi\16i\u0c81\13i\3i\3i\3j\3j\3j\3j\3j\3j\3j\3"+
		"j\3j\3j\3j\3j\3j\3j\3j\3j\3j\3j\5j\u0c97\nj\3k\3k\3k\3k\3k\3k\5k\u0c9f"+
		"\nk\3l\3l\3l\3l\3l\3l\3l\3l\3l\5l\u0caa\nl\3l\3l\3l\3l\7l\u0cb0\nl\fl"+
		"\16l\u0cb3\13l\3l\3l\3l\3l\3l\3l\3l\5l\u0cbc\nl\3l\3l\3l\3l\7l\u0cc2\n"+
		"l\fl\16l\u0cc5\13l\3l\3l\5l\u0cc9\nl\3m\3m\3m\3m\3m\3m\3m\3m\3m\3m\3m"+
		"\3m\3m\3m\3m\3m\3m\3m\3m\3m\5m\u0cdf\nm\3n\3n\3n\3n\3n\3n\3n\3n\3n\3n"+
		"\3o\3o\3o\3o\3o\3o\3o\3o\3o\5o\u0cf4\no\3o\3o\3o\3o\7o\u0cfa\no\fo\16"+
		"o\u0cfd\13o\3o\3o\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3"+
		"p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3"+
		"p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\3p\5p\u0d3b\np\3q\3"+
		"q\3q\3q\3q\3q\5q\u0d43\nq\3r\3r\3r\3r\3r\3r\5r\u0d4b\nr\3s\3s\3s\3s\3"+
		"s\3s\3s\3s\3s\5s\u0d56\ns\3s\3s\3s\3s\7s\u0d5c\ns\fs\16s\u0d5f\13s\3s"+
		"\3s\3s\3s\3s\3s\3s\5s\u0d68\ns\3s\3s\3s\3s\7s\u0d6e\ns\fs\16s\u0d71\13"+
		"s\3s\3s\5s\u0d75\ns\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3t\3"+
		"t\3t\3t\3t\5t\u0d8b\nt\3u\3u\3u\3u\3u\3u\3u\3u\3u\3u\3v\3v\3v\3v\3v\3"+
		"v\3v\3v\3v\5v\u0da0\nv\3v\3v\3v\3v\7v\u0da6\nv\fv\16v\u0da9\13v\3v\3v"+
		"\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w"+
		"\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w"+
		"\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w\3w"+
		"\3w\3w\3w\3w\3w\3w\3w\3w\3w\5w\u0dfb\nw\3x\3x\3x\3x\3x\3x\5x\u0e03\nx"+
		"\3y\3y\3y\3y\3y\3y\5y\u0e0b\ny\3z\3z\3z\3z\3z\3z\3z\3z\3z\5z\u0e16\nz"+
		"\3z\3z\3z\3z\7z\u0e1c\nz\fz\16z\u0e1f\13z\3z\3z\3z\3z\3z\3z\3z\5z\u0e28"+
		"\nz\3z\3z\3z\3z\7z\u0e2e\nz\fz\16z\u0e31\13z\3z\3z\5z\u0e35\nz\3{\3{\3"+
		"{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\3{\5{\u0e4b\n{\3|\3"+
		"|\3|\3|\3|\3|\3|\3|\3|\3}\3}\3}\3}\3}\3}\3}\3}\3}\5}\u0e5f\n}\3}\3}\3"+
		"}\3}\7}\u0e65\n}\f}\16}\u0e68\13}\3}\3}\3~\3~\3~\3~\3~\3~\3~\3~\3~\3~"+
		"\3~\3~\3~\3~\3~\3~\3~\3~\3~\3~\3~\3~\3~\5~\u0e83\n~\3\177\3\177\3\177"+
		"\3\177\3\177\3\177\5\177\u0e8b\n\177\3\u0080\3\u0080\3\u0080\3\u0080\3"+
		"\u0080\3\u0080\3\u0080\3\u0080\3\u0080\5\u0080\u0e96\n\u0080\3\u0080\3"+
		"\u0080\3\u0080\3\u0080\7\u0080\u0e9c\n\u0080\f\u0080\16\u0080\u0e9f\13"+
		"\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\5\u0080"+
		"\u0ea8\n\u0080\3\u0080\3\u0080\3\u0080\3\u0080\7\u0080\u0eae\n\u0080\f"+
		"\u0080\16\u0080\u0eb1\13\u0080\3\u0080\3\u0080\5\u0080\u0eb5\n\u0080\3"+
		"\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081"+
		"\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0081"+
		"\3\u0081\3\u0081\5\u0081\u0ecb\n\u0081\3\u0082\3\u0082\3\u0082\3\u0082"+
		"\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082\3\u0083\3\u0083\3\u0083"+
		"\3\u0083\3\u0083\3\u0083\3\u0083\3\u0083\3\u0083\5\u0083\u0ee0\n\u0083"+
		"\3\u0083\3\u0083\3\u0083\3\u0083\7\u0083\u0ee6\n\u0083\f\u0083\16\u0083"+
		"\u0ee9\13\u0083\3\u0083\3\u0083\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084\3\u0084"+
		"\3\u0084\3\u0084\3\u0084\3\u0084\5\u0084\u0f50\n\u0084\3\u0085\3\u0085"+
		"\3\u0085\3\u0085\3\u0085\3\u0085\5\u0085\u0f58\n\u0085\3\u0086\3\u0086"+
		"\3\u0086\3\u0086\3\u0086\3\u0086\5\u0086\u0f60\n\u0086\3\u0087\3\u0087"+
		"\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\5\u0087\u0f6b"+
		"\n\u0087\3\u0087\3\u0087\3\u0087\3\u0087\7\u0087\u0f71\n\u0087\f\u0087"+
		"\16\u0087\u0f74\13\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087"+
		"\3\u0087\5\u0087\u0f7d\n\u0087\3\u0087\3\u0087\3\u0087\3\u0087\7\u0087"+
		"\u0f83\n\u0087\f\u0087\16\u0087\u0f86\13\u0087\3\u0087\3\u0087\5\u0087"+
		"\u0f8a\n\u0087\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088"+
		"\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088"+
		"\3\u0088\3\u0088\3\u0088\3\u0088\5\u0088\u0fa0\n\u0088\3\u0089\3\u0089"+
		"\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089\3\u008a"+
		"\3\u008a\3\u008a\3\u008a\3\u008a\3\u008a\3\u008a\3\u008a\3\u008a\5\u008a"+
		"\u0fb5\n\u008a\3\u008a\3\u008a\3\u008a\3\u008a\7\u008a\u0fbb\n\u008a\f"+
		"\u008a\16\u008a\u0fbe\13\u008a\3\u008a\3\u008a\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\3\u008b\5\u008b"+
		"\u104b\n\u008b\3\u008c\3\u008c\3\u008c\3\u008c\3\u008c\3\u008c\5\u008c"+
		"\u1053\n\u008c\3\u008d\3\u008d\3\u008d\3\u008d\3\u008d\3\u008d\5\u008d"+
		"\u105b\n\u008d\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e"+
		"\3\u008e\3\u008e\5\u008e\u1066\n\u008e\3\u008e\3\u008e\3\u008e\3\u008e"+
		"\7\u008e\u106c\n\u008e\f\u008e\16\u008e\u106f\13\u008e\3\u008e\3\u008e"+
		"\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e\5\u008e\u1078\n\u008e\3\u008e"+
		"\3\u008e\3\u008e\3\u008e\7\u008e\u107e\n\u008e\f\u008e\16\u008e\u1081"+
		"\13\u008e\3\u008e\3\u008e\5\u008e\u1085\n\u008e\3\u008f\3\u008f\3\u008f"+
		"\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f"+
		"\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\3\u008f\5\u008f"+
		"\u109b\n\u008f\3\u0090\3\u0090\3\u0090\3\u0090\3\u0090\5\u0090\u10a2\n"+
		"\u0090\3\u0090\3\u0090\3\u0091\3\u0091\3\u0091\3\u0091\3\u0091\3\u0091"+
		"\3\u0091\3\u0091\3\u0091\3\u0091\5\u0091\u10b0\n\u0091\3\u0092\3\u0092"+
		"\3\u0092\3\u0093\3\u0093\3\u0093\3\u0094\3\u0094\3\u0095\3\u0095\3\u0095"+
		"\3\u0095\5\u0095\u10be\n\u0095\3\u0095\3\u0095\3\u0095\3\u0095\3\u0095"+
		"\5\u0095\u10c5\n\u0095\7\u0095\u10c7\n\u0095\f\u0095\16\u0095\u10ca\13"+
		"\u0095\3\u0095\5\u0095\u10cd\n\u0095\5\u0095\u10cf\n\u0095\3\u0095\3\u0095"+
		"\3\u0096\3\u0096\3\u0096\3\u0096\5\u0096\u10d7\n\u0096\3\u0096\3\u0096"+
		"\3\u0096\5\u0096\u10dc\n\u0096\3\u0096\3\u0096\3\u0097\3\u0097\3\u0097"+
		"\3\u0097\3\u0097\3\u0097\7\u0097\u10e6\n\u0097\f\u0097\16\u0097\u10e9"+
		"\13\u0097\3\u0097\5\u0097\u10ec\n\u0097\5\u0097\u10ee\n\u0097\3\u0097"+
		"\3\u0097\3\u0097\2\2\u0098\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$"+
		"&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084"+
		"\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c"+
		"\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4"+
		"\u00b6\u00b8\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc"+
		"\u00ce\u00d0\u00d2\u00d4\u00d6\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4"+
		"\u00e6\u00e8\u00ea\u00ec\u00ee\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc"+
		"\u00fe\u0100\u0102\u0104\u0106\u0108\u010a\u010c\u010e\u0110\u0112\u0114"+
		"\u0116\u0118\u011a\u011c\u011e\u0120\u0122\u0124\u0126\u0128\u012a\u012c"+
		"\2\3\3\2QU\u1280\2\u012e\3\2\2\2\4\u0136\3\2\2\2\6\u0142\3\2\2\2\b\u0145"+
		"\3\2\2\2\n\u015a\3\2\2\2\f\u0165\3\2\2\2\16\u0181\3\2\2\2\20\u0190\3\2"+
		"\2\2\22\u019f\3\2\2\2\24\u01af\3\2\2\2\26\u01b6\3\2\2\2\30\u01bd\3\2\2"+
		"\2\32\u01cc\3\2\2\2\34\u01d0\3\2\2\2\36\u01e0\3\2\2\2 \u01e7\3\2\2\2\""+
		"\u01fe\3\2\2\2$\u0218\3\2\2\2&\u0220\3\2\2\2(\u0222\3\2\2\2*\u0225\3\2"+
		"\2\2,\u0229\3\2\2\2.\u023b\3\2\2\2\60\u0255\3\2\2\2\62\u0267\3\2\2\2\64"+
		"\u0288\3\2\2\2\66\u029c\3\2\2\28\u029e\3\2\2\2:\u02ab\3\2\2\2<\u02ae\3"+
		"\2\2\2>\u02b7\3\2\2\2@\u02ba\3\2\2\2B\u02fe\3\2\2\2D\u0306\3\2\2\2F\u0309"+
		"\3\2\2\2H\u033f\3\2\2\2J\u0341\3\2\2\2L\u0349\3\2\2\2N\u034c\3\2\2\2P"+
		"\u03ce\3\2\2\2R\u03d0\3\2\2\2T\u0452\3\2\2\2V\u0454\3\2\2\2X\u045c\3\2"+
		"\2\2Z\u045f\3\2\2\2\\\u0507\3\2\2\2^\u0509\3\2\2\2`\u05b1\3\2\2\2b\u05b3"+
		"\3\2\2\2d\u05bb\3\2\2\2f\u060a\3\2\2\2h\u060c\3\2\2\2j\u0614\3\2\2\2l"+
		"\u0687\3\2\2\2n\u0689\3\2\2\2p\u0691\3\2\2\2r\u0716\3\2\2\2t\u0718\3\2"+
		"\2\2v\u0721\3\2\2\2x\u0749\3\2\2\2z\u0751\3\2\2\2|\u077b\3\2\2\2~\u0791"+
		"\3\2\2\2\u0080\u0793\3\2\2\2\u0082\u079d\3\2\2\2\u0084\u07e9\3\2\2\2\u0086"+
		"\u07f1\3\2\2\2\u0088\u07f9\3\2\2\2\u008a\u0823\3\2\2\2\u008c\u0839\3\2"+
		"\2\2\u008e\u083b\3\2\2\2\u0090\u0845\3\2\2\2\u0092\u08a3\3\2\2\2\u0094"+
		"\u08ab\3\2\2\2\u0096\u08b3\3\2\2\2\u0098\u08dd\3\2\2\2\u009a\u08f3\3\2"+
		"\2\2\u009c\u08f5\3\2\2\2\u009e\u08fd\3\2\2\2\u00a0\u0900\3\2\2\2\u00a2"+
		"\u0936\3\2\2\2\u00a4\u0938\3\2\2\2\u00a6\u0940\3\2\2\2\u00a8\u0943\3\2"+
		"\2\2\u00aa\u09a1\3\2\2\2\u00ac\u09a3\3\2\2\2\u00ae\u0a01\3\2\2\2\u00b0"+
		"\u0a03\3\2\2\2\u00b2\u0a0b\3\2\2\2\u00b4\u0a0e\3\2\2\2\u00b6\u0a80\3\2"+
		"\2\2\u00b8\u0a82\3\2\2\2\u00ba\u0af4\3\2\2\2\u00bc\u0af6\3\2\2\2\u00be"+
		"\u0afe\3\2\2\2\u00c0\u0b4d\3\2\2\2\u00c2\u0b4f\3\2\2\2\u00c4\u0b57\3\2"+
		"\2\2\u00c6\u0bce\3\2\2\2\u00c8\u0bd0\3\2\2\2\u00ca\u0bd8\3\2\2\2\u00cc"+
		"\u0c63\3\2\2\2\u00ce\u0c65\3\2\2\2\u00d0\u0c6e\3\2\2\2\u00d2\u0c96\3\2"+
		"\2\2\u00d4\u0c9e\3\2\2\2\u00d6\u0cc8\3\2\2\2\u00d8\u0cde\3\2\2\2\u00da"+
		"\u0ce0\3\2\2\2\u00dc\u0cea\3\2\2\2\u00de\u0d3a\3\2\2\2\u00e0\u0d42\3\2"+
		"\2\2\u00e2\u0d4a\3\2\2\2\u00e4\u0d74\3\2\2\2\u00e6\u0d8a\3\2\2\2\u00e8"+
		"\u0d8c\3\2\2\2\u00ea\u0d96\3\2\2\2\u00ec\u0dfa\3\2\2\2\u00ee\u0e02\3\2"+
		"\2\2\u00f0\u0e0a\3\2\2\2\u00f2\u0e34\3\2\2\2\u00f4\u0e4a\3\2\2\2\u00f6"+
		"\u0e4c\3\2\2\2\u00f8\u0e55\3\2\2\2\u00fa\u0e82\3\2\2\2\u00fc\u0e8a\3\2"+
		"\2\2\u00fe\u0eb4\3\2\2\2\u0100\u0eca\3\2\2\2\u0102\u0ecc\3\2\2\2\u0104"+
		"\u0ed6\3\2\2\2\u0106\u0f4f\3\2\2\2\u0108\u0f57\3\2\2\2\u010a\u0f5f\3\2"+
		"\2\2\u010c\u0f89\3\2\2\2\u010e\u0f9f\3\2\2\2\u0110\u0fa1\3\2\2\2\u0112"+
		"\u0fab\3\2\2\2\u0114\u104a\3\2\2\2\u0116\u1052\3\2\2\2\u0118\u105a\3\2"+
		"\2\2\u011a\u1084\3\2\2\2\u011c\u109a\3\2\2\2\u011e\u109c\3\2\2\2\u0120"+
		"\u10af\3\2\2\2\u0122\u10b1\3\2\2\2\u0124\u10b4\3\2\2\2\u0126\u10b7\3\2"+
		"\2\2\u0128\u10ce\3\2\2\2\u012a\u10d6\3\2\2\2\u012c\u10ed\3\2\2\2\u012e"+
		"\u012f\b\2\1\2\u012f\u0130\5\4\3\2\u0130\u0131\5\6\4\2\u0131\u0132\5\n"+
		"\6\2\u0132\u0133\5&\24\2\u0133\u0134\5:\36\2\u0134\u0135\b\2\1\2\u0135"+
		"\3\3\2\2\2\u0136\u0137\7\b\2\2\u0137\u0138\7V\2\2\u0138\u013b\b\3\1\2"+
		"\u0139\u013a\7\3\2\2\u013a\u013c\b\3\1\2\u013b\u0139\3\2\2\2\u013b\u013c"+
		"\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013e\7(\2\2\u013e\5\3\2\2\2\u013f"+
		"\u0141\5\b\5\2\u0140\u013f\3\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2"+
		"\2\2\u0142\u0143\3\2\2\2\u0143\7\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u0146"+
		"\7\n\2\2\u0146\u014b\b\5\1\2\u0147\u0148\7\24\2\2\u0148\u014c\b\5\1\2"+
		"\u0149\u014a\7\22\2\2\u014a\u014c\b\5\1\2\u014b\u0147\3\2\2\2\u014b\u0149"+
		"\3\2\2\2\u014c\u014d\3\2\2\2\u014d\u014e\7V\2\2\u014e\u014f\b\5\1\2\u014f"+
		"\u0150\7\"\2\2\u0150\u0151\5(\25\2\u0151\u0152\b\5\1\2\u0152\u0153\7("+
		"\2\2\u0153\t\3\2\2\2\u0154\u0155\5\f\7\2\u0155\u0156\b\6\1\2\u0156\u0159"+
		"\3\2\2\2\u0157\u0159\58\35\2\u0158\u0154\3\2\2\2\u0158\u0157\3\2\2\2\u0159"+
		"\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015a\u015b\3\2\2\2\u015b\13\3\2\2"+
		"\2\u015c\u015a\3\2\2\2\u015d\u015e\7\7\2\2\u015e\u015f\b\7\1\2\u015f\u0166"+
		"\7V\2\2\u0160\u0161\7\17\2\2\u0161\u0162\b\7\1\2\u0162\u0166\7V\2\2\u0163"+
		"\u0164\7\f\2\2\u0164\u0166\b\7\1\2\u0165\u015d\3\2\2\2\u0165\u0160\3\2"+
		"\2\2\u0165\u0163\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0168\b\7\1\2\u0168"+
		"\u016e\7,\2\2\u0169\u016a\5\16\b\2\u016a\u016b\b\7\1\2\u016b\u016d\3\2"+
		"\2\2\u016c\u0169\3\2\2\2\u016d\u0170\3\2\2\2\u016e\u016c\3\2\2\2\u016e"+
		"\u016f\3\2\2\2\u016f\u0174\3\2\2\2\u0170\u016e\3\2\2\2\u0171\u0172\5 "+
		"\21\2\u0172\u0173\b\7\1\2\u0173\u0175\3\2\2\2\u0174\u0171\3\2\2\2\u0174"+
		"\u0175\3\2\2\2\u0175\u017b\3\2\2\2\u0176\u0177\5\"\22\2\u0177\u0178\b"+
		"\7\1\2\u0178\u017a\3\2\2\2\u0179\u0176\3\2\2\2\u017a\u017d\3\2\2\2\u017b"+
		"\u0179\3\2\2\2\u017b\u017c\3\2\2\2\u017c\u017e\3\2\2\2\u017d\u017b\3\2"+
		"\2\2\u017e\u017f\7-\2\2\u017f\u0180\7(\2\2\u0180\r\3\2\2\2\u0181\u0182"+
		"\5\20\t\2\u0182\u0183\7\24\2\2\u0183\u0184\b\b\1\2\u0184\u0185\7,\2\2"+
		"\u0185\u0186\5\22\n\2\u0186\u0187\7-\2\2\u0187\u0188\7V\2\2\u0188\u018b"+
		"\b\b\1\2\u0189\u018a\7\"\2\2\u018a\u018c\5\30\r\2\u018b\u0189\3\2\2\2"+
		"\u018b\u018c\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u018e\7(\2\2\u018e\u018f"+
		"\b\b\1\2\u018f\17\3\2\2\2\u0190\u019b\b\t\1\2\u0191\u0192\7\n\2\2\u0192"+
		"\u0198\b\t\1\2\u0193\u0194\7,\2\2\u0194\u0195\5\u0128\u0095\2\u0195\u0196"+
		"\b\t\1\2\u0196\u0197\7-\2\2\u0197\u0199\3\2\2\2\u0198\u0193\3\2\2\2\u0198"+
		"\u0199\3\2\2\2\u0199\u019a\3\2\2\2\u019a\u019c\b\t\1\2\u019b\u0191\3\2"+
		"\2\2\u019b\u019c\3\2\2\2\u019c\u019d\3\2\2\2\u019d\u019e\b\t\1\2\u019e"+
		"\21\3\2\2\2\u019f\u01a0\5\24\13\2\u01a0\u01a7\b\n\1\2\u01a1\u01a2\7&\2"+
		"\2\u01a2\u01a3\5\24\13\2\u01a3\u01a4\b\n\1\2\u01a4\u01a6\3\2\2\2\u01a5"+
		"\u01a1\3\2\2\2\u01a6\u01a9\3\2\2\2\u01a7\u01a5\3\2\2\2\u01a7\u01a8\3\2"+
		"\2\2\u01a8\u01ab\3\2\2\2\u01a9\u01a7\3\2\2\2\u01aa\u01ac\7&\2\2\u01ab"+
		"\u01aa\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac\u01ad\3\2\2\2\u01ad\u01ae\b\n"+
		"\1\2\u01ae\23\3\2\2\2\u01af\u01b2\5\26\f\2\u01b0\u01b1\7\60\2\2\u01b1"+
		"\u01b3\5\26\f\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2\2\2\u01b3\u01b4\3"+
		"\2\2\2\u01b4\u01b5\b\13\1\2\u01b5\25\3\2\2\2\u01b6\u01b9\5(\25\2\u01b7"+
		"\u01b8\7\"\2\2\u01b8\u01ba\5(\25\2\u01b9\u01b7\3\2\2\2\u01b9\u01ba\3\2"+
		"\2\2\u01ba\u01bb\3\2\2\2\u01bb\u01bc\b\f\1\2\u01bc\27\3\2\2\2\u01bd\u01c4"+
		"\5\32\16\2\u01be\u01bf\7&\2\2\u01bf\u01c0\5\32\16\2\u01c0\u01c1\b\r\1"+
		"\2\u01c1\u01c3\3\2\2\2\u01c2\u01be\3\2\2\2\u01c3\u01c6\3\2\2\2\u01c4\u01c2"+
		"\3\2\2\2\u01c4\u01c5\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c7"+
		"\u01c9\7&\2\2\u01c8\u01c7\3\2\2\2\u01c8\u01c9\3\2\2\2\u01c9\u01ca\3\2"+
		"\2\2\u01ca\u01cb\b\r\1\2\u01cb\31\3\2\2\2\u01cc\u01cd\5\34\17\2\u01cd"+
		"\u01ce\b\16\1\2\u01ce\u01cf\b\16\1\2\u01cf\33\3\2\2\2\u01d0\u01d1\5\36"+
		"\20\2\u01d1\u01d8\b\17\1\2\u01d2\u01d3\7&\2\2\u01d3\u01d4\5\36\20\2\u01d4"+
		"\u01d5\b\17\1\2\u01d5\u01d7\3\2\2\2\u01d6\u01d2\3\2\2\2\u01d7\u01da\3"+
		"\2\2\2\u01d8\u01d6\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9\u01dc\3\2\2\2\u01da"+
		"\u01d8\3\2\2\2\u01db\u01dd\7&\2\2\u01dc\u01db\3\2\2\2\u01dc\u01dd\3\2"+
		"\2\2\u01dd\u01de\3\2\2\2\u01de\u01df\b\17\1\2\u01df\35\3\2\2\2\u01e0\u01e3"+
		"\5(\25\2\u01e1\u01e2\7\60\2\2\u01e2\u01e4\5(\25\2\u01e3\u01e1\3\2\2\2"+
		"\u01e3\u01e4\3\2\2\2\u01e4\u01e5\3\2\2\2\u01e5\u01e6\b\20\1\2\u01e6\37"+
		"\3\2\2\2\u01e7\u01e8\7\21\2\2\u01e8\u01e9\b\21\1\2\u01e9\u01ea\7,\2\2"+
		"\u01ea\u01f8\b\21\1\2\u01eb\u01ec\5\u0128\u0095\2\u01ec\u01f1\7\"\2\2"+
		"\u01ed\u01f2\5*\26\2\u01ee\u01ef\b\21\1\2\u01ef\u01f0\7\4\2\2\u01f0\u01f2"+
		"\b\21\1\2\u01f1\u01ed\3\2\2\2\u01f1\u01ee\3\2\2\2\u01f2\u01f3\3\2\2\2"+
		"\u01f3\u01f4\b\21\1\2\u01f4\u01f5\7(\2\2\u01f5\u01f7\3\2\2\2\u01f6\u01eb"+
		"\3\2\2\2\u01f7\u01fa\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f9"+
		"\u01fb\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fb\u01fc\7-\2\2\u01fc\u01fd\7(\2"+
		"\2\u01fd!\3\2\2\2\u01fe\u01ff\7.\2\2\u01ff\u0201\b\22\1\2\u0200\u0202"+
		"\7V\2\2\u0201\u0200\3\2\2\2\u0201\u0202\3\2\2\2\u0202\u0203\3\2\2\2\u0203"+
		"\u0204\7/\2\2\u0204\u020b\b\22\1\2\u0205\u0206\5*\26\2\u0206\u0207\b\22"+
		"\1\2\u0207\u020c\3\2\2\2\u0208\u0209\b\22\1\2\u0209\u020a\7\4\2\2\u020a"+
		"\u020c\b\22\1\2\u020b\u0205\3\2\2\2\u020b\u0208\3\2\2\2\u020c\u020d\3"+
		"\2\2\2\u020d\u0213\7 \2\2\u020e\u020f\5$\23\2\u020f\u0210\7\"\2\2\u0210"+
		"\u0211\5*\26\2\u0211\u0214\3\2\2\2\u0212\u0214\7\23\2\2\u0213\u020e\3"+
		"\2\2\2\u0213\u0212\3\2\2\2\u0214\u0215\3\2\2\2\u0215\u0216\7(\2\2\u0216"+
		"\u0217\b\22\1\2\u0217#\3\2\2\2\u0218\u0219\5\u012a\u0096\2\u0219\u021a"+
		"\b\23\1\2\u021a%\3\2\2\2\u021b\u021c\7\16\2\2\u021c\u021d\5*\26\2\u021d"+
		"\u021e\b\24\1\2\u021e\u021f\7(\2\2\u021f\u0221\3\2\2\2\u0220\u021b\3\2"+
		"\2\2\u0220\u0221\3\2\2\2\u0221\'\3\2\2\2\u0222\u0223\5*\26\2\u0223\u0224"+
		"\b\25\1\2\u0224)\3\2\2\2\u0225\u0226\b\26\1\2\u0226\u0227\5,\27\2\u0227"+
		"\u0228\b\26\1\2\u0228+\3\2\2\2\u0229\u0236\5.\30\2\u022a\u022f\b\27\1"+
		"\2\u022b\u022c\7#\2\2\u022c\u0230\b\27\1\2\u022d\u022e\7$\2\2\u022e\u0230"+
		"\b\27\1\2\u022f\u022b\3\2\2\2\u022f\u022d\3\2\2\2\u0230\u0231\3\2\2\2"+
		"\u0231\u0232\5.\30\2\u0232\u0233\b\27\1\2\u0233\u0235\3\2\2\2\u0234\u022a"+
		"\3\2\2\2\u0235\u0238\3\2\2\2\u0236\u0234\3\2\2\2\u0236\u0237\3\2\2\2\u0237"+
		"\u0239\3\2\2\2\u0238\u0236\3\2\2\2\u0239\u023a\b\27\1\2\u023a-\3\2\2\2"+
		"\u023b\u0250\5\60\31\2\u023c\u0249\b\30\1\2\u023d\u023e\7\32\2\2\u023e"+
		"\u024a\b\30\1\2\u023f\u0240\7\5\2\2\u0240\u024a\b\30\1\2\u0241\u0242\7"+
		"\33\2\2\u0242\u024a\b\30\1\2\u0243\u0244\7\34\2\2\u0244\u024a\b\30\1\2"+
		"\u0245\u0246\7\35\2\2\u0246\u024a\b\30\1\2\u0247\u0248\7\36\2\2\u0248"+
		"\u024a\b\30\1\2\u0249\u023d\3\2\2\2\u0249\u023f\3\2\2\2\u0249\u0241\3"+
		"\2\2\2\u0249\u0243\3\2\2\2\u0249\u0245\3\2\2\2\u0249\u0247\3\2\2\2\u024a"+
		"\u024b\3\2\2\2\u024b\u024c\5\60\31\2\u024c\u024d\b\30\1\2\u024d\u024f"+
		"\3\2\2\2\u024e\u023c\3\2\2\2\u024f\u0252\3\2\2\2\u0250\u024e\3\2\2\2\u0250"+
		"\u0251\3\2\2\2\u0251\u0253\3\2\2\2\u0252\u0250\3\2\2\2\u0253\u0254\b\30"+
		"\1\2\u0254/\3\2\2\2\u0255\u0262\5\62\32\2\u0256\u025b\b\31\1\2\u0257\u0258"+
		"\7\25\2\2\u0258\u025c\b\31\1\2\u0259\u025a\7\26\2\2\u025a\u025c\b\31\1"+
		"\2\u025b\u0257\3\2\2\2\u025b\u0259\3\2\2\2\u025c\u025d\3\2\2\2\u025d\u025e"+
		"\5\62\32\2\u025e\u025f\b\31\1\2\u025f\u0261\3\2\2\2\u0260\u0256\3\2\2"+
		"\2\u0261\u0264\3\2\2\2\u0262\u0260\3\2\2\2\u0262\u0263\3\2\2\2\u0263\u0265"+
		"\3\2\2\2\u0264\u0262\3\2\2\2\u0265\u0266\b\31\1\2\u0266\61\3\2\2\2\u0267"+
		"\u0278\5\64\33\2\u0268\u0271\b\32\1\2\u0269\u026a\7\27\2\2\u026a\u0272"+
		"\b\32\1\2\u026b\u026c\7\30\2\2\u026c\u0272\b\32\1\2\u026d\u026e\7\31\2"+
		"\2\u026e\u0272\b\32\1\2\u026f\u0270\7\6\2\2\u0270\u0272\b\32\1\2\u0271"+
		"\u0269\3\2\2\2\u0271\u026b\3\2\2\2\u0271\u026d\3\2\2\2\u0271\u026f\3\2"+
		"\2\2\u0272\u0273\3\2\2\2\u0273\u0274\5\64\33\2\u0274\u0275\b\32\1\2\u0275"+
		"\u0277\3\2\2\2\u0276\u0268\3\2\2\2\u0277\u027a\3\2\2\2\u0278\u0276\3\2"+
		"\2\2\u0278\u0279\3\2\2\2\u0279\u027b\3\2\2\2\u027a\u0278\3\2\2\2\u027b"+
		"\u027c\b\32\1\2\u027c\63\3\2\2\2\u027d\u0282\b\33\1\2\u027e\u027f\7\26"+
		"\2\2\u027f\u0283\b\33\1\2\u0280\u0281\7%\2\2\u0281\u0283\b\33\1\2\u0282"+
		"\u027e\3\2\2\2\u0282\u0280\3\2\2\2\u0283\u0284\3\2\2\2\u0284\u0285\5\66"+
		"\34\2\u0285\u0286\b\33\1\2\u0286\u0289\3\2\2\2\u0287\u0289\5\66\34\2\u0288"+
		"\u027d\3\2\2\2\u0288\u0287\3\2\2\2\u0289\u028a\3\2\2\2\u028a\u028b\b\33"+
		"\1\2\u028b\65\3\2\2\2\u028c\u028d\b\34\1\2\u028d\u028e\5<\37\2\u028e\u028f"+
		"\b\34\1\2\u028f\u029d\3\2\2\2\u0290\u0291\7*\2\2\u0291\u0292\5*\26\2\u0292"+
		"\u0293\7+\2\2\u0293\u0294\b\34\1\2\u0294\u029d\3\2\2\2\u0295\u0296\7V"+
		"\2\2\u0296\u0297\b\34\1\2\u0297\u0298\7*\2\2\u0298\u0299\5\u012c\u0097"+
		"\2\u0299\u029a\7+\2\2\u029a\u029b\b\34\1\2\u029b\u029d\3\2\2\2\u029c\u028c"+
		"\3\2\2\2\u029c\u0290\3\2\2\2\u029c\u0295\3\2\2\2\u029d\67\3\2\2\2\u029e"+
		"\u029f\7\13\2\2\u029f\u02a0\b\35\1\2\u02a0\u02a1\7V\2\2\u02a1\u02a2\7"+
		"\"\2\2\u02a2\u02a3\5*\26\2\u02a3\u02a4\7(\2\2\u02a4\u02a5\b\35\1\2\u02a5"+
		"9\3\2\2\2\u02a6\u02a7\5@!\2\u02a7\u02a8\b\36\1\2\u02a8\u02aa\3\2\2\2\u02a9"+
		"\u02a6\3\2\2\2\u02aa\u02ad\3\2\2\2\u02ab\u02a9\3\2\2\2\u02ab\u02ac\3\2"+
		"\2\2\u02ac;\3\2\2\2\u02ad\u02ab\3\2\2\2\u02ae\u02b5\b\37\1\2\u02af\u02b0"+
		"\5\u0120\u0091\2\u02b0\u02b1\b\37\1\2\u02b1\u02b6\3\2\2\2\u02b2\u02b3"+
		"\5\u012a\u0096\2\u02b3\u02b4\b\37\1\2\u02b4\u02b6\3\2\2\2\u02b5\u02af"+
		"\3\2\2\2\u02b5\u02b2\3\2\2\2\u02b6=\3\2\2\2\u02b7\u02b8\7V\2\2\u02b8\u02b9"+
		"\b \1\2\u02b9?\3\2\2\2\u02ba\u02fa\7\20\2\2\u02bb\u02bc\5B\"\2\u02bc\u02bd"+
		"\b!\1\2\u02bd\u02fb\3\2\2\2\u02be\u02bf\5J&\2\u02bf\u02c0\b!\1\2\u02c0"+
		"\u02fb\3\2\2\2\u02c1\u02c2\5V,\2\u02c2\u02c3\b!\1\2\u02c3\u02fb\3\2\2"+
		"\2\u02c4\u02c5\5b\62\2\u02c5\u02c6\b!\1\2\u02c6\u02fb\3\2\2\2\u02c7\u02c8"+
		"\5h\65\2\u02c8\u02c9\b!\1\2\u02c9\u02fb\3\2\2\2\u02ca\u02cb\5n8\2\u02cb"+
		"\u02cc\b!\1\2\u02cc\u02fb\3\2\2\2\u02cd\u02ce\5t;\2\u02ce\u02cf\b!\1\2"+
		"\u02cf\u02fb\3\2\2\2\u02d0\u02d1\5\u0080A\2\u02d1\u02d2\b!\1\2\u02d2\u02fb"+
		"\3\2\2\2\u02d3\u02d4\5\u008eH\2\u02d4\u02d5\b!\1\2\u02d5\u02fb\3\2\2\2"+
		"\u02d6\u02d7\5\u009cO\2\u02d7\u02d8\b!\1\2\u02d8\u02fb\3\2\2\2\u02d9\u02da"+
		"\5\u00a4S\2\u02da\u02db\b!\1\2\u02db\u02fb\3\2\2\2\u02dc\u02dd\5\u00b0"+
		"Y\2\u02dd\u02de\b!\1\2\u02de\u02fb\3\2\2\2\u02df\u02e0\5\u00bc_\2\u02e0"+
		"\u02e1\b!\1\2\u02e1\u02fb\3\2\2\2\u02e2\u02e3\5\u00c2b\2\u02e3\u02e4\b"+
		"!\1\2\u02e4\u02fb\3\2\2\2\u02e5\u02e6\5\u00c8e\2\u02e6\u02e7\b!\1\2\u02e7"+
		"\u02fb\3\2\2\2\u02e8\u02e9\5\u00ceh\2\u02e9\u02ea\b!\1\2\u02ea\u02fb\3"+
		"\2\2\2\u02eb\u02ec\5\u00dan\2\u02ec\u02ed\b!\1\2\u02ed\u02fb\3\2\2\2\u02ee"+
		"\u02ef\5\u00e8u\2\u02ef\u02f0\b!\1\2\u02f0\u02fb\3\2\2\2\u02f1\u02f2\5"+
		"\u00f6|\2\u02f2\u02f3\b!\1\2\u02f3\u02fb\3\2\2\2\u02f4\u02f5\5\u0102\u0082"+
		"\2\u02f5\u02f6\b!\1\2\u02f6\u02fb\3\2\2\2\u02f7\u02f8\5\u0110\u0089\2"+
		"\u02f8\u02f9\b!\1\2\u02f9\u02fb\3\2\2\2\u02fa\u02bb\3\2\2\2\u02fa\u02be"+
		"\3\2\2\2\u02fa\u02c1\3\2\2\2\u02fa\u02c4\3\2\2\2\u02fa\u02c7\3\2\2\2\u02fa"+
		"\u02ca\3\2\2\2\u02fa\u02cd\3\2\2\2\u02fa\u02d0\3\2\2\2\u02fa\u02d3\3\2"+
		"\2\2\u02fa\u02d6\3\2\2\2\u02fa\u02d9\3\2\2\2\u02fa\u02dc\3\2\2\2\u02fa"+
		"\u02df\3\2\2\2\u02fa\u02e2\3\2\2\2\u02fa\u02e5\3\2\2\2\u02fa\u02e8\3\2"+
		"\2\2\u02fa\u02eb\3\2\2\2\u02fa\u02ee\3\2\2\2\u02fa\u02f1\3\2\2\2\u02fa"+
		"\u02f4\3\2\2\2\u02fa\u02f7\3\2\2\2\u02fb\u02fc\3\2\2\2\u02fc\u02fd\7("+
		"\2\2\u02fdA\3\2\2\2\u02fe\u02ff\7\61\2\2\u02ff\u0300\b\"\1\2\u0300\u0301"+
		"\7V\2\2\u0301\u0302\7\"\2\2\u0302\u0303\b\"\1\2\u0303\u0304\5D#\2\u0304"+
		"\u0305\b\"\1\2\u0305C\3\2\2\2\u0306\u0307\5F$\2\u0307\u0308\b#\1\2\u0308"+
		"E\3\2\2\2\u0309\u031e\5H%\2\u030a\u030b\7#\2\2\u030b\u0317\b$\1\2\u030c"+
		"\u030d\7$\2\2\u030d\u0317\b$\1\2\u030e\u030f\7 \2\2\u030f\u0317\b$\1\2"+
		"\u0310\u0311\7!\2\2\u0311\u0317\b$\1\2\u0312\u0313\7C\2\2\u0313\u0317"+
		"\b$\1\2\u0314\u0315\7\22\2\2\u0315\u0317\b$\1\2\u0316\u030a\3\2\2\2\u0316"+
		"\u030c\3\2\2\2\u0316\u030e\3\2\2\2\u0316\u0310\3\2\2\2\u0316\u0312\3\2"+
		"\2\2\u0316\u0314\3\2\2\2\u0317\u0318\3\2\2\2\u0318\u0319\b$\1\2\u0319"+
		"\u031a\5H%\2\u031a\u031b\b$\1\2\u031b\u031d\3\2\2\2\u031c\u0316\3\2\2"+
		"\2\u031d\u0320\3\2\2\2\u031e\u031c\3\2\2\2\u031e\u031f\3\2\2\2\u031f\u0321"+
		"\3\2\2\2\u0320\u031e\3\2\2\2\u0321\u0322\b$\1\2\u0322G\3\2\2\2\u0323\u0324"+
		"\7*\2\2\u0324\u0325\5F$\2\u0325\u0326\7+\2\2\u0326\u0327\b%\1\2\u0327"+
		"\u0340\3\2\2\2\u0328\u0329\7%\2\2\u0329\u032a\b%\1\2\u032a\u032b\5F$\2"+
		"\u032b\u032c\b%\1\2\u032c\u0340\3\2\2\2\u032d\u032e\7D\2\2\u032e\u032f"+
		"\b%\1\2\u032f\u0330\5F$\2\u0330\u0331\b%\1\2\u0331\u0340\3\2\2\2\u0332"+
		"\u0333\7A\2\2\u0333\u0334\b%\1\2\u0334\u0335\5F$\2\u0335\u0336\b%\1\2"+
		"\u0336\u0340\3\2\2\2\u0337\u0338\7B\2\2\u0338\u0339\b%\1\2\u0339\u033a"+
		"\5F$\2\u033a\u033b\b%\1\2\u033b\u0340\3\2\2\2\u033c\u033d\5.\30\2\u033d"+
		"\u033e\b%\1\2\u033e\u0340\3\2\2\2\u033f\u0323\3\2\2\2\u033f\u0328\3\2"+
		"\2\2\u033f\u032d\3\2\2\2\u033f\u0332\3\2\2\2\u033f\u0337\3\2\2\2\u033f"+
		"\u033c\3\2\2\2\u0340I\3\2\2\2\u0341\u0342\7\62\2\2\u0342\u0343\b&\1\2"+
		"\u0343\u0344\7V\2\2\u0344\u0345\7\"\2\2\u0345\u0346\b&\1\2\u0346\u0347"+
		"\5L\'\2\u0347\u0348\b&\1\2\u0348K\3\2\2\2\u0349\u034a\5N(\2\u034a\u034b"+
		"\b\'\1\2\u034bM\3\2\2\2\u034c\u0361\5P)\2\u034d\u034e\7#\2\2\u034e\u035a"+
		"\b(\1\2\u034f\u0350\7$\2\2\u0350\u035a\b(\1\2\u0351\u0352\7 \2\2\u0352"+
		"\u035a\b(\1\2\u0353\u0354\7!\2\2\u0354\u035a\b(\1\2\u0355\u0356\7C\2\2"+
		"\u0356\u035a\b(\1\2\u0357\u0358\7\22\2\2\u0358\u035a\b(\1\2\u0359\u034d"+
		"\3\2\2\2\u0359\u034f\3\2\2\2\u0359\u0351\3\2\2\2\u0359\u0353\3\2\2\2\u0359"+
		"\u0355\3\2\2\2\u0359\u0357\3\2\2\2\u035a\u035b\3\2\2\2\u035b\u035c\b("+
		"\1\2\u035c\u035d\5P)\2\u035d\u035e\b(\1\2\u035e\u0360\3\2\2\2\u035f\u0359"+
		"\3\2\2\2\u0360\u0363\3\2\2\2\u0361\u035f\3\2\2\2\u0361\u0362\3\2\2\2\u0362"+
		"\u0364\3\2\2\2\u0363\u0361\3\2\2\2\u0364\u0365\b(\1\2\u0365O\3\2\2\2\u0366"+
		"\u0367\7*\2\2\u0367\u0368\5N(\2\u0368\u0369\7+\2\2\u0369\u036a\b)\1\2"+
		"\u036a\u03cf\3\2\2\2\u036b\u036c\7%\2\2\u036c\u036d\b)\1\2\u036d\u036e"+
		"\5N(\2\u036e\u036f\b)\1\2\u036f\u03cf\3\2\2\2\u0370\u0371\7D\2\2\u0371"+
		"\u0372\b)\1\2\u0372\u0373\5N(\2\u0373\u0374\b)\1\2\u0374\u03cf\3\2\2\2"+
		"\u0375\u0376\7A\2\2\u0376\u0377\b)\1\2\u0377\u0378\5N(\2\u0378\u0379\b"+
		")\1\2\u0379\u03cf\3\2\2\2\u037a\u037b\7B\2\2\u037b\u037c\b)\1\2\u037c"+
		"\u037d\5N(\2\u037d\u037e\b)\1\2\u037e\u03cf\3\2\2\2\u037f\u0380\7O\2\2"+
		"\u0380\u0381\b)\1\2\u0381\u0382\7*\2\2\u0382\u0383\5> \2\u0383\u0384\7"+
		"&\2\2\u0384\u0385\5R*\2\u0385\u0386\7+\2\2\u0386\u0387\b)\1\2\u0387\u03cf"+
		"\3\2\2\2\u0388\u0389\7K\2\2\u0389\u038a\b)\1\2\u038a\u038b\7*\2\2\u038b"+
		"\u038c\5> \2\u038c\u038d\7&\2\2\u038d\u038e\5R*\2\u038e\u038f\7+\2\2\u038f"+
		"\u0390\b)\1\2\u0390\u03cf\3\2\2\2\u0391\u0392\7L\2\2\u0392\u0393\b)\1"+
		"\2\u0393\u0394\7*\2\2\u0394\u0395\5> \2\u0395\u0396\7&\2\2\u0396\u0397"+
		"\5R*\2\u0397\u0398\7+\2\2\u0398\u0399\b)\1\2\u0399\u03cf\3\2\2\2\u039a"+
		"\u039b\7M\2\2\u039b\u039c\b)\1\2\u039c\u039d\7*\2\2\u039d\u039e\5> \2"+
		"\u039e\u039f\7&\2\2\u039f\u03a0\5R*\2\u03a0\u03a1\7+\2\2\u03a1\u03a2\b"+
		")\1\2\u03a2\u03cf\3\2\2\2\u03a3\u03a4\7%\2\2\u03a4\u03a5\b)\1\2\u03a5"+
		"\u03a6\7O\2\2\u03a6\u03a7\7*\2\2\u03a7\u03a8\5> \2\u03a8\u03a9\7&\2\2"+
		"\u03a9\u03aa\5R*\2\u03aa\u03ab\7+\2\2\u03ab\u03ac\b)\1\2\u03ac\u03cf\3"+
		"\2\2\2\u03ad\u03ae\7%\2\2\u03ae\u03af\b)\1\2\u03af\u03b0\7K\2\2\u03b0"+
		"\u03b1\7*\2\2\u03b1\u03b2\5> \2\u03b2\u03b3\7&\2\2\u03b3\u03b4\5R*\2\u03b4"+
		"\u03b5\7+\2\2\u03b5\u03b6\b)\1\2\u03b6\u03cf\3\2\2\2\u03b7\u03b8\7%\2"+
		"\2\u03b8\u03b9\b)\1\2\u03b9\u03ba\7L\2\2\u03ba\u03bb\7*\2\2\u03bb\u03bc"+
		"\5> \2\u03bc\u03bd\7&\2\2\u03bd\u03be\5R*\2\u03be\u03bf\7+\2\2\u03bf\u03c0"+
		"\b)\1\2\u03c0\u03cf\3\2\2\2\u03c1\u03c2\7%\2\2\u03c2\u03c3\b)\1\2\u03c3"+
		"\u03c4\7M\2\2\u03c4\u03c5\7*\2\2\u03c5\u03c6\5> \2\u03c6\u03c7\7&\2\2"+
		"\u03c7\u03c8\5R*\2\u03c8\u03c9\7+\2\2\u03c9\u03ca\b)\1\2\u03ca\u03cf\3"+
		"\2\2\2\u03cb\u03cc\5.\30\2\u03cc\u03cd\b)\1\2\u03cd\u03cf\3\2\2\2\u03ce"+
		"\u0366\3\2\2\2\u03ce\u036b\3\2\2\2\u03ce\u0370\3\2\2\2\u03ce\u0375\3\2"+
		"\2\2\u03ce\u037a\3\2\2\2\u03ce\u037f\3\2\2\2\u03ce\u0388\3\2\2\2\u03ce"+
		"\u0391\3\2\2\2\u03ce\u039a\3\2\2\2\u03ce\u03a3\3\2\2\2\u03ce\u03ad\3\2"+
		"\2\2\u03ce\u03b7\3\2\2\2\u03ce\u03c1\3\2\2\2\u03ce\u03cb\3\2\2\2\u03cf"+
		"Q\3\2\2\2\u03d0\u03e5\5P)\2\u03d1\u03d2\7#\2\2\u03d2\u03de\b*\1\2\u03d3"+
		"\u03d4\7$\2\2\u03d4\u03de\b*\1\2\u03d5\u03d6\7 \2\2\u03d6\u03de\b*\1\2"+
		"\u03d7\u03d8\7!\2\2\u03d8\u03de\b*\1\2\u03d9\u03da\7C\2\2\u03da\u03de"+
		"\b*\1\2\u03db\u03dc\7\22\2\2\u03dc\u03de\b*\1\2\u03dd\u03d1\3\2\2\2\u03dd"+
		"\u03d3\3\2\2\2\u03dd\u03d5\3\2\2\2\u03dd\u03d7\3\2\2\2\u03dd\u03d9\3\2"+
		"\2\2\u03dd\u03db\3\2\2\2\u03de\u03df\3\2\2\2\u03df\u03e0\b*\1\2\u03e0"+
		"\u03e1\5P)\2\u03e1\u03e2\b*\1\2\u03e2\u03e4\3\2\2\2\u03e3\u03dd\3\2\2"+
		"\2\u03e4\u03e7\3\2\2\2\u03e5\u03e3\3\2\2\2\u03e5\u03e6\3\2\2\2\u03e6\u03e8"+
		"\3\2\2\2\u03e7\u03e5\3\2\2\2\u03e8\u03e9\b*\1\2\u03e9S\3\2\2\2\u03ea\u03eb"+
		"\7*\2\2\u03eb\u03ec\5N(\2\u03ec\u03ed\7+\2\2\u03ed\u03ee\b+\1\2\u03ee"+
		"\u0453\3\2\2\2\u03ef\u03f0\7%\2\2\u03f0\u03f1\b+\1\2\u03f1\u03f2\5N(\2"+
		"\u03f2\u03f3\b+\1\2\u03f3\u0453\3\2\2\2\u03f4\u03f5\7D\2\2\u03f5\u03f6"+
		"\b+\1\2\u03f6\u03f7\5N(\2\u03f7\u03f8\b+\1\2\u03f8\u0453\3\2\2\2\u03f9"+
		"\u03fa\7A\2\2\u03fa\u03fb\b+\1\2\u03fb\u03fc\5N(\2\u03fc\u03fd\b+\1\2"+
		"\u03fd\u0453\3\2\2\2\u03fe\u03ff\7B\2\2\u03ff\u0400\b+\1\2\u0400\u0401"+
		"\5N(\2\u0401\u0402\b+\1\2\u0402\u0453\3\2\2\2\u0403\u0404\7O\2\2\u0404"+
		"\u0405\b+\1\2\u0405\u0406\7*\2\2\u0406\u0407\5> \2\u0407\u0408\7&\2\2"+
		"\u0408\u0409\5R*\2\u0409\u040a\7+\2\2\u040a\u040b\b+\1\2\u040b\u0453\3"+
		"\2\2\2\u040c\u040d\7K\2\2\u040d\u040e\b+\1\2\u040e\u040f\7*\2\2\u040f"+
		"\u0410\5> \2\u0410\u0411\7&\2\2\u0411\u0412\5R*\2\u0412\u0413\7+\2\2\u0413"+
		"\u0414\b+\1\2\u0414\u0453\3\2\2\2\u0415\u0416\7L\2\2\u0416\u0417\b+\1"+
		"\2\u0417\u0418\7*\2\2\u0418\u0419\5> \2\u0419\u041a\7&\2\2\u041a\u041b"+
		"\5R*\2\u041b\u041c\7+\2\2\u041c\u041d\b+\1\2\u041d\u0453\3\2\2\2\u041e"+
		"\u041f\7M\2\2\u041f\u0420\b+\1\2\u0420\u0421\7*\2\2\u0421\u0422\5> \2"+
		"\u0422\u0423\7&\2\2\u0423\u0424\5R*\2\u0424\u0425\7+\2\2\u0425\u0426\b"+
		"+\1\2\u0426\u0453\3\2\2\2\u0427\u0428\7%\2\2\u0428\u0429\b+\1\2\u0429"+
		"\u042a\7O\2\2\u042a\u042b\7*\2\2\u042b\u042c\5> \2\u042c\u042d\7&\2\2"+
		"\u042d\u042e\5R*\2\u042e\u042f\7+\2\2\u042f\u0430\b+\1\2\u0430\u0453\3"+
		"\2\2\2\u0431\u0432\7%\2\2\u0432\u0433\b+\1\2\u0433\u0434\7K\2\2\u0434"+
		"\u0435\7*\2\2\u0435\u0436\5> \2\u0436\u0437\7&\2\2\u0437\u0438\5R*\2\u0438"+
		"\u0439\7+\2\2\u0439\u043a\b+\1\2\u043a\u0453\3\2\2\2\u043b\u043c\7%\2"+
		"\2\u043c\u043d\b+\1\2\u043d\u043e\7L\2\2\u043e\u043f\7*\2\2\u043f\u0440"+
		"\5> \2\u0440\u0441\7&\2\2\u0441\u0442\5R*\2\u0442\u0443\7+\2\2\u0443\u0444"+
		"\b+\1\2\u0444\u0453\3\2\2\2\u0445\u0446\7%\2\2\u0446\u0447\b+\1\2\u0447"+
		"\u0448\7M\2\2\u0448\u0449\7*\2\2\u0449\u044a\5> \2\u044a\u044b\7&\2\2"+
		"\u044b\u044c\5R*\2\u044c\u044d\7+\2\2\u044d\u044e\b+\1\2\u044e\u0453\3"+
		"\2\2\2\u044f\u0450\5.\30\2\u0450\u0451\b+\1\2\u0451\u0453\3\2\2\2\u0452"+
		"\u03ea\3\2\2\2\u0452\u03ef\3\2\2\2\u0452\u03f4\3\2\2\2\u0452\u03f9\3\2"+
		"\2\2\u0452\u03fe\3\2\2\2\u0452\u0403\3\2\2\2\u0452\u040c\3\2\2\2\u0452"+
		"\u0415\3\2\2\2\u0452\u041e\3\2\2\2\u0452\u0427\3\2\2\2\u0452\u0431\3\2"+
		"\2\2\u0452\u043b\3\2\2\2\u0452\u0445\3\2\2\2\u0452\u044f\3\2\2\2\u0453"+
		"U\3\2\2\2\u0454\u0455\7\63\2\2\u0455\u0456\b,\1\2\u0456\u0457\7V\2\2\u0457"+
		"\u0458\7\"\2\2\u0458\u0459\b,\1\2\u0459\u045a\5X-\2\u045a\u045b\b,\1\2"+
		"\u045bW\3\2\2\2\u045c\u045d\5Z.\2\u045d\u045e\b-\1\2\u045eY\3\2\2\2\u045f"+
		"\u0474\5\\/\2\u0460\u0461\7#\2\2\u0461\u046d\b.\1\2\u0462\u0463\7$\2\2"+
		"\u0463\u046d\b.\1\2\u0464\u0465\7 \2\2\u0465\u046d\b.\1\2\u0466\u0467"+
		"\7!\2\2\u0467\u046d\b.\1\2\u0468\u0469\7C\2\2\u0469\u046d\b.\1\2\u046a"+
		"\u046b\7\22\2\2\u046b\u046d\b.\1\2\u046c\u0460\3\2\2\2\u046c\u0462\3\2"+
		"\2\2\u046c\u0464\3\2\2\2\u046c\u0466\3\2\2\2\u046c\u0468\3\2\2\2\u046c"+
		"\u046a\3\2\2\2\u046d\u046e\3\2\2\2\u046e\u046f\b.\1\2\u046f\u0470\5\\"+
		"/\2\u0470\u0471\b.\1\2\u0471\u0473\3\2\2\2\u0472\u046c\3\2\2\2\u0473\u0476"+
		"\3\2\2\2\u0474\u0472\3\2\2\2\u0474\u0475\3\2\2\2\u0475\u0477\3\2\2\2\u0476"+
		"\u0474\3\2\2\2\u0477\u0478\b.\1\2\u0478[\3\2\2\2\u0479\u047a\7*\2\2\u047a"+
		"\u047b\5Z.\2\u047b\u047c\7+\2\2\u047c\u047d\b/\1\2\u047d\u0508\3\2\2\2"+
		"\u047e\u047f\7%\2\2\u047f\u0480\b/\1\2\u0480\u0481\5Z.\2\u0481\u0482\b"+
		"/\1\2\u0482\u0508\3\2\2\2\u0483\u0484\7D\2\2\u0484\u0485\b/\1\2\u0485"+
		"\u0486\5Z.\2\u0486\u0487\b/\1\2\u0487\u0508\3\2\2\2\u0488\u0489\7A\2\2"+
		"\u0489\u048a\b/\1\2\u048a\u048b\5Z.\2\u048b\u048c\b/\1\2\u048c\u0508\3"+
		"\2\2\2\u048d\u048e\7B\2\2\u048e\u048f\b/\1\2\u048f\u0490\5Z.\2\u0490\u0491"+
		"\b/\1\2\u0491\u0508\3\2\2\2\u0492\u0493\7O\2\2\u0493\u0494\b/\1\2\u0494"+
		"\u0495\7*\2\2\u0495\u0496\5> \2\u0496\u0497\7&\2\2\u0497\u0498\5^\60\2"+
		"\u0498\u0499\7+\2\2\u0499\u049a\b/\1\2\u049a\u0508\3\2\2\2\u049b\u049c"+
		"\7K\2\2\u049c\u049d\b/\1\2\u049d\u049e\7*\2\2\u049e\u049f\5> \2\u049f"+
		"\u04a0\7&\2\2\u04a0\u04a1\5^\60\2\u04a1\u04a2\7+\2\2\u04a2\u04a3\b/\1"+
		"\2\u04a3\u0508\3\2\2\2\u04a4\u04a5\7L\2\2\u04a5\u04a6\b/\1\2\u04a6\u04a7"+
		"\7*\2\2\u04a7\u04a8\5> \2\u04a8\u04a9\7&\2\2\u04a9\u04aa\5^\60\2\u04aa"+
		"\u04ab\7+\2\2\u04ab\u04ac\b/\1\2\u04ac\u0508\3\2\2\2\u04ad\u04ae\7M\2"+
		"\2\u04ae\u04af\b/\1\2\u04af\u04b0\7*\2\2\u04b0\u04b1\5> \2\u04b1\u04b2"+
		"\7&\2\2\u04b2\u04b3\5^\60\2\u04b3\u04b4\7+\2\2\u04b4\u04b5\b/\1\2\u04b5"+
		"\u0508\3\2\2\2\u04b6\u04b7\7%\2\2\u04b7\u04b8\b/\1\2\u04b8\u04b9\7O\2"+
		"\2\u04b9\u04ba\7*\2\2\u04ba\u04bb\5> \2\u04bb\u04bc\7&\2\2\u04bc\u04bd"+
		"\5^\60\2\u04bd\u04be\7+\2\2\u04be\u04bf\b/\1\2\u04bf\u0508\3\2\2\2\u04c0"+
		"\u04c1\7%\2\2\u04c1\u04c2\b/\1\2\u04c2\u04c3\7K\2\2\u04c3\u04c4\7*\2\2"+
		"\u04c4\u04c5\5> \2\u04c5\u04c6\7&\2\2\u04c6\u04c7\5^\60\2\u04c7\u04c8"+
		"\7+\2\2\u04c8\u04c9\b/\1\2\u04c9\u0508\3\2\2\2\u04ca\u04cb\7%\2\2\u04cb"+
		"\u04cc\b/\1\2\u04cc\u04cd\7L\2\2\u04cd\u04ce\7*\2\2\u04ce\u04cf\5> \2"+
		"\u04cf\u04d0\7&\2\2\u04d0\u04d1\5^\60\2\u04d1\u04d2\7+\2\2\u04d2\u04d3"+
		"\b/\1\2\u04d3\u0508\3\2\2\2\u04d4\u04d5\7%\2\2\u04d5\u04d6\b/\1\2\u04d6"+
		"\u04d7\7M\2\2\u04d7\u04d8\7*\2\2\u04d8\u04d9\5> \2\u04d9\u04da\7&\2\2"+
		"\u04da\u04db\5^\60\2\u04db\u04dc\7+\2\2\u04dc\u04dd\b/\1\2\u04dd\u0508"+
		"\3\2\2\2\u04de\u04df\7P\2\2\u04df\u04e0\b/\1\2\u04e0\u04e1\7*\2\2\u04e1"+
		"\u04e2\5> \2\u04e2\u04e3\7&\2\2\u04e3\u04e4\5^\60\2\u04e4\u04e5\7+\2\2"+
		"\u04e5\u04e6\b/\1\2\u04e6\u0508\3\2\2\2\u04e7\u04e8\7N\2\2\u04e8\u04e9"+
		"\b/\1\2\u04e9\u04ea\7*\2\2\u04ea\u04eb\5> \2\u04eb\u04ec\7&\2\2\u04ec"+
		"\u04ed\5^\60\2\u04ed\u04ee\7+\2\2\u04ee\u04ef\b/\1\2\u04ef\u0508\3\2\2"+
		"\2\u04f0\u04f1\7%\2\2\u04f1\u04f2\b/\1\2\u04f2\u04f3\7P\2\2\u04f3\u04f4"+
		"\7*\2\2\u04f4\u04f5\5> \2\u04f5\u04f6\7&\2\2\u04f6\u04f7\5^\60\2\u04f7"+
		"\u04f8\7+\2\2\u04f8\u04f9\b/\1\2\u04f9\u0508\3\2\2\2\u04fa\u04fb\7%\2"+
		"\2\u04fb\u04fc\b/\1\2\u04fc\u04fd\7N\2\2\u04fd\u04fe\7*\2\2\u04fe\u04ff"+
		"\5> \2\u04ff\u0500\7&\2\2\u0500\u0501\5^\60\2\u0501\u0502\7+\2\2\u0502"+
		"\u0503\b/\1\2\u0503\u0508\3\2\2\2\u0504\u0505\5.\30\2\u0505\u0506\b/\1"+
		"\2\u0506\u0508\3\2\2\2\u0507\u0479\3\2\2\2\u0507\u047e\3\2\2\2\u0507\u0483"+
		"\3\2\2\2\u0507\u0488\3\2\2\2\u0507\u048d\3\2\2\2\u0507\u0492\3\2\2\2\u0507"+
		"\u049b\3\2\2\2\u0507\u04a4\3\2\2\2\u0507\u04ad\3\2\2\2\u0507\u04b6\3\2"+
		"\2\2\u0507\u04c0\3\2\2\2\u0507\u04ca\3\2\2\2\u0507\u04d4\3\2\2\2\u0507"+
		"\u04de\3\2\2\2\u0507\u04e7\3\2\2\2\u0507\u04f0\3\2\2\2\u0507\u04fa\3\2"+
		"\2\2\u0507\u0504\3\2\2\2\u0508]\3\2\2\2\u0509\u051e\5\\/\2\u050a\u050b"+
		"\7#\2\2\u050b\u0517\b\60\1\2\u050c\u050d\7$\2\2\u050d\u0517\b\60\1\2\u050e"+
		"\u050f\7 \2\2\u050f\u0517\b\60\1\2\u0510\u0511\7!\2\2\u0511\u0517\b\60"+
		"\1\2\u0512\u0513\7C\2\2\u0513\u0517\b\60\1\2\u0514\u0515\7\22\2\2\u0515"+
		"\u0517\b\60\1\2\u0516\u050a\3\2\2\2\u0516\u050c\3\2\2\2\u0516\u050e\3"+
		"\2\2\2\u0516\u0510\3\2\2\2\u0516\u0512\3\2\2\2\u0516\u0514\3\2\2\2\u0517"+
		"\u0518\3\2\2\2\u0518\u0519\b\60\1\2\u0519\u051a\5\\/\2\u051a\u051b\b\60"+
		"\1\2\u051b\u051d\3\2\2\2\u051c\u0516\3\2\2\2\u051d\u0520\3\2\2\2\u051e"+
		"\u051c\3\2\2\2\u051e\u051f\3\2\2\2\u051f\u0521\3\2\2\2\u0520\u051e\3\2"+
		"\2\2\u0521\u0522\b\60\1\2\u0522_\3\2\2\2\u0523\u0524\7*\2\2\u0524\u0525"+
		"\5Z.\2\u0525\u0526\7+\2\2\u0526\u0527\b\61\1\2\u0527\u05b2\3\2\2\2\u0528"+
		"\u0529\7%\2\2\u0529\u052a\b\61\1\2\u052a\u052b\5Z.\2\u052b\u052c\b\61"+
		"\1\2\u052c\u05b2\3\2\2\2\u052d\u052e\7D\2\2\u052e\u052f\b\61\1\2\u052f"+
		"\u0530\5Z.\2\u0530\u0531\b\61\1\2\u0531\u05b2\3\2\2\2\u0532\u0533\7A\2"+
		"\2\u0533\u0534\b\61\1\2\u0534\u0535\5Z.\2\u0535\u0536\b\61\1\2\u0536\u05b2"+
		"\3\2\2\2\u0537\u0538\7B\2\2\u0538\u0539\b\61\1\2\u0539\u053a\5Z.\2\u053a"+
		"\u053b\b\61\1\2\u053b\u05b2\3\2\2\2\u053c\u053d\7O\2\2\u053d\u053e\b\61"+
		"\1\2\u053e\u053f\7*\2\2\u053f\u0540\5> \2\u0540\u0541\7&\2\2\u0541\u0542"+
		"\5^\60\2\u0542\u0543\7+\2\2\u0543\u0544\b\61\1\2\u0544\u05b2\3\2\2\2\u0545"+
		"\u0546\7K\2\2\u0546\u0547\b\61\1\2\u0547\u0548\7*\2\2\u0548\u0549\5> "+
		"\2\u0549\u054a\7&\2\2\u054a\u054b\5^\60\2\u054b\u054c\7+\2\2\u054c\u054d"+
		"\b\61\1\2\u054d\u05b2\3\2\2\2\u054e\u054f\7L\2\2\u054f\u0550\b\61\1\2"+
		"\u0550\u0551\7*\2\2\u0551\u0552\5> \2\u0552\u0553\7&\2\2\u0553\u0554\5"+
		"^\60\2\u0554\u0555\7+\2\2\u0555\u0556\b\61\1\2\u0556\u05b2\3\2\2\2\u0557"+
		"\u0558\7M\2\2\u0558\u0559\b\61\1\2\u0559\u055a\7*\2\2\u055a\u055b\5> "+
		"\2\u055b\u055c\7&\2\2\u055c\u055d\5^\60\2\u055d\u055e\7+\2\2\u055e\u055f"+
		"\b\61\1\2\u055f\u05b2\3\2\2\2\u0560\u0561\7%\2\2\u0561\u0562\b\61\1\2"+
		"\u0562\u0563\7O\2\2\u0563\u0564\7*\2\2\u0564\u0565\5> \2\u0565\u0566\7"+
		"&\2\2\u0566\u0567\5^\60\2\u0567\u0568\7+\2\2\u0568\u0569\b\61\1\2\u0569"+
		"\u05b2\3\2\2\2\u056a\u056b\7%\2\2\u056b\u056c\b\61\1\2\u056c\u056d\7K"+
		"\2\2\u056d\u056e\7*\2\2\u056e\u056f\5> \2\u056f\u0570\7&\2\2\u0570\u0571"+
		"\5^\60\2\u0571\u0572\7+\2\2\u0572\u0573\b\61\1\2\u0573\u05b2\3\2\2\2\u0574"+
		"\u0575\7%\2\2\u0575\u0576\b\61\1\2\u0576\u0577\7L\2\2\u0577\u0578\7*\2"+
		"\2\u0578\u0579\5> \2\u0579\u057a\7&\2\2\u057a\u057b\5^\60\2\u057b\u057c"+
		"\7+\2\2\u057c\u057d\b\61\1\2\u057d\u05b2\3\2\2\2\u057e\u057f\7%\2\2\u057f"+
		"\u0580\b\61\1\2\u0580\u0581\7M\2\2\u0581\u0582\7*\2\2\u0582\u0583\5> "+
		"\2\u0583\u0584\7&\2\2\u0584\u0585\5^\60\2\u0585\u0586\7+\2\2\u0586\u0587"+
		"\b\61\1\2\u0587\u05b2\3\2\2\2\u0588\u0589\7P\2\2\u0589\u058a\b\61\1\2"+
		"\u058a\u058b\7*\2\2\u058b\u058c\5> \2\u058c\u058d\7&\2\2\u058d\u058e\5"+
		"^\60\2\u058e\u058f\7+\2\2\u058f\u0590\b\61\1\2\u0590\u05b2\3\2\2\2\u0591"+
		"\u0592\7N\2\2\u0592\u0593\b\61\1\2\u0593\u0594\7*\2\2\u0594\u0595\5> "+
		"\2\u0595\u0596\7&\2\2\u0596\u0597\5^\60\2\u0597\u0598\7+\2\2\u0598\u0599"+
		"\b\61\1\2\u0599\u05b2\3\2\2\2\u059a\u059b\7%\2\2\u059b\u059c\b\61\1\2"+
		"\u059c\u059d\7P\2\2\u059d\u059e\7*\2\2\u059e\u059f\5> \2\u059f\u05a0\7"+
		"&\2\2\u05a0\u05a1\5^\60\2\u05a1\u05a2\7+\2\2\u05a2\u05a3\b\61\1\2\u05a3"+
		"\u05b2\3\2\2\2\u05a4\u05a5\7%\2\2\u05a5\u05a6\b\61\1\2\u05a6\u05a7\7N"+
		"\2\2\u05a7\u05a8\7*\2\2\u05a8\u05a9\5> \2\u05a9\u05aa\7&\2\2\u05aa\u05ab"+
		"\5^\60\2\u05ab\u05ac\7+\2\2\u05ac\u05ad\b\61\1\2\u05ad\u05b2\3\2\2\2\u05ae"+
		"\u05af\5.\30\2\u05af\u05b0\b\61\1\2\u05b0\u05b2\3\2\2\2\u05b1\u0523\3"+
		"\2\2\2\u05b1\u0528\3\2\2\2\u05b1\u052d\3\2\2\2\u05b1\u0532\3\2\2\2\u05b1"+
		"\u0537\3\2\2\2\u05b1\u053c\3\2\2\2\u05b1\u0545\3\2\2\2\u05b1\u054e\3\2"+
		"\2\2\u05b1\u0557\3\2\2\2\u05b1\u0560\3\2\2\2\u05b1\u056a\3\2\2\2\u05b1"+
		"\u0574\3\2\2\2\u05b1\u057e\3\2\2\2\u05b1\u0588\3\2\2\2\u05b1\u0591\3\2"+
		"\2\2\u05b1\u059a\3\2\2\2\u05b1\u05a4\3\2\2\2\u05b1\u05ae\3\2\2\2\u05b2"+
		"a\3\2\2\2\u05b3\u05b4\7:\2\2\u05b4\u05b5\b\62\1\2\u05b5\u05b6\7V\2\2\u05b6"+
		"\u05b7\7\"\2\2\u05b7\u05b8\b\62\1\2\u05b8\u05b9\5d\63\2\u05b9\u05ba\b"+
		"\62\1\2\u05bac\3\2\2\2\u05bb\u05cc\5f\64\2\u05bc\u05bd\7#\2\2\u05bd\u05c5"+
		"\b\63\1\2\u05be\u05bf\7$\2\2\u05bf\u05c5\b\63\1\2\u05c0\u05c1\7 \2\2\u05c1"+
		"\u05c5\b\63\1\2\u05c2\u05c3\7!\2\2\u05c3\u05c5\b\63\1\2\u05c4\u05bc\3"+
		"\2\2\2\u05c4\u05be\3\2\2\2\u05c4\u05c0\3\2\2\2\u05c4\u05c2\3\2\2\2\u05c5"+
		"\u05c6\3\2\2\2\u05c6\u05c7\b\63\1\2\u05c7\u05c8\5f\64\2\u05c8\u05c9\b"+
		"\63\1\2\u05c9\u05cb\3\2\2\2\u05ca\u05c4\3\2\2\2\u05cb\u05ce\3\2\2\2\u05cc"+
		"\u05ca\3\2\2\2\u05cc\u05cd\3\2\2\2\u05cd\u05cf\3\2\2\2\u05ce\u05cc\3\2"+
		"\2\2\u05cf\u05d0\b\63\1\2\u05d0e\3\2\2\2\u05d1\u05d2\7*\2\2\u05d2\u05d3"+
		"\5d\63\2\u05d3\u05d4\7+\2\2\u05d4\u05d5\b\64\1\2\u05d5\u060b\3\2\2\2\u05d6"+
		"\u05d7\7%\2\2\u05d7\u05d8\b\64\1\2\u05d8\u05d9\5d\63\2\u05d9\u05da\b\64"+
		"\1\2\u05da\u060b\3\2\2\2\u05db\u05dc\7?\2\2\u05dc\u05dd\b\64\1\2\u05dd"+
		"\u05de\7D\2\2\u05de\u05df\b\64\1\2\u05df\u05e0\3\2\2\2\u05e0\u05e1\5d"+
		"\63\2\u05e1\u05e2\b\64\1\2\u05e2\u060b\3\2\2\2\u05e3\u05e4\7?\2\2\u05e4"+
		"\u05e5\b\64\1\2\u05e5\u05e6\7A\2\2\u05e6\u05e7\b\64\1\2\u05e7\u05e8\3"+
		"\2\2\2\u05e8\u05e9\5d\63\2\u05e9\u05ea\b\64\1\2\u05ea\u060b\3\2\2\2\u05eb"+
		"\u05ec\7?\2\2\u05ec\u05ed\b\64\1\2\u05ed\u05ee\7B\2\2\u05ee\u05ef\b\64"+
		"\1\2\u05ef\u05f0\3\2\2\2\u05f0\u05f1\5d\63\2\u05f1\u05f2\b\64\1\2\u05f2"+
		"\u060b\3\2\2\2\u05f3\u05f4\7?\2\2\u05f4\u05f5\b\64\1\2\u05f5\u05f6\7*"+
		"\2\2\u05f6\u05f7\5d\63\2\u05f7\u05f8\7C\2\2\u05f8\u05f9\b\64\1\2\u05f9"+
		"\u05fa\5d\63\2\u05fa\u05fb\7+\2\2\u05fb\u05fc\b\64\1\2\u05fc\u060b\3\2"+
		"\2\2\u05fd\u05fe\7?\2\2\u05fe\u05ff\b\64\1\2\u05ff\u0600\7*\2\2\u0600"+
		"\u0601\5d\63\2\u0601\u0602\7\22\2\2\u0602\u0603\b\64\1\2\u0603\u0604\5"+
		"d\63\2\u0604\u0605\7+\2\2\u0605\u0606\b\64\1\2\u0606\u060b\3\2\2\2\u0607"+
		"\u0608\5.\30\2\u0608\u0609\b\64\1\2\u0609\u060b\3\2\2\2\u060a\u05d1\3"+
		"\2\2\2\u060a\u05d6\3\2\2\2\u060a\u05db\3\2\2\2\u060a\u05e3\3\2\2\2\u060a"+
		"\u05eb\3\2\2\2\u060a\u05f3\3\2\2\2\u060a\u05fd\3\2\2\2\u060a\u0607\3\2"+
		"\2\2\u060bg\3\2\2\2\u060c\u060d\7;\2\2\u060d\u060e\b\65\1\2\u060e\u060f"+
		"\7V\2\2\u060f\u0610\7\"\2\2\u0610\u0611\b\65\1\2\u0611\u0612\5j\66\2\u0612"+
		"\u0613\b\65\1\2\u0613i\3\2\2\2\u0614\u0625\5l\67\2\u0615\u0616\7#\2\2"+
		"\u0616\u061e\b\66\1\2\u0617\u0618\7$\2\2\u0618\u061e\b\66\1\2\u0619\u061a"+
		"\7 \2\2\u061a\u061e\b\66\1\2\u061b\u061c\7!\2\2\u061c\u061e\b\66\1\2\u061d"+
		"\u0615\3\2\2\2\u061d\u0617\3\2\2\2\u061d\u0619\3\2\2\2\u061d\u061b\3\2"+
		"\2\2\u061e\u061f\3\2\2\2\u061f\u0620\b\66\1\2\u0620\u0621\5l\67\2\u0621"+
		"\u0622\b\66\1\2\u0622\u0624\3\2\2\2\u0623\u061d\3\2\2\2\u0624\u0627\3"+
		"\2\2\2\u0625\u0623\3\2\2\2\u0625\u0626\3\2\2\2\u0626\u0628\3\2\2\2\u0627"+
		"\u0625\3\2\2\2\u0628\u0629\b\66\1\2\u0629k\3\2\2\2\u062a\u062b\7*\2\2"+
		"\u062b\u062c\5j\66\2\u062c\u062d\7+\2\2\u062d\u062e\b\67\1\2\u062e\u0688"+
		"\3\2\2\2\u062f\u0630\7%\2\2\u0630\u0631\b\67\1\2\u0631\u0632\5j\66\2\u0632"+
		"\u0633\b\67\1\2\u0633\u0688\3\2\2\2\u0634\u0635\7?\2\2\u0635\u0636\b\67"+
		"\1\2\u0636\u0637\7D\2\2\u0637\u0638\b\67\1\2\u0638\u0639\3\2\2\2\u0639"+
		"\u063a\5j\66\2\u063a\u063b\b\67\1\2\u063b\u0688\3\2\2\2\u063c\u063d\7"+
		"?\2\2\u063d\u063e\b\67\1\2\u063e\u063f\7A\2\2\u063f\u0640\b\67\1\2\u0640"+
		"\u0641\3\2\2\2\u0641\u0642\5j\66\2\u0642\u0643\b\67\1\2\u0643\u0688\3"+
		"\2\2\2\u0644\u0645\7?\2\2\u0645\u0646\b\67\1\2\u0646\u0647\7B\2\2\u0647"+
		"\u0648\b\67\1\2\u0648\u0649\3\2\2\2\u0649\u064a\5j\66\2\u064a\u064b\b"+
		"\67\1\2\u064b\u0688\3\2\2\2\u064c\u064d\7?\2\2\u064d\u064e\b\67\1\2\u064e"+
		"\u064f\7*\2\2\u064f\u0650\5j\66\2\u0650\u0651\7C\2\2\u0651\u0652\b\67"+
		"\1\2\u0652\u0653\5j\66\2\u0653\u0654\7+\2\2\u0654\u0655\b\67\1\2\u0655"+
		"\u0688\3\2\2\2\u0656\u0657\7?\2\2\u0657\u0658\b\67\1\2\u0658\u0659\7*"+
		"\2\2\u0659\u065a\5j\66\2\u065a\u065b\7\22\2\2\u065b\u065c\b\67\1\2\u065c"+
		"\u065d\5j\66\2\u065d\u065e\7+\2\2\u065e\u065f\b\67\1\2\u065f\u0688\3\2"+
		"\2\2\u0660\u0661\7O\2\2\u0661\u0662\b\67\1\2\u0662\u0663\7*\2\2\u0663"+
		"\u0664\5> \2\u0664\u0665\7&\2\2\u0665\u0666\5j\66\2\u0666\u0667\7+\2\2"+
		"\u0667\u0668\b\67\1\2\u0668\u0688\3\2\2\2\u0669\u066a\7K\2\2\u066a\u066b"+
		"\b\67\1\2\u066b\u066c\7*\2\2\u066c\u066d\5> \2\u066d\u066e\7&\2\2\u066e"+
		"\u066f\5j\66\2\u066f\u0670\7+\2\2\u0670\u0671\b\67\1\2\u0671\u0688\3\2"+
		"\2\2\u0672\u0673\7L\2\2\u0673\u0674\b\67\1\2\u0674\u0675\7*\2\2\u0675"+
		"\u0676\5> \2\u0676\u0677\7&\2\2\u0677\u0678\5j\66\2\u0678\u0679\7+\2\2"+
		"\u0679\u067a\b\67\1\2\u067a\u0688\3\2\2\2\u067b\u067c\7M\2\2\u067c\u067d"+
		"\b\67\1\2\u067d\u067e\7*\2\2\u067e\u067f\5> \2\u067f\u0680\7&\2\2\u0680"+
		"\u0681\5j\66\2\u0681\u0682\7+\2\2\u0682\u0683\b\67\1\2\u0683\u0688\3\2"+
		"\2\2\u0684\u0685\5.\30\2\u0685\u0686\b\67\1\2\u0686\u0688\3\2\2\2\u0687"+
		"\u062a\3\2\2\2\u0687\u062f\3\2\2\2\u0687\u0634\3\2\2\2\u0687\u063c\3\2"+
		"\2\2\u0687\u0644\3\2\2\2\u0687\u064c\3\2\2\2\u0687\u0656\3\2\2\2\u0687"+
		"\u0660\3\2\2\2\u0687\u0669\3\2\2\2\u0687\u0672\3\2\2\2\u0687\u067b\3\2"+
		"\2\2\u0687\u0684\3\2\2\2\u0688m\3\2\2\2\u0689\u068a\7<\2\2\u068a\u068b"+
		"\b8\1\2\u068b\u068c\7V\2\2\u068c\u068d\7\"\2\2\u068d\u068e\b8\1\2\u068e"+
		"\u068f\5p9\2\u068f\u0690\b8\1\2\u0690o\3\2\2\2\u0691\u06a2\5r:\2\u0692"+
		"\u0693\7#\2\2\u0693\u069b\b9\1\2\u0694\u0695\7$\2\2\u0695\u069b\b9\1\2"+
		"\u0696\u0697\7 \2\2\u0697\u069b\b9\1\2\u0698\u0699\7!\2\2\u0699\u069b"+
		"\b9\1\2\u069a\u0692\3\2\2\2\u069a\u0694\3\2\2\2\u069a\u0696\3\2\2\2\u069a"+
		"\u0698\3\2\2\2\u069b\u069c\3\2\2\2\u069c\u069d\b9\1\2\u069d\u069e\5r:"+
		"\2\u069e\u069f\b9\1\2\u069f\u06a1\3\2\2\2\u06a0\u069a\3\2\2\2\u06a1\u06a4"+
		"\3\2\2\2\u06a2\u06a0\3\2\2\2\u06a2\u06a3\3\2\2\2\u06a3\u06a5\3\2\2\2\u06a4"+
		"\u06a2\3\2\2\2\u06a5\u06a6\b9\1\2\u06a6q\3\2\2\2\u06a7\u06a8\7*\2\2\u06a8"+
		"\u06a9\5p9\2\u06a9\u06aa\7+\2\2\u06aa\u06ab\b:\1\2\u06ab\u0717\3\2\2\2"+
		"\u06ac\u06ad\7%\2\2\u06ad\u06ae\b:\1\2\u06ae\u06af\5p9\2\u06af\u06b0\b"+
		":\1\2\u06b0\u0717\3\2\2\2\u06b1\u06b2\7?\2\2\u06b2\u06b3\b:\1\2\u06b3"+
		"\u06b4\7D\2\2\u06b4\u06b5\b:\1\2\u06b5\u06b6\3\2\2\2\u06b6\u06b7\5p9\2"+
		"\u06b7\u06b8\b:\1\2\u06b8\u0717\3\2\2\2\u06b9\u06ba\7?\2\2\u06ba\u06bb"+
		"\b:\1\2\u06bb\u06bc\7A\2\2\u06bc\u06bd\b:\1\2\u06bd\u06be\3\2\2\2\u06be"+
		"\u06bf\5p9\2\u06bf\u06c0\b:\1\2\u06c0\u0717\3\2\2\2\u06c1\u06c2\7?\2\2"+
		"\u06c2\u06c3\b:\1\2\u06c3\u06c4\7B\2\2\u06c4\u06c5\b:\1\2\u06c5\u06c6"+
		"\3\2\2\2\u06c6\u06c7\5p9\2\u06c7\u06c8\b:\1\2\u06c8\u0717\3\2\2\2\u06c9"+
		"\u06ca\7?\2\2\u06ca\u06cb\b:\1\2\u06cb\u06cc\7*\2\2\u06cc\u06cd\5p9\2"+
		"\u06cd\u06ce\7C\2\2\u06ce\u06cf\b:\1\2\u06cf\u06d0\5p9\2\u06d0\u06d1\7"+
		"+\2\2\u06d1\u06d2\b:\1\2\u06d2\u0717\3\2\2\2\u06d3\u06d4\7?\2\2\u06d4"+
		"\u06d5\b:\1\2\u06d5\u06d6\7*\2\2\u06d6\u06d7\5p9\2\u06d7\u06d8\7\22\2"+
		"\2\u06d8\u06d9\b:\1\2\u06d9\u06da\5p9\2\u06da\u06db\7+\2\2\u06db\u06dc"+
		"\b:\1\2\u06dc\u0717\3\2\2\2\u06dd\u06de\7O\2\2\u06de\u06df\b:\1\2\u06df"+
		"\u06e0\7*\2\2\u06e0\u06e1\5> \2\u06e1\u06e2\7&\2\2\u06e2\u06e3\5p9\2\u06e3"+
		"\u06e4\7+\2\2\u06e4\u06e5\b:\1\2\u06e5\u0717\3\2\2\2\u06e6\u06e7\7K\2"+
		"\2\u06e7\u06e8\b:\1\2\u06e8\u06e9\7*\2\2\u06e9\u06ea\5> \2\u06ea\u06eb"+
		"\7&\2\2\u06eb\u06ec\5p9\2\u06ec\u06ed\7+\2\2\u06ed\u06ee\b:\1\2\u06ee"+
		"\u0717\3\2\2\2\u06ef\u06f0\7L\2\2\u06f0\u06f1\b:\1\2\u06f1\u06f2\7*\2"+
		"\2\u06f2\u06f3\5> \2\u06f3\u06f4\7&\2\2\u06f4\u06f5\5p9\2\u06f5\u06f6"+
		"\7+\2\2\u06f6\u06f7\b:\1\2\u06f7\u0717\3\2\2\2\u06f8\u06f9\7M\2\2\u06f9"+
		"\u06fa\b:\1\2\u06fa\u06fb\7*\2\2\u06fb\u06fc\5> \2\u06fc\u06fd\7&\2\2"+
		"\u06fd\u06fe\5p9\2\u06fe\u06ff\7+\2\2\u06ff\u0700\b:\1\2\u0700\u0717\3"+
		"\2\2\2\u0701\u0702\7P\2\2\u0702\u0703\b:\1\2\u0703\u0704\7*\2\2\u0704"+
		"\u0705\5> \2\u0705\u0706\7&\2\2\u0706\u0707\5p9\2\u0707\u0708\7+\2\2\u0708"+
		"\u0709\b:\1\2\u0709\u0717\3\2\2\2\u070a\u070b\7N\2\2\u070b\u070c\b:\1"+
		"\2\u070c\u070d\7*\2\2\u070d\u070e\5> \2\u070e\u070f\7&\2\2\u070f\u0710"+
		"\5p9\2\u0710\u0711\7+\2\2\u0711\u0712\b:\1\2\u0712\u0717\3\2\2\2\u0713"+
		"\u0714\5.\30\2\u0714\u0715\b:\1\2\u0715\u0717\3\2\2\2\u0716\u06a7\3\2"+
		"\2\2\u0716\u06ac\3\2\2\2\u0716\u06b1\3\2\2\2\u0716\u06b9\3\2\2\2\u0716"+
		"\u06c1\3\2\2\2\u0716\u06c9\3\2\2\2\u0716\u06d3\3\2\2\2\u0716\u06dd\3\2"+
		"\2\2\u0716\u06e6\3\2\2\2\u0716\u06ef\3\2\2\2\u0716\u06f8\3\2\2\2\u0716"+
		"\u0701\3\2\2\2\u0716\u070a\3\2\2\2\u0716\u0713\3\2\2\2\u0717s\3\2\2\2"+
		"\u0718\u0719\7:\2\2\u0719\u071a\b;\1\2\u071a\u071b\7\27\2\2\u071b\u071c"+
		"\7V\2\2\u071c\u071d\7\"\2\2\u071d\u071e\b;\1\2\u071e\u071f\5v<\2\u071f"+
		"\u0720\b;\1\2\u0720u\3\2\2\2\u0721\u0732\5x=\2\u0722\u0723\7#\2\2\u0723"+
		"\u072b\b<\1\2\u0724\u0725\7$\2\2\u0725\u072b\b<\1\2\u0726\u0727\7 \2\2"+
		"\u0727\u072b\b<\1\2\u0728\u0729\7!\2\2\u0729\u072b\b<\1\2\u072a\u0722"+
		"\3\2\2\2\u072a\u0724\3\2\2\2\u072a\u0726\3\2\2\2\u072a\u0728\3\2\2\2\u072b"+
		"\u072c\3\2\2\2\u072c\u072d\b<\1\2\u072d\u072e\5x=\2\u072e\u072f\b<\1\2"+
		"\u072f\u0731\3\2\2\2\u0730\u072a\3\2\2\2\u0731\u0734\3\2\2\2\u0732\u0730"+
		"\3\2\2\2\u0732\u0733\3\2\2\2\u0733\u0735\3\2\2\2\u0734\u0732\3\2\2\2\u0735"+
		"\u0736\b<\1\2\u0736w\3\2\2\2\u0737\u0738\7*\2\2\u0738\u0739\5v<\2\u0739"+
		"\u073a\7+\2\2\u073a\u073b\b=\1\2\u073b\u074a\3\2\2\2\u073c\u073d\7%\2"+
		"\2\u073d\u073e\b=\1\2\u073e\u073f\5v<\2\u073f\u0740\b=\1\2\u0740\u074a"+
		"\3\2\2\2\u0741\u0742\7?\2\2\u0742\u0743\b=\1\2\u0743\u0744\5z>\2\u0744"+
		"\u0745\b=\1\2\u0745\u074a\3\2\2\2\u0746\u0747\5.\30\2\u0747\u0748\b=\1"+
		"\2\u0748\u074a\3\2\2\2\u0749\u0737\3\2\2\2\u0749\u073c\3\2\2\2\u0749\u0741"+
		"\3\2\2\2\u0749\u0746\3\2\2\2\u074ay\3\2\2\2\u074b\u074c\5x=\2\u074c\u074d"+
		"\b>\1\2\u074d\u0752\3\2\2\2\u074e\u074f\5~@\2\u074f\u0750\b>\1\2\u0750"+
		"\u0752\3\2\2\2\u0751\u074b\3\2\2\2\u0751\u074e\3\2\2\2\u0752{\3\2\2\2"+
		"\u0753\u0764\5~@\2\u0754\u0755\7#\2\2\u0755\u075d\b?\1\2\u0756\u0757\7"+
		"$\2\2\u0757\u075d\b?\1\2\u0758\u0759\7 \2\2\u0759\u075d\b?\1\2\u075a\u075b"+
		"\7!\2\2\u075b\u075d\b?\1\2\u075c\u0754\3\2\2\2\u075c\u0756\3\2\2\2\u075c"+
		"\u0758\3\2\2\2\u075c\u075a\3\2\2\2\u075d\u075e\3\2\2\2\u075e\u075f\b?"+
		"\1\2\u075f\u0760\5~@\2\u0760\u0761\b?\1\2\u0761\u0763\3\2\2\2\u0762\u075c"+
		"\3\2\2\2\u0763\u0766\3\2\2\2\u0764\u0762\3\2\2\2\u0764\u0765\3\2\2\2\u0765"+
		"\u0767\3\2\2\2\u0766\u0764\3\2\2\2\u0767\u0768\b?\1\2\u0768\u077c\3\2"+
		"\2\2\u0769\u0776\5z>\2\u076a\u076b\7C\2\2\u076b\u076f\b?\1\2\u076c\u076d"+
		"\7\22\2\2\u076d\u076f\b?\1\2\u076e\u076a\3\2\2\2\u076e\u076c\3\2\2\2\u076f"+
		"\u0770\3\2\2\2\u0770\u0771\b?\1\2\u0771\u0772\5z>\2\u0772\u0773\b?\1\2"+
		"\u0773\u0775\3\2\2\2\u0774\u076e\3\2\2\2\u0775\u0778\3\2\2\2\u0776\u0774"+
		"\3\2\2\2\u0776\u0777\3\2\2\2\u0777\u0779\3\2\2\2\u0778\u0776\3\2\2\2\u0779"+
		"\u077a\b?\1\2\u077a\u077c\3\2\2\2\u077b\u0753\3\2\2\2\u077b\u0769\3\2"+
		"\2\2\u077c}\3\2\2\2\u077d\u077e\7*\2\2\u077e\u077f\5|?\2\u077f\u0780\7"+
		"+\2\2\u0780\u0781\b@\1\2\u0781\u0792\3\2\2\2\u0782\u0783\7D\2\2\u0783"+
		"\u0784\b@\1\2\u0784\u0785\5z>\2\u0785\u0786\b@\1\2\u0786\u0792\3\2\2\2"+
		"\u0787\u0788\7A\2\2\u0788\u0789\b@\1\2\u0789\u078a\5z>\2\u078a\u078b\b"+
		"@\1\2\u078b\u0792\3\2\2\2\u078c\u078d\7B\2\2\u078d\u078e\b@\1\2\u078e"+
		"\u078f\5z>\2\u078f\u0790\b@\1\2\u0790\u0792\3\2\2\2\u0791\u077d\3\2\2"+
		"\2\u0791\u0782\3\2\2\2\u0791\u0787\3\2\2\2\u0791\u078c\3\2\2\2\u0792\177"+
		"\3\2\2\2\u0793\u0794\7:\2\2\u0794\u0795\bA\1\2\u0795\u0796\7\27\2\2\u0796"+
		"\u0797\7O\2\2\u0797\u0798\7V\2\2\u0798\u0799\7\"\2\2\u0799\u079a\bA\1"+
		"\2\u079a\u079b\5\u0082B\2\u079b\u079c\bA\1\2\u079c\u0081\3\2\2\2\u079d"+
		"\u07ae\5\u0084C\2\u079e\u079f\7#\2\2\u079f\u07a7\bB\1\2\u07a0\u07a1\7"+
		"$\2\2\u07a1\u07a7\bB\1\2\u07a2\u07a3\7 \2\2\u07a3\u07a7\bB\1\2\u07a4\u07a5"+
		"\7!\2\2\u07a5\u07a7\bB\1\2\u07a6\u079e\3\2\2\2\u07a6\u07a0\3\2\2\2\u07a6"+
		"\u07a2\3\2\2\2\u07a6\u07a4\3\2\2\2\u07a7\u07a8\3\2\2\2\u07a8\u07a9\bB"+
		"\1\2\u07a9\u07aa\5\u0084C\2\u07aa\u07ab\bB\1\2\u07ab\u07ad\3\2\2\2\u07ac"+
		"\u07a6\3\2\2\2\u07ad\u07b0\3\2\2\2\u07ae\u07ac\3\2\2\2\u07ae\u07af\3\2"+
		"\2\2\u07af\u07b1\3\2\2\2\u07b0\u07ae\3\2\2\2\u07b1\u07b2\bB\1\2\u07b2"+
		"\u0083\3\2\2\2\u07b3\u07b4\7*\2\2\u07b4\u07b5\5\u0082B\2\u07b5\u07b6\7"+
		"+\2\2\u07b6\u07b7\bC\1\2\u07b7\u07ea\3\2\2\2\u07b8\u07b9\7%\2\2\u07b9"+
		"\u07ba\bC\1\2\u07ba\u07bb\5\u0082B\2\u07bb\u07bc\bC\1\2\u07bc\u07ea\3"+
		"\2\2\2\u07bd\u07be\7?\2\2\u07be\u07bf\bC\1\2\u07bf\u07c0\5\u0086D\2\u07c0"+
		"\u07c1\bC\1\2\u07c1\u07ea\3\2\2\2\u07c2\u07c3\7O\2\2\u07c3\u07c4\bC\1"+
		"\2\u07c4\u07c5\7*\2\2\u07c5\u07c6\5> \2\u07c6\u07c7\7&\2\2\u07c7\u07c8"+
		"\5\u0088E\2\u07c8\u07c9\7+\2\2\u07c9\u07ca\bC\1\2\u07ca\u07ea\3\2\2\2"+
		"\u07cb\u07cc\7K\2\2\u07cc\u07cd\bC\1\2\u07cd\u07ce\7*\2\2\u07ce\u07cf"+
		"\5> \2\u07cf\u07d0\7&\2\2\u07d0\u07d1\5\u0088E\2\u07d1\u07d2\7+\2\2\u07d2"+
		"\u07d3\bC\1\2\u07d3\u07ea\3\2\2\2\u07d4\u07d5\7L\2\2\u07d5\u07d6\bC\1"+
		"\2\u07d6\u07d7\7*\2\2\u07d7\u07d8\5> \2\u07d8\u07d9\7&\2\2\u07d9\u07da"+
		"\5\u0088E\2\u07da\u07db\7+\2\2\u07db\u07dc\bC\1\2\u07dc\u07ea\3\2\2\2"+
		"\u07dd\u07de\7M\2\2\u07de\u07df\bC\1\2\u07df\u07e0\7*\2\2\u07e0\u07e1"+
		"\5> \2\u07e1\u07e2\7&\2\2\u07e2\u07e3\5\u0088E\2\u07e3\u07e4\7+\2\2\u07e4"+
		"\u07e5\bC\1\2\u07e5\u07ea\3\2\2\2\u07e6\u07e7\5.\30\2\u07e7\u07e8\bC\1"+
		"\2\u07e8\u07ea\3\2\2\2\u07e9\u07b3\3\2\2\2\u07e9\u07b8\3\2\2\2\u07e9\u07bd"+
		"\3\2\2\2\u07e9\u07c2\3\2\2\2\u07e9\u07cb\3\2\2\2\u07e9\u07d4\3\2\2\2\u07e9"+
		"\u07dd\3\2\2\2\u07e9\u07e6\3\2\2\2\u07ea\u0085\3\2\2\2\u07eb\u07ec\5\u0084"+
		"C\2\u07ec\u07ed\bD\1\2\u07ed\u07f2\3\2\2\2\u07ee\u07ef\5\u008cG\2\u07ef"+
		"\u07f0\bD\1\2\u07f0\u07f2\3\2\2\2\u07f1\u07eb\3\2\2\2\u07f1\u07ee\3\2"+
		"\2\2\u07f2\u0087\3\2\2\2\u07f3\u07f4\5\u0082B\2\u07f4\u07f5\bE\1\2\u07f5"+
		"\u07fa\3\2\2\2\u07f6\u07f7\5\u008aF\2\u07f7\u07f8\bE\1\2\u07f8\u07fa\3";
	private static final String _serializedATNSegment1 =
		"\2\2\2\u07f9\u07f3\3\2\2\2\u07f9\u07f6\3\2\2\2\u07fa\u0089\3\2\2\2\u07fb"+
		"\u080c\5\u008cG\2\u07fc\u07fd\7#\2\2\u07fd\u0805\bF\1\2\u07fe\u07ff\7"+
		"$\2\2\u07ff\u0805\bF\1\2\u0800\u0801\7 \2\2\u0801\u0805\bF\1\2\u0802\u0803"+
		"\7!\2\2\u0803\u0805\bF\1\2\u0804\u07fc\3\2\2\2\u0804\u07fe\3\2\2\2\u0804"+
		"\u0800\3\2\2\2\u0804\u0802\3\2\2\2\u0805\u0806\3\2\2\2\u0806\u0807\bF"+
		"\1\2\u0807\u0808\5\u008cG\2\u0808\u0809\bF\1\2\u0809\u080b\3\2\2\2\u080a"+
		"\u0804\3\2\2\2\u080b\u080e\3\2\2\2\u080c\u080a\3\2\2\2\u080c\u080d\3\2"+
		"\2\2\u080d\u080f\3\2\2\2\u080e\u080c\3\2\2\2\u080f\u0810\bF\1\2\u0810"+
		"\u0824\3\2\2\2\u0811\u081e\5\u0086D\2\u0812\u0813\7C\2\2\u0813\u0817\b"+
		"F\1\2\u0814\u0815\7\22\2\2\u0815\u0817\bF\1\2\u0816\u0812\3\2\2\2\u0816"+
		"\u0814\3\2\2\2\u0817\u0818\3\2\2\2\u0818\u0819\bF\1\2\u0819\u081a\5\u0086"+
		"D\2\u081a\u081b\bF\1\2\u081b\u081d\3\2\2\2\u081c\u0816\3\2\2\2\u081d\u0820"+
		"\3\2\2\2\u081e\u081c\3\2\2\2\u081e\u081f\3\2\2\2\u081f\u0821\3\2\2\2\u0820"+
		"\u081e\3\2\2\2\u0821\u0822\bF\1\2\u0822\u0824\3\2\2\2\u0823\u07fb\3\2"+
		"\2\2\u0823\u0811\3\2\2\2\u0824\u008b\3\2\2\2\u0825\u0826\7*\2\2\u0826"+
		"\u0827\5\u008aF\2\u0827\u0828\7+\2\2\u0828\u0829\bG\1\2\u0829\u083a\3"+
		"\2\2\2\u082a\u082b\7D\2\2\u082b\u082c\bG\1\2\u082c\u082d\5\u0086D\2\u082d"+
		"\u082e\bG\1\2\u082e\u083a\3\2\2\2\u082f\u0830\7A\2\2\u0830\u0831\bG\1"+
		"\2\u0831\u0832\5\u0086D\2\u0832\u0833\bG\1\2\u0833\u083a\3\2\2\2\u0834"+
		"\u0835\7B\2\2\u0835\u0836\bG\1\2\u0836\u0837\5\u0086D\2\u0837\u0838\b"+
		"G\1\2\u0838\u083a\3\2\2\2\u0839\u0825\3\2\2\2\u0839\u082a\3\2\2\2\u0839"+
		"\u082f\3\2\2\2\u0839\u0834\3\2\2\2\u083a\u008d\3\2\2\2\u083b\u083c\7:"+
		"\2\2\u083c\u083d\bH\1\2\u083d\u083e\7\27\2\2\u083e\u083f\7>\2\2\u083f"+
		"\u0840\7V\2\2\u0840\u0841\7\"\2\2\u0841\u0842\bH\1\2\u0842\u0843\5\u0090"+
		"I\2\u0843\u0844\bH\1\2\u0844\u008f\3\2\2\2\u0845\u0856\5\u0092J\2\u0846"+
		"\u0847\7#\2\2\u0847\u084f\bI\1\2\u0848\u0849\7$\2\2\u0849\u084f\bI\1\2"+
		"\u084a\u084b\7 \2\2\u084b\u084f\bI\1\2\u084c\u084d\7!\2\2\u084d\u084f"+
		"\bI\1\2\u084e\u0846\3\2\2\2\u084e\u0848\3\2\2\2\u084e\u084a\3\2\2\2\u084e"+
		"\u084c\3\2\2\2\u084f\u0850\3\2\2\2\u0850\u0851\bI\1\2\u0851\u0852\5\u0092"+
		"J\2\u0852\u0853\bI\1\2\u0853\u0855\3\2\2\2\u0854\u084e\3\2\2\2\u0855\u0858"+
		"\3\2\2\2\u0856\u0854\3\2\2\2\u0856\u0857\3\2\2\2\u0857\u0859\3\2\2\2\u0858"+
		"\u0856\3\2\2\2\u0859\u085a\bI\1\2\u085a\u0091\3\2\2\2\u085b\u085c\7*\2"+
		"\2\u085c\u085d\5\u0090I\2\u085d\u085e\7+\2\2\u085e\u085f\bJ\1\2\u085f"+
		"\u08a4\3\2\2\2\u0860\u0861\7%\2\2\u0861\u0862\bJ\1\2\u0862\u0863\5\u0090"+
		"I\2\u0863\u0864\bJ\1\2\u0864\u08a4\3\2\2\2\u0865\u0866\7?\2\2\u0866\u0867"+
		"\bJ\1\2\u0867\u0868\5\u0094K\2\u0868\u0869\bJ\1\2\u0869\u08a4\3\2\2\2"+
		"\u086a\u086b\7O\2\2\u086b\u086c\bJ\1\2\u086c\u086d\7*\2\2\u086d\u086e"+
		"\5> \2\u086e\u086f\7&\2\2\u086f\u0870\5\u0096L\2\u0870\u0871\7+\2\2\u0871"+
		"\u0872\bJ\1\2\u0872\u08a4\3\2\2\2\u0873\u0874\7K\2\2\u0874\u0875\bJ\1"+
		"\2\u0875\u0876\7*\2\2\u0876\u0877\5> \2\u0877\u0878\7&\2\2\u0878\u0879"+
		"\5\u0096L\2\u0879\u087a\7+\2\2\u087a\u087b\bJ\1\2\u087b\u08a4\3\2\2\2"+
		"\u087c\u087d\7L\2\2\u087d\u087e\bJ\1\2\u087e\u087f\7*\2\2\u087f\u0880"+
		"\5> \2\u0880\u0881\7&\2\2\u0881\u0882\5\u0096L\2\u0882\u0883\7+\2\2\u0883"+
		"\u0884\bJ\1\2\u0884\u08a4\3\2\2\2\u0885\u0886\7M\2\2\u0886\u0887\bJ\1"+
		"\2\u0887\u0888\7*\2\2\u0888\u0889\5> \2\u0889\u088a\7&\2\2\u088a\u088b"+
		"\5\u0096L\2\u088b\u088c\7+\2\2\u088c\u088d\bJ\1\2\u088d\u08a4\3\2\2\2"+
		"\u088e\u088f\7P\2\2\u088f\u0890\bJ\1\2\u0890\u0891\7*\2\2\u0891\u0892"+
		"\5> \2\u0892\u0893\7&\2\2\u0893\u0894\5\u0096L\2\u0894\u0895\7+\2\2\u0895"+
		"\u0896\bJ\1\2\u0896\u08a4\3\2\2\2\u0897\u0898\7N\2\2\u0898\u0899\bJ\1"+
		"\2\u0899\u089a\7*\2\2\u089a\u089b\5> \2\u089b\u089c\7&\2\2\u089c\u089d"+
		"\5\u0096L\2\u089d\u089e\7+\2\2\u089e\u089f\bJ\1\2\u089f\u08a4\3\2\2\2"+
		"\u08a0\u08a1\5.\30\2\u08a1\u08a2\bJ\1\2\u08a2\u08a4\3\2\2\2\u08a3\u085b"+
		"\3\2\2\2\u08a3\u0860\3\2\2\2\u08a3\u0865\3\2\2\2\u08a3\u086a\3\2\2\2\u08a3"+
		"\u0873\3\2\2\2\u08a3\u087c\3\2\2\2\u08a3\u0885\3\2\2\2\u08a3\u088e\3\2"+
		"\2\2\u08a3\u0897\3\2\2\2\u08a3\u08a0\3\2\2\2\u08a4\u0093\3\2\2\2\u08a5"+
		"\u08a6\5\u0092J\2\u08a6\u08a7\bK\1\2\u08a7\u08ac\3\2\2\2\u08a8\u08a9\5"+
		"\u009aN\2\u08a9\u08aa\bK\1\2\u08aa\u08ac\3\2\2\2\u08ab\u08a5\3\2\2\2\u08ab"+
		"\u08a8\3\2\2\2\u08ac\u0095\3\2\2\2\u08ad\u08ae\5\u0090I\2\u08ae\u08af"+
		"\bL\1\2\u08af\u08b4\3\2\2\2\u08b0\u08b1\5\u0098M\2\u08b1\u08b2\bL\1\2"+
		"\u08b2\u08b4\3\2\2\2\u08b3\u08ad\3\2\2\2\u08b3\u08b0\3\2\2\2\u08b4\u0097"+
		"\3\2\2\2\u08b5\u08c6\5\u009aN\2\u08b6\u08b7\7#\2\2\u08b7\u08bf\bM\1\2"+
		"\u08b8\u08b9\7$\2\2\u08b9\u08bf\bM\1\2\u08ba\u08bb\7 \2\2\u08bb\u08bf"+
		"\bM\1\2\u08bc\u08bd\7!\2\2\u08bd\u08bf\bM\1\2\u08be\u08b6\3\2\2\2\u08be"+
		"\u08b8\3\2\2\2\u08be\u08ba\3\2\2\2\u08be\u08bc\3\2\2\2\u08bf\u08c0\3\2"+
		"\2\2\u08c0\u08c1\bM\1\2\u08c1\u08c2\5\u009aN\2\u08c2\u08c3\bM\1\2\u08c3"+
		"\u08c5\3\2\2\2\u08c4\u08be\3\2\2\2\u08c5\u08c8\3\2\2\2\u08c6\u08c4\3\2"+
		"\2\2\u08c6\u08c7\3\2\2\2\u08c7\u08c9\3\2\2\2\u08c8\u08c6\3\2\2\2\u08c9"+
		"\u08ca\bM\1\2\u08ca\u08de\3\2\2\2\u08cb\u08d8\5\u0094K\2\u08cc\u08cd\7"+
		"C\2\2\u08cd\u08d1\bM\1\2\u08ce\u08cf\7\22\2\2\u08cf\u08d1\bM\1\2\u08d0"+
		"\u08cc\3\2\2\2\u08d0\u08ce\3\2\2\2\u08d1\u08d2\3\2\2\2\u08d2\u08d3\bM"+
		"\1\2\u08d3\u08d4\5\u0094K\2\u08d4\u08d5\bM\1\2\u08d5\u08d7\3\2\2\2\u08d6"+
		"\u08d0\3\2\2\2\u08d7\u08da\3\2\2\2\u08d8\u08d6\3\2\2\2\u08d8\u08d9\3\2"+
		"\2\2\u08d9\u08db\3\2\2\2\u08da\u08d8\3\2\2\2\u08db\u08dc\bM\1\2\u08dc"+
		"\u08de\3\2\2\2\u08dd\u08b5\3\2\2\2\u08dd\u08cb\3\2\2\2\u08de\u0099\3\2"+
		"\2\2\u08df\u08e0\7*\2\2\u08e0\u08e1\5\u0098M\2\u08e1\u08e2\7+\2\2\u08e2"+
		"\u08e3\bN\1\2\u08e3\u08f4\3\2\2\2\u08e4\u08e5\7D\2\2\u08e5\u08e6\bN\1"+
		"\2\u08e6\u08e7\5\u0094K\2\u08e7\u08e8\bN\1\2\u08e8\u08f4\3\2\2\2\u08e9"+
		"\u08ea\7A\2\2\u08ea\u08eb\bN\1\2\u08eb\u08ec\5\u0094K\2\u08ec\u08ed\b"+
		"N\1\2\u08ed\u08f4\3\2\2\2\u08ee\u08ef\7B\2\2\u08ef\u08f0\bN\1\2\u08f0"+
		"\u08f1\5\u0094K\2\u08f1\u08f2\bN\1\2\u08f2\u08f4\3\2\2\2\u08f3\u08df\3"+
		"\2\2\2\u08f3\u08e4\3\2\2\2\u08f3\u08e9\3\2\2\2\u08f3\u08ee\3\2\2\2\u08f4"+
		"\u009b\3\2\2\2\u08f5\u08f6\7\64\2\2\u08f6\u08f7\bO\1\2\u08f7\u08f8\7V"+
		"\2\2\u08f8\u08f9\7\"\2\2\u08f9\u08fa\bO\1\2\u08fa\u08fb\5\u009eP\2\u08fb"+
		"\u08fc\bO\1\2\u08fc\u009d\3\2\2\2\u08fd\u08fe\5\u00a0Q\2\u08fe\u08ff\b"+
		"P\1\2\u08ff\u009f\3\2\2\2\u0900\u0915\5\u00a2R\2\u0901\u0902\7#\2\2\u0902"+
		"\u090e\bQ\1\2\u0903\u0904\7$\2\2\u0904\u090e\bQ\1\2\u0905\u0906\7 \2\2"+
		"\u0906\u090e\bQ\1\2\u0907\u0908\7!\2\2\u0908\u090e\bQ\1\2\u0909\u090a"+
		"\7C\2\2\u090a\u090e\bQ\1\2\u090b\u090c\7\22\2\2\u090c\u090e\bQ\1\2\u090d"+
		"\u0901\3\2\2\2\u090d\u0903\3\2\2\2\u090d\u0905\3\2\2\2\u090d\u0907\3\2"+
		"\2\2\u090d\u0909\3\2\2\2\u090d\u090b\3\2\2\2\u090e\u090f\3\2\2\2\u090f"+
		"\u0910\bQ\1\2\u0910\u0911\5\u00a2R\2\u0911\u0912\bQ\1\2\u0912\u0914\3"+
		"\2\2\2\u0913\u090d\3\2\2\2\u0914\u0917\3\2\2\2\u0915\u0913\3\2\2\2\u0915"+
		"\u0916\3\2\2\2\u0916\u0918\3\2\2\2\u0917\u0915\3\2\2\2\u0918\u0919\bQ"+
		"\1\2\u0919\u00a1\3\2\2\2\u091a\u091b\7*\2\2\u091b\u091c\5\u00a0Q\2\u091c"+
		"\u091d\7+\2\2\u091d\u091e\bR\1\2\u091e\u0937\3\2\2\2\u091f\u0920\7%\2"+
		"\2\u0920\u0921\bR\1\2\u0921\u0922\5\u00a0Q\2\u0922\u0923\bR\1\2\u0923"+
		"\u0937\3\2\2\2\u0924\u0925\7D\2\2\u0925\u0926\bR\1\2\u0926\u0927\5\u00a0"+
		"Q\2\u0927\u0928\bR\1\2\u0928\u0937\3\2\2\2\u0929\u092a\7A\2\2\u092a\u092b"+
		"\bR\1\2\u092b\u092c\5\u00a0Q\2\u092c\u092d\bR\1\2\u092d\u0937\3\2\2\2"+
		"\u092e\u092f\7B\2\2\u092f\u0930\bR\1\2\u0930\u0931\5\u00a0Q\2\u0931\u0932"+
		"\bR\1\2\u0932\u0937\3\2\2\2\u0933\u0934\5.\30\2\u0934\u0935\bR\1\2\u0935"+
		"\u0937\3\2\2\2\u0936\u091a\3\2\2\2\u0936\u091f\3\2\2\2\u0936\u0924\3\2"+
		"\2\2\u0936\u0929\3\2\2\2\u0936\u092e\3\2\2\2\u0936\u0933\3\2\2\2\u0937"+
		"\u00a3\3\2\2\2\u0938\u0939\7\65\2\2\u0939\u093a\bS\1\2\u093a\u093b\7V"+
		"\2\2\u093b\u093c\7\"\2\2\u093c\u093d\bS\1\2\u093d\u093e\5\u00a6T\2\u093e"+
		"\u093f\bS\1\2\u093f\u00a5\3\2\2\2\u0940\u0941\5\u00a8U\2\u0941\u0942\b"+
		"T\1\2\u0942\u00a7\3\2\2\2\u0943\u0958\5\u00aaV\2\u0944\u0945\7#\2\2\u0945"+
		"\u0951\bU\1\2\u0946\u0947\7$\2\2\u0947\u0951\bU\1\2\u0948\u0949\7 \2\2"+
		"\u0949\u0951\bU\1\2\u094a\u094b\7!\2\2\u094b\u0951\bU\1\2\u094c\u094d"+
		"\7C\2\2\u094d\u0951\bU\1\2\u094e\u094f\7\22\2\2\u094f\u0951\bU\1\2\u0950"+
		"\u0944\3\2\2\2\u0950\u0946\3\2\2\2\u0950\u0948\3\2\2\2\u0950\u094a\3\2"+
		"\2\2\u0950\u094c\3\2\2\2\u0950\u094e\3\2\2\2\u0951\u0952\3\2\2\2\u0952"+
		"\u0953\bU\1\2\u0953\u0954\5\u00aaV\2\u0954\u0955\bU\1\2\u0955\u0957\3"+
		"\2\2\2\u0956\u0950\3\2\2\2\u0957\u095a\3\2\2\2\u0958\u0956\3\2\2\2\u0958"+
		"\u0959\3\2\2\2\u0959\u095b\3\2\2\2\u095a\u0958\3\2\2\2\u095b\u095c\bU"+
		"\1\2\u095c\u00a9\3\2\2\2\u095d\u095e\7*\2\2\u095e\u095f\5\u00a8U\2\u095f"+
		"\u0960\7+\2\2\u0960\u0961\bV\1\2\u0961\u09a2\3\2\2\2\u0962\u0963\7%\2"+
		"\2\u0963\u0964\bV\1\2\u0964\u0965\5\u00a8U\2\u0965\u0966\bV\1\2\u0966"+
		"\u09a2\3\2\2\2\u0967\u0968\7D\2\2\u0968\u0969\bV\1\2\u0969\u096a\5\u00a8"+
		"U\2\u096a\u096b\bV\1\2\u096b\u09a2\3\2\2\2\u096c\u096d\7A\2\2\u096d\u096e"+
		"\bV\1\2\u096e\u096f\5\u00a8U\2\u096f\u0970\bV\1\2\u0970\u09a2\3\2\2\2"+
		"\u0971\u0972\7B\2\2\u0972\u0973\bV\1\2\u0973\u0974\5\u00a8U\2\u0974\u0975"+
		"\bV\1\2\u0975\u09a2\3\2\2\2\u0976\u0977\7%\2\2\u0977\u0978\bV\1\2\u0978"+
		"\u0979\7O\2\2\u0979\u097a\7*\2\2\u097a\u097b\5> \2\u097b\u097c\7&\2\2"+
		"\u097c\u097d\5\u00acW\2\u097d\u097e\7+\2\2\u097e\u097f\bV\1\2\u097f\u09a2"+
		"\3\2\2\2\u0980\u0981\7%\2\2\u0981\u0982\bV\1\2\u0982\u0983\7K\2\2\u0983"+
		"\u0984\7*\2\2\u0984\u0985\5> \2\u0985\u0986\7&\2\2\u0986\u0987\5\u00ac"+
		"W\2\u0987\u0988\7+\2\2\u0988\u0989\bV\1\2\u0989\u09a2\3\2\2\2\u098a\u098b"+
		"\7%\2\2\u098b\u098c\bV\1\2\u098c\u098d\7L\2\2\u098d\u098e\7*\2\2\u098e"+
		"\u098f\5> \2\u098f\u0990\7&\2\2\u0990\u0991\5\u00acW\2\u0991\u0992\7+"+
		"\2\2\u0992\u0993\bV\1\2\u0993\u09a2\3\2\2\2\u0994\u0995\7%\2\2\u0995\u0996"+
		"\bV\1\2\u0996\u0997\7M\2\2\u0997\u0998\7*\2\2\u0998\u0999\5> \2\u0999"+
		"\u099a\7&\2\2\u099a\u099b\5\u00acW\2\u099b\u099c\7+\2\2\u099c\u099d\b"+
		"V\1\2\u099d\u09a2\3\2\2\2\u099e\u099f\5.\30\2\u099f\u09a0\bV\1\2\u09a0"+
		"\u09a2\3\2\2\2\u09a1\u095d\3\2\2\2\u09a1\u0962\3\2\2\2\u09a1\u0967\3\2"+
		"\2\2\u09a1\u096c\3\2\2\2\u09a1\u0971\3\2\2\2\u09a1\u0976\3\2\2\2\u09a1"+
		"\u0980\3\2\2\2\u09a1\u098a\3\2\2\2\u09a1\u0994\3\2\2\2\u09a1\u099e\3\2"+
		"\2\2\u09a2\u00ab\3\2\2\2\u09a3\u09b8\5\u00aaV\2\u09a4\u09a5\7#\2\2\u09a5"+
		"\u09b1\bW\1\2\u09a6\u09a7\7$\2\2\u09a7\u09b1\bW\1\2\u09a8\u09a9\7 \2\2"+
		"\u09a9\u09b1\bW\1\2\u09aa\u09ab\7!\2\2\u09ab\u09b1\bW\1\2\u09ac\u09ad"+
		"\7C\2\2\u09ad\u09b1\bW\1\2\u09ae\u09af\7\22\2\2\u09af\u09b1\bW\1\2\u09b0"+
		"\u09a4\3\2\2\2\u09b0\u09a6\3\2\2\2\u09b0\u09a8\3\2\2\2\u09b0\u09aa\3\2"+
		"\2\2\u09b0\u09ac\3\2\2\2\u09b0\u09ae\3\2\2\2\u09b1\u09b2\3\2\2\2\u09b2"+
		"\u09b3\bW\1\2\u09b3\u09b4\5\u00aaV\2\u09b4\u09b5\bW\1\2\u09b5\u09b7\3"+
		"\2\2\2\u09b6\u09b0\3\2\2\2\u09b7\u09ba\3\2\2\2\u09b8\u09b6\3\2\2\2\u09b8"+
		"\u09b9\3\2\2\2\u09b9\u09bb\3\2\2\2\u09ba\u09b8\3\2\2\2\u09bb\u09bc\bW"+
		"\1\2\u09bc\u00ad\3\2\2\2\u09bd\u09be\7*\2\2\u09be\u09bf\5\u00a8U\2\u09bf"+
		"\u09c0\7+\2\2\u09c0\u09c1\bX\1\2\u09c1\u0a02\3\2\2\2\u09c2\u09c3\7%\2"+
		"\2\u09c3\u09c4\bX\1\2\u09c4\u09c5\5\u00a8U\2\u09c5\u09c6\bX\1\2\u09c6"+
		"\u0a02\3\2\2\2\u09c7\u09c8\7D\2\2\u09c8\u09c9\bX\1\2\u09c9\u09ca\5\u00a8"+
		"U\2\u09ca\u09cb\bX\1\2\u09cb\u0a02\3\2\2\2\u09cc\u09cd\7A\2\2\u09cd\u09ce"+
		"\bX\1\2\u09ce\u09cf\5\u00a8U\2\u09cf\u09d0\bX\1\2\u09d0\u0a02\3\2\2\2"+
		"\u09d1\u09d2\7B\2\2\u09d2\u09d3\bX\1\2\u09d3\u09d4\5\u00a8U\2\u09d4\u09d5"+
		"\bX\1\2\u09d5\u0a02\3\2\2\2\u09d6\u09d7\7%\2\2\u09d7\u09d8\bX\1\2\u09d8"+
		"\u09d9\7O\2\2\u09d9\u09da\7*\2\2\u09da\u09db\5> \2\u09db\u09dc\7&\2\2"+
		"\u09dc\u09dd\5\u00acW\2\u09dd\u09de\7+\2\2\u09de\u09df\bX\1\2\u09df\u0a02"+
		"\3\2\2\2\u09e0\u09e1\7%\2\2\u09e1\u09e2\bX\1\2\u09e2\u09e3\7K\2\2\u09e3"+
		"\u09e4\7*\2\2\u09e4\u09e5\5> \2\u09e5\u09e6\7&\2\2\u09e6\u09e7\5\u00ac"+
		"W\2\u09e7\u09e8\7+\2\2\u09e8\u09e9\bX\1\2\u09e9\u0a02\3\2\2\2\u09ea\u09eb"+
		"\7%\2\2\u09eb\u09ec\bX\1\2\u09ec\u09ed\7L\2\2\u09ed\u09ee\7*\2\2\u09ee"+
		"\u09ef\5> \2\u09ef\u09f0\7&\2\2\u09f0\u09f1\5\u00acW\2\u09f1\u09f2\7+"+
		"\2\2\u09f2\u09f3\bX\1\2\u09f3\u0a02\3\2\2\2\u09f4\u09f5\7%\2\2\u09f5\u09f6"+
		"\bX\1\2\u09f6\u09f7\7M\2\2\u09f7\u09f8\7*\2\2\u09f8\u09f9\5> \2\u09f9"+
		"\u09fa\7&\2\2\u09fa\u09fb\5\u00acW\2\u09fb\u09fc\7+\2\2\u09fc\u09fd\b"+
		"X\1\2\u09fd\u0a02\3\2\2\2\u09fe\u09ff\5.\30\2\u09ff\u0a00\bX\1\2\u0a00"+
		"\u0a02\3\2\2\2\u0a01\u09bd\3\2\2\2\u0a01\u09c2\3\2\2\2\u0a01\u09c7\3\2"+
		"\2\2\u0a01\u09cc\3\2\2\2\u0a01\u09d1\3\2\2\2\u0a01\u09d6\3\2\2\2\u0a01"+
		"\u09e0\3\2\2\2\u0a01\u09ea\3\2\2\2\u0a01\u09f4\3\2\2\2\u0a01\u09fe\3\2"+
		"\2\2\u0a02\u00af\3\2\2\2\u0a03\u0a04\7\66\2\2\u0a04\u0a05\bY\1\2\u0a05"+
		"\u0a06\7V\2\2\u0a06\u0a07\7\"\2\2\u0a07\u0a08\bY\1\2\u0a08\u0a09\5\u00b2"+
		"Z\2\u0a09\u0a0a\bY\1\2\u0a0a\u00b1\3\2\2\2\u0a0b\u0a0c\5\u00b4[\2\u0a0c"+
		"\u0a0d\bZ\1\2\u0a0d\u00b3\3\2\2\2\u0a0e\u0a23\5\u00b6\\\2\u0a0f\u0a10"+
		"\7#\2\2\u0a10\u0a1c\b[\1\2\u0a11\u0a12\7$\2\2\u0a12\u0a1c\b[\1\2\u0a13"+
		"\u0a14\7 \2\2\u0a14\u0a1c\b[\1\2\u0a15\u0a16\7!\2\2\u0a16\u0a1c\b[\1\2"+
		"\u0a17\u0a18\7C\2\2\u0a18\u0a1c\b[\1\2\u0a19\u0a1a\7\22\2\2\u0a1a\u0a1c"+
		"\b[\1\2\u0a1b\u0a0f\3\2\2\2\u0a1b\u0a11\3\2\2\2\u0a1b\u0a13\3\2\2\2\u0a1b"+
		"\u0a15\3\2\2\2\u0a1b\u0a17\3\2\2\2\u0a1b\u0a19\3\2\2\2\u0a1c\u0a1d\3\2"+
		"\2\2\u0a1d\u0a1e\b[\1\2\u0a1e\u0a1f\5\u00b6\\\2\u0a1f\u0a20\b[\1\2\u0a20"+
		"\u0a22\3\2\2\2\u0a21\u0a1b\3\2\2\2\u0a22\u0a25\3\2\2\2\u0a23\u0a21\3\2"+
		"\2\2\u0a23\u0a24\3\2\2\2\u0a24\u0a26\3\2\2\2\u0a25\u0a23\3\2\2\2\u0a26"+
		"\u0a27\b[\1\2\u0a27\u00b5\3\2\2\2\u0a28\u0a29\7*\2\2\u0a29\u0a2a\5\u00b4"+
		"[\2\u0a2a\u0a2b\7+\2\2\u0a2b\u0a2c\b\\\1\2\u0a2c\u0a81\3\2\2\2\u0a2d\u0a2e"+
		"\7%\2\2\u0a2e\u0a2f\b\\\1\2\u0a2f\u0a30\5\u00b4[\2\u0a30\u0a31\b\\\1\2"+
		"\u0a31\u0a81\3\2\2\2\u0a32\u0a33\7D\2\2\u0a33\u0a34\b\\\1\2\u0a34\u0a35"+
		"\5\u00b4[\2\u0a35\u0a36\b\\\1\2\u0a36\u0a81\3\2\2\2\u0a37\u0a38\7A\2\2"+
		"\u0a38\u0a39\b\\\1\2\u0a39\u0a3a\5\u00b4[\2\u0a3a\u0a3b\b\\\1\2\u0a3b"+
		"\u0a81\3\2\2\2\u0a3c\u0a3d\7B\2\2\u0a3d\u0a3e\b\\\1\2\u0a3e\u0a3f\5\u00b4"+
		"[\2\u0a3f\u0a40\b\\\1\2\u0a40\u0a81\3\2\2\2\u0a41\u0a42\7%\2\2\u0a42\u0a43"+
		"\b\\\1\2\u0a43\u0a44\7O\2\2\u0a44\u0a45\7*\2\2\u0a45\u0a46\5> \2\u0a46"+
		"\u0a47\7&\2\2\u0a47\u0a48\5\u00b8]\2\u0a48\u0a49\7+\2\2\u0a49\u0a4a\b"+
		"\\\1\2\u0a4a\u0a81\3\2\2\2\u0a4b\u0a4c\7%\2\2\u0a4c\u0a4d\b\\\1\2\u0a4d"+
		"\u0a4e\7K\2\2\u0a4e\u0a4f\7*\2\2\u0a4f\u0a50\5> \2\u0a50\u0a51\7&\2\2"+
		"\u0a51\u0a52\5\u00b8]\2\u0a52\u0a53\7+\2\2\u0a53\u0a54\b\\\1\2\u0a54\u0a81"+
		"\3\2\2\2\u0a55\u0a56\7%\2\2\u0a56\u0a57\b\\\1\2\u0a57\u0a58\7L\2\2\u0a58"+
		"\u0a59\7*\2\2\u0a59\u0a5a\5> \2\u0a5a\u0a5b\7&\2\2\u0a5b\u0a5c\5\u00b8"+
		"]\2\u0a5c\u0a5d\7+\2\2\u0a5d\u0a5e\b\\\1\2\u0a5e\u0a81\3\2\2\2\u0a5f\u0a60"+
		"\7%\2\2\u0a60\u0a61\b\\\1\2\u0a61\u0a62\7M\2\2\u0a62\u0a63\7*\2\2\u0a63"+
		"\u0a64\5> \2\u0a64\u0a65\7&\2\2\u0a65\u0a66\5\u00b8]\2\u0a66\u0a67\7+"+
		"\2\2\u0a67\u0a68\b\\\1\2\u0a68\u0a81\3\2\2\2\u0a69\u0a6a\7%\2\2\u0a6a"+
		"\u0a6b\b\\\1\2\u0a6b\u0a6c\7P\2\2\u0a6c\u0a6d\7*\2\2\u0a6d\u0a6e\5> \2"+
		"\u0a6e\u0a6f\7&\2\2\u0a6f\u0a70\5\u00b8]\2\u0a70\u0a71\7+\2\2\u0a71\u0a72"+
		"\b\\\1\2\u0a72\u0a81\3\2\2\2\u0a73\u0a74\7%\2\2\u0a74\u0a75\b\\\1\2\u0a75"+
		"\u0a76\7N\2\2\u0a76\u0a77\7*\2\2\u0a77\u0a78\5> \2\u0a78\u0a79\7&\2\2"+
		"\u0a79\u0a7a\5\u00b8]\2\u0a7a\u0a7b\7+\2\2\u0a7b\u0a7c\b\\\1\2\u0a7c\u0a81"+
		"\3\2\2\2\u0a7d\u0a7e\5.\30\2\u0a7e\u0a7f\b\\\1\2\u0a7f\u0a81\3\2\2\2\u0a80"+
		"\u0a28\3\2\2\2\u0a80\u0a2d\3\2\2\2\u0a80\u0a32\3\2\2\2\u0a80\u0a37\3\2"+
		"\2\2\u0a80\u0a3c\3\2\2\2\u0a80\u0a41\3\2\2\2\u0a80\u0a4b\3\2\2\2\u0a80"+
		"\u0a55\3\2\2\2\u0a80\u0a5f\3\2\2\2\u0a80\u0a69\3\2\2\2\u0a80\u0a73\3\2"+
		"\2\2\u0a80\u0a7d\3\2\2\2\u0a81\u00b7\3\2\2\2\u0a82\u0a97\5\u00b6\\\2\u0a83"+
		"\u0a84\7#\2\2\u0a84\u0a90\b]\1\2\u0a85\u0a86\7$\2\2\u0a86\u0a90\b]\1\2"+
		"\u0a87\u0a88\7 \2\2\u0a88\u0a90\b]\1\2\u0a89\u0a8a\7!\2\2\u0a8a\u0a90"+
		"\b]\1\2\u0a8b\u0a8c\7C\2\2\u0a8c\u0a90\b]\1\2\u0a8d\u0a8e\7\22\2\2\u0a8e"+
		"\u0a90\b]\1\2\u0a8f\u0a83\3\2\2\2\u0a8f\u0a85\3\2\2\2\u0a8f\u0a87\3\2"+
		"\2\2\u0a8f\u0a89\3\2\2\2\u0a8f\u0a8b\3\2\2\2\u0a8f\u0a8d\3\2\2\2\u0a90"+
		"\u0a91\3\2\2\2\u0a91\u0a92\b]\1\2\u0a92\u0a93\5\u00b6\\\2\u0a93\u0a94"+
		"\b]\1\2\u0a94\u0a96\3\2\2\2\u0a95\u0a8f\3\2\2\2\u0a96\u0a99\3\2\2\2\u0a97"+
		"\u0a95\3\2\2\2\u0a97\u0a98\3\2\2\2\u0a98\u0a9a\3\2\2\2\u0a99\u0a97\3\2"+
		"\2\2\u0a9a\u0a9b\b]\1\2\u0a9b\u00b9\3\2\2\2\u0a9c\u0a9d\7*\2\2\u0a9d\u0a9e"+
		"\5\u00b4[\2\u0a9e\u0a9f\7+\2\2\u0a9f\u0aa0\b^\1\2\u0aa0\u0af5\3\2\2\2"+
		"\u0aa1\u0aa2\7%\2\2\u0aa2\u0aa3\b^\1\2\u0aa3\u0aa4\5\u00b4[\2\u0aa4\u0aa5"+
		"\b^\1\2\u0aa5\u0af5\3\2\2\2\u0aa6\u0aa7\7D\2\2\u0aa7\u0aa8\b^\1\2\u0aa8"+
		"\u0aa9\5\u00b4[\2\u0aa9\u0aaa\b^\1\2\u0aaa\u0af5\3\2\2\2\u0aab\u0aac\7"+
		"A\2\2\u0aac\u0aad\b^\1\2\u0aad\u0aae\5\u00b4[\2\u0aae\u0aaf\b^\1\2\u0aaf"+
		"\u0af5\3\2\2\2\u0ab0\u0ab1\7B\2\2\u0ab1\u0ab2\b^\1\2\u0ab2\u0ab3\5\u00b4"+
		"[\2\u0ab3\u0ab4\b^\1\2\u0ab4\u0af5\3\2\2\2\u0ab5\u0ab6\7%\2\2\u0ab6\u0ab7"+
		"\b^\1\2\u0ab7\u0ab8\7O\2\2\u0ab8\u0ab9\7*\2\2\u0ab9\u0aba\5> \2\u0aba"+
		"\u0abb\7&\2\2\u0abb\u0abc\5\u00b8]\2\u0abc\u0abd\7+\2\2\u0abd\u0abe\b"+
		"^\1\2\u0abe\u0af5\3\2\2\2\u0abf\u0ac0\7%\2\2\u0ac0\u0ac1\b^\1\2\u0ac1"+
		"\u0ac2\7K\2\2\u0ac2\u0ac3\7*\2\2\u0ac3\u0ac4\5> \2\u0ac4\u0ac5\7&\2\2"+
		"\u0ac5\u0ac6\5\u00b8]\2\u0ac6\u0ac7\7+\2\2\u0ac7\u0ac8\b^\1\2\u0ac8\u0af5"+
		"\3\2\2\2\u0ac9\u0aca\7%\2\2\u0aca\u0acb\b^\1\2\u0acb\u0acc\7L\2\2\u0acc"+
		"\u0acd\7*\2\2\u0acd\u0ace\5> \2\u0ace\u0acf\7&\2\2\u0acf\u0ad0\5\u00b8"+
		"]\2\u0ad0\u0ad1\7+\2\2\u0ad1\u0ad2\b^\1\2\u0ad2\u0af5\3\2\2\2\u0ad3\u0ad4"+
		"\7%\2\2\u0ad4\u0ad5\b^\1\2\u0ad5\u0ad6\7M\2\2\u0ad6\u0ad7\7*\2\2\u0ad7"+
		"\u0ad8\5> \2\u0ad8\u0ad9\7&\2\2\u0ad9\u0ada\5\u00b8]\2\u0ada\u0adb\7+"+
		"\2\2\u0adb\u0adc\b^\1\2\u0adc\u0af5\3\2\2\2\u0add\u0ade\7%\2\2\u0ade\u0adf"+
		"\b^\1\2\u0adf\u0ae0\7P\2\2\u0ae0\u0ae1\7*\2\2\u0ae1\u0ae2\5> \2\u0ae2"+
		"\u0ae3\7&\2\2\u0ae3\u0ae4\5\u00b8]\2\u0ae4\u0ae5\7+\2\2\u0ae5\u0ae6\b"+
		"^\1\2\u0ae6\u0af5\3\2\2\2\u0ae7\u0ae8\7%\2\2\u0ae8\u0ae9\b^\1\2\u0ae9"+
		"\u0aea\7N\2\2\u0aea\u0aeb\7*\2\2\u0aeb\u0aec\5> \2\u0aec\u0aed\7&\2\2"+
		"\u0aed\u0aee\5\u00b8]\2\u0aee\u0aef\7+\2\2\u0aef\u0af0\b^\1\2\u0af0\u0af5"+
		"\3\2\2\2\u0af1\u0af2\5.\30\2\u0af2\u0af3\b^\1\2\u0af3\u0af5\3\2\2\2\u0af4"+
		"\u0a9c\3\2\2\2\u0af4\u0aa1\3\2\2\2\u0af4\u0aa6\3\2\2\2\u0af4\u0aab\3\2"+
		"\2\2\u0af4\u0ab0\3\2\2\2\u0af4\u0ab5\3\2\2\2\u0af4\u0abf\3\2\2\2\u0af4"+
		"\u0ac9\3\2\2\2\u0af4\u0ad3\3\2\2\2\u0af4\u0add\3\2\2\2\u0af4\u0ae7\3\2"+
		"\2\2\u0af4\u0af1\3\2\2\2\u0af5\u00bb\3\2\2\2\u0af6\u0af7\7\67\2\2\u0af7"+
		"\u0af8\b_\1\2\u0af8\u0af9\7V\2\2\u0af9\u0afa\7\"\2\2\u0afa\u0afb\b_\1"+
		"\2\u0afb\u0afc\5\u00be`\2\u0afc\u0afd\b_\1\2\u0afd\u00bd\3\2\2\2\u0afe"+
		"\u0b0f\5\u00c0a\2\u0aff\u0b00\7#\2\2\u0b00\u0b08\b`\1\2\u0b01\u0b02\7"+
		"$\2\2\u0b02\u0b08\b`\1\2\u0b03\u0b04\7 \2\2\u0b04\u0b08\b`\1\2\u0b05\u0b06"+
		"\7!\2\2\u0b06\u0b08\b`\1\2\u0b07\u0aff\3\2\2\2\u0b07\u0b01\3\2\2\2\u0b07"+
		"\u0b03\3\2\2\2\u0b07\u0b05\3\2\2\2\u0b08\u0b09\3\2\2\2\u0b09\u0b0a\b`"+
		"\1\2\u0b0a\u0b0b\5\u00c0a\2\u0b0b\u0b0c\b`\1\2\u0b0c\u0b0e\3\2\2\2\u0b0d"+
		"\u0b07\3\2\2\2\u0b0e\u0b11\3\2\2\2\u0b0f\u0b0d\3\2\2\2\u0b0f\u0b10\3\2"+
		"\2\2\u0b10\u0b12\3\2\2\2\u0b11\u0b0f\3\2\2\2\u0b12\u0b13\b`\1\2\u0b13"+
		"\u00bf\3\2\2\2\u0b14\u0b15\7*\2\2\u0b15\u0b16\5\u00be`\2\u0b16\u0b17\7"+
		"+\2\2\u0b17\u0b18\ba\1\2\u0b18\u0b4e\3\2\2\2\u0b19\u0b1a\7%\2\2\u0b1a"+
		"\u0b1b\ba\1\2\u0b1b\u0b1c\5\u00be`\2\u0b1c\u0b1d\ba\1\2\u0b1d\u0b4e\3"+
		"\2\2\2\u0b1e\u0b1f\7@\2\2\u0b1f\u0b20\ba\1\2\u0b20\u0b21\7D\2\2\u0b21"+
		"\u0b22\ba\1\2\u0b22\u0b23\3\2\2\2\u0b23\u0b24\5\u00be`\2\u0b24\u0b25\b"+
		"a\1\2\u0b25\u0b4e\3\2\2\2\u0b26\u0b27\7@\2\2\u0b27\u0b28\ba\1\2\u0b28"+
		"\u0b29\7A\2\2\u0b29\u0b2a\ba\1\2\u0b2a\u0b2b\3\2\2\2\u0b2b\u0b2c\5\u00be"+
		"`\2\u0b2c\u0b2d\ba\1\2\u0b2d\u0b4e\3\2\2\2\u0b2e\u0b2f\7@\2\2\u0b2f\u0b30"+
		"\ba\1\2\u0b30\u0b31\7B\2\2\u0b31\u0b32\ba\1\2\u0b32\u0b33\3\2\2\2\u0b33"+
		"\u0b34\5\u00be`\2\u0b34\u0b35\ba\1\2\u0b35\u0b4e\3\2\2\2\u0b36\u0b37\7"+
		"@\2\2\u0b37\u0b38\ba\1\2\u0b38\u0b39\7*\2\2\u0b39\u0b3a\5\u00be`\2\u0b3a"+
		"\u0b3b\7C\2\2\u0b3b\u0b3c\ba\1\2\u0b3c\u0b3d\5\u00be`\2\u0b3d\u0b3e\7"+
		"+\2\2\u0b3e\u0b3f\ba\1\2\u0b3f\u0b4e\3\2\2\2\u0b40\u0b41\7@\2\2\u0b41"+
		"\u0b42\ba\1\2\u0b42\u0b43\7*\2\2\u0b43\u0b44\5\u00be`\2\u0b44\u0b45\7"+
		"\22\2\2\u0b45\u0b46\ba\1\2\u0b46\u0b47\5\u00be`\2\u0b47\u0b48\7+\2\2\u0b48"+
		"\u0b49\ba\1\2\u0b49\u0b4e\3\2\2\2\u0b4a\u0b4b\5.\30\2\u0b4b\u0b4c\ba\1"+
		"\2\u0b4c\u0b4e\3\2\2\2\u0b4d\u0b14\3\2\2\2\u0b4d\u0b19\3\2\2\2\u0b4d\u0b1e"+
		"\3\2\2\2\u0b4d\u0b26\3\2\2\2\u0b4d\u0b2e\3\2\2\2\u0b4d\u0b36\3\2\2\2\u0b4d"+
		"\u0b40\3\2\2\2\u0b4d\u0b4a\3\2\2\2\u0b4e\u00c1\3\2\2\2\u0b4f\u0b50\78"+
		"\2\2\u0b50\u0b51\bb\1\2\u0b51\u0b52\7V\2\2\u0b52\u0b53\7\"\2\2\u0b53\u0b54"+
		"\bb\1\2\u0b54\u0b55\5\u00c4c\2\u0b55\u0b56\bb\1\2\u0b56\u00c3\3\2\2\2"+
		"\u0b57\u0b68\5\u00c6d\2\u0b58\u0b59\7#\2\2\u0b59\u0b61\bc\1\2\u0b5a\u0b5b"+
		"\7$\2\2\u0b5b\u0b61\bc\1\2\u0b5c\u0b5d\7 \2\2\u0b5d\u0b61\bc\1\2\u0b5e"+
		"\u0b5f\7!\2\2\u0b5f\u0b61\bc\1\2\u0b60\u0b58\3\2\2\2\u0b60\u0b5a\3\2\2"+
		"\2\u0b60\u0b5c\3\2\2\2\u0b60\u0b5e\3\2\2\2\u0b61\u0b62\3\2\2\2\u0b62\u0b63"+
		"\bc\1\2\u0b63\u0b64\5\u00c6d\2\u0b64\u0b65\bc\1\2\u0b65\u0b67\3\2\2\2"+
		"\u0b66\u0b60\3\2\2\2\u0b67\u0b6a\3\2\2\2\u0b68\u0b66\3\2\2\2\u0b68\u0b69"+
		"\3\2\2\2\u0b69\u0b6b\3\2\2\2\u0b6a\u0b68\3\2\2\2\u0b6b\u0b6c\bc\1\2\u0b6c"+
		"\u00c5\3\2\2\2\u0b6d\u0b6e\7*\2\2\u0b6e\u0b6f\5\u00c4c\2\u0b6f\u0b70\7"+
		"+\2\2\u0b70\u0b71\bd\1\2\u0b71\u0bcf\3\2\2\2\u0b72\u0b73\7%\2\2\u0b73"+
		"\u0b74\bd\1\2\u0b74\u0b75\5\u00c4c\2\u0b75\u0b76\bd\1\2\u0b76\u0bcf\3"+
		"\2\2\2\u0b77\u0b78\7@\2\2\u0b78\u0b79\bd\1\2\u0b79\u0b7a\7D\2\2\u0b7a"+
		"\u0b7b\bd\1\2\u0b7b\u0b7c\3\2\2\2\u0b7c\u0b7d\5\u00c4c\2\u0b7d\u0b7e\b"+
		"d\1\2\u0b7e\u0bcf\3\2\2\2\u0b7f\u0b80\7@\2\2\u0b80\u0b81\bd\1\2\u0b81"+
		"\u0b82\7A\2\2\u0b82\u0b83\bd\1\2\u0b83\u0b84\3\2\2\2\u0b84\u0b85\5\u00c4"+
		"c\2\u0b85\u0b86\bd\1\2\u0b86\u0bcf\3\2\2\2\u0b87\u0b88\7@\2\2\u0b88\u0b89"+
		"\bd\1\2\u0b89\u0b8a\7B\2\2\u0b8a\u0b8b\bd\1\2\u0b8b\u0b8c\3\2\2\2\u0b8c"+
		"\u0b8d\5\u00c4c\2\u0b8d\u0b8e\bd\1\2\u0b8e\u0bcf\3\2\2\2\u0b8f\u0b90\7"+
		"@\2\2\u0b90\u0b91\bd\1\2\u0b91\u0b92\7*\2\2\u0b92\u0b93\5\u00c4c\2\u0b93"+
		"\u0b94\7C\2\2\u0b94\u0b95\bd\1\2\u0b95\u0b96\5\u00c4c\2\u0b96\u0b97\7"+
		"+\2\2\u0b97\u0b98\bd\1\2\u0b98\u0bcf\3\2\2\2\u0b99\u0b9a\7@\2\2\u0b9a"+
		"\u0b9b\bd\1\2\u0b9b\u0b9c\7*\2\2\u0b9c\u0b9d\5\u00c4c\2\u0b9d\u0b9e\7"+
		"\22\2\2\u0b9e\u0b9f\bd\1\2\u0b9f\u0ba0\5\u00c4c\2\u0ba0\u0ba1\7+\2\2\u0ba1"+
		"\u0ba2\bd\1\2\u0ba2\u0bcf\3\2\2\2\u0ba3\u0ba4\7%\2\2\u0ba4\u0ba5\bd\1"+
		"\2\u0ba5\u0ba6\7O\2\2\u0ba6\u0ba7\7*\2\2\u0ba7\u0ba8\5> \2\u0ba8\u0ba9"+
		"\7&\2\2\u0ba9\u0baa\5\u00c4c\2\u0baa\u0bab\7+\2\2\u0bab\u0bac\bd\1\2\u0bac"+
		"\u0bcf\3\2\2\2\u0bad\u0bae\7%\2\2\u0bae\u0baf\bd\1\2\u0baf\u0bb0\7K\2"+
		"\2\u0bb0\u0bb1\7*\2\2\u0bb1\u0bb2\5> \2\u0bb2\u0bb3\7&\2\2\u0bb3\u0bb4"+
		"\5\u00c4c\2\u0bb4\u0bb5\7+\2\2\u0bb5\u0bb6\bd\1\2\u0bb6\u0bcf\3\2\2\2"+
		"\u0bb7\u0bb8\7%\2\2\u0bb8\u0bb9\bd\1\2\u0bb9\u0bba\7L\2\2\u0bba\u0bbb"+
		"\7*\2\2\u0bbb\u0bbc\5> \2\u0bbc\u0bbd\7&\2\2\u0bbd\u0bbe\5\u00c4c\2\u0bbe"+
		"\u0bbf\7+\2\2\u0bbf\u0bc0\bd\1\2\u0bc0\u0bcf\3\2\2\2\u0bc1\u0bc2\7%\2"+
		"\2\u0bc2\u0bc3\bd\1\2\u0bc3\u0bc4\7M\2\2\u0bc4\u0bc5\7*\2\2\u0bc5\u0bc6"+
		"\5> \2\u0bc6\u0bc7\7&\2\2\u0bc7\u0bc8\5\u00c4c\2\u0bc8\u0bc9\7+\2\2\u0bc9"+
		"\u0bca\bd\1\2\u0bca\u0bcf\3\2\2\2\u0bcb\u0bcc\5.\30\2\u0bcc\u0bcd\bd\1"+
		"\2\u0bcd\u0bcf\3\2\2\2\u0bce\u0b6d\3\2\2\2\u0bce\u0b72\3\2\2\2\u0bce\u0b77"+
		"\3\2\2\2\u0bce\u0b7f\3\2\2\2\u0bce\u0b87\3\2\2\2\u0bce\u0b8f\3\2\2\2\u0bce"+
		"\u0b99\3\2\2\2\u0bce\u0ba3\3\2\2\2\u0bce\u0bad\3\2\2\2\u0bce\u0bb7\3\2"+
		"\2\2\u0bce\u0bc1\3\2\2\2\u0bce\u0bcb\3\2\2\2\u0bcf\u00c7\3\2\2\2\u0bd0"+
		"\u0bd1\79\2\2\u0bd1\u0bd2\be\1\2\u0bd2\u0bd3\7V\2\2\u0bd3\u0bd4\7\"\2"+
		"\2\u0bd4\u0bd5\be\1\2\u0bd5\u0bd6\5\u00caf\2\u0bd6\u0bd7\be\1\2\u0bd7"+
		"\u00c9\3\2\2\2\u0bd8\u0be9\5\u00ccg\2\u0bd9\u0bda\7#\2\2\u0bda\u0be2\b"+
		"f\1\2\u0bdb\u0bdc\7$\2\2\u0bdc\u0be2\bf\1\2\u0bdd\u0bde\7 \2\2\u0bde\u0be2"+
		"\bf\1\2\u0bdf\u0be0\7!\2\2\u0be0\u0be2\bf\1\2\u0be1\u0bd9\3\2\2\2\u0be1"+
		"\u0bdb\3\2\2\2\u0be1\u0bdd\3\2\2\2\u0be1\u0bdf\3\2\2\2\u0be2\u0be3\3\2"+
		"\2\2\u0be3\u0be4\bf\1\2\u0be4\u0be5\5\u00ccg\2\u0be5\u0be6\bf\1\2\u0be6"+
		"\u0be8\3\2\2\2\u0be7\u0be1\3\2\2\2\u0be8\u0beb\3\2\2\2\u0be9\u0be7\3\2"+
		"\2\2\u0be9\u0bea\3\2\2\2\u0bea\u0bec\3\2\2\2\u0beb\u0be9\3\2\2\2\u0bec"+
		"\u0bed\bf\1\2\u0bed\u00cb\3\2\2\2\u0bee\u0bef\7*\2\2\u0bef\u0bf0\5\u00ca"+
		"f\2\u0bf0\u0bf1\7+\2\2\u0bf1\u0bf2\bg\1\2\u0bf2\u0c64\3\2\2\2\u0bf3\u0bf4"+
		"\7%\2\2\u0bf4\u0bf5\bg\1\2\u0bf5\u0bf6\5\u00caf\2\u0bf6\u0bf7\bg\1\2\u0bf7"+
		"\u0c64\3\2\2\2\u0bf8\u0bf9\7@\2\2\u0bf9\u0bfa\bg\1\2\u0bfa\u0bfb\7D\2"+
		"\2\u0bfb\u0bfc\bg\1\2\u0bfc\u0bfd\3\2\2\2\u0bfd\u0bfe\5\u00caf\2\u0bfe"+
		"\u0bff\bg\1\2\u0bff\u0c64\3\2\2\2\u0c00\u0c01\7@\2\2\u0c01\u0c02\bg\1"+
		"\2\u0c02\u0c03\7A\2\2\u0c03\u0c04\bg\1\2\u0c04\u0c05\3\2\2\2\u0c05\u0c06"+
		"\5\u00caf\2\u0c06\u0c07\bg\1\2\u0c07\u0c64\3\2\2\2\u0c08\u0c09\7@\2\2"+
		"\u0c09\u0c0a\bg\1\2\u0c0a\u0c0b\7B\2\2\u0c0b\u0c0c\bg\1\2\u0c0c\u0c0d"+
		"\3\2\2\2\u0c0d\u0c0e\5\u00caf\2\u0c0e\u0c0f\bg\1\2\u0c0f\u0c64\3\2\2\2"+
		"\u0c10\u0c11\7@\2\2\u0c11\u0c12\bg\1\2\u0c12\u0c13\7*\2\2\u0c13\u0c14"+
		"\5\u00caf\2\u0c14\u0c15\7C\2\2\u0c15\u0c16\bg\1\2\u0c16\u0c17\5\u00ca"+
		"f\2\u0c17\u0c18\7+\2\2\u0c18\u0c19\bg\1\2\u0c19\u0c64\3\2\2\2\u0c1a\u0c1b"+
		"\7@\2\2\u0c1b\u0c1c\bg\1\2\u0c1c\u0c1d\7*\2\2\u0c1d\u0c1e\5\u00caf\2\u0c1e"+
		"\u0c1f\7\22\2\2\u0c1f\u0c20\bg\1\2\u0c20\u0c21\5\u00caf\2\u0c21\u0c22"+
		"\7+\2\2\u0c22\u0c23\bg\1\2\u0c23\u0c64\3\2\2\2\u0c24\u0c25\7%\2\2\u0c25"+
		"\u0c26\bg\1\2\u0c26\u0c27\7O\2\2\u0c27\u0c28\7*\2\2\u0c28\u0c29\5> \2"+
		"\u0c29\u0c2a\7&\2\2\u0c2a\u0c2b\5\u00caf\2\u0c2b\u0c2c\7+\2\2\u0c2c\u0c2d"+
		"\bg\1\2\u0c2d\u0c64\3\2\2\2\u0c2e\u0c2f\7%\2\2\u0c2f\u0c30\bg\1\2\u0c30"+
		"\u0c31\7K\2\2\u0c31\u0c32\7*\2\2\u0c32\u0c33\5> \2\u0c33\u0c34\7&\2\2"+
		"\u0c34\u0c35\5\u00caf\2\u0c35\u0c36\7+\2\2\u0c36\u0c37\bg\1\2\u0c37\u0c64"+
		"\3\2\2\2\u0c38\u0c39\7%\2\2\u0c39\u0c3a\bg\1\2\u0c3a\u0c3b\7L\2\2\u0c3b"+
		"\u0c3c\7*\2\2\u0c3c\u0c3d\5> \2\u0c3d\u0c3e\7&\2\2\u0c3e\u0c3f\5\u00ca"+
		"f\2\u0c3f\u0c40\7+\2\2\u0c40\u0c41\bg\1\2\u0c41\u0c64\3\2\2\2\u0c42\u0c43"+
		"\7%\2\2\u0c43\u0c44\bg\1\2\u0c44\u0c45\7M\2\2\u0c45\u0c46\7*\2\2\u0c46"+
		"\u0c47\5> \2\u0c47\u0c48\7&\2\2\u0c48\u0c49\5\u00caf\2\u0c49\u0c4a\7+"+
		"\2\2\u0c4a\u0c4b\bg\1\2\u0c4b\u0c64\3\2\2\2\u0c4c\u0c4d\7%\2\2\u0c4d\u0c4e"+
		"\bg\1\2\u0c4e\u0c4f\7P\2\2\u0c4f\u0c50\7*\2\2\u0c50\u0c51\5> \2\u0c51"+
		"\u0c52\7&\2\2\u0c52\u0c53\5\u00caf\2\u0c53\u0c54\7+\2\2\u0c54\u0c55\b"+
		"g\1\2\u0c55\u0c64\3\2\2\2\u0c56\u0c57\7%\2\2\u0c57\u0c58\bg\1\2\u0c58"+
		"\u0c59\7N\2\2\u0c59\u0c5a\7*\2\2\u0c5a\u0c5b\5> \2\u0c5b\u0c5c\7&\2\2"+
		"\u0c5c\u0c5d\5\u00caf\2\u0c5d\u0c5e\7+\2\2\u0c5e\u0c5f\bg\1\2\u0c5f\u0c64"+
		"\3\2\2\2\u0c60\u0c61\5.\30\2\u0c61\u0c62\bg\1\2\u0c62\u0c64\3\2\2\2\u0c63"+
		"\u0bee\3\2\2\2\u0c63\u0bf3\3\2\2\2\u0c63\u0bf8\3\2\2\2\u0c63\u0c00\3\2"+
		"\2\2\u0c63\u0c08\3\2\2\2\u0c63\u0c10\3\2\2\2\u0c63\u0c1a\3\2\2\2\u0c63"+
		"\u0c24\3\2\2\2\u0c63\u0c2e\3\2\2\2\u0c63\u0c38\3\2\2\2\u0c63\u0c42\3\2"+
		"\2\2\u0c63\u0c4c\3\2\2\2\u0c63\u0c56\3\2\2\2\u0c63\u0c60\3\2\2\2\u0c64"+
		"\u00cd\3\2\2\2\u0c65\u0c66\7\67\2\2\u0c66\u0c67\bh\1\2\u0c67\u0c68\7\27"+
		"\2\2\u0c68\u0c69\7V\2\2\u0c69\u0c6a\7\"\2\2\u0c6a\u0c6b\bh\1\2\u0c6b\u0c6c"+
		"\5\u00d0i\2\u0c6c\u0c6d\bh\1\2\u0c6d\u00cf\3\2\2\2\u0c6e\u0c7f\5\u00d2"+
		"j\2\u0c6f\u0c70\7#\2\2\u0c70\u0c78\bi\1\2\u0c71\u0c72\7$\2\2\u0c72\u0c78"+
		"\bi\1\2\u0c73\u0c74\7 \2\2\u0c74\u0c78\bi\1\2\u0c75\u0c76\7!\2\2\u0c76"+
		"\u0c78\bi\1\2\u0c77\u0c6f\3\2\2\2\u0c77\u0c71\3\2\2\2\u0c77\u0c73\3\2"+
		"\2\2\u0c77\u0c75\3\2\2\2\u0c78\u0c79\3\2\2\2\u0c79\u0c7a\bi\1\2\u0c7a"+
		"\u0c7b\5\u00d2j\2\u0c7b\u0c7c\bi\1\2\u0c7c\u0c7e\3\2\2\2\u0c7d\u0c77\3"+
		"\2\2\2\u0c7e\u0c81\3\2\2\2\u0c7f\u0c7d\3\2\2\2\u0c7f\u0c80\3\2\2\2\u0c80"+
		"\u0c82\3\2\2\2\u0c81\u0c7f\3\2\2\2\u0c82\u0c83\bi\1\2\u0c83\u00d1\3\2"+
		"\2\2\u0c84\u0c85\7*\2\2\u0c85\u0c86\5\u00d0i\2\u0c86\u0c87\7+\2\2\u0c87"+
		"\u0c88\bj\1\2\u0c88\u0c97\3\2\2\2\u0c89\u0c8a\7%\2\2\u0c8a\u0c8b\bj\1"+
		"\2\u0c8b\u0c8c\5\u00d0i\2\u0c8c\u0c8d\bj\1\2\u0c8d\u0c97\3\2\2\2\u0c8e"+
		"\u0c8f\7@\2\2\u0c8f\u0c90\bj\1\2\u0c90\u0c91\5\u00d4k\2\u0c91\u0c92\b"+
		"j\1\2\u0c92\u0c97\3\2\2\2\u0c93\u0c94\5.\30\2\u0c94\u0c95\bj\1\2\u0c95"+
		"\u0c97\3\2\2\2\u0c96\u0c84\3\2\2\2\u0c96\u0c89\3\2\2\2\u0c96\u0c8e\3\2"+
		"\2\2\u0c96\u0c93\3\2\2\2\u0c97\u00d3\3\2\2\2\u0c98\u0c99\5\u00d2j\2\u0c99"+
		"\u0c9a\bk\1\2\u0c9a\u0c9f\3\2\2\2\u0c9b\u0c9c\5\u00d8m\2\u0c9c\u0c9d\b"+
		"k\1\2\u0c9d\u0c9f\3\2\2\2\u0c9e\u0c98\3\2\2\2\u0c9e\u0c9b\3\2\2\2\u0c9f"+
		"\u00d5\3\2\2\2\u0ca0\u0cb1\5\u00d8m\2\u0ca1\u0ca2\7#\2\2\u0ca2\u0caa\b"+
		"l\1\2\u0ca3\u0ca4\7$\2\2\u0ca4\u0caa\bl\1\2\u0ca5\u0ca6\7 \2\2\u0ca6\u0caa"+
		"\bl\1\2\u0ca7\u0ca8\7!\2\2\u0ca8\u0caa\bl\1\2\u0ca9\u0ca1\3\2\2\2\u0ca9"+
		"\u0ca3\3\2\2\2\u0ca9\u0ca5\3\2\2\2\u0ca9\u0ca7\3\2\2\2\u0caa\u0cab\3\2"+
		"\2\2\u0cab\u0cac\bl\1\2\u0cac\u0cad\5\u00d8m\2\u0cad\u0cae\bl\1\2\u0cae"+
		"\u0cb0\3\2\2\2\u0caf\u0ca9\3\2\2\2\u0cb0\u0cb3\3\2\2\2\u0cb1\u0caf\3\2"+
		"\2\2\u0cb1\u0cb2\3\2\2\2\u0cb2\u0cb4\3\2\2\2\u0cb3\u0cb1\3\2\2\2\u0cb4"+
		"\u0cb5\bl\1\2\u0cb5\u0cc9\3\2\2\2\u0cb6\u0cc3\5\u00d4k\2\u0cb7\u0cb8\7"+
		"C\2\2\u0cb8\u0cbc\bl\1\2\u0cb9\u0cba\7\22\2\2\u0cba\u0cbc\bl\1\2\u0cbb"+
		"\u0cb7\3\2\2\2\u0cbb\u0cb9\3\2\2\2\u0cbc\u0cbd\3\2\2\2\u0cbd\u0cbe\bl"+
		"\1\2\u0cbe\u0cbf\5\u00d4k\2\u0cbf\u0cc0\bl\1\2\u0cc0\u0cc2\3\2\2\2\u0cc1"+
		"\u0cbb\3\2\2\2\u0cc2\u0cc5\3\2\2\2\u0cc3\u0cc1\3\2\2\2\u0cc3\u0cc4\3\2"+
		"\2\2\u0cc4\u0cc6\3\2\2\2\u0cc5\u0cc3\3\2\2\2\u0cc6\u0cc7\bl\1\2\u0cc7"+
		"\u0cc9\3\2\2\2\u0cc8\u0ca0\3\2\2\2\u0cc8\u0cb6\3\2\2\2\u0cc9\u00d7\3\2"+
		"\2\2\u0cca\u0ccb\7*\2\2\u0ccb\u0ccc\5\u00d6l\2\u0ccc\u0ccd\7+\2\2\u0ccd"+
		"\u0cce\bm\1\2\u0cce\u0cdf\3\2\2\2\u0ccf\u0cd0\7D\2\2\u0cd0\u0cd1\bm\1"+
		"\2\u0cd1\u0cd2\5\u00d4k\2\u0cd2\u0cd3\bm\1\2\u0cd3\u0cdf\3\2\2\2\u0cd4"+
		"\u0cd5\7A\2\2\u0cd5\u0cd6\bm\1\2\u0cd6\u0cd7\5\u00d4k\2\u0cd7\u0cd8\b"+
		"m\1\2\u0cd8\u0cdf\3\2\2\2\u0cd9\u0cda\7B\2\2\u0cda\u0cdb\bm\1\2\u0cdb"+
		"\u0cdc\5\u00d4k\2\u0cdc\u0cdd\bm\1\2\u0cdd\u0cdf\3\2\2\2\u0cde\u0cca\3"+
		"\2\2\2\u0cde\u0ccf\3\2\2\2\u0cde\u0cd4\3\2\2\2\u0cde\u0cd9\3\2\2\2\u0cdf"+
		"\u00d9\3\2\2\2\u0ce0\u0ce1\7\67\2\2\u0ce1\u0ce2\bn\1\2\u0ce2\u0ce3\7\27"+
		"\2\2\u0ce3\u0ce4\7O\2\2\u0ce4\u0ce5\7V\2\2\u0ce5\u0ce6\7\"\2\2\u0ce6\u0ce7"+
		"\bn\1\2\u0ce7\u0ce8\5\u00dco\2\u0ce8\u0ce9\bn\1\2\u0ce9\u00db\3\2\2\2"+
		"\u0cea\u0cfb\5\u00dep\2\u0ceb\u0cec\7#\2\2\u0cec\u0cf4\bo\1\2\u0ced\u0cee"+
		"\7$\2\2\u0cee\u0cf4\bo\1\2\u0cef\u0cf0\7 \2\2\u0cf0\u0cf4\bo\1\2\u0cf1"+
		"\u0cf2\7!\2\2\u0cf2\u0cf4\bo\1\2\u0cf3\u0ceb\3\2\2\2\u0cf3\u0ced\3\2\2"+
		"\2\u0cf3\u0cef\3\2\2\2\u0cf3\u0cf1\3\2\2\2\u0cf4\u0cf5\3\2\2\2\u0cf5\u0cf6"+
		"\bo\1\2\u0cf6\u0cf7\5\u00dep\2\u0cf7\u0cf8\bo\1\2\u0cf8\u0cfa\3\2\2\2"+
		"\u0cf9\u0cf3\3\2\2\2\u0cfa\u0cfd\3\2\2\2\u0cfb\u0cf9\3\2\2\2\u0cfb\u0cfc"+
		"\3\2\2\2\u0cfc\u0cfe\3\2\2\2\u0cfd\u0cfb\3\2\2\2\u0cfe\u0cff\bo\1\2\u0cff"+
		"\u00dd\3\2\2\2\u0d00\u0d01\7*\2\2\u0d01\u0d02\5\u00dco\2\u0d02\u0d03\7"+
		"+\2\2\u0d03\u0d04\bp\1\2\u0d04\u0d3b\3\2\2\2\u0d05\u0d06\7%\2\2\u0d06"+
		"\u0d07\bp\1\2\u0d07\u0d08\5\u00dco\2\u0d08\u0d09\bp\1\2\u0d09\u0d3b\3"+
		"\2\2\2\u0d0a\u0d0b\7@\2\2\u0d0b\u0d0c\bp\1\2\u0d0c\u0d0d\5\u00e0q\2\u0d0d"+
		"\u0d0e\bp\1\2\u0d0e\u0d3b\3\2\2\2\u0d0f\u0d10\7%\2\2\u0d10\u0d11\bp\1"+
		"\2\u0d11\u0d12\7O\2\2\u0d12\u0d13\7*\2\2\u0d13\u0d14\5> \2\u0d14\u0d15"+
		"\7&\2\2\u0d15\u0d16\5\u00e2r\2\u0d16\u0d17\7+\2\2\u0d17\u0d18\bp\1\2\u0d18"+
		"\u0d3b\3\2\2\2\u0d19\u0d1a\7%\2\2\u0d1a\u0d1b\bp\1\2\u0d1b\u0d1c\7K\2"+
		"\2\u0d1c\u0d1d\7*\2\2\u0d1d\u0d1e\5> \2\u0d1e\u0d1f\7&\2\2\u0d1f\u0d20"+
		"\5\u00e2r\2\u0d20\u0d21\7+\2\2\u0d21\u0d22\bp\1\2\u0d22\u0d3b\3\2\2\2"+
		"\u0d23\u0d24\7%\2\2\u0d24\u0d25\bp\1\2\u0d25\u0d26\7L\2\2\u0d26\u0d27"+
		"\7*\2\2\u0d27\u0d28\5> \2\u0d28\u0d29\7&\2\2\u0d29\u0d2a\5\u00e2r\2\u0d2a"+
		"\u0d2b\7+\2\2\u0d2b\u0d2c\bp\1\2\u0d2c\u0d3b\3\2\2\2\u0d2d\u0d2e\7%\2"+
		"\2\u0d2e\u0d2f\bp\1\2\u0d2f\u0d30\7M\2\2\u0d30\u0d31\7*\2\2\u0d31\u0d32"+
		"\5> \2\u0d32\u0d33\7&\2\2\u0d33\u0d34\5\u00e2r\2\u0d34\u0d35\7+\2\2\u0d35"+
		"\u0d36\bp\1\2\u0d36\u0d3b\3\2\2\2\u0d37\u0d38\5.\30\2\u0d38\u0d39\bp\1"+
		"\2\u0d39\u0d3b\3\2\2\2\u0d3a\u0d00\3\2\2\2\u0d3a\u0d05\3\2\2\2\u0d3a\u0d0a"+
		"\3\2\2\2\u0d3a\u0d0f\3\2\2\2\u0d3a\u0d19\3\2\2\2\u0d3a\u0d23\3\2\2\2\u0d3a"+
		"\u0d2d\3\2\2\2\u0d3a\u0d37\3\2\2\2\u0d3b\u00df\3\2\2\2\u0d3c\u0d3d\5\u00de"+
		"p\2\u0d3d\u0d3e\bq\1\2\u0d3e\u0d43\3\2\2\2\u0d3f\u0d40\5\u00e6t\2\u0d40"+
		"\u0d41\bq\1\2\u0d41\u0d43\3\2\2\2\u0d42\u0d3c\3\2\2\2\u0d42\u0d3f\3\2"+
		"\2\2\u0d43\u00e1\3\2\2\2\u0d44\u0d45\5\u00dco\2\u0d45\u0d46\br\1\2\u0d46"+
		"\u0d4b\3\2\2\2\u0d47\u0d48\5\u00e4s\2\u0d48\u0d49\br\1\2\u0d49\u0d4b\3"+
		"\2\2\2\u0d4a\u0d44\3\2\2\2\u0d4a\u0d47\3\2\2\2\u0d4b\u00e3\3\2\2\2\u0d4c"+
		"\u0d5d\5\u00e6t\2\u0d4d\u0d4e\7#\2\2\u0d4e\u0d56\bs\1\2\u0d4f\u0d50\7"+
		"$\2\2\u0d50\u0d56\bs\1\2\u0d51\u0d52\7 \2\2\u0d52\u0d56\bs\1\2\u0d53\u0d54"+
		"\7!\2\2\u0d54\u0d56\bs\1\2\u0d55\u0d4d\3\2\2\2\u0d55\u0d4f\3\2\2\2\u0d55"+
		"\u0d51\3\2\2\2\u0d55\u0d53\3\2\2\2\u0d56\u0d57\3\2\2\2\u0d57\u0d58\bs"+
		"\1\2\u0d58\u0d59\5\u00e6t\2\u0d59\u0d5a\bs\1\2\u0d5a\u0d5c\3\2\2\2\u0d5b"+
		"\u0d55\3\2\2\2\u0d5c\u0d5f\3\2\2\2\u0d5d\u0d5b\3\2\2\2\u0d5d\u0d5e\3\2"+
		"\2\2\u0d5e\u0d60\3\2\2\2\u0d5f\u0d5d\3\2\2\2\u0d60\u0d61\bs\1\2\u0d61"+
		"\u0d75\3\2\2\2\u0d62\u0d6f\5\u00e0q\2\u0d63\u0d64\7C\2\2\u0d64\u0d68\b"+
		"s\1\2\u0d65\u0d66\7\22\2\2\u0d66\u0d68\bs\1\2\u0d67\u0d63\3\2\2\2\u0d67"+
		"\u0d65\3\2\2\2\u0d68\u0d69\3\2\2\2\u0d69\u0d6a\bs\1\2\u0d6a\u0d6b\5\u00e0"+
		"q\2\u0d6b\u0d6c\bs\1\2\u0d6c\u0d6e\3\2\2\2\u0d6d\u0d67\3\2\2\2\u0d6e\u0d71"+
		"\3\2\2\2\u0d6f\u0d6d\3\2\2\2\u0d6f\u0d70\3\2\2\2\u0d70\u0d72\3\2\2\2\u0d71"+
		"\u0d6f\3\2\2\2\u0d72\u0d73\bs\1\2\u0d73\u0d75\3\2\2\2\u0d74\u0d4c\3\2"+
		"\2\2\u0d74\u0d62\3\2\2\2\u0d75\u00e5\3\2\2\2\u0d76\u0d77\7*\2\2\u0d77"+
		"\u0d78\5\u00e4s\2\u0d78\u0d79\7+\2\2\u0d79\u0d7a\bt\1\2\u0d7a\u0d8b\3"+
		"\2\2\2\u0d7b\u0d7c\7D\2\2\u0d7c\u0d7d\bt\1\2\u0d7d\u0d7e\5\u00e0q\2\u0d7e"+
		"\u0d7f\bt\1\2\u0d7f\u0d8b\3\2\2\2\u0d80\u0d81\7A\2\2\u0d81\u0d82\bt\1"+
		"\2\u0d82\u0d83\5\u00e0q\2\u0d83\u0d84\bt\1\2\u0d84\u0d8b\3\2\2\2\u0d85"+
		"\u0d86\7B\2\2\u0d86\u0d87\bt\1\2\u0d87\u0d88\5\u00e0q\2\u0d88\u0d89\b"+
		"t\1\2\u0d89\u0d8b\3\2\2\2\u0d8a\u0d76\3\2\2\2\u0d8a\u0d7b\3\2\2\2\u0d8a"+
		"\u0d80\3\2\2\2\u0d8a\u0d85\3\2\2\2\u0d8b\u00e7\3\2\2\2\u0d8c\u0d8d\7\67"+
		"\2\2\u0d8d\u0d8e\bu\1\2\u0d8e\u0d8f\7\27\2\2\u0d8f\u0d90\7>\2\2\u0d90"+
		"\u0d91\7V\2\2\u0d91\u0d92\7\"\2\2\u0d92\u0d93\bu\1\2\u0d93\u0d94\5\u00ea"+
		"v\2\u0d94\u0d95\bu\1\2\u0d95\u00e9\3\2\2\2\u0d96\u0da7\5\u00ecw\2\u0d97"+
		"\u0d98\7#\2\2\u0d98\u0da0\bv\1\2\u0d99\u0d9a\7$\2\2\u0d9a\u0da0\bv\1\2"+
		"\u0d9b\u0d9c\7 \2\2\u0d9c\u0da0\bv\1\2\u0d9d\u0d9e\7!\2\2\u0d9e\u0da0"+
		"\bv\1\2\u0d9f\u0d97\3\2\2\2\u0d9f\u0d99\3\2\2\2\u0d9f\u0d9b\3\2\2\2\u0d9f"+
		"\u0d9d\3\2\2\2\u0da0\u0da1\3\2\2\2\u0da1\u0da2\bv\1\2\u0da2\u0da3\5\u00ec"+
		"w\2\u0da3\u0da4\bv\1\2\u0da4\u0da6\3\2\2\2\u0da5\u0d9f\3\2\2\2\u0da6\u0da9"+
		"\3\2\2\2\u0da7\u0da5\3\2\2\2\u0da7\u0da8\3\2\2\2\u0da8\u0daa\3\2\2\2\u0da9"+
		"\u0da7\3\2\2\2\u0daa\u0dab\bv\1\2\u0dab\u00eb\3\2\2\2\u0dac\u0dad\7*\2"+
		"\2\u0dad\u0dae\5\u00eav\2\u0dae\u0daf\7+\2\2\u0daf\u0db0\bw\1\2\u0db0"+
		"\u0dfb\3\2\2\2\u0db1\u0db2\7%\2\2\u0db2\u0db3\bw\1\2\u0db3\u0db4\5\u00ea"+
		"v\2\u0db4\u0db5\bw\1\2\u0db5\u0dfb\3\2\2\2\u0db6\u0db7\7@\2\2\u0db7\u0db8"+
		"\bw\1\2\u0db8\u0db9\5\u00eex\2\u0db9\u0dba\bw\1\2\u0dba\u0dfb\3\2\2\2"+
		"\u0dbb\u0dbc\7%\2\2\u0dbc\u0dbd\bw\1\2\u0dbd\u0dbe\7O\2\2\u0dbe\u0dbf"+
		"\7*\2\2\u0dbf\u0dc0\5> \2\u0dc0\u0dc1\7&\2\2\u0dc1\u0dc2\5\u00f0y\2\u0dc2"+
		"\u0dc3\7+\2\2\u0dc3\u0dc4\bw\1\2\u0dc4\u0dfb\3\2\2\2\u0dc5\u0dc6\7%\2"+
		"\2\u0dc6\u0dc7\bw\1\2\u0dc7\u0dc8\7K\2\2\u0dc8\u0dc9\7*\2\2\u0dc9\u0dca"+
		"\5> \2\u0dca\u0dcb\7&\2\2\u0dcb\u0dcc\5\u00f0y\2\u0dcc\u0dcd\7+\2\2\u0dcd"+
		"\u0dce\bw\1\2\u0dce\u0dfb\3\2\2\2\u0dcf\u0dd0\7%\2\2\u0dd0\u0dd1\bw\1"+
		"\2\u0dd1\u0dd2\7L\2\2\u0dd2\u0dd3\7*\2\2\u0dd3\u0dd4\5> \2\u0dd4\u0dd5"+
		"\7&\2\2\u0dd5\u0dd6\5\u00f0y\2\u0dd6\u0dd7\7+\2\2\u0dd7\u0dd8\bw\1\2\u0dd8"+
		"\u0dfb\3\2\2\2\u0dd9\u0dda\7%\2\2\u0dda\u0ddb\bw\1\2\u0ddb\u0ddc\7M\2"+
		"\2\u0ddc\u0ddd\7*\2\2\u0ddd\u0dde\5> \2\u0dde\u0ddf\7&\2\2\u0ddf\u0de0"+
		"\5\u00f0y\2\u0de0\u0de1\7+\2\2\u0de1\u0de2\bw\1\2\u0de2\u0dfb\3\2\2\2"+
		"\u0de3\u0de4\7%\2\2\u0de4\u0de5\bw\1\2\u0de5\u0de6\7P\2\2\u0de6\u0de7"+
		"\7*\2\2\u0de7\u0de8\5> \2\u0de8\u0de9\7&\2\2\u0de9\u0dea\5\u00f0y\2\u0dea"+
		"\u0deb\7+\2\2\u0deb\u0dec\bw\1\2\u0dec\u0dfb\3\2\2\2\u0ded\u0dee\7%\2"+
		"\2\u0dee\u0def\bw\1\2\u0def\u0df0\7N\2\2\u0df0\u0df1\7*\2\2\u0df1\u0df2"+
		"\5> \2\u0df2\u0df3\7&\2\2\u0df3\u0df4\5\u00f0y\2\u0df4\u0df5\7+\2\2\u0df5"+
		"\u0df6\bw\1\2\u0df6\u0dfb\3\2\2\2\u0df7\u0df8\5.\30\2\u0df8\u0df9\bw\1"+
		"\2\u0df9\u0dfb\3\2\2\2\u0dfa\u0dac\3\2\2\2\u0dfa\u0db1\3\2\2\2\u0dfa\u0db6"+
		"\3\2\2\2\u0dfa\u0dbb\3\2\2\2\u0dfa\u0dc5\3\2\2\2\u0dfa\u0dcf\3\2\2\2\u0dfa"+
		"\u0dd9\3\2\2\2\u0dfa\u0de3\3\2\2\2\u0dfa\u0ded\3\2\2\2\u0dfa\u0df7\3\2"+
		"\2\2\u0dfb\u00ed\3\2\2\2\u0dfc\u0dfd\5\u00ecw\2\u0dfd\u0dfe\bx\1\2\u0dfe"+
		"\u0e03\3\2\2\2\u0dff\u0e00\5\u00f4{\2\u0e00\u0e01\bx\1\2\u0e01\u0e03\3"+
		"\2\2\2\u0e02\u0dfc\3\2\2\2\u0e02\u0dff\3\2\2\2\u0e03\u00ef\3\2\2\2\u0e04"+
		"\u0e05\5\u00eav\2\u0e05\u0e06\by\1\2\u0e06\u0e0b\3\2\2\2\u0e07\u0e08\5"+
		"\u00f2z\2\u0e08\u0e09\by\1\2\u0e09\u0e0b\3\2\2\2\u0e0a\u0e04\3\2\2\2\u0e0a"+
		"\u0e07\3\2\2\2\u0e0b\u00f1\3\2\2\2\u0e0c\u0e1d\5\u00f4{\2\u0e0d\u0e0e"+
		"\7#\2\2\u0e0e\u0e16\bz\1\2\u0e0f\u0e10\7$\2\2\u0e10\u0e16\bz\1\2\u0e11"+
		"\u0e12\7 \2\2\u0e12\u0e16\bz\1\2\u0e13\u0e14\7!\2\2\u0e14\u0e16\bz\1\2"+
		"\u0e15\u0e0d\3\2\2\2\u0e15\u0e0f\3\2\2\2\u0e15\u0e11\3\2\2\2\u0e15\u0e13"+
		"\3\2\2\2\u0e16\u0e17\3\2\2\2\u0e17\u0e18\bz\1\2\u0e18\u0e19\5\u00f4{\2"+
		"\u0e19\u0e1a\bz\1\2\u0e1a\u0e1c\3\2\2\2\u0e1b\u0e15\3\2\2\2\u0e1c\u0e1f"+
		"\3\2\2\2\u0e1d\u0e1b\3\2\2\2\u0e1d\u0e1e\3\2\2\2\u0e1e\u0e20\3\2\2\2\u0e1f"+
		"\u0e1d\3\2\2\2\u0e20\u0e21\bz\1\2\u0e21\u0e35\3\2\2\2\u0e22\u0e2f\5\u00ee"+
		"x\2\u0e23\u0e24\7C\2\2\u0e24\u0e28\bz\1\2\u0e25\u0e26\7\22\2\2\u0e26\u0e28"+
		"\bz\1\2\u0e27\u0e23\3\2\2\2\u0e27\u0e25\3\2\2\2\u0e28\u0e29\3\2\2\2\u0e29"+
		"\u0e2a\bz\1\2\u0e2a\u0e2b\5\u00eex\2\u0e2b\u0e2c\bz\1\2\u0e2c\u0e2e\3"+
		"\2\2\2\u0e2d\u0e27\3\2\2\2\u0e2e\u0e31\3\2\2\2\u0e2f\u0e2d\3\2\2\2\u0e2f"+
		"\u0e30\3\2\2\2\u0e30\u0e32\3\2\2\2\u0e31\u0e2f\3\2\2\2\u0e32\u0e33\bz"+
		"\1\2\u0e33\u0e35\3\2\2\2\u0e34\u0e0c\3\2\2\2\u0e34\u0e22\3\2\2\2\u0e35"+
		"\u00f3\3\2\2\2\u0e36\u0e37\7*\2\2\u0e37\u0e38\5\u00f2z\2\u0e38\u0e39\7"+
		"+\2\2\u0e39\u0e3a\b{\1\2\u0e3a\u0e4b\3\2\2\2\u0e3b\u0e3c\7D\2\2\u0e3c"+
		"\u0e3d\b{\1\2\u0e3d\u0e3e\5\u00eex\2\u0e3e\u0e3f\b{\1\2\u0e3f\u0e4b\3"+
		"\2\2\2\u0e40\u0e41\7A\2\2\u0e41\u0e42\b{\1\2\u0e42\u0e43\5\u00eex\2\u0e43"+
		"\u0e44\b{\1\2\u0e44\u0e4b\3\2\2\2\u0e45\u0e46\7B\2\2\u0e46\u0e47\b{\1"+
		"\2\u0e47\u0e48\5\u00eex\2\u0e48\u0e49\b{\1\2\u0e49\u0e4b\3\2\2\2\u0e4a"+
		"\u0e36\3\2\2\2\u0e4a\u0e3b\3\2\2\2\u0e4a\u0e40\3\2\2\2\u0e4a\u0e45\3\2"+
		"\2\2\u0e4b\u00f5\3\2\2\2\u0e4c\u0e4d\7=\2\2\u0e4d\u0e4e\b|\1\2\u0e4e\u0e4f"+
		"\7\27\2\2\u0e4f\u0e50\7V\2\2\u0e50\u0e51\7\"\2\2\u0e51\u0e52\b|\1\2\u0e52"+
		"\u0e53\5\u00f8}\2\u0e53\u0e54\b|\1\2\u0e54\u00f7\3\2\2\2\u0e55\u0e66\5"+
		"\u00fa~\2\u0e56\u0e57\7#\2\2\u0e57\u0e5f\b}\1\2\u0e58\u0e59\7$\2\2\u0e59"+
		"\u0e5f\b}\1\2\u0e5a\u0e5b\7 \2\2\u0e5b\u0e5f\b}\1\2\u0e5c\u0e5d\7!\2\2"+
		"\u0e5d\u0e5f\b}\1\2\u0e5e\u0e56\3\2\2\2\u0e5e\u0e58\3\2\2\2\u0e5e\u0e5a"+
		"\3\2\2\2\u0e5e\u0e5c\3\2\2\2\u0e5f\u0e60\3\2\2\2\u0e60\u0e61\b}\1\2\u0e61"+
		"\u0e62\5\u00fa~\2\u0e62\u0e63\b}\1\2\u0e63\u0e65\3\2\2\2\u0e64\u0e5e\3"+
		"\2\2\2\u0e65\u0e68\3\2\2\2\u0e66\u0e64\3\2\2\2\u0e66\u0e67\3\2\2\2\u0e67"+
		"\u0e69\3\2\2\2\u0e68\u0e66\3\2\2\2\u0e69\u0e6a\b}\1\2\u0e6a\u00f9\3\2"+
		"\2\2\u0e6b\u0e6c\7*\2\2\u0e6c\u0e6d\5\u00f8}\2\u0e6d\u0e6e\7+\2\2\u0e6e"+
		"\u0e6f\b~\1\2\u0e6f\u0e83\3\2\2\2\u0e70\u0e71\7%\2\2\u0e71\u0e72\b~\1"+
		"\2\u0e72\u0e73\5\u00f8}\2\u0e73\u0e74\b~\1\2\u0e74\u0e83\3\2\2\2\u0e75"+
		"\u0e76\7@\2\2\u0e76\u0e77\b~\1\2\u0e77\u0e78\5\u00fc\177\2\u0e78\u0e79"+
		"\b~\1\2\u0e79\u0e83\3\2\2\2\u0e7a\u0e7b\7?\2\2\u0e7b\u0e7c\b~\1\2\u0e7c"+
		"\u0e7d\5\u00fc\177\2\u0e7d\u0e7e\b~\1\2\u0e7e\u0e83\3\2\2\2\u0e7f\u0e80"+
		"\5.\30\2\u0e80\u0e81\b~\1\2\u0e81\u0e83\3\2\2\2\u0e82\u0e6b\3\2\2\2\u0e82"+
		"\u0e70\3\2\2\2\u0e82\u0e75\3\2\2\2\u0e82\u0e7a\3\2\2\2\u0e82\u0e7f\3\2"+
		"\2\2\u0e83\u00fb\3\2\2\2\u0e84\u0e85\5\u00fa~\2\u0e85\u0e86\b\177\1\2"+
		"\u0e86\u0e8b\3\2\2\2\u0e87\u0e88\5\u0100\u0081\2\u0e88\u0e89\b\177\1\2"+
		"\u0e89\u0e8b\3\2\2\2\u0e8a\u0e84\3\2\2\2\u0e8a\u0e87\3\2\2\2\u0e8b\u00fd"+
		"\3\2\2\2\u0e8c\u0e9d\5\u0100\u0081\2\u0e8d\u0e8e\7#\2\2\u0e8e\u0e96\b"+
		"\u0080\1\2\u0e8f\u0e90\7$\2\2\u0e90\u0e96\b\u0080\1\2\u0e91\u0e92\7 \2"+
		"\2\u0e92\u0e96\b\u0080\1\2\u0e93\u0e94\7!\2\2\u0e94\u0e96\b\u0080\1\2"+
		"\u0e95\u0e8d\3\2\2\2\u0e95\u0e8f\3\2\2\2\u0e95\u0e91\3\2\2\2\u0e95\u0e93"+
		"\3\2\2\2\u0e96\u0e97\3\2\2\2\u0e97\u0e98\b\u0080\1\2\u0e98\u0e99\5\u0100"+
		"\u0081\2\u0e99\u0e9a\b\u0080\1\2\u0e9a\u0e9c\3\2\2\2\u0e9b\u0e95\3\2\2"+
		"\2\u0e9c\u0e9f\3\2\2\2\u0e9d\u0e9b\3\2\2\2\u0e9d\u0e9e\3\2\2\2\u0e9e\u0ea0"+
		"\3\2\2\2\u0e9f\u0e9d\3\2\2\2\u0ea0\u0ea1\b\u0080\1\2\u0ea1\u0eb5\3\2\2"+
		"\2\u0ea2\u0eaf\5\u00fc\177\2\u0ea3\u0ea4\7C\2\2\u0ea4\u0ea8\b\u0080\1"+
		"\2\u0ea5\u0ea6\7\22\2\2\u0ea6\u0ea8\b\u0080\1\2\u0ea7\u0ea3\3\2\2\2\u0ea7"+
		"\u0ea5\3\2\2\2\u0ea8\u0ea9\3\2\2\2\u0ea9\u0eaa\b\u0080\1\2\u0eaa\u0eab"+
		"\5\u00fc\177\2\u0eab\u0eac\b\u0080\1\2\u0eac\u0eae\3\2\2\2\u0ead\u0ea7"+
		"\3\2\2\2\u0eae\u0eb1\3\2\2\2\u0eaf\u0ead\3\2\2\2\u0eaf\u0eb0\3\2\2\2\u0eb0"+
		"\u0eb2\3\2\2\2\u0eb1\u0eaf\3\2\2\2\u0eb2\u0eb3\b\u0080\1\2\u0eb3\u0eb5"+
		"\3\2\2\2\u0eb4\u0e8c\3\2\2\2\u0eb4\u0ea2\3\2\2\2\u0eb5\u00ff\3\2\2\2\u0eb6"+
		"\u0eb7\7*\2\2\u0eb7\u0eb8\5\u00fe\u0080\2\u0eb8\u0eb9\7+\2\2\u0eb9\u0eba"+
		"\b\u0081\1\2\u0eba\u0ecb\3\2\2\2\u0ebb\u0ebc\7D\2\2\u0ebc\u0ebd\b\u0081"+
		"\1\2\u0ebd\u0ebe\5\u00fc\177\2\u0ebe\u0ebf\b\u0081\1\2\u0ebf\u0ecb\3\2"+
		"\2\2\u0ec0\u0ec1\7A\2\2\u0ec1\u0ec2\b\u0081\1\2\u0ec2\u0ec3\5\u00fc\177"+
		"\2\u0ec3\u0ec4\b\u0081\1\2\u0ec4\u0ecb\3\2\2\2\u0ec5\u0ec6\7B\2\2\u0ec6"+
		"\u0ec7\b\u0081\1\2\u0ec7\u0ec8\5\u00fc\177\2\u0ec8\u0ec9\b\u0081\1\2\u0ec9"+
		"\u0ecb\3\2\2\2\u0eca\u0eb6\3\2\2\2\u0eca\u0ebb\3\2\2\2\u0eca\u0ec0\3\2"+
		"\2\2\u0eca\u0ec5\3\2\2\2\u0ecb\u0101\3\2\2\2\u0ecc\u0ecd\7=\2\2\u0ecd"+
		"\u0ece\b\u0082\1\2\u0ece\u0ecf\7\27\2\2\u0ecf\u0ed0\7O\2\2\u0ed0\u0ed1"+
		"\7V\2\2\u0ed1\u0ed2\7\"\2\2\u0ed2\u0ed3\b\u0082\1\2\u0ed3\u0ed4\5\u0104"+
		"\u0083\2\u0ed4\u0ed5\b\u0082\1\2\u0ed5\u0103\3\2\2\2\u0ed6\u0ee7\5\u0106"+
		"\u0084\2\u0ed7\u0ed8\7#\2\2\u0ed8\u0ee0\b\u0083\1\2\u0ed9\u0eda\7$\2\2"+
		"\u0eda\u0ee0\b\u0083\1\2\u0edb\u0edc\7 \2\2\u0edc\u0ee0\b\u0083\1\2\u0edd"+
		"\u0ede\7!\2\2\u0ede\u0ee0\b\u0083\1\2\u0edf\u0ed7\3\2\2\2\u0edf\u0ed9"+
		"\3\2\2\2\u0edf\u0edb\3\2\2\2\u0edf\u0edd\3\2\2\2\u0ee0\u0ee1\3\2\2\2\u0ee1"+
		"\u0ee2\b\u0083\1\2\u0ee2\u0ee3\5\u0106\u0084\2\u0ee3\u0ee4\b\u0083\1\2"+
		"\u0ee4\u0ee6\3\2\2\2\u0ee5\u0edf\3\2\2\2\u0ee6\u0ee9\3\2\2\2\u0ee7\u0ee5"+
		"\3\2\2\2\u0ee7\u0ee8\3\2\2\2\u0ee8\u0eea\3\2\2\2\u0ee9\u0ee7\3\2\2\2\u0eea"+
		"\u0eeb\b\u0083\1\2\u0eeb\u0105\3\2\2\2\u0eec\u0eed\7*\2\2\u0eed\u0eee"+
		"\5\u0104\u0083\2\u0eee\u0eef\7+\2\2\u0eef\u0ef0\b\u0084\1\2\u0ef0\u0f50"+
		"\3\2\2\2\u0ef1\u0ef2\7%\2\2\u0ef2\u0ef3\b\u0084\1\2\u0ef3\u0ef4\5\u0104"+
		"\u0083\2\u0ef4\u0ef5\b\u0084\1\2\u0ef5\u0f50\3\2\2\2\u0ef6\u0ef7\7@\2"+
		"\2\u0ef7\u0ef8\b\u0084\1\2\u0ef8\u0ef9\5\u0108\u0085\2\u0ef9\u0efa\b\u0084"+
		"\1\2\u0efa\u0f50\3\2\2\2\u0efb\u0efc\7?\2\2\u0efc\u0efd\b\u0084\1\2\u0efd"+
		"\u0efe\5\u0108\u0085\2\u0efe\u0eff\b\u0084\1\2\u0eff\u0f50\3\2\2\2\u0f00"+
		"\u0f01\7O\2\2\u0f01\u0f02\b\u0084\1\2\u0f02\u0f03\7*\2\2\u0f03\u0f04\5"+
		"> \2\u0f04\u0f05\7&\2\2\u0f05\u0f06\5\u010a\u0086\2\u0f06\u0f07\7+\2\2"+
		"\u0f07\u0f08\b\u0084\1\2\u0f08\u0f50\3\2\2\2\u0f09\u0f0a\7K\2\2\u0f0a"+
		"\u0f0b\b\u0084\1\2\u0f0b\u0f0c\7*\2\2\u0f0c\u0f0d\5> \2\u0f0d\u0f0e\7"+
		"&\2\2\u0f0e\u0f0f\5\u010a\u0086\2\u0f0f\u0f10\7+\2\2\u0f10\u0f11\b\u0084"+
		"\1\2\u0f11\u0f50\3\2\2\2\u0f12\u0f13\7L\2\2\u0f13\u0f14\b\u0084\1\2\u0f14"+
		"\u0f15\7*\2\2\u0f15\u0f16\5> \2\u0f16\u0f17\7&\2\2\u0f17\u0f18\5\u010a"+
		"\u0086\2\u0f18\u0f19\7+\2\2\u0f19\u0f1a\b\u0084\1\2\u0f1a\u0f50\3\2\2"+
		"\2\u0f1b\u0f1c\7M\2\2\u0f1c\u0f1d\b\u0084\1\2\u0f1d\u0f1e\7*\2\2\u0f1e"+
		"\u0f1f\5> \2\u0f1f\u0f20\7&\2\2\u0f20\u0f21\5\u010a\u0086\2\u0f21\u0f22"+
		"\7+\2\2\u0f22\u0f23\b\u0084\1\2\u0f23\u0f50\3\2\2\2\u0f24\u0f25\7%\2\2"+
		"\u0f25\u0f26\b\u0084\1\2\u0f26\u0f27\7O\2\2\u0f27\u0f28\7*\2\2\u0f28\u0f29"+
		"\5> \2\u0f29\u0f2a\7&\2\2\u0f2a\u0f2b\5\u010a\u0086\2\u0f2b\u0f2c\7+\2"+
		"\2\u0f2c\u0f2d\b\u0084\1\2\u0f2d\u0f50\3\2\2\2\u0f2e\u0f2f\7%\2\2\u0f2f"+
		"\u0f30\b\u0084\1\2\u0f30\u0f31\7K\2\2\u0f31\u0f32\7*\2\2\u0f32\u0f33\5"+
		"> \2\u0f33\u0f34\7&\2\2\u0f34\u0f35\5\u010a\u0086\2\u0f35\u0f36\7+\2\2"+
		"\u0f36\u0f37\b\u0084\1\2\u0f37\u0f50\3\2\2\2\u0f38\u0f39\7%\2\2\u0f39"+
		"\u0f3a\b\u0084\1\2\u0f3a\u0f3b\7L\2\2\u0f3b\u0f3c\7*\2\2\u0f3c\u0f3d\5"+
		"> \2\u0f3d\u0f3e\7&\2\2\u0f3e\u0f3f\5\u010a\u0086\2\u0f3f\u0f40\7+\2\2"+
		"\u0f40\u0f41\b\u0084\1\2\u0f41\u0f50\3\2\2\2\u0f42\u0f43\7%\2\2\u0f43"+
		"\u0f44\b\u0084\1\2\u0f44\u0f45\7M\2\2\u0f45\u0f46\7*\2\2\u0f46\u0f47\5"+
		"> \2\u0f47\u0f48\7&\2\2\u0f48\u0f49\5\u010a\u0086\2\u0f49\u0f4a\7+\2\2"+
		"\u0f4a\u0f4b\b\u0084\1\2\u0f4b\u0f50\3\2\2\2\u0f4c\u0f4d\5.\30\2\u0f4d"+
		"\u0f4e\b\u0084\1\2\u0f4e\u0f50\3\2\2\2\u0f4f\u0eec\3\2\2\2\u0f4f\u0ef1"+
		"\3\2\2\2\u0f4f\u0ef6\3\2\2\2\u0f4f\u0efb\3\2\2\2\u0f4f\u0f00\3\2\2\2\u0f4f"+
		"\u0f09\3\2\2\2\u0f4f\u0f12\3\2\2\2\u0f4f\u0f1b\3\2\2\2\u0f4f\u0f24\3\2"+
		"\2\2\u0f4f\u0f2e\3\2\2\2\u0f4f\u0f38\3\2\2\2\u0f4f\u0f42\3\2\2\2\u0f4f"+
		"\u0f4c\3\2\2\2\u0f50\u0107\3\2\2\2\u0f51\u0f52\5\u0106\u0084\2\u0f52\u0f53"+
		"\b\u0085\1\2\u0f53\u0f58\3\2\2\2\u0f54\u0f55\5\u010e\u0088\2\u0f55\u0f56"+
		"\b\u0085\1\2\u0f56\u0f58\3\2\2\2\u0f57\u0f51\3\2\2\2\u0f57\u0f54\3\2\2"+
		"\2\u0f58\u0109\3\2\2\2\u0f59\u0f5a\5\u0104\u0083\2\u0f5a\u0f5b\b\u0086"+
		"\1\2\u0f5b\u0f60\3\2\2\2\u0f5c\u0f5d\5\u010c\u0087\2\u0f5d\u0f5e\b\u0086"+
		"\1\2\u0f5e\u0f60\3\2\2\2\u0f5f\u0f59\3\2\2\2\u0f5f\u0f5c\3\2\2\2\u0f60"+
		"\u010b\3\2\2\2\u0f61\u0f72\5\u010e\u0088\2\u0f62\u0f63\7#\2\2\u0f63\u0f6b"+
		"\b\u0087\1\2\u0f64\u0f65\7$\2\2\u0f65\u0f6b\b\u0087\1\2\u0f66\u0f67\7"+
		" \2\2\u0f67\u0f6b\b\u0087\1\2\u0f68\u0f69\7!\2\2\u0f69\u0f6b\b\u0087\1"+
		"\2\u0f6a\u0f62\3\2\2\2\u0f6a\u0f64\3\2\2\2\u0f6a\u0f66\3\2\2\2\u0f6a\u0f68"+
		"\3\2\2\2\u0f6b\u0f6c\3\2\2\2\u0f6c\u0f6d\b\u0087\1\2\u0f6d\u0f6e\5\u010e"+
		"\u0088\2\u0f6e\u0f6f\b\u0087\1\2\u0f6f\u0f71\3\2\2\2\u0f70\u0f6a\3\2\2"+
		"\2\u0f71\u0f74\3\2\2\2\u0f72\u0f70\3\2\2\2\u0f72\u0f73\3\2\2\2\u0f73\u0f75"+
		"\3\2\2\2\u0f74\u0f72\3\2\2\2\u0f75\u0f76\b\u0087\1\2\u0f76\u0f8a\3\2\2"+
		"\2\u0f77\u0f84\5\u0108\u0085\2\u0f78\u0f79\7C\2\2\u0f79\u0f7d\b\u0087"+
		"\1\2\u0f7a\u0f7b\7\22\2\2\u0f7b\u0f7d\b\u0087\1\2\u0f7c\u0f78\3\2\2\2"+
		"\u0f7c\u0f7a\3\2\2\2\u0f7d\u0f7e\3\2\2\2\u0f7e\u0f7f\b\u0087\1\2\u0f7f"+
		"\u0f80\5\u0108\u0085\2\u0f80\u0f81\b\u0087\1\2\u0f81\u0f83\3\2\2\2\u0f82"+
		"\u0f7c\3\2\2\2\u0f83\u0f86\3\2\2\2\u0f84\u0f82\3\2\2\2\u0f84\u0f85\3\2"+
		"\2\2\u0f85\u0f87\3\2\2\2\u0f86\u0f84\3\2\2\2\u0f87\u0f88\b\u0087\1\2\u0f88"+
		"\u0f8a\3\2\2\2\u0f89\u0f61\3\2\2\2\u0f89\u0f77\3\2\2\2\u0f8a\u010d\3\2"+
		"\2\2\u0f8b\u0f8c\7*\2\2\u0f8c\u0f8d\5\u010c\u0087\2\u0f8d\u0f8e\7+\2\2"+
		"\u0f8e\u0f8f\b\u0088\1\2\u0f8f\u0fa0\3\2\2\2\u0f90\u0f91\7D\2\2\u0f91"+
		"\u0f92\b\u0088\1\2\u0f92\u0f93\5\u0108\u0085\2\u0f93\u0f94\b\u0088\1\2"+
		"\u0f94\u0fa0\3\2\2\2\u0f95\u0f96\7A\2\2\u0f96\u0f97\b\u0088\1\2\u0f97"+
		"\u0f98\5\u0108\u0085\2\u0f98\u0f99\b\u0088\1\2\u0f99\u0fa0\3\2\2\2\u0f9a"+
		"\u0f9b\7B\2\2\u0f9b\u0f9c\b\u0088\1\2\u0f9c\u0f9d\5\u0108\u0085\2\u0f9d"+
		"\u0f9e\b\u0088\1\2\u0f9e\u0fa0\3\2\2\2\u0f9f\u0f8b\3\2\2\2\u0f9f\u0f90"+
		"\3\2\2\2\u0f9f\u0f95\3\2\2\2\u0f9f\u0f9a\3\2\2\2\u0fa0\u010f\3\2\2\2\u0fa1"+
		"\u0fa2\7=\2\2\u0fa2\u0fa3\b\u0089\1\2\u0fa3\u0fa4\7\27\2\2\u0fa4\u0fa5"+
		"\7>\2\2\u0fa5\u0fa6\7V\2\2\u0fa6\u0fa7\7\"\2\2\u0fa7\u0fa8\b\u0089\1\2"+
		"\u0fa8\u0fa9\5\u0112\u008a\2\u0fa9\u0faa\b\u0089\1\2\u0faa\u0111\3\2\2"+
		"\2\u0fab\u0fbc\5\u0114\u008b\2\u0fac\u0fad\7#\2\2\u0fad\u0fb5\b\u008a"+
		"\1\2\u0fae\u0faf\7$\2\2\u0faf\u0fb5\b\u008a\1\2\u0fb0\u0fb1\7 \2\2\u0fb1"+
		"\u0fb5\b\u008a\1\2\u0fb2\u0fb3\7!\2\2\u0fb3\u0fb5\b\u008a\1\2\u0fb4\u0fac"+
		"\3\2\2\2\u0fb4\u0fae\3\2\2\2\u0fb4\u0fb0\3\2\2\2\u0fb4\u0fb2\3\2\2\2\u0fb5"+
		"\u0fb6\3\2\2\2\u0fb6\u0fb7\b\u008a\1\2\u0fb7\u0fb8\5\u0114\u008b\2\u0fb8"+
		"\u0fb9\b\u008a\1\2\u0fb9\u0fbb\3\2\2\2\u0fba\u0fb4\3\2\2\2\u0fbb\u0fbe"+
		"\3\2\2\2\u0fbc\u0fba\3\2\2\2\u0fbc\u0fbd\3\2\2\2\u0fbd\u0fbf\3\2\2\2\u0fbe"+
		"\u0fbc\3\2\2\2\u0fbf\u0fc0\b\u008a\1\2\u0fc0\u0113\3\2\2\2\u0fc1\u0fc2"+
		"\7*\2\2\u0fc2\u0fc3\5\u0112\u008a\2\u0fc3\u0fc4\7+\2\2\u0fc4\u0fc5\b\u008b"+
		"\1\2\u0fc5\u104b\3\2\2\2\u0fc6\u0fc7\7%\2\2\u0fc7\u0fc8\b\u008b\1\2\u0fc8"+
		"\u0fc9\5\u0112\u008a\2\u0fc9\u0fca\b\u008b\1\2\u0fca\u104b\3\2\2\2\u0fcb"+
		"\u0fcc\7@\2\2\u0fcc\u0fcd\b\u008b\1\2\u0fcd\u0fce\5\u0116\u008c\2\u0fce"+
		"\u0fcf\b\u008b\1\2\u0fcf\u104b\3\2\2\2\u0fd0\u0fd1\7?\2\2\u0fd1\u0fd2"+
		"\b\u008b\1\2\u0fd2\u0fd3\5\u0116\u008c\2\u0fd3\u0fd4\b\u008b\1\2\u0fd4"+
		"\u104b\3\2\2\2\u0fd5\u0fd6\7O\2\2\u0fd6\u0fd7\b\u008b\1\2\u0fd7\u0fd8"+
		"\7*\2\2\u0fd8\u0fd9\5> \2\u0fd9\u0fda\7&\2\2\u0fda\u0fdb\5\u0118\u008d"+
		"\2\u0fdb\u0fdc\7+\2\2\u0fdc\u0fdd\b\u008b\1\2\u0fdd\u104b\3\2\2\2\u0fde"+
		"\u0fdf\7K\2\2\u0fdf\u0fe0\b\u008b\1\2\u0fe0\u0fe1\7*\2\2\u0fe1\u0fe2\5"+
		"> \2\u0fe2\u0fe3\7&\2\2\u0fe3\u0fe4\5\u0118\u008d\2\u0fe4\u0fe5\7+\2\2"+
		"\u0fe5\u0fe6\b\u008b\1\2\u0fe6\u104b\3\2\2\2\u0fe7\u0fe8\7L\2\2\u0fe8"+
		"\u0fe9\b\u008b\1\2\u0fe9\u0fea\7*\2\2\u0fea\u0feb\5> \2\u0feb\u0fec\7"+
		"&\2\2\u0fec\u0fed\5\u0118\u008d\2\u0fed\u0fee\7+\2\2\u0fee\u0fef\b\u008b"+
		"\1\2\u0fef\u104b\3\2\2\2\u0ff0\u0ff1\7M\2\2\u0ff1\u0ff2\b\u008b\1\2\u0ff2"+
		"\u0ff3\7*\2\2\u0ff3\u0ff4\5> \2\u0ff4\u0ff5\7&\2\2\u0ff5\u0ff6\5\u0118"+
		"\u008d\2\u0ff6\u0ff7\7+\2\2\u0ff7\u0ff8\b\u008b\1\2\u0ff8\u104b\3\2\2"+
		"\2\u0ff9\u0ffa\7%\2\2\u0ffa\u0ffb\b\u008b\1\2\u0ffb\u0ffc\7O\2\2\u0ffc"+
		"\u0ffd\7*\2\2\u0ffd\u0ffe\5> \2\u0ffe\u0fff\7&\2\2\u0fff\u1000\5\u0118"+
		"\u008d\2\u1000\u1001\7+\2\2\u1001\u1002\b\u008b\1\2\u1002\u104b\3\2\2"+
		"\2\u1003\u1004\7%\2\2\u1004\u1005\b\u008b\1\2\u1005\u1006\7K\2\2\u1006"+
		"\u1007\7*\2\2\u1007\u1008\5> \2\u1008\u1009\7&\2\2\u1009\u100a\5\u0118"+
		"\u008d\2\u100a\u100b\7+\2\2\u100b\u100c\b\u008b\1\2\u100c\u104b\3\2\2"+
		"\2\u100d\u100e\7%\2\2\u100e\u100f\b\u008b\1\2\u100f\u1010\7L\2\2\u1010"+
		"\u1011\7*\2\2\u1011\u1012\5> \2\u1012\u1013\7&\2\2\u1013\u1014\5\u0118"+
		"\u008d\2\u1014\u1015\7+\2\2\u1015\u1016\b\u008b\1\2\u1016\u104b\3\2\2"+
		"\2\u1017\u1018\7%\2\2\u1018\u1019\b\u008b\1\2\u1019\u101a\7M\2\2\u101a"+
		"\u101b\7*\2\2\u101b\u101c\5> \2\u101c\u101d\7&\2\2\u101d\u101e\5\u0118"+
		"\u008d\2\u101e\u101f\7+\2\2\u101f\u1020\b\u008b\1\2\u1020\u104b\3\2\2"+
		"\2\u1021\u1022\7P\2\2\u1022\u1023\b\u008b\1\2\u1023\u1024\7*\2\2\u1024"+
		"\u1025\5> \2\u1025\u1026\7&\2\2\u1026\u1027\5\u0118\u008d\2\u1027\u1028"+
		"\7+\2\2\u1028\u1029\b\u008b\1\2\u1029\u104b\3\2\2\2\u102a\u102b\7N\2\2"+
		"\u102b\u102c\b\u008b\1\2\u102c\u102d\7*\2\2\u102d\u102e\5> \2\u102e\u102f"+
		"\7&\2\2\u102f\u1030\5\u0118\u008d\2\u1030\u1031\7+\2\2\u1031\u1032\b\u008b"+
		"\1\2\u1032\u104b\3\2\2\2\u1033\u1034\7%\2\2\u1034\u1035\b\u008b\1\2\u1035"+
		"\u1036\7P\2\2\u1036\u1037\7*\2\2\u1037\u1038\5> \2\u1038\u1039\7&\2\2"+
		"\u1039\u103a\5\u0118\u008d\2\u103a\u103b\7+\2\2\u103b\u103c\b\u008b\1"+
		"\2\u103c\u104b\3\2\2\2\u103d\u103e\7%\2\2\u103e\u103f\b\u008b\1\2\u103f"+
		"\u1040\7N\2\2\u1040\u1041\7*\2\2\u1041\u1042\5> \2\u1042\u1043\7&\2\2"+
		"\u1043\u1044\5\u0118\u008d\2\u1044\u1045\7+\2\2\u1045\u1046\b\u008b\1"+
		"\2\u1046\u104b\3\2\2\2\u1047\u1048\5.\30\2\u1048\u1049\b\u008b\1\2\u1049"+
		"\u104b\3\2\2\2\u104a\u0fc1\3\2\2\2\u104a\u0fc6\3\2\2\2\u104a\u0fcb\3\2"+
		"\2\2\u104a\u0fd0\3\2\2\2\u104a\u0fd5\3\2\2\2\u104a\u0fde\3\2\2\2\u104a"+
		"\u0fe7\3\2\2\2\u104a\u0ff0\3\2\2\2\u104a\u0ff9\3\2\2\2\u104a\u1003\3\2"+
		"\2\2\u104a\u100d\3\2\2\2\u104a\u1017\3\2\2\2\u104a\u1021\3\2\2\2\u104a"+
		"\u102a\3\2\2\2\u104a\u1033\3\2\2\2\u104a\u103d\3\2\2\2\u104a\u1047\3\2"+
		"\2\2\u104b\u0115\3\2\2\2\u104c\u104d\5\u0114\u008b\2\u104d\u104e\b\u008c"+
		"\1\2\u104e\u1053\3\2\2\2\u104f\u1050\5\u011c\u008f\2\u1050\u1051\b\u008c"+
		"\1\2\u1051\u1053\3\2\2\2\u1052\u104c\3\2\2\2\u1052\u104f\3\2\2\2\u1053"+
		"\u0117\3\2\2\2\u1054\u1055\5\u0112\u008a\2\u1055\u1056\b\u008d\1\2\u1056"+
		"\u105b\3\2\2\2\u1057\u1058\5\u011a\u008e\2\u1058\u1059\b\u008d\1\2\u1059"+
		"\u105b\3\2\2\2\u105a\u1054\3\2\2\2\u105a\u1057\3\2\2\2\u105b\u0119\3\2"+
		"\2\2\u105c\u106d\5\u011c\u008f\2\u105d\u105e\7#\2\2\u105e\u1066\b\u008e"+
		"\1\2\u105f\u1060\7$\2\2\u1060\u1066\b\u008e\1\2\u1061\u1062\7 \2\2\u1062"+
		"\u1066\b\u008e\1\2\u1063\u1064\7!\2\2\u1064\u1066\b\u008e\1\2\u1065\u105d"+
		"\3\2\2\2\u1065\u105f\3\2\2\2\u1065\u1061\3\2\2\2\u1065\u1063\3\2\2\2\u1066"+
		"\u1067\3\2\2\2\u1067\u1068\b\u008e\1\2\u1068\u1069\5\u011c\u008f\2\u1069"+
		"\u106a\b\u008e\1\2\u106a\u106c\3\2\2\2\u106b\u1065\3\2\2\2\u106c\u106f"+
		"\3\2\2\2\u106d\u106b\3\2\2\2\u106d\u106e\3\2\2\2\u106e\u1070\3\2\2\2\u106f"+
		"\u106d\3\2\2\2\u1070\u1071\b\u008e\1\2\u1071\u1085\3\2\2\2\u1072\u107f"+
		"\5\u0116\u008c\2\u1073\u1074\7C\2\2\u1074\u1078\b\u008e\1\2\u1075\u1076"+
		"\7\22\2\2\u1076\u1078\b\u008e\1\2\u1077\u1073\3\2\2\2\u1077\u1075\3\2"+
		"\2\2\u1078\u1079\3\2\2\2\u1079\u107a\b\u008e\1\2\u107a\u107b\5\u0116\u008c"+
		"\2\u107b\u107c\b\u008e\1\2\u107c\u107e\3\2\2\2\u107d\u1077\3\2\2\2\u107e"+
		"\u1081\3\2\2\2\u107f\u107d\3\2\2\2\u107f\u1080\3\2\2\2\u1080\u1082\3\2"+
		"\2\2\u1081\u107f\3\2\2\2\u1082\u1083\b\u008e\1\2\u1083\u1085\3\2\2\2\u1084"+
		"\u105c\3\2\2\2\u1084\u1072\3\2\2\2\u1085\u011b\3\2\2\2\u1086\u1087\7*"+
		"\2\2\u1087\u1088\5\u011a\u008e\2\u1088\u1089\7+\2\2\u1089\u108a\b\u008f"+
		"\1\2\u108a\u109b\3\2\2\2\u108b\u108c\7D\2\2\u108c\u108d\b\u008f\1\2\u108d"+
		"\u108e\5\u0116\u008c\2\u108e\u108f\b\u008f\1\2\u108f\u109b\3\2\2\2\u1090"+
		"\u1091\7A\2\2\u1091\u1092\b\u008f\1\2\u1092\u1093\5\u0116\u008c\2\u1093"+
		"\u1094\b\u008f\1\2\u1094\u109b\3\2\2\2\u1095\u1096\7B\2\2\u1096\u1097"+
		"\b\u008f\1\2\u1097\u1098\5\u0116\u008c\2\u1098\u1099\b\u008f\1\2\u1099"+
		"\u109b\3\2\2\2\u109a\u1086\3\2\2\2\u109a\u108b\3\2\2\2\u109a\u1090\3\2"+
		"\2\2\u109a\u1095\3\2\2\2\u109b\u011d\3\2\2\2\u109c\u109d\7V\2\2\u109d"+
		"\u10a1\b\u0090\1\2\u109e\u109f\7)\2\2\u109f\u10a0\7V\2\2\u10a0\u10a2\b"+
		"\u0090\1\2\u10a1\u109e\3\2\2\2\u10a1\u10a2\3\2\2\2\u10a2\u10a3\3\2\2\2"+
		"\u10a3\u10a4\b\u0090\1\2\u10a4\u011f\3\2\2\2\u10a5\u10a6\5\u0122\u0092"+
		"\2\u10a6\u10a7\b\u0091\1\2\u10a7\u10b0\3\2\2\2\u10a8\u10a9\5\u0124\u0093"+
		"\2\u10a9\u10aa\b\u0091\1\2\u10aa\u10b0\3\2\2\2\u10ab\u10ac\7\23\2\2\u10ac"+
		"\u10b0\b\u0091\1\2\u10ad\u10ae\7\r\2\2\u10ae\u10b0\b\u0091\1\2\u10af\u10a5"+
		"\3\2\2\2\u10af\u10a8\3\2\2\2\u10af\u10ab\3\2\2\2\u10af\u10ad\3\2\2\2\u10b0"+
		"\u0121\3\2\2\2\u10b1\u10b2\7Q\2\2\u10b2\u10b3\b\u0092\1\2\u10b3\u0123"+
		"\3\2\2\2\u10b4\u10b5\7R\2\2\u10b5\u10b6\b\u0093\1\2\u10b6\u0125\3\2\2"+
		"\2\u10b7\u10b8\t\2\2\2\u10b8\u0127\3\2\2\2\u10b9\u10ba\7V\2\2\u10ba\u10be"+
		"\b\u0095\1\2\u10bb\u10bc\7\f\2\2\u10bc\u10be\b\u0095\1\2\u10bd\u10b9\3"+
		"\2\2\2\u10bd\u10bb\3\2\2\2\u10be\u10c8\3\2\2\2\u10bf\u10c4\7&\2\2\u10c0"+
		"\u10c1\7V\2\2\u10c1\u10c5\b\u0095\1\2\u10c2\u10c3\7\f\2\2\u10c3\u10c5"+
		"\b\u0095\1\2\u10c4\u10c0\3\2\2\2\u10c4\u10c2\3\2\2\2\u10c5\u10c7\3\2\2"+
		"\2\u10c6\u10bf\3\2\2\2\u10c7\u10ca\3\2\2\2\u10c8\u10c6\3\2\2\2\u10c8\u10c9"+
		"\3\2\2\2\u10c9\u10cc\3\2\2\2\u10ca\u10c8\3\2\2\2\u10cb\u10cd\7&\2\2\u10cc"+
		"\u10cb\3\2\2\2\u10cc\u10cd\3\2\2\2\u10cd\u10cf\3\2\2\2\u10ce\u10bd\3\2"+
		"\2\2\u10ce\u10cf\3\2\2\2\u10cf\u10d0\3\2\2\2\u10d0\u10d1\b\u0095\1\2\u10d1"+
		"\u0129\3\2\2\2\u10d2\u10d3\7V\2\2\u10d3\u10d7\b\u0096\1\2\u10d4\u10d5"+
		"\7\f\2\2\u10d5\u10d7\b\u0096\1\2\u10d6\u10d2\3\2\2\2\u10d6\u10d4\3\2\2"+
		"\2\u10d7\u10db\3\2\2\2\u10d8\u10d9\7)\2\2\u10d9\u10da\7V\2\2\u10da\u10dc"+
		"\b\u0096\1\2\u10db\u10d8\3\2\2\2\u10db\u10dc\3\2\2\2\u10dc\u10dd\3\2\2"+
		"\2\u10dd\u10de\b\u0096\1\2\u10de\u012b\3\2\2\2\u10df\u10e0\5*\26\2\u10e0"+
		"\u10e7\b\u0097\1\2\u10e1\u10e2\7&\2\2\u10e2\u10e3\5*\26\2\u10e3\u10e4"+
		"\b\u0097\1\2\u10e4\u10e6\3\2\2\2\u10e5\u10e1\3\2\2\2\u10e6\u10e9\3\2\2"+
		"\2\u10e7\u10e5\3\2\2\2\u10e7\u10e8\3\2\2\2\u10e8\u10eb\3\2\2\2\u10e9\u10e7"+
		"\3\2\2\2\u10ea\u10ec\7&\2\2\u10eb\u10ea\3\2\2\2\u10eb\u10ec\3\2\2\2\u10ec"+
		"\u10ee\3\2\2\2\u10ed\u10df\3\2\2\2\u10ed\u10ee\3\2\2\2\u10ee\u10ef\3\2"+
		"\2\2\u10ef\u10f0\b\u0097\1\2\u10f0\u012d\3\2\2\2\u00c7\u013b\u0142\u014b"+
		"\u0158\u015a\u0165\u016e\u0174\u017b\u018b\u0198\u019b\u01a7\u01ab\u01b2"+
		"\u01b9\u01c4\u01c8\u01d8\u01dc\u01e3\u01f1\u01f8\u0201\u020b\u0213\u0220"+
		"\u022f\u0236\u0249\u0250\u025b\u0262\u0271\u0278\u0282\u0288\u029c\u02ab"+
		"\u02b5\u02fa\u0316\u031e\u033f\u0359\u0361\u03ce\u03dd\u03e5\u0452\u046c"+
		"\u0474\u0507\u0516\u051e\u05b1\u05c4\u05cc\u060a\u061d\u0625\u0687\u069a"+
		"\u06a2\u0716\u072a\u0732\u0749\u0751\u075c\u0764\u076e\u0776\u077b\u0791"+
		"\u07a6\u07ae\u07e9\u07f1\u07f9\u0804\u080c\u0816\u081e\u0823\u0839\u084e"+
		"\u0856\u08a3\u08ab\u08b3\u08be\u08c6\u08d0\u08d8\u08dd\u08f3\u090d\u0915"+
		"\u0936\u0950\u0958\u09a1\u09b0\u09b8\u0a01\u0a1b\u0a23\u0a80\u0a8f\u0a97"+
		"\u0af4\u0b07\u0b0f\u0b4d\u0b60\u0b68\u0bce\u0be1\u0be9\u0c63\u0c77\u0c7f"+
		"\u0c96\u0c9e\u0ca9\u0cb1\u0cbb\u0cc3\u0cc8\u0cde\u0cf3\u0cfb\u0d3a\u0d42"+
		"\u0d4a\u0d55\u0d5d\u0d67\u0d6f\u0d74\u0d8a\u0d9f\u0da7\u0dfa\u0e02\u0e0a"+
		"\u0e15\u0e1d\u0e27\u0e2f\u0e34\u0e4a\u0e5e\u0e66\u0e82\u0e8a\u0e95\u0e9d"+
		"\u0ea7\u0eaf\u0eb4\u0eca\u0edf\u0ee7\u0f4f\u0f57\u0f5f\u0f6a\u0f72\u0f7c"+
		"\u0f84\u0f89\u0f9f\u0fb4\u0fbc\u104a\u1052\u105a\u1065\u106d\u1077\u107f"+
		"\u1084\u109a\u10a1\u10af\u10bd\u10c4\u10c8\u10cc\u10ce\u10d6\u10db\u10e7"+
		"\u10eb\u10ed";
	public static final String _serializedATN = Utils.join(
		new String[] {
			_serializedATNSegment0,
			_serializedATNSegment1
		},
		""
	);
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}