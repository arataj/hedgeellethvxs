dtmc 

// bus 
const int DESTINATIONS = 2; 
const int SLOTS = 1 + DESTINATIONS; 
const int SUBSLOTS = 4; 
const int TICK_RATE = 10; 
const int TICK_PHASES = 4; 

const int BUFFER_SIZE = 4; 

const double switch_RATE = 1.0; 
const double bridge_RATE = 1.0; 

global s0_0 = 0; 
global s0_1 = 0; 
global s0_2 = 0; 
global s0_3 = 0; 
global s1_0 = 0; 
global s1_1 = 0; 
global s1_2 = 0; 
global s1_3 = 0; 
global s2_0 = 0; 
global s2_1 = 0; 
global s2_2 = 0; 
global s2_3 = 0; 

module bus 
    s0 : [0..TICK_PHASES] init 0; 

    // tick 
    [] s0<TICK_PHASES-1 -> TICK_RATE*TICK_PHASES:(s0'=s0+1); 
    // bus shift 
    [] s0=TICK_PHASES-1 -> TICK_RATE*TICK_PHASES:(s0'=0) 
            &(s2_0'=s1_0) 
            &(s2_1'=s1_1) 
            &(s2_2'=s1_2) 
            &(s2_3'=s1_3) 
            &(s1_0'=s0_0) 
            &(s1_1'=s0_1) 
            &(s1_2'=s0_2) 
            &(s1_3'=s0_3) 
            ; 
endmodule 


module source_switch 
    s1 : [0..5] init 0; 
    queue_switch_0 : [0..BUFFER_SIZE] init 0; 
    queue_switch_1 : [0..BUFFER_SIZE] init 0; 

    [] s1=0 -> 
	switch_RATE/DESTINATIONS: queue_switch_0'= min(BUFFER_SIZE,queue_switch_0+1) + 
	switch_RATE/DESTINATIONS: queue_switch_1'= min(BUFFER_SIZE,queue_switch_1+1); 
endmodule 

module source_bridge 
    s1 : [0..5] init 0; 
    queue_bridge_0 : [0..BUFFER_SIZE] init 0; 
    queue_bridge_1 : [0..BUFFER_SIZE] init 0; 

    [] s1=0 -> 
	bridge_RATE/DESTINATIONS: queue_bridge_0'= min(BUFFER_SIZE,queue_bridge_0+1) + 
	bridge_RATE/DESTINATIONS: queue_bridge_1'= min(BUFFER_SIZE,queue_bridge_1+1); 
endmodule 

