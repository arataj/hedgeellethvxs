#String COL_F := "m{2.0505cm}";
#String V_BOX := "\\strut{}";
#String C_F := (verics.D.COMPRESS ? "\\cf" else "") + V_BOX;
\noindent\begin{tabular}{|${COL_F}${COL_F}${COL_F}${COL_F}${COL_F}|}
\multicolumn{2}{@{}l}{${verics.D.LABEL <> null ? "\\label{" + verics.D.LABEL + "}" else ""}\ef
${verics.D.INFINITIVE}
${$PRONOMINAL ? "(se)" else ""}\vphantom{Ay} {\tf ${verics.D.GROUP}}} & \multicolumn{3}{r@{}}{${verics.D.TRANSLATION}} \\[1mm]
#if !verics.D.COMMENT.isEmpty() {
\multicolumn{5}{@{}>{\setlength{\baselineskip}{0.7\baselineskip}}m{11.25cm}@{}}{\exf\textit{${verics.D.COMMENT}}} \\[0mm]
#}
\hline
\rowcolor{colorHead}
#String T_F := "\\tf\\color{white}\\contour{white}{\\strut{}";
	{${T_F}présent}} &
	{${T_F}imparfait}} &
	{${T_F}futur}} &
	{${T_F}conditionel}} &
	{${T_F}subjonctive}} \\[-0.5mm]
\rowcolor{white}& & & &\\[-3.5mm]
#for Z n~ := 1; n <= 2; ++n {
#    for Z p~ := 1; p <= 3; ++p {
#        Z i := (n - 1)*3 + (p - 1);
         {${C_F}${verics.D.PRÉSENT[i]}} &
         {${C_F}${verics.D.IMPARFAIT[i]}} &
         {${C_F}${verics.D.FUTUR[i]}} &
         {${C_F}${verics.D.CONDITIONEL[i]}} &
         {${C_F}${verics.D.SUBJONCTIVE[i]}} \\
#    }
#}
\rowcolor{colorHead}
	{${T_F}part. prés.}} &
	{${T_F}part. passé}} &
	{${T_F}imp. 2s}} &
	{${T_F}imp. 1p}} &
	{${T_F}imp. 2p}} \\[-0.5mm]
\rowcolor{white}& & & &\\[-3.5mm]
{${C_F}${verics.D.PARTICIPE_PRÉSENT}} &
{${C_F}${verics.D.PARTICIPE_PASSÉ}} & 
#for Z i~ := 0; i < 3; ++i {
    {${C_F}${verics.D.IMPERATIVE[i]}} ${i < 3 - 1 ? "&" else "\\\\"}
#}
\hline
\end{tabular}

#if !verics.D.COMME_T.isEmpty() || verics.D.SIMILAR.size() <> 0 {
{\tf ${verics.D.COMME_T}} ${verics.D.COMME_VERBES}${V_BOX}
#    if verics.D.SIMILAR.size() <> 0 {
#        if $PEU_SIMILAR {
\hfill{\tf sim.~à} \squiggly{${verics.D.SIMILAR}}
#        } else {
\hfill{\tf sim.~à} ${verics.D.SIMILAR}
#        }
#    }
#}

\begin{example}
${verics.D.EXAMPLE.isEmpty() ? "~\\strut\\\\\n~" else
	("\\strut " + verics.D.EXAMPLE + "\\strut ")}\\[-1mm]
\end{example}
