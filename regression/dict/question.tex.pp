#String SKIP := "-3.70mm";
#Z T1 := 45;
#Z T2 := 60;
#Z T3 := 75;
#Z size := verics.D.TRANSLATION.size() +
#	verics.D.FORM.size()*2 +
#	verics.D.VERB_PREFIX.size() +
#	verics.D.ANSWER.size();
#String translation~ := $TRANSLATION;
#if !$IS_VERB {
#    translation := "\\fcolorbox{white}{colorHeadContour}{\\raisebox{0mm}[1.5mm][-1mm]{\\hspace{-1mm}}} " +
#        translation;
#}
#if size > T2 {
{\ccf ${translation}}
#} else if size > T1 {
{\cf ${translation}}
#} else {
${translation}
#}
#String tf;
#if size > T2 {
#    tf := "\\tfc";
#} else {
#    tf := "\\tf";
#}
#String br;
#if size > T3 || verics.D.ANSWER.size() > 20 {
#    br := "\\\\[" + SKIP + "]\n\n\\hspace*{7mm}";
#} else {
#    br := "";
#}
#if !verics.D.FORM.isEmpty() { print("\\fcolorbox{white}{colorFormHighlight}{\\raisebox{0mm}[1.5mm][-1mm]{" + tf + " " + verics.D.FORM + "}}"); } else { print("~"); }
{}
#String answer~ := $ANSWER;
#if($INDEX_ONLY) {
#    //answer := "\\underline{\\smash{" + answer + "}}";
#    answer := "\\varul{" + answer + "}";
#}
#if size > T3 && br.isEmpty() {
{\ccf ${verics.D.VERB_PREFIX}\hfill{}${answer}}\\[${SKIP}]
#} else if size > T2 && br.isEmpty() {
{\cf ${verics.D.VERB_PREFIX}\hfill{}${answer}}\\[${SKIP}]
#} else {
${br + verics.D.VERB_PREFIX}{${br.isEmpty() ? "" else "\\raggedleft"}\hfill{}${answer}}\\[${SKIP}]
#}
