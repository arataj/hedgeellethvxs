//
// Test "Faulty train protocol" for any number of trains
//
class pomdp sync;

// number of trains
common Z NUM := 3;

environment {
    common{environment, train4} Z{GREEN, RED} lights := GREEN;

    [out1] lights == RED   -> lights := GREEN;
    [in1]  lights == GREEN -> lights := RED;
    [out2] lights == RED   -> lights := GREEN;
    [in2]  lights == GREEN -> lights := RED;
};

agent train1 {
    common{} Z{WAIT, TUNNEL, AWAY} phase := AWAY;

    []              true -> true;
    [approach1]  phase == AWAY -> phase := WAIT;
    // the train <code>NUM</code> is faulty
    [in1]  phase == WAIT   -> phase := TUNNEL;
    [out1]  phase == TUNNEL -> phase := AWAY;
};

def in_tunnel1 := train1.phase == TUNNEL;

agent train2 {
    common{train2} Z{WAIT, TUNNEL, AWAY} phase := AWAY;

    []              true -> true;
    [approach2]  phase == AWAY -> phase := WAIT;
    // the train <code>NUM</code> is faulty
    [in2]  phase == WAIT   -> phase := TUNNEL;
    [out2]  phase == TUNNEL -> phase := AWAY;
};

def in_tunnel2 := train2.phase == TUNNEL;

agent train3 {
    common{train2} Z{WAIT, TUNNEL, AWAY} phase := AWAY;

    []              true -> true;
    [approach3]  phase == AWAY -> phase := WAIT;
    // the train <code>NUM</code> is faulty
    [in3]  phase == WAIT   -> phase := TUNNEL;
    [out3]  phase == TUNNEL -> phase := AWAY;
};

agent train_ {
    common Z{WAIT, TUNNEL, AWAY} phase := AWAY;

    []              true -> true;
    [approach3]  phase == AWAY -> phase := WAIT;
    // the train <code>NUM</code> is faulty
    [in3]  phase == WAIT   -> phase := TUNNEL;
    [out3]  phase == TUNNEL -> phase := AWAY;
};

def in_tunnel3 := train3.phase == TUNNEL;

property CTL*K test_train1 := !A G(K(train1,
              (!in_tunnel1 || !in_tunnel2)
             && (!in_tunnel1 || !in_tunnel3)
             && (!in_tunnel2 || !in_tunnel3)
));
