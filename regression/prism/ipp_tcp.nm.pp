//
// Self--similar packet arrival. Packet service
// time dependent on packet length, which
// approximates values from a real TCP network.
//
ctmc

//
// Constants
//

// mean service time of a packet
const double SERVICE_MEAN = 1;
// maximum buffer size
const int MAX_BUFFER = 64;
// number of service time Erlang branches
const int BRANCHES = 3;

#Z IPP_TYPE;
#switch(verics.D.HURST) {
#    case 60:
#        switch(verics.D.LAMBDA) {
#            case 50:
#               IPP_TYPE := IPP.INDEX_0p5_60;
#               break;
#            case 80:
#               IPP_TYPE := IPP.INDEX_0p8_60;
#               break;
#            default:
#               assert else "unknown lambda";
#        }
#        break;
#    case 80:
#        switch(verics.D.LAMBDA) {
#            case 50:
#               IPP_TYPE := IPP.INDEX_0p5_80;
#               break;
#            case 80:
#               IPP_TYPE := IPP.INDEX_0p8_80;
#               break;
#            default:
#               assert else "unknown lambda";
#        }
#        break;
#    default:
#        assert else "unknown Hurst coeficient";
#}
#for Z i~ := 0; i < IPP.BRANCHES; ++i {
module ipp_generator${i}
#    if IPP.RATE[IPP_TYPE][i][2] > 1e-4 {
  switch${i} : [0..2] init 0;

#        R fast := 10/$min(IPP.RATE[IPP_TYPE][i][3], IPP.RATE[IPP_TYPE][i][2]);
  []           switch${i}=0 ->
			${IPP.RATE[IPP_TYPE][i][3]*fast}:(switch${i}'=1) +
			${IPP.RATE[IPP_TYPE][i][2]*fast}:(switch${i}'=2);
  [packet${i}] switch${i}=1 -> ${IPP.RATE[IPP_TYPE][i][0]}:true;
  []           switch${i}=1 -> ${IPP.RATE[IPP_TYPE][i][2]}:(switch${i}'=2);
  []           switch${i}=2 -> ${IPP.RATE[IPP_TYPE][i][1]}:true +
                  ${IPP.RATE[IPP_TYPE][i][3]}:(switch${i}'=1);
#    } else {
  [packet${i}] true -> ${IPP.RATE[IPP_TYPE][i][0]*IPP.RATE[IPP_TYPE][i][3]/IPP.RATE[IPP_TYPE][i][2]}:true;
#    }
endmodule

#}
#// Hypererlang parameters
#pv$ R[] PI := {0.4078, 0.2288, 0.3634};
#pv$ R[] RATE := {616.25, 954.43, 3.267};
#pv$ Z[] NUM := {21, 1387, 2};
#// normalisation
#R mean~ := 0;
#for Z i~ := 0; i < .BRANCHES; ++i {
#    mean += .PI[i]*.NUM[i]/.RATE[i];
#}
#for Z i~ := 0; i < .BRANCHES; ++i {
#    .RATE[i] *= mean/.SERVICE_MEAN;
#}
#pv$ Z getMaxPhases() {
#    Z max~ := 0;
#    for Z i~ := 0; i < .BRANCHES; ++i {
#        if max < .NUM[i] {
#            max := .NUM[i];
#        }
#    }
#    return max;
#}
#
module tcp_service
  length : [0..MAX_BUFFER] init 0;
  branch : [0..BRANCHES] init 0;
  phase : [0..${getMaxPhases()}] init 0;

#for Z i~ := 0; i < IPP.BRANCHES; ++i {
  [packet${i}] length<MAX_BUFFER -> 1:(length'=length+1);
#}
  [] branch=0 & length>0 ->
#for Z i~ := 0; i < .BRANCHES; ++i {
           ${.PI[i]}*${.RATE[i]}:(branch'=${i + 1})&(phase'=1) ${i < .BRANCHES - 1 ? "+" else ";"}
#}
#for Z i~ := 0; i < .BRANCHES; ++i {
  [] branch=${i + 1} & phase<${.NUM[i] - 1} -> ${.RATE[i]}:(phase'=phase+1);
  [] branch=${i + 1} & phase=${.NUM[i] - 1} & length>0 ->
          ${.RATE[i]}:(phase'=0)&(branch'=0)&(length'=length-1);
#}
endmodule

rewards "queue_size"
        true : length;
endrewards
