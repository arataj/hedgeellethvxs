// 
// A model of RED with self--similar packet 
// arrival. 
// 
// An example command to extract average 
// buffer length and drop ratio: 
// 
// veric red.nm.pp -p > red.pctl && \ 
//         prism red.nm red.pctl -nofixdl | \ 
//         grep --line-buffered Result | \ 
//         awk '{if(NR % 2 == 0){}else{print (NR - 1)/2*5}; print $2; system("")}' | \ 
//         xargs -n3 
// 
ctmc 

// 
// Constants 
// 

// number of phases of the generator 
const int An = 5; 
// estimated traffic parameters for 
// the Ethernet 
//const double Aa = 11.249898; 
//const double Aq = 0.684268; 
// estimated traffic parameters for 
// MPEG-2 
const double Aa = 2.755029; 
const double Aq = 1.075841; 
// maximum buffer size 
const int MAX_BUFFER = 8; 
// RED parameters 
const int T_MIN = 4; 
const int T_MAX = 6; 
const double P_MAX = 0.8; 
// service parameters 
const double P_SINK = 0.1; 
// avg parameter 
const double w = 0.2; 
// precision of avg 
const int AVG_MULT = 100; 

// 
// Traffic generator 
// 


module traffic 
  phase : [1..An+1] init 1; 

  [tick] phase=1 -> 
            0.3629725857695146:(phase'=2) + 
            0.13174909802020762:(phase'=3) + 
            0.047821310781196:(phase'=4) + 
            0.017357824829138276:(phase'=5) + 
            0.4400991805999436:true; 
  [tick] phase=2 -> 0.39050078964686036:(phase'=6) + 
            0.6094992103531396:true; 
  [tick] phase=3 -> 0.15249086671482148:(phase'=6) + 
            0.8475091332851785:true; 
  [tick] phase=4 -> 0.059547803866071926:(phase'=6) + 
            0.940452196133928:true; 
  [tick] phase=5 -> 0.02325346443143745:(phase'=6) + 
            0.9767465355685625:true; 
  [arrive] phase=An+1 -> (phase'=1); 
endmodule 

// 
// RED 
// 


module red 
  length : [0..MAX_BUFFER] init 0; 
  avg : [0..MAX_BUFFER*AVG_MULT] init 0; 
  drop : [0..1] init 0; 

  [arrive] floor(avg/AVG_MULT + 0.5)<=T_MIN -> (length'=min(MAX_BUFFER, length+1))&(drop'=0)&(avg'=floor(((1-w)*(avg/AVG_MULT)+w*length)*AVG_MULT+0.5)); 
  [arrive] floor(avg/AVG_MULT + 0.5)=5 -> 
              0.6:(length'=min(MAX_BUFFER, length+1))&(drop'=0)&(avg'=floor(((1-w)*(avg/AVG_MULT)+w*length)*AVG_MULT+0.5)) + 
              0.4:(drop'=1)&(avg'=floor(((1-w)*(avg/AVG_MULT)+w*length)*AVG_MULT+0.5)); 
  [arrive] floor(avg/AVG_MULT + 0.5)=6 -> 
              0.19999999999999996:(length'=min(MAX_BUFFER, length+1))&(drop'=0)&(avg'=floor(((1-w)*(avg/AVG_MULT)+w*length)*AVG_MULT+0.5)) + 
              0.8:(drop'=1)&(avg'=floor(((1-w)*(avg/AVG_MULT)+w*length)*AVG_MULT+0.5)); 
  [arrive] floor(avg/AVG_MULT + 0.5)>T_MAX -> (drop'=1)&(avg'=floor(((1-w)*(avg/AVG_MULT)+w*length)*AVG_MULT+0.5)); 
  [tick] length=0 -> true; 
  [tick] length>0 -> P_SINK:(length'=length-1) + 
                 (1-P_SINK):true; 
endmodule 

rewards "queue_size" 
	true : length; 
endrewards 

rewards "drop_prob" 
	true : drop; 
endrewards 

