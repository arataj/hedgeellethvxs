//
// A model of RED with self--similar packet
// arrival.
//
// An example command to extract average
// buffer length and drop ratio:
//
// veric red.nm.pp -p > red.pctl && \
//         prism red.nm red.pctl -nofixdl | \
//         grep --line-buffered Result | \
//         awk '{if(NR % 2 == 0){}else{print (NR - 1)/2*5}; print $2; system("")}' | \
//         xargs -n3
//
ctmc

//
// Constants
//

// number of phases of the generator
const int An = 5;
// estimated traffic parameters for
// the Ethernet
//const double Aa = 11.249898;
//const double Aq = 0.684268;
// estimated traffic parameters for
// MPEG-2
const double Aa = 2.755029;
const double Aq = 1.075841;
// maximum buffer size
const int MAX_BUFFER = 8;
// RED parameters
const int T_MIN = 4;
const int T_MAX = 6;
const double P_MAX = 0.8;
// service parameters
const double P_SINK = 0.1;
// avg parameter
const double w = 0.2;
// precision of avg
const int AVG_MULT = 100;

//
// Traffic generator
//

#//probability of getting from the 1st phase
#//to the nth phase
#pv$ R pN(Z i) {
#    return 1/$pow(.Aa, i - 1);
#}
#//probability of staying in the 1st phase
#pv$ R pS() {
#   R sum~ := 0;
#   for Z i~ := 2; i <= .An; ++i {
#       sum += pN(i);
#   }
#   return 1 - sum;
#}
#//probability of getting from the nth phase
#//to the 1st phase
#pv$ R p1(Z i) {
#    return $pow(.Aq/.Aa, i - 1);
#}

module traffic
  phase : [1..An+1] init 1;

  [tick] phase=1 ->
#for Z i~ := 2; i <= .An; ++i {
            ${pN(i)}:(phase'=${i}) +
#}
            ${pS()}:true;
#for Z i~ := 2; i <= .An; ++i {
  [tick] phase=${i} -> ${p1(i)}:(phase'=${.An+1}) +
            ${1 - p1(i)}:true;
#}
  [arrive] phase=An+1 -> (phase'=1);
endmodule

//
// RED
//

#//probability of packet drop at a given
#//queue length
#pv$ R pDrop(Z i) {
#    return (i - .T_MIN)*.P_MAX/
#           (.T_MAX - .T_MIN);
#}
#String ROUND_AVG :=
#    "floor(avg/AVG_MULT + 0.5)";
#String UPDATE_AVG := "(avg'=floor(" +
#    "((1-w)*(avg/AVG_MULT)+w*length)*" +
#    "AVG_MULT+0.5))";
#String ARRIVAL :=
#    "(length'=min(MAX_BUFFER, length+1))&" +
#    "(drop'=0)&" + UPDATE_AVG;
#String DROP := "(drop'=1)&" + UPDATE_AVG;

module red
  length : [0..MAX_BUFFER] init 0;
  avg : [0..MAX_BUFFER*AVG_MULT] init 0;
  drop : [0..1] init 0;

  [arrive] ${ROUND_AVG}<=T_MIN -> ${ARRIVAL};
#for Z i~ := .T_MIN + 1; i <= .T_MAX; ++i {
  [arrive] ${ROUND_AVG}=${i} ->
              ${1 - pDrop(i)}:${ARRIVAL} +
              ${pDrop(i)}:${DROP};
#}
  [arrive] ${ROUND_AVG}>T_MAX -> ${DROP};
  [tick] length=0 -> true;
  [tick] length>0 -> P_SINK:(length'=length-1) +
                 (1-P_SINK):true;
endmodule

rewards "queue_size"
	true : length;
endrewards

rewards "drop_prob"
	true : drop;
endrewards

#for Z i~ := 0; i <= 5; i += 5 {
#    println("##R{\"queue_size\"}=? [ I=" + i + " ]");
#    println("##R{\"drop_prob\"}=? [ I=" + i + " ]");
#}
