//
// Verics language prototype 
// 
// Test "Faulty train protocol" for any number of trains 
// 
class pomdp sync;

// number of trains 
common Z NUM := 3;

environment { 
    Z{GREEN, RED} lights := GREEN; 

#for Z i~ := 1; i <= .NUM; ++i { 
    [out${i}] lights == RED   -> lights := GREEN; 
    [in${i}]  lights == GREEN -> lights := RED; 
#} 
};

#for Z i~ := 1; i <= .NUM; ++i { 
agent train${i} { 
    Z{WAIT, TUNNEL, AWAY} phase := AWAY; 

    []         true -> true; 
    [approach] phase == AWAY   -> phase := WAIT; 
    // the train <code>NUM</code> is faulty
    [${i <> .NUM ? "in"  + i else "inFree"}]  phase == WAIT   -> phase := TUNNEL; 
    [${i <> .NUM ? "out" + i else "outFree"}]  phase == TUNNEL -> phase := AWAY; 
};

def in_tunnel${i} := train${i}.phase == TUNNEL; 

#} 

property LTL test_train1 := F (in_tunnel1 + in_tunnel2);