<?xml version="1.0" encoding="UTF-8"?>
<model class="mdp" name="regression/FTCp.vxs" observable="partial" sync="true">
  <constants>
    <declaration name="NUM" type="Z" value="3" />
    <declaration name="GREEN" type="Z" value="4" />
    <declaration name="RED" type="Z" value="5" />
    <declaration name="WAIT" type="Z" value="6" />
    <declaration name="TUNNEL" type="Z" value="7" />
    <declaration name="AWAY" type="Z" value="8" />
  </constants>
  <module name="environment">
    <variables>
      <declaration name="environment.lights">
        <domain>
          <const>GREEN</const>
          <const>RED</const>
        </domain>
        <init>
          <const>GREEN</const>
        </init>
      </declaration>
    </variables>
    <observable>
      <local>
        <var>environment.lights</var>
      </local>
      <external />
    </observable>
    <actions>
      <expr_action>in1</expr_action>
      <expr_action>in2</expr_action>
      <expr_action>out1</expr_action>
      <expr_action>out2</expr_action>
    </actions>
    <protocol>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>environment.lights</var>
            <const>GREEN</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>in1</expr_action>
          <expr_action>in2</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>environment.lights</var>
            <const>RED</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>out1</expr_action>
          <expr_action>out2</expr_action>
        </actions>
      </statement>
    </protocol>
    <transitions>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>environment.lights</var>
              <const>GREEN</const>
            </expr_binary>
            <expr_binary operator="or">
              <expr_binary operator="and">
                <expr_action>environment.in1</expr_action>
                <expr_action>train1.in</expr_action>
              </expr_binary>
              <expr_binary operator="and">
                <expr_action>environment.in2</expr_action>
                <expr_action>train2.in</expr_action>
              </expr_binary>
            </expr_binary>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>environment.lights</var>
            <const>RED</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>environment.lights</var>
              <const>RED</const>
            </expr_binary>
            <expr_binary operator="or">
              <expr_binary operator="and">
                <expr_action>environment.out1</expr_action>
                <expr_action>train1.out</expr_action>
              </expr_binary>
              <expr_binary operator="and">
                <expr_action>environment.out2</expr_action>
                <expr_action>train2.out</expr_action>
              </expr_binary>
            </expr_binary>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>environment.lights</var>
            <const>GREEN</const>
          </expr_assign>
        </update>
      </statement>
    </transitions>
  </module>
  <module name="train1">
    <variables>
      <declaration name="train1.phase">
        <domain>
          <const>WAIT</const>
          <const>TUNNEL</const>
          <const>AWAY</const>
        </domain>
        <init>
          <const>AWAY</const>
        </init>
      </declaration>
    </variables>
    <observable>
      <local>
        <var>train1.phase</var>
      </local>
      <external />
    </observable>
    <actions>
      <expr_action>approach</expr_action>
      <expr_action>in</expr_action>
      <expr_action>none</expr_action>
      <expr_action>out</expr_action>
    </actions>
    <protocol>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train1.phase</var>
            <const>WAIT</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>in</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train1.phase</var>
            <const>TUNNEL</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>out</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train1.phase</var>
            <const>AWAY</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>approach</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
    </protocol>
    <transitions>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>train1.phase</var>
              <const>AWAY</const>
            </expr_binary>
            <expr_action>train1.approach</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train1.phase</var>
            <const>WAIT</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="and">
              <expr_binary operator="=">
                <var>train1.phase</var>
                <const>WAIT</const>
              </expr_binary>
              <expr_action>train1.in</expr_action>
            </expr_binary>
            <expr_action>environment.in1</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train1.phase</var>
            <const>TUNNEL</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="and">
              <expr_binary operator="=">
                <var>train1.phase</var>
                <const>TUNNEL</const>
              </expr_binary>
              <expr_action>train1.out</expr_action>
            </expr_binary>
            <expr_action>environment.out1</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train1.phase</var>
            <const>AWAY</const>
          </expr_assign>
        </update>
      </statement>
    </transitions>
  </module>
  <module name="train2">
    <variables>
      <declaration name="train2.phase">
        <domain>
          <const>WAIT</const>
          <const>TUNNEL</const>
          <const>AWAY</const>
        </domain>
        <init>
          <const>AWAY</const>
        </init>
      </declaration>
    </variables>
    <observable>
      <local>
        <var>train2.phase</var>
      </local>
      <external />
    </observable>
    <actions>
      <expr_action>approach</expr_action>
      <expr_action>in</expr_action>
      <expr_action>none</expr_action>
      <expr_action>out</expr_action>
    </actions>
    <protocol>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train2.phase</var>
            <const>WAIT</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>in</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train2.phase</var>
            <const>TUNNEL</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>out</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train2.phase</var>
            <const>AWAY</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>approach</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
    </protocol>
    <transitions>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>train2.phase</var>
              <const>AWAY</const>
            </expr_binary>
            <expr_action>train2.approach</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train2.phase</var>
            <const>WAIT</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="and">
              <expr_binary operator="=">
                <var>train2.phase</var>
                <const>WAIT</const>
              </expr_binary>
              <expr_action>train2.in</expr_action>
            </expr_binary>
            <expr_action>environment.in2</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train2.phase</var>
            <const>TUNNEL</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="and">
              <expr_binary operator="=">
                <var>train2.phase</var>
                <const>TUNNEL</const>
              </expr_binary>
              <expr_action>train2.out</expr_action>
            </expr_binary>
            <expr_action>environment.out2</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train2.phase</var>
            <const>AWAY</const>
          </expr_assign>
        </update>
      </statement>
    </transitions>
  </module>
  <module name="train3">
    <variables>
      <declaration name="train3.phase">
        <domain>
          <const>WAIT</const>
          <const>TUNNEL</const>
          <const>AWAY</const>
        </domain>
        <init>
          <const>AWAY</const>
        </init>
      </declaration>
    </variables>
    <observable>
      <local>
        <var>train3.phase</var>
      </local>
      <external />
    </observable>
    <actions>
      <expr_action>approach</expr_action>
      <expr_action>in</expr_action>
      <expr_action>none</expr_action>
      <expr_action>out</expr_action>
    </actions>
    <protocol>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train3.phase</var>
            <const>WAIT</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>in</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train3.phase</var>
            <const>TUNNEL</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>out</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="=">
            <var>train3.phase</var>
            <const>AWAY</const>
          </expr_binary>
        </guard>
        <actions>
          <expr_action>approach</expr_action>
          <expr_action>none</expr_action>
        </actions>
      </statement>
    </protocol>
    <transitions>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>train3.phase</var>
              <const>AWAY</const>
            </expr_binary>
            <expr_action>train3.approach</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train3.phase</var>
            <const>WAIT</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>train3.phase</var>
              <const>WAIT</const>
            </expr_binary>
            <expr_action>train3.in</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train3.phase</var>
            <const>TUNNEL</const>
          </expr_assign>
        </update>
      </statement>
      <statement>
        <guard>
          <expr_binary operator="and">
            <expr_binary operator="=">
              <var>train3.phase</var>
              <const>TUNNEL</const>
            </expr_binary>
            <expr_action>train3.out</expr_action>
          </expr_binary>
        </guard>
        <update>
          <expr_assign>
            <var>train3.phase</var>
            <const>AWAY</const>
          </expr_assign>
        </update>
      </statement>
    </transitions>
  </module>
  <formulas>
    <formula name="in_tunnel1">
      <expr_binary operator="=">
        <var>train1.phase</var>
        <const>TUNNEL</const>
      </expr_binary>
    </formula>
    <formula name="in_tunnel2">
      <expr_binary operator="=">
        <var>train2.phase</var>
        <const>TUNNEL</const>
      </expr_binary>
    </formula>
    <formula name="in_tunnel3">
      <expr_binary operator="=">
        <var>train3.phase</var>
        <const>TUNNEL</const>
      </expr_binary>
    </formula>
  </formulas>
  <init_states source="declaration">
    <expr_binary operator="and">
      <expr_binary operator="and">
        <expr_binary operator="and">
          <expr_binary operator="=">
            <var>environment.lights</var>
            <const>GREEN</const>
          </expr_binary>
          <expr_binary operator="=">
            <var>train1.phase</var>
            <const>AWAY</const>
          </expr_binary>
        </expr_binary>
        <expr_binary operator="=">
          <var>train2.phase</var>
          <const>AWAY</const>
        </expr_binary>
      </expr_binary>
      <expr_binary operator="=">
        <var>train3.phase</var>
        <const>AWAY</const>
      </expr_binary>
    </expr_binary>
  </init_states>
  <property class="CTLAK" name="test_train1">
    <expr_unary operator="not">
      <expr_unary operator="A">
        <expr_unary operator="G">
          <expr_unary operator="K">
            <coalition>
              <module_ref>train1</module_ref>
            </coalition>
            <expr_binary operator="and">
              <expr_binary operator="and">
                <expr_unary operator="not">
                  <expr_binary operator="or">
                    <formula_ref>in_tunnel1</formula_ref>
                    <expr_unary operator="not">
                      <formula_ref>in_tunnel2</formula_ref>
                    </expr_unary>
                  </expr_binary>
                </expr_unary>
                <expr_unary operator="not">
                  <expr_binary operator="or">
                    <formula_ref>in_tunnel1</formula_ref>
                    <expr_unary operator="not">
                      <formula_ref>in_tunnel3</formula_ref>
                    </expr_unary>
                  </expr_binary>
                </expr_unary>
              </expr_binary>
              <expr_unary operator="not">
                <expr_binary operator="or">
                  <formula_ref>in_tunnel2</formula_ref>
                  <expr_unary operator="not">
                    <formula_ref>in_tunnel3</formula_ref>
                  </expr_unary>
                </expr_binary>
              </expr_unary>
            </expr_binary>
          </expr_unary>
        </expr_unary>
      </expr_unary>
    </expr_unary>
  </property>
</model>
