/*
 * VxsTest.java
 *
 * Created on Jun 26, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.test;

import java.util.*;
import java.io.*;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.CompilerException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;
import pl.gliwice.iitis.verics.VericsFrontend;
import pl.iitis.verics.agent.property.Generator;
import pl.iitis.verics.assembler.PreprocessorAPI;
import pl.iitis.verics.assembler.VxsCLI;
import pl.iitis.verics.assembler.VxsCompilation;
import pl.iitis.verics.assembler.VxsLibrary;
import pl.iitis.verics.assembler.VxsOptions;

/**
 * <p>Regression tests of the compiler.</p>
 * 
 * @author Artur Rataj
 */
public class VxsTest extends AbstractCompilerTest {
    /**
     * A schema for validating XML output.
     */
    public static String VXS_XML_SCHEMA_PATH = "regression/vxs.xsd";
    /**
     * If not null, forces the output file name in <code>VxsCLI.compileVxs</code>.
     * Used to set the output format in the case of failing compilations.
     */
    static String FORCE_OUT_FILE_NAME = null;
    /**
     * Forces the translation of action labels to guards, even if the output format
     * does not require it.
     */
    static boolean FORCE_SYNC_TO_GUARDS = false;
    /**
     * If not null, forces a given conversion, independently of the output file type.
     */
    static VxsOptions.Conversion FORCE_CONVERSION = null;
    /**
     * Test CLI constants. The default is none.
     */
    CliConstants CLI_CONSTANTS = new CliConstants();
    
    public VxsTest() {
        super();
    }
    @Override
    protected void compile(PWD directory, String[] files,
            CompilerException pattern, String patternFileName) {
        PWD topDirectory = new PWD();
        List<String> localLibrary = new LinkedList<>();
        String inputFileName = null;
        for(String f : files) {
            if(f.endsWith(VericsFrontend.FILE_NAME_EXTENSION))
                localLibrary.add(topDirectory.getRelativePath(directory.getFile(
                        f).getAbsolutePath()));
            else if(inputFileName == null)
                inputFileName = f;
            else
                throw new RuntimeException("expected single model file");
        }
        VxsLibrary library;
        try {
            library = new VxsLibrary(new PWD(),
                        pl.gliwice.iitis.hedgeelleth.cli.Compiler.getCompilerLibraryDirectory(VxsCLI.class, System.getProperty("user.dir")),
                        true, localLibrary);
        } catch(CompilerException e) {
            throw new RuntimeException("could not load library");
        }
        String relInputFileName = topDirectory.getRelativePath(directory.getFile(
                inputFileName).getAbsolutePath());
        String relPatternFileName = null;
        System.out.println("TEST:");
        System.out.println("\tfile " + relInputFileName);
        // target file type
        VxsCLI.FileType mode = null;
        if(pattern == null && patternFileName == null)
                throw new RuntimeException("missing pattern");
        String outputFileName = null;
        if(patternFileName != null) {
            System.out.print("\tpattern");
            System.out.println(" " + patternFileName);
            relPatternFileName = topDirectory.getRelativePath(directory.getFile(
                patternFileName).getAbsolutePath());
            if((mode = VxsCLI.getFileType(patternFileName.substring(0,
                        patternFileName.length() - REGRESSION_STRING.length()))) == null)
                throw new RuntimeException("unrecognized pattern file");
            outputFileName = relPatternFileName.substring(0,
                relPatternFileName.length() - REGRESSION_STRING.length());
        }
        if(pattern != null) {
            System.out.print("\tpattern");
            System.out.print("\n" +
                    CompilerUtils.indent(2, pattern.toString(), "\t"));
            // process the input file as far as possible
            if(mode == null) {
                if(FORCE_OUT_FILE_NAME != null)
                    mode = VxsCLI.getFileType(FORCE_OUT_FILE_NAME);
                else
                    mode = VxsCLI.FileType.MCMAS;
            }
        } else if(FORCE_OUT_FILE_NAME != null)
                throw new RuntimeException("output file name can be forced " +
                        "only if the compilation is going to fail");
        String outputContent = null;
        CompilerException error = null;
        boolean preprocessorOnly = false;
        try {
            switch(mode) {
                case VERICS:
                case VXSP:
                case MCMAS:
                case XML:
                case PRISM:
                case EXTERNAL:
                {
                    VxsCompilation vxsc = null;
                    CompilerException preprocessError = new CompilerException();
                    try {
                        switch(mode) {
                            case VERICS:
                            case VXSP:
                            case PRISM:
                            case EXTERNAL:
                                preprocessorOnly = true;
                                vxsc = PreprocessorAPI.parseVxs(library,
                                        relInputFileName, null, CLI_CONSTANTS,
                                        mode == VxsCLI.FileType.VXSP,
                                        // Prism does not handle line number suffixes
                                        mode == VxsCLI.FileType.PRISM ||
                                        mode == VxsCLI.FileType.EXTERNAL);
                                break;
                                
                            case MCMAS:
                            case XML:
                                VxsOptions.Translation options = new VxsOptions.Translation();
                                options.syncToGuards = mode == VxsCLI.FileType.MCMAS ||
                                        FORCE_SYNC_TO_GUARDS;
                                options.conversion.integerActionToBoolean =
                                        FORCE_CONVERSION != null ?
                                                FORCE_CONVERSION.integerActionToBoolean :
                                                false;
                                options.conversion.integerFunctionToBoolean =
                                        FORCE_CONVERSION != null ?
                                                FORCE_CONVERSION.integerFunctionToBoolean :
                                                false;
                                options.conversion.integerToBoolean =
                                        FORCE_CONVERSION != null ?
                                                FORCE_CONVERSION.integerToBoolean :
                                                mode == VxsCLI.FileType.MCMAS;
                                switch(mode) {
                                    case MCMAS:
                                        options.outputFormat = VxsCLI.FileType.MCMAS;
                                        break;
                                        
                                    case XML:
                                        options.outputFormat = VxsCLI.FileType.XML;
                                        break;
                                        
                                    default:
                                        throw new RuntimeException("unexpected output format");
                                }
                                vxsc = VxsCLI.compileVxs(localLibrary,
                                        relInputFileName, CLI_CONSTANTS, options,
                                        true, FORCE_OUT_FILE_NAME != null ?
                                                topDirectory.getRelativePath(directory.getFile(
                                                    FORCE_OUT_FILE_NAME).getAbsolutePath()) : outputFileName);
                                break;
                                
                            default:
                                throw new RuntimeException("unknown mode");
                        }
                    } catch(CompilerException e) {
                        preprocessError.addAllReports(e);
                        vxsc = VxsCLI.VXSC;
                    }
                    switch(mode) {
                        case VERICS:
                            outputContent = vxsc.PREPROCESSOR.VERICS_RAW;
                            break;
                            
                        case VXSP:
                        case PRISM:
                        case EXTERNAL:
                            outputContent = vxsc.VXSP2;
                            break;
                            
                        case MCMAS:
                        case XML:
                            if(vxsc.OUT != null)
                                outputContent = vxsc.OUT.toString();
                            break;
                            
                        default:
                            throw new RuntimeException("unknown mode");
                    }
                    if(relPatternFileName != null) {
                        try {
                            if(outputContent == null) {
                                 String errorMessage = "\tNO OUTPUT: " +
                                         outputFileName;
                                 if(mode != VxsCLI.FileType.VERICS &&
                                         preprocessError.reportsExist())
                                    errorMessage += "\n\talso preprocessor errors:\n" +
                                             CompilerUtils.indent(
                                                     2, preprocessError.getMessage(), "\t");
                                 System.out.println(errorMessage);
                                 throw new AssertionError(errorMessage);
                            }
                            if(preprocessorOnly)
                                    try (PrintWriter out = new PrintWriter(outputFileName)) {
                                        out.print(outputContent);
                                    }
                            File pf = new File(relPatternFileName);
                            if(!pf.exists()) {
                                 String errorMessage = "\tpattern file missing: " +
                                         patternFileName;
                                 System.out.println(errorMessage);
                                 throw new AssertionError(errorMessage);
                            }
                            String patternContent = CompilerUtils.readFile(pf);
                            if(!outputContent.equals(patternContent)) {
                                 String errorMessage = "\tOUTPUT DIFFERS: " +
                                         patternFileName.substring(0,
                                            patternFileName.length() - REGRESSION_STRING.length()) + " " +
                                         patternFileName;
                                 if(mode != VxsCLI.FileType.VERICS &&
                                         preprocessError.reportsExist())
                                    errorMessage += "\n\talso preprocessor errors:\n" +
                                             CompilerUtils.indent(
                                                     2, preprocessError.getMessage(), "\t");
                                 System.out.println(errorMessage);
                                 throw new AssertionError(errorMessage);
                            }
                        } catch(IOException e) {
                            throw new RuntimeException("could not compare to pattern file: " +
                                    e.getMessage());
                        }
                    }
                    if(preprocessError.reportsExist())
                        throw preprocessError;
                    if(mode == VxsCLI.FileType.XML && patternFileName != null) {
                        System.out.print("\tvalidating against " + VXS_XML_SCHEMA_PATH + "... ");
                        try {
                            String validationError = XSDValidator.validate(VXS_XML_SCHEMA_PATH,
                                    new StreamSource(new StringReader(outputContent)));
                            if(validationError == null)
                                System.out.println("ok.");
                            else {
                                System.out.println("error:\n\t\t" + validationError);
                                throw new AssertionError(validationError);
                            }
                        } catch(IOException e) {
                            throw new RuntimeException("unexpected: " + e.getMessage());
                        }
                    }
                    break;
                }
                default:
                    throw new RuntimeException("mode unimplemented");
            }
        } catch(CompilerException e) {
            error = e;
        }
        /* error.toCodeString() */ 
        errorEquals(error, pattern);
        if(error == null) {
            try {
                if(outputContent == null)
                    outputContent = CompilerUtils.readFile(
                         directory.getFile(outputFileName));
                String patternContent = CompilerUtils.readFile(
                         directory.getFile(patternFileName));
                if(!outputContent.equals(patternContent)) {
                     String errorMessage = "\tOUTPUT DIFFERS: " +
                             inputFileName + " " + patternFileName;
                     System.out.println(errorMessage);
                     throw new AssertionError(errorMessage);
                }
            } catch(IOException e) {
                String errorMessage = "\tI/O ERROR: " + e.toString();
                System.out.println(errorMessage);
                throw new AssertionError(errorMessage);
            }
        }
        System.out.println("OK.");
        ++compilationsPerformed;
    }
    /**
     * Tests translation of the source of Faulty Train Protocol.
     */
    protected void testFTC() {
        testPerformed();
        String[] filesParse = {
            "FTCParse.vxs",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParse.vxs", 9, 14),
                CompilerException.Code.PARSE,
                "unexpected \"=\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParse.vxs", 10, 16),
                CompilerException.Code.PARSE,
                "unexpected \"=\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParse.vxs", 12, 7),
                CompilerException.Code.PARSE,
                "unexpected \"=\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParse.vxs", 15, 4),
                CompilerException.Code.PARSE,
                "unexpected \"=\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParse.vxs", 23, 8),
                CompilerException.Code.PARSE,
                "unexpected \"=\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParse.vxs", 30, 7),
                CompilerException.Code.DUPLICATE,
                "duplicate declaration of NUM2"));        
        compile(new PWD("regression"), filesParse,
                pattern, "FTCParse.verics.regression");
        String[] filesParsePreprocessor = {
            "FTCParsePreprocessor.vxs",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParsePreprocessor.vxs", 19, 13),
                CompilerException.Code.ILLEGAL,
                "nested opening brace"));        
        compile(new PWD("regression"), filesParsePreprocessor,
                pattern, null);
        String[] filesParseInvalidBracketed = {
            "FTCParseInvalidBracketed.vxs",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseInvalidBracketed.vxs", 25, 7),
                CompilerException.Code.PARSE,
                "unexpected \";\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseInvalidBracketed.vxs", 25, 12),
                CompilerException.Code.PARSE,
                "invalid separation"));        
        compile(new PWD("regression"), filesParseInvalidBracketed,
                pattern, "FTCParseInvalidBracketed.verics.regression");
        String[] filesParseSemantic = {
            "FTCParseSemantic.vxs",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseSemantic.vxs", 15, 13),
                CompilerException.Code.ILLEGAL,
                "illegal operands: Z, String for binary -"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseSemantic.vxs", 16, 5),
                CompilerException.Code.ILLEGAL,
                "illegal operands: Z, String for binary *"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseSemantic.vxs", 16, 16),
                CompilerException.Code.ILLEGAL,
                "illegal operands: String, Z for binary /"));        
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseSemantic.vxs", 18, 5),
                CompilerException.Code.ILLEGAL,
                "illegal operands: String, Z for binary /"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCParseSemantic.vxs", 19, 3),
                CompilerException.Code.LOOK_UP,
                "symbol not found: j"));
        compile(new PWD("regression"), filesParseSemantic,
                pattern, "FTCParseSemantic.verics.regression");
        String[] filesPreprocessor = {
            "FTC.vxs",
        };
        compile(new PWD("regression"), filesPreprocessor,
                null, "FTC.vxsp.regression");
        String[] filesVxspSemantic = {
            "FTCSemantic.vxs",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/FTCSemantic.vxs", 35, 43),
                CompilerException.Code.ILLEGAL,
                "illegal operands: B, B for binary +"));        
        compile(new PWD("regression"), filesVxspSemantic,
                pattern, null);
        String[] filesMCMAS = {
            "FTC.vxs",
        };
        compile(new PWD("regression"), filesMCMAS,
                null, "FTC.ispl.regression");
        String[] filesMCMAS_IIS = {
            "FTCInterleaved.vxs",
        };
        compile(new PWD("regression"), filesMCMAS_IIS,
                null, "FTCInterleaved.ispl.regression");
        String[] filesProtocolMCMAS = {
            "FTCp.vxs",
        };
        compile(new PWD("regression"), filesProtocolMCMAS,
                null, "FTCp.ispl.regression");
        compile(new PWD("regression"), filesMCMAS,
                null, "FTC.xml.regression");
        compile(new PWD("regression"), filesMCMAS_IIS,
                null, "FTCInterleaved.xml.regression");
        compile(new PWD("regression"), filesProtocolMCMAS,
                null, "FTCp.xml.regression");
    }
    /**
     * Tests in-- and out--of--preprocessor comments.
     */
    protected void testComments() {
        testPerformed();
        String[] filesComment1 = {
            "FTCComment1.vxs",
        };
        compile(new PWD("regression"), filesComment1,
                null, "FTCComment1.vxsp.regression");
    }
    /**
     * Tests, if the syntax of logics is correct.
     */
    protected void testLogicSyntax() {
        testPerformed();
        System.out.println("TEST:");
        try {
            Generator.test();
        } catch(CompilerException e) {
            String s = "SYNTAX MISMATCH: " + e.getMessage();
            System.out.println(s);
            throw new AssertionError(s);
        }
        System.out.println("OK.");
    }
    /**
     * Tests translation of the source of Faulty Train Protocol.
     */
    protected void testProperties() {
        testPerformed();
        String[] filesParse = {
            "PropertyParse.vxs",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/property/PropertyParse.vxs", 8, 31),
                CompilerException.Code.PARSE,
                "unexpected ';'"));
        compile(new PWD("regression/property"), filesParse,
                pattern, null);
    }
    /**
     * Tests translation of the source of Faulty Train Protocol.
     */
    protected void testPrism() {
        testPerformed();
        String[] filesParse = {
            "red.nm.pp",
        };
        compile(new PWD("regression/prism"), filesParse,
                null, "red.nm.regression");
        String[] filesLibrary = {
            "ipp_tcp.nm.pp",
            "IPP.verics",
        };
        CliConstants tmp = CLI_CONSTANTS;
        CLI_CONSTANTS = new CliConstants();
        List<Literal> valueHurst = new LinkedList<>();
        valueHurst.add(new Literal(80));
        List<Literal> valueLambda = new LinkedList<>();
        valueLambda.add(new Literal(50));
        CLI_CONSTANTS.defines.put("HURST", valueHurst);
        CLI_CONSTANTS.defines.put("LAMBDA", valueLambda);
        CLI_CONSTANTS.type.put("HURST", CliConstants.CType.INTEGER);
        CLI_CONSTANTS.type.put("LAMBDA", CliConstants.CType.INTEGER);
        CLI_CONSTANTS.array.put("HURST", false);
        CLI_CONSTANTS.array.put("LAMBDA", false);
        compile(new PWD("regression/prism"), filesLibrary,
                null, "ipp_tcp.nm.regression");
        CLI_CONSTANTS = tmp;
    }
    /**
     * Tests translation of dictionary entries in tex format.
     */
    protected void testDict() {
        testPerformed();
        String[] filesÊtre = {
            "Dêtre.verics",
            "c1_entry_verb.tex.pp",
        };
        compile(new PWD("regression/dict"), filesÊtre,
                null, "c1_entry_verb_être.tex.regression");
        String[] filesAmener = {
            "Damener.verics",
            "c1_entry_verb.tex.pp",
        };
        compile(new PWD("regression/dict"), filesAmener,
                null, "c1_entry_verb_amener.tex.regression");
        String[] filesQuestion = {
            "Dêtre.verics",
            "question.tex.pp",
        };
        compile(new PWD("regression/dict"), filesQuestion,
                null, "question.tex.regression");
    }
    /**
     * Tests unusual errors positions.
     */
    protected void testErrorPos() {
        testPerformed();
        // tests an operand error reported at an operator,
        // and also an empty preprocessor expression <code>${}</code>
        String[] filesAtOperator = {
            "ErrorAtOperator.tex.pp",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/ErrorAtOperator.tex.pp", 8, 38),
                CompilerException.Code.ILLEGAL,
                "symbol c is used uninitialized in this expression"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/ErrorAtOperator.tex.pp", 13, 1),
                CompilerException.Code.ILLEGAL,
                "symbol c is used uninitialized in this expression"));        
        compile(new PWD("regression/"), filesAtOperator,
                pattern, null);
    }
    /**
     * Tests name conflicts.
     */
    protected void testNameConflict() {
        testPerformed();
        String[] filesDuplicate = {
            "duplicate.vxs",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/duplicate.vxs", 9, 13),
                CompilerException.Code.DUPLICATE,
                "name conflict between a protocol action and an action label: lights"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/duplicate.vxs", 9, 13),
                CompilerException.Code.DUPLICATE,
                "name conflict between a variable and a protocol action: lights"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/duplicate.vxs", 9, 13),
                CompilerException.Code.DUPLICATE,
                "name conflict between a variable and an action label: lights"));        
        FORCE_OUT_FILE_NAME = "duplicate.xml";
        compile(new PWD("regression"), filesDuplicate,
                pattern, null);
        FORCE_OUT_FILE_NAME = null;
    }
    /**
     * Tests access rights.
     */
    protected void testAccessRights() {
        testPerformed();
        String[] filesParse = {
            "Common.vxs",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/Common.vxs", 10, 11),
                CompilerException.Code.LOOK_UP,
                "module not found: train4"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/Common.vxs", 43, 11),
                CompilerException.Code.ILLEGAL,
                "only private variables allowed in this module"));        
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/Common.vxs", 53, 12),
                CompilerException.Code.ILLEGAL,
                "only private variables allowed in this module"));
        compile(new PWD("regression"), filesParse,
                pattern, null);
        String[] filesAccess = {
            "dcNAccess.vxs",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/dcNAccess.vxs", 18, 0),
                CompilerException.Code.ILLEGAL,
                "variable dc2.see_diff is private: «            mod(dc1.say_equal + dc2.say_equal + dc3.say_equal, 2)==⚑dc2.see_diff -> is_even := NO;»"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/dcNAccess.vxs", 35, 0),
                CompilerException.Code.ILLEGAL,
                "variable environment.is_even is private: «           mod(environment.head1 + environment.head2, 2)==⚑environment.is_even -> see_diff := YES;»"));
        compile(new PWD("regression"), filesAccess,
                pattern, null);
    }
    /**
     * Tests the pipeline model, in particular the translation of sync labels
     * to protocols.
     */
    protected void testPipeline() {
        testPerformed();
        String[] filesPipeline = {
            "pipeline.vxs",
        };
        compile(new PWD("regression"), filesPipeline,
                null, "pipeline.ispl.regression");
        compile(new PWD("regression"), filesPipeline,
                null, "pipeline.xml.regression");
        String[] filesPipelineGuards = {
            "pipelineGuards.vxs",
        };
        FORCE_SYNC_TO_GUARDS = true;
        compile(new PWD("regression"), filesPipelineGuards,
                null, "pipelineGuards.xml.regression");
        FORCE_SYNC_TO_GUARDS = false;
    }
    /**
     * Tests the init states, and none action out of an empty statement
     * in XML
     */
    protected void testInitStates() {
        testPerformed();
        String[] files = {
            "dcN.vxs",
        };
        // init states only
        FORCE_SYNC_TO_GUARDS = true;
        compile(new PWD("regression"), files,
                null, "dcN.xml.regression");
        FORCE_SYNC_TO_GUARDS = false;
    }
    /**
     * Tests the conversion of protocol actions embedded in integer arithmetics,
     * and of mathematical functions.
     */
    protected void testConversion() {
        testPerformed();
        String[] files = {
            "dcN.vxs",
        };
        compile(new PWD("regression"), files,
                null, "dcN.ispl.regression");
        // as ooposed to dcN, this does not have the "other" guards for variety
        String[] filesCustomOptions = {
            "dcNb.vxs",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("regression/dcNb.vxs", 33, 0),
                CompilerException.Code.ILLEGAL,
                "MCMAS does not support math functions, use a conversion to boolean: «           mod⚑(environment.head1 + environment.head2, 2)==0 -> see_diff := NO;»"));
        FORCE_CONVERSION = new VxsOptions.Conversion();
        FORCE_CONVERSION.integerActionToBoolean = true;
        compile(new PWD("regression"), filesCustomOptions,
                pattern, null);
        FORCE_CONVERSION.integerFunctionToBoolean = true;
        compile(new PWD("regression"), filesCustomOptions,
                null, "dcNb.ispl.regression");
        compile(new PWD("regression"), filesCustomOptions,
                null, "dcNb.xml.regression");
        FORCE_CONVERSION = null;
    }
    /**
     * Tests the conversion of protocol actions embedded in integer arithmetics,
     * and of mathematical functions.
     */
    protected void testMethods() {
        testPerformed();
        String[] files = {
            "simple.nm.pp",
        };
        compile(new PWD("regression"), files,
                null, "simple.nm.regression");
    }
    @Override
    protected void test() {
        testFTC();
        testComments();
        testLogicSyntax();
        testProperties();
        testPrism();
        testDict();
        testErrorPos();
        testNameConflict();
        testAccessRights();
        testPipeline();
        testInitStates();
        testConversion();
        testMethods();
        TESTS_PERFORMED = 13;
        COMPILATIONS_PERFORMED = 32;
    }
}
