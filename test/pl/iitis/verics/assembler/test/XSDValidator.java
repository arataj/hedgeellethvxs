/*
 * XSDValidator.java based on:
 *   https://stackoverflow.com/questions/15732/whats-the-best-way-to-validate-an-xml-file-against-an-xsd-file
 *   https://stackoverflow.com/questions/5456680/xml-document-to-string
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.iitis.verics.assembler.test;

import java.io.*;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;

public class XSDValidator {
    /**
     * Validates an XML file agains a schema.
     * 
     * @param xsdPath XSD file
     * @param xmlSource source of XML
     * @return null if the XML file is valid, otherwise an error message
     */
   public static String validate(String xsdPath, Source xmlSource)
            throws IOException {
        File schemaFile = new File(xsdPath);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(
                XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(xmlSource);
            return null;
        } catch (SAXException e) {
            return e.toString();
        }
    }
    /**
     * Validates an XML file agains a schema. This is a convenience method.
     * 
     * @param xsdPath XSD file
     * @param xmlPath XML file
     * @return if the XML file is valid
     */
    public static boolean validate(String xsdPath, String xmlPath)
             throws IOException {
         Source xmlFile = new StreamSource(new File(xmlPath));
         String error = validate(xsdPath, xmlFile);
         if(error != null)
             System.out.println(xmlPath + " is NOT valid reason:\n" + error);
         return error == null;
    }
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, TransformerException {
        String[] args_ = {
            "regression/vxs.xsd",
            "regression/dcN.xml",
//            "regression/garment.xsd",
//            "regression/garment.xml",
        }; args = args_;
        if(args.length != 2)
            System.out.println("syntax: <xsd file> <xml file>");
        else if(validate(args[0], args[1])) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder(); 
            Document doc = db.parse(new File(args[1]));
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            System.out.println(writer.getBuffer().toString());
        }
    }
}
